import Component from '../basic/Component';
import ConfirmDialog from '../dialog/ConfirmDialog';
import r18n from '../i18n/reactjs-tag.i18n';
import Form from '../basic/Form';
import config from 'config';
import { PageContext, ValidatorContext, ComponentContext, SessionContext, StoreContext, LocalContext } from 'rainbow-desktop-cache';
import UniqueId from '../basic/UniqueId';
import { Util, UrlUtil } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';
import { CodeTableService } from 'rainbow-desktop-codetable';
export default class Page extends Component {

    constructor(props) {
        super(props);
        this.loginTimer = null;
        this.loginTimeOut = null;
        this.countDownTimer = null;
        this.doWorkTimer = null;
        this.hasPopuped = false;
        this.secondCount = 0;
        this.countdownLimit = 5;
        this.userCodetable = null;
        this.count = 0;
        this.DEFAULT_LOGOUT_IS_WORKING = config ? config.DEFAULT_LOGOUT_IS_WORKING : false;
    }

    static postMessage() {
        let config = JSON.parse(sessionStorage.getItem("project_config"))|| {};
        let iframeHeight = $('#app').outerHeight(true);
        let currentUrl = window.currentUrl;
        if (currentUrl.port != '' && !config.UI_POSTMESSAGE_HOST) {
            parent.postMessage(iframeHeight, 'http://localhost:8901');
        } else if (currentUrl.port == '') {
            parent.postMessage(iframeHeight, window.location.protocol + '//' + window.location.host);
        } else {
            parent.postMessage(iframeHeight, config.UI_POSTMESSAGE_HOST);
        }
    }

    render() {
        return (
            <div className={this.props.className} style={{ width: '100%' }}>
                <div id="exception_container"></div>
                <Form id="registerForm" className={this.props.className} position={this.props.position}>{this.renderPage()}</Form>
                <ConfirmDialog id="login_confirm_dialog" title={r18n.Page.needContinue}
                    message={r18n.Page.messageFront + (this.countdownLimit * 60) + r18n.Page.messageEnd}
                    confirmText={r18n.Page.confirm} cancelButton="false" closeable="false" ignorePageReadOnly="true" confirmEnabled="true"
                    onConfirm={this.onClickConfirm.bind(this)} noI18n='true' />
                {this.renderPageFooter()}
            </div>
        );
    }

    /**@ignore
     * Render page footer
     */
    renderPageFooter() {
        if (Util.parseBool(this.props.showPageFooter)) {
            let PageFooter = config.DEFAULT_CLASS ? config.DEFAULT_CLASS.PAGE_FOOTER : null;
            if (PageFooter) {
                return (<PageFooter />);
            }
        }
    }

    /**@ignore
     * Render page
     */
    renderPage() {
        return this.props.children;
    }

    loadCodetableService(url, data, callBack, methods) {
        const config = JSON.parse(sessionStorage.getItem('project_config'));
        $.ajax({
            beforeSend: function (request) {
                const setionToken = SessionContext.get('Authorization');
                request.setRequestHeader('Authorization', 'Bearer ' + setionToken.substr(13).split('&')[0]);
                if (config && config.UI_CAS) {
                    request.setRequestHeader('x-ebao-auth-protocal', config.UI_CAS);
                }
                if (config && config.UI_CAS_SERVICE_URL) {
                    request.setRequestHeader('x-ebao-cas-service-url', config.UI_CAS_SERVICE_URL);
                }
                const SystemId = sessionStorage.getItem('x-ebao-system-id');
                if (SystemId) {
                    request.setRequestHeader('x-ebao-system-id', SystemId);
                }
            },
            method: methods ? methods : 'post',
            url: url,
            async: false,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: data ? JSON.stringify(data) : null,
            xhrFields: { withCredentials: true },
            crossDomain: true,
            success: (codetables) => {
                this.handleCodetable(codetables);
            },
            error: (text) => {
                console.error('Load Codetable error', text)
            }
        })
    }

    loadUserCodetable(url) {
        const config = JSON.parse(sessionStorage.getItem('project_config'));
        $.ajax({
            beforeSend: function (request) {
                const setionToken = SessionContext.get('Authorization', false);
                request.setRequestHeader('Authorization', 'Bearer ' + setionToken.substr(13).split('&')[0]);
                if (config && config.UI_CAS) {
                    request.setRequestHeader('x-ebao-auth-protocal', config.UI_CAS);
                }
                if (config && config.UI_CAS_SERVICE_URL) {
                    request.setRequestHeader('x-ebao-cas-service-url', config.UI_CAS_SERVICE_URL);
                }
                const SystemId = sessionStorage.getItem('x-ebao-system-id');
                if (SystemId) {
                    request.setRequestHeader('x-ebao-system-id', SystemId);
                }
            },
            method: 'get',
            url: url,
            async: false,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            xhrFields: { withCredentials: true },
            crossDomain: true,
            success: (codetables) => {
                this.userCodetable = codetables;
                SessionContext.put('USER_CODETABLE', codetables, true);
            },
            error: (text) => {
                console.error('Load User Codetable error', text)
            }
        })
    }


    handleCodetable(codetables) {
        _.each(codetables, (codetable) => {
            const name = codetable['BusinessCodeTable']['Name'];
            const UserSign = codetable['BusinessCodeTable']['UserSign'];
            const statusCodeList = SessionContext.get('__CODETABLE_STATUS');
            const returnArray = [];
            if (!_.isEmpty(codetable.BusinessCodeTableValueList)) {
                _.each(codetable.BusinessCodeTableValueList, (code) => {
                    code['id'] = code['Id'];
                    code['text'] = code['Description'];
                    delete code['Code'];
                    delete code['Description'];
                    delete code['Id'];
                    if (statusCodeList && code.Status) {
                        const status = _.find(statusCodeList, (statusCode) => {
                            return statusCode == code.Status;
                        });
                        if (status) {
                            returnArray.push(code);
                        }
                    } else {
                        returnArray.push(code);
                    }
                });
                codetable.BusinessCodeTableValueList = returnArray;
            } else {
                codetable['BusinessCodeTableValueList'] = [];
            }

            if (UserSign && UserSign == 'Y' && this.userCodetable) {
                const commonData = [];
                _.each(_.keys(this.userCodetable), (key) => {
                    if (key == name) {
                        _.each(this.userCodetable[key], (item) => {
                            commonData.push({ 'id': item['key'], 'text': item['value'] });
                        })
                    }
                })

                if (!_.isEmpty(commonData)) {
                    codetable['common'] = commonData;
                }
            }
            if (codetable.ConditionMap) {
                let key = this.buildKey(codetable.ConditionMap, name);
                StoreContext.put(key, JSON.stringify(codetable))
            } else {
                StoreContext.put('C_' + name, JSON.stringify(codetable));
            }
        });
    }

    buildKey(ConditionMap, CodeTableName) {
        if (ConditionMap instanceof Array && ConditionMap.length == 0) {
            return 'C_' + CodeTableName;
        }
        return ConditionMap ? 'C_' + CodeTableName + (JSON.stringify(ConditionMap) == '{}' ? '' : JSON.stringify(ConditionMap)) : 'C_' + CodeTableName;
    }

    getPath() {
        const urlObject = UrlUtil.parseURL(window.location.href);
        if (urlObject.host == 'localhost' || urlObject.host == '127.0.0.1') {
            return 'localhost';
        }
        return urlObject['segments'][0] + urlObject['segments'][1];

    }

    loadCodetableList() {
        const path = this.getPath();
        const key = SessionContext.get('LOAD_CODETABLE_' + path, false);
        const setionToken = SessionContext.get('Authorization');
        if (config['DEFINE_CODETABLE'] && !key && config['DEFINE_CODETABLE_API'] && setionToken) {
            const _config = SessionContext.get('project_config');
            const API_GATEWAY_PROXY_PATH = _config != null ? _config.UI_API_GATEWAY_PROXY : '';
            const url = API_GATEWAY_PROXY_PATH + config['DEFINE_CODETABLE_API'];
            if (config['DEFINE_CODETABLE_USER_API']) {
                const common_codetable_url = API_GATEWAY_PROXY_PATH + config['DEFINE_CODETABLE_USER_API'];
                this.loadUserCodetable(common_codetable_url);
            }
            this.loadCodetable(url, API_GATEWAY_PROXY_PATH);
            SessionContext.put('LOAD_CODETABLE_' + path, 'CODETABLE_LOADED', true);
        }
    }

    loadCodetable(url, API_GATEWAY_PROXY_PATH) {
        const codtableList = [];
        const configList = JSON.parse(sessionStorage.getItem('project_config'));
        _.each(config['DEFINE_CODETABLE']['HOME_CODETABLE'], (codetable) => {
            // if (codetable.ConditionMap) {
            //     let arrConditionMap = JSON.stringify(codetable.ConditionMap).replace('{', '').replace('}', '').replace(/:/ig,'=').replace(/"/ig,'').replace(/'/ig,'').split(',');
            //     let conditionMap = '';
            //     _.each(arrConditionMap, (item) => {
            //         conditionMap += item + '&';
            //     })
            //     codtableList.push(codetable.CodeTableName+'?'+conditionMap);
            // } else {
            // codtableList.push(codetable.CodeTableName);
            codtableList.push(codetable['CodeTableName']);
            // }
        })
        _.each(config['DEFINE_CODETABLE']['OTHER_CODETABLE'], (codetable) => {
            // codtableList.push(codetable.CodeTableName);
            codtableList.push(codetable['CodeTableName']);
            // if (codetable.ConditionMap) {
            //     let arrConditionMap = JSON.stringify(codetable.ConditionMap).replace('{', '').replace('}', '').replace(/:/ig,'=').replace(/"/ig,'').replace(/'/ig,'').split(',');
            //     let conditionMap = '';
            //     _.each(arrConditionMap, (item) => {
            //         conditionMap += item + '&';
            //     })
            //     codtableList.push(codetable.CodeTableName+'?'+conditionMap);
            // } else {
            //     codtableList.push(codetable.CodeTableName);
            // }
        })
        if (codtableList.length > 0) {
            $.ajax({
                beforeSend: function (request) {
                    const setionToken = SessionContext.get('Authorization');
                    request.setRequestHeader('Authorization', 'Bearer ' + setionToken.substr(13).split('&')[0]);
                    if (configList && configList.UI_CAS) {
                        request.setRequestHeader('x-ebao-auth-protocal', configList.UI_CAS);
                    }
                    if (config && config.UI_CAS_SERVICE_URL) {
                        request.setRequestHeader('x-ebao-cas-service-url', configList.UI_CAS_SERVICE_URL);
                    }
                    const SystemId = sessionStorage.getItem('x-ebao-system-id');
                    if (SystemId) {
                        request.setRequestHeader('x-ebao-system-id', SystemId);
                    }
                },
                method: 'post',
                url: API_GATEWAY_PROXY_PATH + 'dd/public/codetable/v1/byCodeTableNameList', // byCodeTableNameList // 'dd/public/codetable/v1/codeTableVoList/byNameList'
                async: false,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                xhrFields: { withCredentials: true },
                crossDomain: true,
                data: JSON.stringify(codtableList),
                success: (codetables) => {
                    const requestCodetable = [{ 'serviceName': 'dd', 'codetableNameList': [] }];
                    _.each(codetables, (codetable) => {
                        const serveceName = codetable.ServiceName; // codetable.BusinessCodeTable.ServiceName;
                        const serveceNameObj = _.find(requestCodetable, (rc) => {
                            return rc.serviceName == serveceName;
                        });
                        if (serveceNameObj) {
                            if (codetable.ConditionMap) {
                                // codetable.BusinessCodeTable.Name
                                serveceNameObj['codetableNameList'].push({ 'CodeTableName': codetable.Name, 'ConditionMap': codetable.ConditionMap });
                            } else {
                                serveceNameObj['codetableNameList'].push({ 'CodeTableName': codetable.Name });
                            }
                        } else {
                            const codetableNameList = [];
                            if (codetable.ConditionMap) {
                                codetableNameList.push({ 'CodeTableName': codetable.Name, 'ConditionMap': codetable.ConditionMap });
                            } else {
                                codetableNameList.push({ 'CodeTableName': codetable.Name });
                            }
                            const serveceNameObj = { 'serviceName': serveceName, 'codetableNameList': codetableNameList };
                            requestCodetable.push(serveceNameObj);
                        }
                    })
                    _.each(requestCodetable, (codetableItem) => {
                        const serviceName = codetableItem.serviceName;
                        const codeTableUrl = url.replace(/dd/, serviceName);
                        this.loadCodetableService(codeTableUrl, codetableItem.codetableNameList);
                    });
                },
                error: (text) => {
                    console.error('load codetable list define error', text)
                }
            })

        }


        // if(!_.isEmpty(config['DEFINE_CODETABLE']['HOME_CODETABLE'])){
        //     this.loadCodetableService(url,config['DEFINE_CODETABLE']['HOME_CODETABLE']);
        // }
        // if(!_.isEmpty(config['DEFINE_CODETABLE']['OTHER_CODETABLE'])){
        //     AjaxUtil.call(url,config['DEFINE_CODETABLE']['OTHER_CODETABLE'],{"method":"POST"}).then((codetables)=>{
        //         this.handleCodetable(codetables);
        //     });
        // }
    }

    permissionCode() {
        let _url = config['DEFINE_PERMISSION_URL'];
        const configList = JSON.parse(sessionStorage.getItem('project_config'));
        // sessionStorage.setItem("PERMISSION-CODE1","M26004141001");
        if (_url) {
            const _config = SessionContext.get('project_config') || null;
            const API_GATEWAY_PROXY_PATH = _config != null ? _config.UI_API_GATEWAY_PROXY : '';
            const url = API_GATEWAY_PROXY_PATH + _url;
            const _code = SessionContext.get('PERMISSION-CODE');
            if (_code && _code.Code) {
                $.ajax({
                    beforeSend: function (request) {
                        const setionToken = SessionContext.get('Authorization');
                        request.setRequestHeader('Authorization', 'Bearer ' + setionToken.substr(13).split('&')[0]);
                        if (configList && configList.UI_CAS) {
                            request.setRequestHeader('x-ebao-auth-protocal', configList.UI_CAS);
                        }
                        if (configList && configList.UI_CAS_SERVICE_URL) {
                            request.setRequestHeader('x-ebao-cas-service-url', configList.UI_CAS_SERVICE_URL);
                        }
                        const SystemId = sessionStorage.getItem('x-ebao-system-id');
                        if (SystemId) {
                            request.setRequestHeader('x-ebao-system-id', SystemId);
                        }
                    },
                    method: 'get',
                    url: `${url}?code=${_code.Code}`,
                    async: false,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    xhrFields: { withCredentials: true },
                    crossDomain: true,
                    success: (codetables) => {
                        PageContext.put(JSON.stringify(_code), codetables);
                        SessionContext.put(JSON.stringify(_code), Util.compile(JSON.stringify(codetables)), true);
                    },
                    error: (text) => {
                        console.error('get PERMISSION CODE error', text)
                    }
                })
            }
        }
    }

    componentWillMount() {
        this.loadCodetableList();
        this.permissionCode();
        UniqueId.clearComponentId();
    }

    componentDidMount() {
        let _self = this;
        $('[data-toggle=\'tooltip\']').tooltip();

        UniqueId.validateComponentId();

        // if (Util.parseBool(config.DEFAULT_LOGOUT_IS_WORKING)) {
        //     this.setTimer(this);
        // }
        //$("#loadings").hide();
        const app = $('#app');
        const pageLoading = $('#page-loading');
        if (pageLoading && pageLoading.length > 0) {
            pageLoading.hide();
        }
        app.show();
        // app.unbind('DOMSubtreeModified');
        //
        //
        // app.bind('DOMSubtreeModified', function(e) {
        //     const appDivHight = $("#app").height();
        //     const screenHight = $(window).height();
        //     if(screenHight-appDivHight>0){
        //         $(".pagefooter").addClass("fixed-bottom");
        //     }else{
        //         $(".pagefooter").removeClass("fixed-bottom");
        //     }
        // });

        //add onfocus style for some input conponent start
        // $(".input-group > input.form-control").focus(function () {

        //     $(this).parent('.input-group').addClass('focusborder')

        // })
        // $(".input-group > input.form-control").blur(function () {

        //     $(this).parent('.input-group').removeClass('focusborder')

        // })
        //add onfocus style for some input conponent end
        //CodeTableService.handleCodetableList();
        _self.escCloseDialog();
        _self.handleWindowBack();
        const projectConfig = JSON.parse(sessionStorage.getItem('project_config')) || {};
        if (projectConfig && projectConfig.UI_DEFAULT_LOGOUT_IS_WORKING) {
            this.DEFAULT_LOGOUT_IS_WORKING = projectConfig.UI_DEFAULT_LOGOUT_IS_WORKING;
        }
        if (Util.parseBool(this.DEFAULT_LOGOUT_IS_WORKING)) {
            this.setTimer(this);
            localStorage.setItem('isAlive', new Date().getTime());
            document.body.onkeydown = function () {
                localStorage.setItem('isAlive', new Date().getTime());
            }
        }
        let loginFlag = sessionStorage.getItem('goInAdmin')
        if (loginFlag) {
            let iframeHeight = $('#app').outerHeight(true);
            let currentUrl = window.currentUrl;
            if (currentUrl.port != '' && !projectConfig.UI_POSTMESSAGE_HOST) {
                parent.postMessage(iframeHeight, 'http://localhost:8901');
            } else if (currentUrl.port == '') {
                parent.postMessage(iframeHeight, window.location.protocol + '//' + window.location.host);
            } else {
                parent.postMessage(iframeHeight, projectConfig.UI_POSTMESSAGE_HOST);
            }
        }

        if (Util.parseBool(this.DEFAULT_LOGOUT_IS_WORKING))  {
            _self.doWorkTimer = window.setInterval(function () {
                _self.count++;
                // console.log('dowork', _self.count, _self.countdownLimit )
                $(document).unbind('mousemove');
                let x;
                let y;
                //�������
                $(document).mousemove(function (event) {
                    let x1 = event.clientX;
                    let y1 = event.clientY;
                    // console.log('����ʱ', _self.count, _self.countdownLimit)
                    if (x != x1 || y != y1) {
                        // console.log('����ڶ�')
                        _self.count = 0;
                        _self.onClickConfirm();
                        $(document).unbind('mousemove');
                        _self.setTimer(_self);
                    }
                    x = x1;
                    y = y1;
                });   
            }, 1000 * 60 * 4);
        }
    }

    componentDidUpdate() {
        $('[data-toggle=\'tooltip\']').tooltip();
        if (Util.parseBool(this.DEFAULT_LOGOUT_IS_WORKING)) {
            this.setTimer(this);
        }

    }

    componentWillUnmount() {
        PageContext.clear();
        ValidatorContext.clear();
        ComponentContext.clear();
        $('#loadings').show();
    }

    onClickConfirm() {
        ConfirmDialog.hide('login_confirm_dialog');
        const cfg = SessionContext.get('project_config') || {};
        AjaxUtil.call(cfg.UI_API_GATEWAY_PROXY + 'v1/login', { 'REDIRECT_URL': window.location.host });
        this.setTimer(this);
    }

    setTimer(_self) {
        const projectConfig = SessionContext.get('project_config');
        let DEFAULT_LOGOUT_TIME_MINUTES = config.DEFAULT_LOGOUT_TIME_MINUTES;
        if (projectConfig && projectConfig.UI_DEFAULT_LOGOUT_TIME_MINUTES) {
            DEFAULT_LOGOUT_TIME_MINUTES = projectConfig.UI_DEFAULT_LOGOUT_TIME_MINUTES;
        }
        localStorage.setItem('isAlive', new Date().getTime());
        let logoutLimit = Number(DEFAULT_LOGOUT_TIME_MINUTES);
        _self.count = 0;
        window.clearTimeout(_self.loginTimer);
        window.clearTimeout(_self.loginTimeOut);
        window.clearInterval(_self.countDownTimer);
        _self.loginTimer = setTimeout(function () { return _self.showConfirmDialog(_self, logoutLimit); }, (logoutLimit - this.countdownLimit) * 60 * 1000);
    }


    showConfirmDialog(_self, logoutLimit) {
        const now = new Date().getTime();
        const isAlive = localStorage.getItem('isAlive');
        if (parseInt((now - Number(isAlive)) / 1000 / 60) < (logoutLimit - _self.countdownLimit)) {
            _self.setTimer(_self);
        } else {
            ConfirmDialog.show('login_confirm_dialog');
            _self.secondCount = 0;
            window.clearInterval(_self.countDownTimer);
            const dialogMsg = $('#login_confirm_dialog_msg');
            _self.countDownTimer = window.setInterval(function () {
                _self.secondCount++;
                const showSec = _self.countdownLimit * 60 - _self.secondCount;
                let info = r18n.Page.messageFront + showSec + r18n.Page.messageEnd;
                // console.log('��ʱ�������', showSec)

                dialogMsg.text(info);
                if (Number(showSec) < 0) {
                    window.clearTimeout(_self.loginTimer);
                    window.clearTimeout(_self.loginTimeOut);
                    window.clearInterval(_self.countDownTimer);
                    window.clearInterval(_self.doWorkTimer);
                    _self.pageLogout(_self);
                }
            }, 1000);
            _self.loginTimeOut = setTimeout(function () {
                _self.pageLogout(_self);
            }, _self.countdownLimit * 60 * 1000);
        }

    }

    async pageLogout(_self) {
        const _config = SessionContext.get('project_config');
        let DEFAULT_LOGOUT_TIME_MINUTES = config.DEFAULT_LOGOUT_TIME_MINUTES;
        if (_config && _config.UI_DEFAULT_LOGOUT_TIME_MINUTES) {
            DEFAULT_LOGOUT_TIME_MINUTES = _config.UI_DEFAULT_LOGOUT_TIME_MINUTES;
        }
        let logoutLimit = Number(DEFAULT_LOGOUT_TIME_MINUTES);
        const _now = new Date().getTime();
        const isAlive = localStorage.getItem('isAlive');
        if (parseInt((_now - Number(isAlive)) / 1000 / 60) < (logoutLimit - _self.countdownLimit)) {
            ConfirmDialog.hide('login_confirm_dialog');
            _self.setTimer(_self);
        } else {
            ConfirmDialog.hide('login_confirm_dialog');
            let url = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'USER', 'LOGOUT');
            await AjaxUtil.call(url);
            window.location.hash = '/';
        }
    }

    escCloseDialog() {
        if (config.DOES_ESC_CLOSE_DIALOG) {
            $(document).keydown(function (event) {
                if (event.keyCode == '27') {
                    let body = $(document.body).eq(0);
                    if (body.hasClass('modal-open')) {
                        body.removeClass('modal-open');
                        body.css('overflow', 'auto');
                    }
                    if (body.hasClass('modal-open-self')) {
                        body.removeClass('modal-open-self');
                        body.css('overflow', 'auto');
                    }
                }
            });
        }
    }

    handleWindowBack() {
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            history.pushState(null, null, document.URL);
        });
    }
}

/**@ignore
 * Page component prop types
 */
Page.propTypes = $.extend({}, Component.propTypes, {
    pageName: PropTypes.string,
    className: PropTypes.string,
    skin: PropTypes.string,
    showPageFooter: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
});

/**@ignore
 * Get Page component default props
 */
Page.defaultProps = $.extend({}, Component.defaultProps, {
    showPageFooter: false
});

