
import UIText from './Text';
import { CodeTableService } from 'rainbow-desktop-codetable';


export default class UIOutputCodetable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            codes: null
        };
    }

    async componentDidMount() {
        const data = await CodeTableService.getCodeTable({
            CodeTableName: this.props.codeTableName
        });
        this.setState({
            codes: data.codes || []
        });
    }

    render () {
        // 去 codes 找 和 data 某个字段的值相同的元素并且返回 该元素对应的默认字段 text 内容
        if(this.state.codes) {
            const dataWrapper = this.state.codes.find((item)=>{
                return item.id == this.props.model[this.props.property];
            });
            return <UIText io="out" model={dataWrapper} property="text" />;
        }
        return <UIText io="out" />;
    }
}

