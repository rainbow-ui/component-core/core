import "../js/css/filterMore.min.css";
import "../js/css/mumayi_top.css";
import "../js/filterMore.min.js";
import "../js/layer.min.js";
import "../js/mumayi_top.min.js";
// import { OnClickEvent, Param, Component } from "rainbowui-core";
import PropTypes from 'prop-types';
import r18n from "../../../i18n/reactjs-tag.i18n.jsx";
import OnClickEvent from "../../../event/OnClickEvent";
import Param from "../../../basic/Param";
import Component from "../../../basic/Component";

export default class Filter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpenList: [],
            isHeight:{height:null},
            isboxAuto:false,
            isBoxOpenList: [],
            isExpand:false
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.initComponent();
        this.hidenMore_a();
        this.hidenUnlimited_span();
        //this.chooseDefalutCkecked();
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (_.isEqual(this.props.dataSource, nextProps.dataSource)) {
            return false
        }
        else {
            return true
        }
    }

    componentWillReceiveProps(nextProps){
        let element = document.createElement("div");
        element.id = this.props.id;
        let openElement = $("#" + element.id).children().children().closest(".is_open");
        let expandElement = $("#" + element.id).siblings(".filter_btn").children().text();
        let boxOpenClass = $("#" + element.id)[0].className;
        let isBoxOpenInfo = {};
        if(boxOpenClass=="searchbox is_boxOpen"){
            this.setState({isboxAuto:true})
        }
        if(expandElement==r18n.expandMessage){
            this.setState({isExpand:true})
        }else{
            this.setState({isExpand:false})
        }
    }

    componentWillUpdate() {
        let element = document.createElement("div");
        element.id = this.props.id;
        let heightBox = $("#" + element.id)[0].offsetHeight
        let openElement = $("#" + element.id).children().children().siblings(".is_open")
        // let openElement = $("#" + element.id).children().children().closest(".is_open");
        let boxOpenClass = $("#" + element.id)[0].className;
        let isBoxOpenInfo = {};
        if(boxOpenClass=="searchbox is_boxOpen"){
            this.state.isboxAuto = true
            isBoxOpenInfo.id = element.id
            let countBox = 0;
            this.state.isBoxOpenList.forEach((each) => {
                if (each.id == isBoxOpenInfo.id) {
                    countBox++;
                }
            })
            if (countBox == 0&&isBoxOpenInfo&&isBoxOpenInfo.id) {
                this.state.isBoxOpenList.push(isBoxOpenInfo)
            }
        }
        let isOpenInfo = {};
        if (openElement.length > 0) {
            let openCount = openElement.length;
            for(let i=0;i<openCount;){
                let isOpenInfo = {};
                isOpenInfo.id = openElement[i].id; 
                i++
            if(heightBox){
                this.state.isHeight.height = heightBox;
            }
            let countSave = 0;
            this.state.isOpenList.forEach((each) => {
                if (each.id == isOpenInfo.id) {
                    countSave++;
                }
            })
            if (countSave == 0) {
                this.state.isOpenList.push(isOpenInfo)
            }
        }
        }
    }

    componentDidUpdate() {
        super.componentDidUpdate();
        $(".filter_" + this.props.id).empty();
        $(".filter_btn").remove();
        let element = document.createElement("div");
        element.id = this.props.id;
        element.className = "searchbox";
        $(".filter_" + this.props.id).append(element);
        this.initComponent();
        if(this.state.isOpenList.length>0){
            this.state.isOpenList.forEach((each) => {
                let item = each.id;
                let autoHeight = this.state.isHeight.height+'px'
                let aa = $("#"+item);
                $("#"+item).closest(".searchbox").css({height:autoHeight})
                $("#"+item).siblings(".r").children().text(r18n.closeMessage);
                $("#"+item).css({height:"auto"})
                if(!this.state.isExpand){
                    $("#"+item).closest(".searchbox").siblings(".filter_btn").children().text(r18n.closeMessage);
                    $("#"+item).closest(".searchbox").siblings(".filter_btn").children().removeClass("expand")
                }
                //$("#"+item).closest(".searchbox").siblings(".filter_btn").children().addClass("removeBefore")
            })
        }
        if(this.state.isboxAuto){
            this.state.isBoxOpenList.forEach((each) => {
                let idBox = each.id;
                $("#"+idBox).css({height:"auto"})
                let aa =  $("#"+idBox).siblings(".filter_btn").children()
                $("#"+idBox).siblings(".filter_btn").children().text(r18n.closeMessage);
                $("#"+idBox).siblings(".filter_btn").children().removeClass("expand")
                //$("#"+idBox).siblings(".filter_btn").children().addClass("removeBefore")
            })
        }
    }

    renderComponent() {
        return (
            <div className={"filter_" + this.props.id}>
                <div className="searchbox" id={this.props.id}></div>
            </div>
        );
    }

    initComponent() {
        let self = this;
        let expandRow;
        if (this.props.expandRow) {
            expandRow = this.props.expandRow;
        } else {
            expandRow = 2;
        }
        let options = {
            //查询事件
            "search": function (paramList) {
                self.onFilter(paramList);
            },
            //默认展开条件数
            "expandRow": expandRow,
            "expandEvent": function (state) {
                //展开更多条件触发事件 参数：state  true表示展开  false 收缩
                if (self.props.onExpand) {
                    self.props.onExpand(new OnClickEvent(self, event, Param.getParameter(self)));
                }
            },
            //查询条件
            "searchBoxs": this.props.dataSource,
            //国际化
            "expandMessage": r18n.expandMessage,
            "closeMessage": r18n.closeMessage,
            "allMessage": r18n.allMessage,
            "customMessage": r18n.customMessage,
            "expandRowNumMessage": r18n.expandRowNumMessage,
            "filterMoreNotSupportMessage": r18n.filterMoreNotSupportMessage,
            "confirmMessage": r18n.confirmMessage,
            "multiselectMessage": r18n.multiselectMessage,
            "singleSelectMessage": r18n.singleSelectMessage
        };
        $("#" + self.props.id).fiterMore(options);
    }
    hidenMore_a() {
        const filterObject_a = $("#" + this.componentId + ".searchbox a");
        if (this.props.hidenMore == 'true') {
            filterObject_a.css("display", "none");
        }
    }
    hidenUnlimited_span() {
        const filterObject_span = $("#" + this.componentId + ".searchbox .filter_option span:first-child");
        const filterObject_div = $("#" + this.componentId + ".searchbox .control-type");
        if (this.props.hidenUnlimited == 'true') {
            filterObject_span.css("display", "none");
            filterObject_div.css("display", "none");
        }
    }
    onFilter(paramList) {
        //this.chooseCkecked(paramList);
        if (this.props.onClick) {
            this.props.onClick(new OnClickEvent(this, event, paramList));
        }
    }

    chooseDefalutCkecked() {
        let dataSourceList = this.props.dataSource;
        let results = [];
        dataSourceList.forEach((dataSource, index) => {
            if (dataSource.defaults) {
                let resultMap = { "ValueList": dataSource.defaults, "id": dataSource.id.replace("searchitem_", ""), "isMultiple": dataSource.isMultiple };
                results.push(resultMap);
            }
        });
        this.chooseCkecked(results);
    }

    chooseCkecked(paramList) {
        let dataSourceList = this.props.dataSource;
        paramList.forEach((param, index) => {
            let id = param.id;
            let valueList = param.ValueList;
            valueList.forEach((value, index) => {
                dataSourceList.forEach((dataSource, index) => {
                    if (dataSource.id == "searchitem_" + id) {
                        let data = dataSource.data;
                        data.forEach((d, index) => {
                            if (d.value == value) {
                                console.log("value", { "id": id, "value": d.value, "text": d.text });
                            }
                        });
                    }
                });
            });
        }
        );
    }

};


Filter.propTypes = $.extend({}, Component.propTypes, {
    hidenMore: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    hidenUnlimited: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),

});


Filter.defaultProps = $.extend({}, Component.defaultProps, {
    hidenMore: 'false',
    hidenUnlimited: 'false',

});