import Digit from "../basic/Digit";
import config from "config";
import { Util } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';
import { ValidatorContext } from 'rainbow-desktop-cache';

export default class Currency extends Digit {

    renderInput() {
        if (this.props.unitPosition == "right") {
            return (
                <div className="input-group">
                    <input id={this.componentId} name={this.getName()} type="text" className="form-control currency textRight" style={this.props.style} title={this.getI18n(this.props.title)}
                        placeholder={this.props.placeHolder} data-auto-test={this.getNameForTest()} maxLength={this.props.maxLength} />
                    {this.renderDeleteIcon()}
                    <span className="input-group-addon currencyoverflowRight">
                        {this.props.unit}
                    </span>
                </div>
            );
        } else if (this.props.unitPosition == "none") {
            return (
                <div className="input-group">
                    <input id={this.componentId} name={this.getName()} type="text" className="form-control currency textRight" style={this.props.style} title={this.getI18n(this.props.title)}
                        placeholder={this.props.placeHolder} data-auto-test={this.getNameForTest()} maxLength={this.props.maxLength} />
                    {this.renderDeleteIcon()}
                </div>
            )
        } else {
            return (
                <div className="input-group">
                    <span className="input-group-addon currencyoverflowLeft">
                        {this.props.unit}
                    </span>
                    <input id={this.componentId} name={this.getName()} type="text" className="form-control currency textRight" style={this.props.style} title={this.getI18n(this.props.title)}
                        placeholder={this.props.placeHolder} data-auto-test={this.getNameForTest()} maxLength={this.props.maxLength} />
                    {this.renderDeleteIcon()}
                </div>
            );
        }
    }

    renderDeleteIcon() {        
		if (Util.parseBool(this.props.enabled)&&Util.parseBool(this.props.showDeleteIcon)&&this.props.style.textAlign=="left") {
			return (
				// <button id={this.componentId + "_deleteIcon"} class="input-remove" type="button" tabindex="-1"
				// style={{display:"none"}} >x</button>
                <span class="rainbow Clear deleteIcon" id={this.componentId + "_deleteIcon"} 
                    style={{display: 'none'}}></span>
			)
		}
	}
    /*** Render input component */
    renderInputComponent() {
        if (this.props.io == "in" || this.props.io == null) {
            return this.renderInput();
        }

        else if (this.props.io == "out") {
            return this.renderOutput();
        }

        return <noscript />;
    }

    /*** Render output */
    renderOutput() {
        let output = this.getOutputValue();
        if (!output){
            return <noscript/>;
        }
        
        if(Util.parseBool(this.props.isShowValueTooltip)){
            return (
                <span id={this.componentId} className="outPutText" data-toggle="tooltip" data-placement="bottom" data-original-title={output} style={{display: 'block', textAlign: this.props.outDirection}}>
                    {this.props.unit}{" "}{output}
                </span>
            );
        }else{
            return (
                <span id={this.componentId} className="outPutText" style={{display: 'block', textAlign: this.props.outDirection}}>
                    {this.props.unit}{" "}{output}
                </span>
            );
        }

    }

    /**@ignore
     * Get output value
     */
    getOutputValue() {
        return this.getComponentValue() || '';
    }

    componentDidUpdate(nextProps, nextState) {
        super.componentDidUpdate(nextProps, nextState);
        this.clearValidationInfo(nextProps);
    }

    clearValidationInfo(nextProps) {
        const inputObject = $("#" + this.componentId);
		if ($("form").data('bootstrapValidator') && !$("form").data('bootstrapValidator').isValidField(this.componentId) && inputObject.val() != '') {
            $("form").data('bootstrapValidator').enableFieldValidators(this.componentId,false);
            ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
        }
    }

};

/**@ignore
 * Currency component prop types
 */
Currency.PropTypes = $.extend({}, Digit.propTypes, {
    enabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    componentType: PropTypes.string,
    unit: PropTypes.string,
    unitPosition: PropTypes.oneOf(["left", "right", "none"]),
    showDeleteIcon:PropTypes.bool,
    style: PropTypes.string,
    outDirection: PropTypes.oneOf(["left", "right", "center"])
});

/**@ignore
 * Get Currency component default props
 */
Currency.defaultProps = $.extend({}, Digit.defaultProps, {
    componentType: "currency",
    format: config.DEFAULT_CURRENCY_FORMAT,
    unit: config.DEFAULT_CURRENCY_UNIT,
    unitPosition: "left",
    subType: "UICurrency",
    showDeleteIcon:true,
    style: { textAlign: 'right' },
    outDirection: "right"
});
