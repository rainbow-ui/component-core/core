// import { Param, Component } from "rainbowui-core";
import Param from "../../../basic/Param";
import Component from "../../../basic/Component";
import { Util, StringUtil } from 'rainbow-desktop-tools';
import "../plugin/jqueryPicklist/jquery.ui.widget.js";
import "../plugin/jqueryPicklist/jquery-picklist.js";
import "../plugin/jqueryPicklist/jquery-picklist.css";
import PropTypes from 'prop-types';
import r18n from "../../../i18n/reactjs-tag.i18n";

export default class Picklist extends Component {

	constructor(props) {
		super(props);
		this.state = {
			searchValue: ""
		}
		this.dataSource = [];
	}

	componentDidMount() {
		this.dataSource = this.props.dataSource;
		this.handlerComponent();
		this.initValue()
	}

	componentWillMount() {
		super.componentWillMount();
	}

	componentDidUpdate() {
		this.handleSearchValue();
	}

	static getPickedValue(picklistId) {
		return $("#" + picklistId).val();
	}

	handleSearchValue() {
		const self = this;
		const picklistObj = $("#" + this.componentId);
		let searchValue = self.state.searchValue;
		let dataSource = this.props.dataSource;
		if (searchValue) {
			let result = [];
			_.each(dataSource, (data) => {
				let label = String(data.label).toUpperCase();
				if ((data.label && label.indexOf(StringUtil.trim(searchValue).toUpperCase()) != -1) || Util.parseBool(data.selected)) {
					result.push(data);
				}
			}
			);
			this.dataSource = result;
		} else {
			this.dataSource = dataSource;
		}
		picklistObj.siblings(".pickList").find(".pickList_listItem").remove();
		$("#" + this.componentId + " option").remove();
		picklistObj.pickList("insertItems", this.dataSource);
	}

	afterAddFun(event) {
		let dataSource = this.dataSource;
		let { model, property, afterAdd } = this.props;
		let selectedData = $("#" + event.target.id + " [selected ='selected']");
		let result = [];
		_.each(this.dataSource, (data) => {
			data.selected = false;
		});
		selectedData.each((index) => {
			result.push({ value: selectedData[index].value, label: selectedData[index].label, selected: true });
			_.each(dataSource, (data) => {
				if (data.value == selectedData[index].value) {
					data.selected = true;
				}
			});
		});
		this.setValue(result);
		if (afterAdd) afterAdd(event);

	}

	afterAddAllFun(event) {
		let { model, property, afterAdd, afterAddAll } = this.props;
		let result = [];
		_.each(this.dataSource, (data) => {
			data.selected = true;
			result.push({ value: data.value, label: data.label, selected: true });
		});
		this.setValue(result);
		if (afterAddAll) {
			afterAddAll(event)
		}
	}

	afterRemoveFun(event) {
		let { model, property, afterRemove } = this.props;
		let selectedData = $("#" + event.target.id + " [selected ='selected']");
		let result = [];
		_.each(this.dataSource, (data) => {
			data.selected = false;
		});
		selectedData.each((index) => {
			result.push({ value: selectedData[index].value, label: selectedData[index].label, selected: true });
			_.each(this.dataSource, (data) => {
				if (data.value == selectedData[index].value) {
					data.selected = true;
				}
			});
		})

		this.setValue(result);
		this.handleSearchValue();
		if (afterRemove) {
			afterRemove(event);
		}
	}

	afterRemoveAllFun(event) {
		let { afterRemoveAll } = this.props;
		_.each(this.dataSource, (data) => {
			data.selected = false;
		});
		this.setValue(null);
		this.handleSearchValue();
		if (afterRemoveAll) {
			afterRemoveAll(event);
		}
	}

	handlerComponent() {
		const { sourceListLabel, targetListLabel, addAllLabel, addLabel, removeAllLabel, removeLabel, sortAttribute, beforeBuild, afterBuild, beforePopulate, afterPopulate, beforeAddAll, beforeAdd, beforeRemove, beforeRemoveAll, beforeRefresh, afterRefresh, beforeRefreshControls, afterRefreshControls, onDestroy } = this.props;
		let afterAdd = this.afterAddFun.bind(this);
		let afterAddAll = this.afterAddAllFun.bind(this);
		let afterRemove = this.afterRemoveFun.bind(this);
		let afterRemoveAll = this.afterRemoveAllFun.bind(this);
		const pickListId = this.componentId
		$("#" + this.componentId).pickList(
			{
				id: pickListId,
				sourceListLabel: sourceListLabel || r18n.UnSelected,
				targetListLabel: targetListLabel || r18n.Selected,
				addAllLabel: r18n.AddAll,
				addLabel: r18n.Add,
				removeAllLabel: r18n.RemoveAll,
				removeLabel: r18n.Remove,
				sortAttribute: sortAttribute,
				items: this.dataSource,
				beforeBuild: beforeBuild,
				afterBuild: afterBuild,
				beforePopulate: beforePopulate,
				afterPopulate: afterPopulate,
				beforeAddAll: beforeAddAll,
				afterAddAll: afterAddAll,
				beforeAdd: beforeAdd,
				afterAdd: afterAdd,
				beforeRemove: beforeRemove,
				afterRemove: afterRemove,
				beforeRemoveAll: beforeRemoveAll,
				afterRemoveAll: afterRemoveAll,
				beforeRefresh: beforeRefresh,
				afterRefresh: afterRefresh,
				beforeRefreshControls: beforeRefreshControls,
				afterRefreshControls: afterRefreshControls,
				onDestroy: onDestroy
			});
	}

	render() {
		const { multiple, style } = this.props;
		return (
			<div>
				{this.renderSearch()}
				<select id={this.componentId} name={this.componentId} multiple={multiple} style={style}>
				</select>
			</div>
		)
	}

	renderSearch() {
		if (Util.parseBool(this.props.searchable)) {
			return (
				<div className="tb-searchbar">
					<div id={this.componentId + "_search"} className="col-sm-6 col-md-6 col-lg-6" >
						<span className='rainbow Search'></span>
						<input type="text" onChange={this.onSearchData.bind(this)} />

					</div>
				</div>
			);
		} else {
			<div className="col-sm-6 col-md-6 col-lg-6"></div>
		}
	}

	onSearchData(event) {
		this.setState({ searchValue: event.target.value });
	}

	initValue() {
		let self = this;
		let dataSource = self.props.dataSource;
		let result = [];
		if (dataSource) {
			_.each(dataSource, (data) => {
				if (Util.parseBool(data.selected)) {
					result.push({ value: data.value, label: data.label, selected: true });
				}
			}
			);
			self.setValue(result);
		}
	}

	setValue(value) {
		const model = this.props.model;
		const property = this.props.property;
		if (model && property) {
			model[property] = value;
		}
	}

	getValue() {
		const model = this.props.model;
		const property = this.props.property;
		if (model && property) {
			return model[property] = value;
		} else {
			return null;
		}
	}

}

Picklist.propTypes = $.extend({}, Component.propTypes, {
	id: PropTypes.string,
	value: PropTypes.object,
	dataSource: PropTypes.array,
	style: PropTypes.string,
	multiple: PropTypes.string,
	beforeBuild: PropTypes.func,
	afterBuild: PropTypes.func,
	beforePopulate: PropTypes.func,
	afterPopulate: PropTypes.func,
	beforeAddAll: PropTypes.func,
	afterAddAll: PropTypes.func,
	beforeAdd: PropTypes.func,
	afterAdd: PropTypes.func,
	beforeRemove: PropTypes.func,
	afterRemove: PropTypes.func,
	beforeRemoveAll: PropTypes.func,
	afterRemoveAll: PropTypes.func,
	beforeRefresh: PropTypes.func,
	afterRefresh: PropTypes.func,
	beforeRefreshControls: PropTypes.func,
	afterRefreshControls: PropTypes.func,
	onDestroy: PropTypes.func,
	searchable: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
})

Picklist.defaultProps = $.extend({}, Component.defaultProps, {
	sourceListLabel: r18n.UnSelected || 'UnSelected',
	targetListLabel: r18n.Selected || 'Selected',
	addAllLabel: "AddAll",
	addLabel: "Add",
	removeAllLabel: "RemoveAll",
	removeLabel: "Remove",
	sortAttribute: "value",
	multiple: "multiple",
	style: {},
	searchable: true
})