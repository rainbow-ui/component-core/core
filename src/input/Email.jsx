import Input from "../basic/Input";
import ValidatorConstant from '../validator/ValidatorConstant';
import { Util } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';
import { ValidatorContext } from 'rainbow-desktop-cache';

export default class Email extends Input {

    renderInput() {
        return (
            <div className="input-group">
                <input id={this.componentId} name={this.getName()} type="text" className="form-control" style={this.props.style}
                    title={this.getI18n(this.props.title)} placeholder={this.props.placeHolder} data-auto-test={this.getNameForTest()} />
                {this.renderDeleteIcon()}
                <span className="input-group-addon pickerposition">
                    <span className="rainbow Email" />
                </span>
            </div>
        );
    }
    renderDeleteIcon() {
        if (Util.parseBool(this.props.enabled) && Util.parseBool(this.props.showDeleteIcon)) {
            return (
                // <button id={this.componentId + "_deleteIcon"} class="input-remove" type="button" tabindex="-1"
                //     style={{ display: "none" }} >x</button>
                <span class="rainbow Clear deleteIcon" id={this.componentId + "_deleteIcon"} 
                    style={{display: 'none'}}></span>
            )
        }
    }
    getValidatorId() {
        return ValidatorConstant.EMAIL_VALIDATOR;
    }

    componentDidUpdate(nextProps, nextState) {
        super.componentDidUpdate(nextProps, nextState);
        this.clearValidationInfo(nextProps);
    }

    clearValidationInfo(nextProps) {
        const inputObject = $("#" + this.componentId);
		if ($("form").data('bootstrapValidator') && !$("form").data('bootstrapValidator').isValidField(this.componentId) && inputObject.val() != '') {
            $("form").data('bootstrapValidator').enableFieldValidators(this.componentId,false);
            ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
        }
    }

};

Email.propTypes = $.extend({}, Input.propTypes, {
    enabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    componentType: PropTypes.string
});
Email.defaultProps = $.extend({}, Input.defaultProps, {
    showDeleteIcon: true,
    componentType: "email"
});
