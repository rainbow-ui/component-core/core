import Digit from "../basic/Digit";
import config from "config";
import ConvertorConstant from '../convertor/ConvertorConstant';
import { Util } from "rainbow-desktop-tools";
import PropTypes from 'prop-types';
import { ValidatorContext } from 'rainbow-desktop-cache';

export default class Percent extends Digit {

    renderInput() {
        if (this.props.symbolPosition == "none") {
            return (<div className="input-group">{this.renderInputElement()}{this.renderDeleteIcon()}</div>);
        } else {
            return (<div className="input-group">{this.renderInputElement()}{this.renderDeleteIcon()}{this.renderSymbol()}</div>);
        }
    }
    renderDeleteIcon() {
		if (Util.parseBool(this.props.enabled)&&Util.parseBool(this.props.showDeleteIcon)) {
			return (
                // <button id={this.componentId + "_deleteIcon"} class="input-remove" type="button" tabindex="-1"
				// style={{display:"none"}} >x</button>
				<span class="rainbow Clear deleteIcon" id={this.componentId + "_deleteIcon"} 
                    style={{display: 'none'}}></span>
			)
		}
	}
    /*** Render input component */
    renderInputComponent() {
        if (this.props.io == "in" || this.props.io == null) {
            return this.renderInput();
        }

        else if (this.props.io == "out") {
            return this.renderOutput();
        }

        return <noscript />;
    }

    /*** Render output */
    renderOutput() {
        if (this.props.symbolPosition == "none") {
            return (
                <span>
                    <span className="outPutText textRight disBlock" style={{display: 'block', textAlign: this.props.outDirection}}>
                        {this.getOutputValue()}
                    </span>
                </span>
            );
        } else {
            return (
                <span style={{display: 'block', textAlign: this.props.outDirection}}>
                    <span className="outPutText textRight disBlock">
                        {this.getOutputValue()}
                    </span>
                    {this.renderOutPutSymbol()}
                </span>
            );
        }
    }

    /**@ignore
     * Render input element
     */
    renderInputElement() {
        return (
            <input id={this.componentId} name={this.getName()} type="text" className="form-control textRight paddingRight0" style={this.props.style}
                placeholder={this.props.placeHolder} title={this.getI18n(this.props.title)} data-auto-test={this.getNameForTest()} maxLength={this.props.maxLength} />
        );
    }

    /**@ignore
     * Render symbol
     */
    renderSymbol() {
        if (Util.parseBool(this.props.isPermillage)) {
            return (
                <span className="input-group-addon percentcover">
                    <span className="glyphicon2 glyphicon-envelope2">{Percent.PermillageSymbol}</span>
                </span>
            );
        } else {
            return (
                <span className="input-group-addon percentcover">
                    <span className="glyphicon2 glyphicon-envelope2">{Percent.SYMBOL}</span>
                </span>
            );
        }
    }

    /**@ignore
     * Render output symbol
     */
    renderOutPutSymbol() {
        if (Util.parseBool(this.props.isPermillage)) {
            return (
                <span className="input-group-addon percentcover">
                    {Percent.PermillageSymbol}
                </span>
            );
        } else {
            return (
                <span className="input-group-addon percentcover">
                    {Percent.SYMBOL}
                </span>
            );
        }

    }

    getOutputValue() {
        return this.getComponentValue().replace(".", this.props.decimalSep);
        // render percent component by symbol position
        // if(this.props.symbolPosition == "left"){
        // 	return Percent.SYMBOL + " " + value;
        // } else if(this.props.symbolPosition == "none"){
        // 	return value;
        // } else {
        // 	return value + " " + Percent.SYMBOL;
        // }
    }

    initEvent() {
        super.initEvent();

        if (!Util.parseBool(this.props.allowBigNum)) {
            let _self = this;
            $("#" + this.componentId).change((event) => {
                let value = event.target.value;
                value = value.replace(/[^0-9\.]/g, "");
                if (parseFloat(value) > parseInt(_self.props.limit) && !this.props.ignoreLimit) {
                    _self.setComponentValue(event);
                    $("#" + _self.componentId).val(_self.formatDigit(_self.canonicalDigit(parseInt(_self.props.limit))));
                    _self.onChangeCallback(_self);
                }
            });
        }
    }

    getConvertorId() {
        if (Util.parseBool(this.props.isPermillage)) {
            return ConvertorConstant.PERMILLAGE_CONVERTOR;
        } else {
            return ConvertorConstant.PERCENT_CONVERTOR;
        }
    }

    componentDidUpdate(nextProps, nextState) {
        super.componentDidUpdate(nextProps, nextState);
        this.clearValidationInfo(nextProps);
    }

    clearValidationInfo(nextProps) {
        const inputObject = $("#" + this.componentId);
		if ($("form").data('bootstrapValidator') && !$("form").data('bootstrapValidator').isValidField(this.componentId) && inputObject.val() != '') {
            $("form").data('bootstrapValidator').enableFieldValidators(this.componentId,false);
            ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
        }
    }

};

/**@ignore
 * Percent symbol
 */
Percent.SYMBOL = "%";
Percent.PermillageSymbol = "‰";

/**@ignore
 * Percent component prop types
 */
Percent.PropTypes = $.extend({}, Digit.propTypes, {
    enabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    componentType: PropTypes.string,
    limit: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    symbolPosition: PropTypes.oneOf(["right", "none"]),
    isPermillage: "PropTypes.bool, Define whether this element is supported permillage, default is false.",
    showDeleteIcon:PropTypes.bool,
    style: PropTypes.string,
    allowBigNum: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    ignoreLimit:PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    decimalSep: PropTypes.string,
    outDirection: PropTypes.oneOf(["left", "right", "center"])
});

/**@ignore
 * Get Percent component default props
 */
Percent.defaultProps = $.extend({}, Digit.defaultProps, {
    componentType: "percent",
    format: config.DEFAULT_NUMBER_FORMAT,
    limit: 100,
    symbolPosition: "right",
    isPermillage: false,
    showDeleteIcon:true,
    allowBigNum: false,
    style: { textAlign: 'right' },
    ignoreLimit:false,
    decimalSep: ".",
    outDirection: "right"
});
