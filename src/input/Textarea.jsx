import Input from "../basic/Input";
import ConvertorConstant from '../convertor/ConvertorConstant';
import { Util } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';
import { ValidatorContext } from 'rainbow-desktop-cache';

export default class Textarea extends Input {

    renderInput() {
        if (this.props.maxLength) {
            return (
                <textarea id={this.componentId} name={this.getName()} className="form-control" maxLength={this.props.maxLength}
                    cols={this.props.cols} rows={this.props.rows} placeholder={this.props.placeHolder}
                    style={this.getStyle()} data-auto-test={this.getNameForTest()}>
                </textarea>
            );
        } else {
            return (
                <textarea id={this.componentId} name={this.getName()} className="form-control"
                    cols={this.props.cols} rows={this.props.rows} placeholder={this.props.placeHolder}
                    style={this.getStyle()} data-auto-test={this.getNameForTest()}>
                </textarea>
            );
        }
    }

    checkMaxInput() {
        if (this.props.maxLength) {
            const $obj = $("#" + this.componentId);
            // if (this.props.model && this.props.property) {
            //     $obj.val(this.props.model[this.props.property])
            // }
            const newid = $obj.attr("id") + 'msg';
            const strResult = '<span id="' + newid + '" class=\'textarea-max-msg\' >' + (this.getStrLeng($obj.val()) + '/' + this.props.maxLength + '</span>');
            const $msg = $("#" + newid);
            if ($msg.length == 0) {
                $obj.after(strResult);
            }
            else {
                $msg.html(strResult);
            }
        }
    }

    renderOutput() {
        const output = this.getOutputValue();
        return (
            <textarea className="form-control" style={this.getStyle()} value={output}
                cols={this.props.cols} title={this.getI18n(this.props.title)} rows={this.props.rows} readonly="readonly">
            </textarea>);
    }

    getStyle() {
        let { cols, rows, style, resize } = this.props;
        let styleDefine = '';
        if (cols && rows) {
            styleDefine = 'both';
        } else if (cols && !rows) {
            styleDefine = 'cols';
        } else if (!cols && rows) {
            styleDefine = 'rows';
        } else {
            styleDefine = 'none';
        }

        switch (styleDefine) {
            case 'cols':
                return $.extend({}, style, { width: "auto", resize: resize, maxWidth: "100%" });
            case 'rows':
                return $.extend({}, style, { height: "auto", resize: resize, maxWidth: "100%" });
            case 'both':
                return $.extend({}, style, { width: "auto", height: "auto", resize: resize, maxWidth: "100%" });
            case 'none':
                return $.extend({}, style, { resize: resize, maxWidth: "100%" });
        }
    }

    getConvertorId() {
        return ConvertorConstant.TEXTAREA_CONVERTOR;
    }

    componentDidUpdate(nextProps, nextState) {
        super.componentDidUpdate(nextProps, nextState);
        this.clearValidationInfo(nextProps);
        //if ($("#" + this.componentId).val() != '') {
            this.checkMaxInput();
        //}
    }

    clearValidationInfo(nextProps) {
        const inputObject = $("#" + this.componentId);
		if ($("form").data('bootstrapValidator') && !$("form").data('bootstrapValidator').isValidField(this.componentId) && inputObject.val() != '') {
            $("form").data('bootstrapValidator').enableFieldValidators(this.componentId,false);
            ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
        }
    }

};

/**@ignore
 * Textarea component prop types
 */
Textarea.propTypes = $.extend({}, Input.propTypes, {
    cols: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    rows: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    resize: PropTypes.oneOf(["none", "both", "horizontal", "vertical"]),
    componentType: PropTypes.string,
    maxLength: PropTypes.string,
});

/**@ignore
 * Get Textarea component default props
 */
Textarea.defaultProps = $.extend({}, Input.defaultProps, {
    resize: "both",
    componentType: "textarea",
    rows:"1"
});
