﻿import Digit from "../basic/Digit";
import config from "config";
import { Util } from "rainbow-desktop-tools";
import { ValidatorContext} from "rainbow-desktop-cache";
import PropTypes from 'prop-types';
import Event from "../basic/Event";
import Param from "../basic/Param";
import OnChangeEvent from '../event/OnChangeEvent';
import OnBlurEvent from '../event/OnBlurEvent';

export default class Number extends Digit {

	renderInput() {
		if (this.props.prefixIcon != undefined || this.props.suffixIcon != undefined || this.props.prefixText != undefined || this.props.suffixText != undefined) {
			return (
				<div className="input-group">
					{this.renderPrefixText()}
					{this.renderPrefixIcon()}
					{this.renderInputElement()}
					{this.renderDeleteIcon()}
					{this.renderSuffixIcon()}
					{this.renderSuffixText()}
				</div>
			);
		} else {
			return (
				<div className="input-group">
					{this.renderInputElement()}
					{this.renderDeleteIcon()}
				</div>
			);
		}
	}

	// componentDidMount() {
	// 	if (this.props.io != "out") {
	// 		this.initValidator();
    //     }
	// }
	renderDeleteIcon() {
		if (Util.parseBool(this.props.enabled)&&Util.parseBool(this.props.showDeleteIcon)&&this.props.style.textAlign=="left") {
			return (
				<button id={this.componentId + "_deleteIcon"} class="input-remove" type="button" tabindex="-1"
				style={{display:"none"}} >x</button>
			)
		}
	}
	/**@ignore
	 * Render input element
	 */
	renderInputElement() {
		
		return (
			<input id={this.componentId} name={this.getName()} type="text" className="form-control"
				placeholder={this.props.placeHolder} data-auto-test={this.getNameForTest()}
				style={this.props.style} data-max-length={this.props.maxLength} />
		);
	}

	/**@ignore
	 * Render prefix icon
	 */
	renderPrefixIcon() {
		if (this.props.prefixIcon != undefined) {
			return (
				<span className="input-group-addon fixleftposition">
					<span id={this.componentId + "_prefixIcon"} className={this.props.prefixIcon} style={{ cursor: "pointer" }} onClick={this.onPrefixIconClick.bind(this)} />
				</span>
			);
		}
		return <noscript />;
	}

	/**@ignore
	 * Render suffix icon
	 */
	renderSuffixIcon() {
		if (this.props.suffixIcon != undefined) {
			return (
				<span className="input-group-addon fixalliconposition">
					<span id={this.componentId + "_suffixIcon"} className={this.props.suffixIcon} style={{ cursor: "pointer" }} onClick={this.onSuffixIconClick.bind(this)} />
				</span>
			);
		}
		return <noscript />;
	}

	/**@ignore
	 * Render prefix text
	 */
	renderPrefixText() {
		if (this.props.prefixText != undefined) {
			return (
				<span className="input-group-addon fixleftposition">
					<span>{this.props.prefixText}</span>
				</span>
			);
		}
		return <noscript />;
	}

	/**@ignore
	 * Render suffix text
	 */
	renderSuffixText() {
		if (this.props.suffixText != undefined) {
			return (
				<span className="input-group-addon fixalliconposition">
					<span>{this.props.suffixText}</span>
				</span>
			);
		}
		return <noscript />;
	}

	componentDidUpdate(nextProps, nextState) {
		super.componentDidUpdate(nextProps, nextState);
		this.clearValidationInfo(nextProps);
		// console.log('number...')
		// if (this.props.io != "out") {
		// 	this.initEvent();
		// 	this.onChangeColorByEndorsement();
		// }
	}
	initEvent() {
		super.initEvent();
		// let _self = this;
		// let me = $("#" + this.componentId);

		// // handler input propertychange
        // me.bind("input propertychange", (event) => {
        //    _self.setComponentValue(event)
		// });
		
		// // handle onchange event
        // me.bind("change", (event) => {
        //     _self.onChangeCallback(_self);
        //     _self.onChangeColorByEndorsement();
        // });

        // // handle onblur event
        // me.bind("blur", (event) => {
		// 	_self.handleBlurEvent();
		// 	_self.setComponentValue(event)
        //     if (_self.props.onBlur) {
        //         _self.props.onBlur(new OnBlurEvent(_self, event, Param.getParameter(_self), null, null));
        //     }
        // });

	}
	clearValidationInfo(nextProps) {
        const inputObject = $("#" + this.componentId);
		if ($("form").data('bootstrapValidator') && !$("form").data('bootstrapValidator').isValidField(this.componentId) && inputObject.val() != '') {
            $("form").data('bootstrapValidator').enableFieldValidators(this.componentId,false);
            ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
        }
    }
};

/**@ignore
 * Number component prop types
 */
Number.PropTypes = $.extend({}, Digit.propTypes, {
	enabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
	componentType: PropTypes.string,
	required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
	showDeleteIcon:PropTypes.bool,
});

/**@ignore
 * Get Number component default props
 */
Number.defaultProps = $.extend({}, Digit.defaultProps, {
	format: config.DEFAULT_NUMBER_FORMAT,
	style: { textAlign: 'right' },
	subType: "UINumber",
	componentType: "number",
	required: false,
	showDeleteIcon:true
});
