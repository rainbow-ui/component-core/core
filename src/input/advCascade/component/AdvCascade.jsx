// import { Component, UIDialog, UIInput, OnChangeEvent } from "rainbowui-core";
// import { UICascadeSelector } from "rainbowui-cascade-selector";
import PropTypes from 'prop-types';
import { PageContext, ValidatorContext } from "rainbow-desktop-cache";
import { CodeTableService } from "rainbow-desktop-codetable";
import config from "config";
import { Util } from 'rainbow-desktop-tools'
import I18nUtil from "../../../i18n/I18NUtil";
import Component from "../../../basic/Component";
import UIDialog from "../../../dialog/Dialog";
import UIInput from "../../../basic/Input";
import OnChangeEvent from "../../../event/OnChangeEvent";
// import { I18nUtil } from "rainbowui-core";
import './index.css';
export default class AdvCascade extends UIInput {
  constructor(props) {
      super(props);
      this.state = {
          isShow: false,
          // isEnableSeachData: true, // 每次请求从props里取
          secondChild: [], // second selecte children
          thirdChild: [],  // third select option
          fourChild: [],
          fiveChild:[],
          firstIndex: 0,
          secondIndex: 0,
          thirdIndex: 0,
          fourIndex: 0,
          fiveIndex: 0,
          firstKey: '',
          secondKey: '',
          thirdKey: '',
          firstValue: '',
          secondValue:'',
          thirdValue: '',
          fourKey: '',
          fourValue: '',
          fiveKey: '',
          fiveValue: '',
          selectKeyResult: '',
          selectValResult: '',
          caseResult: {allKey:'', allVal:''},
          searchOptionsList: [],
          inputValue: '',  // 输入框绑定值
          isShowSearchListContent: false,  // 是否展示搜索列表
      }
      this.res = [];
  }
  componentDidMount() {
    let _self = this;
    $('body').bind('click',function(event){
      let evt = event.srcElement ? event.srcElement : event.target; 
      if (($(evt) && $(evt).attr('class') && $(evt).attr('class').indexOf('el-') == -1) || !$(evt).attr('class')) {
        _self.setState({
          isShow: false
        })
      }
    })
    if (this.props.io != "out") {
      this.initValidator();
    }

    $(document).click((event) => {
      let targetParent = $("#" + this.componentId + "-input")
      if (targetParent.has(event.target).length === 0) {
        this.setState({
          isShowSearchListContent: false
        })
      }
    })

    // 进行options的处理
    this.handleDataSource();

    this.getModelVal();
    super.showLabelTooltip();
    this.showValueTooltip();
  }

  // 处理dataSource
  handleDataSource(nextProps) {
    let dataSource = null;
    this.res = [];
    if (nextProps) { // 异步传递pros数据
      dataSource = nextProps.dataSource;
    } else {
      dataSource = this.props.dataSource;
    }
    for (let i = 0; i < dataSource.length; i++) {
      let key = dataSource[i][this.labelKey()];
      let value = dataSource[i][this.valueKey()];
      this.res.push({label:key, value: value});
      if (dataSource[i][this.childrenKey()] && dataSource[i][this.childrenKey()].length > 0) {
          this.flatOptionObject(dataSource[i][this.childrenKey()], key, value);
      }
    }
  }

  // 扁平化option对象为搜索功能做准备
  flatOptionObject = (dataList, prefixKey, prefixValue) => {
    let tempKey, tempValue = '';
    for (let i = 0; i < dataList.length; i++) {
        if (dataList[i].children && dataList[i].children.length > 0) {
            tempKey = prefixKey + "/" + dataList[i][this.labelKey()];
            tempValue = prefixValue + "/" + dataList[i][this.valueKey()];
            this.res.push({label: tempKey, value: tempValue});
            this.flatOptionObject(dataList[i][this.childrenKey()], tempKey, tempValue);
        } else {
            this.res.push({label: prefixKey + "/" + dataList[i][this.labelKey()],value: prefixValue + "/" + dataList[i][this.valueKey()]});
        }
    }
  }

  componentDidUpdate(){
    this.showValueTooltip();
  }
  clearComponentValidata(selectObj) {
    let formGroupObj = selectObj.closest(".form-group");
    if (formGroupObj.hasClass('has-feedback') || formGroupObj.hasClass('has-error')) {
        formGroupObj.removeClass('has-feedback');
        formGroupObj.removeClass('has-error');
        let removeIcon = selectObj.next();
        let errorMessage = selectObj.parent().next();
        if (errorMessage.length == 0) {
            errorMessage = selectObj.parent();
        }
        if (removeIcon && removeIcon.hasClass("form-control-feedback")) {
            removeIcon.remove();
        }
        if (errorMessage && errorMessage.hasClass("help-block")) {
            errorMessage.remove();
        }
        if ($(errorMessage[0]).children('small')) {
            $(errorMessage[0]).children('small').remove()
        }
        if (formGroupObj.find('i.form-control-feedback')) {
          formGroupObj.find('i.form-control-feedback').remove()
        }
        if (formGroupObj.find('small')) {
          formGroupObj.find('small').remove()
        }
    }
  }
  clearValidationInfo() {
    const self = this;
    const selectObj = $("#" + self.componentId);
    const selectObjInput = $("#" + self.componentId + '-input');
    this.clearComponentValidata(selectObj);
    this.clearComponentValidata(selectObjInput);
    ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);  
    // if ($("form").data("bootstrapValidator")) {
    //   $("form").data('bootstrapValidator').enableFieldValidators($("#" + self.componentId).attr('name'),false);
    //   // $("form").data('bootstrapValidator').enableFieldValidators( $("#" + self.componentId + '-input').attr('name'),false);
    // }
}

  componentWillMount() {
    // this.componentId = this.uuid()
    super.componentWillMount();
    if (this.props.dataSource) {
      // this.props.handleEvent(new OnChangeEvent(this, null, null, this.state.caseResult, this.onEvent.newValue));
      this.onEvent = { newValue: this.state.caseResult, oldValue: null };
      // this.clearValidationInfo();
    }
  }

  componentWillReceiveProps(nextProps) {
    this.getModelVal();
    if (nextProps.dataSource != this.props.dataSource) {
      this.handleDataSource(nextProps);
    }
  }
// ------------------------------------------------------------------
  // 将值反向绑定到select级联列表中显示 递归 子函数
  reverseLoad(res, list, i, data) {
    if (!list[i]){return;}
    data.forEach((item, index) => {
      let obj = {};
      if (item[this.labelKey()] === list[i]) {
        obj.index = index;
        if (item[this.childrenKey()] && item[this.childrenKey()].length > 0) {
          obj.children = item[this.childrenKey()];
          i = i+1;
          this.reverseLoad(res, list, i, item[this.childrenKey()]);
        } else {
          obj.children = null;
        }
        res.push(obj);
      }
    })
  }

  // 反向加载的主函数
  reverseLoadMainFunction(dataSource, list) {
    let objList = [];
    this.reverseLoad(objList, list, 0, dataSource);
    let preList = ['first', 'second', 'third', 'four', 'five'];
    // 先将数据清空
    preList.forEach((item, index, arr) => {
      this.setState({
        [item + 'Index']: '',
        [arr[index + 1] + 'Child']: []
      })
    });
    let i = 0;
    while(objList.length > 0) {
      let obj = objList.pop();
      if (objList.length > 0) {
        this.setState({
          [preList[i] + 'Index']: obj.index,
          [preList[i+1] + 'Child']: obj.children
        });
      } else {
        this.setState({
          [preList[i] + 'Index']: obj.index,
          [preList[i+1] + 'Child']: []
        });
      }
      
      i++;
    }        
  }
//  --------------------------------------------------------------------------
  getModelVal() {
    if (this.props.model && this.props.property) {
      let propsVal = this.props.model[this.props.property]; // 获取值，即value
      let propsKey = null;
      // 获取key,即label
      for (let i = 0 ; i < this.res.length; i++) {
        if (this.res[i].value === propsVal) {
          propsKey = this.res[i].label;
        }
      }
      // 绑定值
      this.setState({
        selectKeyResult: propsKey? propsKey: '',
        inputValue: propsKey? propsKey: '值错误',
        selectValResult: propsVal
      });
      // 将label反向绑定到select级联列表中
      if (propsKey) {
        let {dataSource} = this.props;
        let list = propsKey.split('/');
        this.reverseLoadMainFunction(dataSource, list);
      }
    }
  }

  labelKey  ()  {
      return this.props.dataSource.label || 'label'
  }
  valueKey  ()  {
      return this.props.dataSource.value || 'value'
  }
  childrenKey ()  {
      return this.props.dataSource.children || 'children'
  }
  changeStatus(event) {
    if (!this.getDisabled()) {
      this.reverseLoadMainFunction(this.props.dataSource, this.state.selectKeyResult.split('/'));
      this.setState({
        isShow: true
      });
      if (event.target.value) {
        this.setState({
          isShowDelBtn: true
        });
      }
    }
  }
  // mark
  buildSecondChildren  (index, key, val, item)  {
    this.setState({
      secondChild: item,
      firstIndex: index,
      thirdChild: [],
      fourChild: [],
      fiveChild: []
    })
  }
  buildThridChildren  (index, key, val, item)  {
    this.setState({
      thirdChild: item,
      secondIndex: index,
      fourChild: [],
      fiveChild: []
    })
  }
  buildFourChildren  (index, key, val, item) {
      this.setState({
        fourChild: item,
        thirdIndex: index,
        fiveChild: []
      })
  }
  buildFiveChildren  (index, key, val, item) {
      this.setState({
          fiveChild: item,
          fourIndex: index
      })
  }

  totalResult  (index, dynamicAttr, target)  {
    this.setState({
      // selectKeyResult: this.state.firstKey + this.state.secondKey + this.state.thirdKey + this.state.fourKey + item[this.labelKey()],
      // selectValResult: this.state.firstValue + this.state.secondValue + this.state.thirdValue + this.state.fiveValue + item[this.valueKey()],
      // caseResult: {allKey: this.state.firstKey + this.state.secondKey + this.state.thirdKey + this.state.fourKey + item[this.labelKey()], allVal: this.state.firstValue + this.state.secondValue + this.state.thirdValue + this.state.fiveValue + item[this.valueKey()]},
      selectKeyResult: target.label,
      selectValResult: target.value,
      caseResult: {allKey: target.label, allVal: target.value},
      isShow: false,
      inputValue: target.label,
      [dynamicAttr]: index,
    },() => {
      this.onEvent = {newValue: this.state.caseResult, oldValue: this.onEvent.newValue};
      if (!this.props.isEnableSeachData && this.props.handleEvent) {
        this.props.handleEvent(new OnChangeEvent(this, null, null, this.state.caseResult, this.onEvent.oldValue));
      }
    });
    if (this.props.model && this.props.property) {
      this.props.model[this.props.property] = target.value;
    }
  }
  getCaseResult  ()  {
    // this.onEvent = { newValue: this.state.caseResult, oldValue: this.onEvent.newValue };
    this.clearValidationInfo();
    return this.state.caseResult;
  }
  // mark
  submitSelect(index, dynamicAttr, key, val) {
    let attrIndex = 0;
    switch(dynamicAttr) {
      case 'firstIndex': attrIndex = 0;break;
      case 'secondIndex': attrIndex = 1;break;
      case 'thirdIndex': attrIndex = 2;break;
      case 'fourIndex': attrIndex = 3;break;
      case 'fiveIndex': attrIndex = 4; break;
    }
    let target = this.res.find((item) => {
      return item[this.labelKey()].split('/')[attrIndex] === key &&
        item[this.valueKey()].split('/')[attrIndex] === val;
    });
    this.totalResult(index, dynamicAttr, target);
  }
  // submitSelect  (index, dynamicAttr, key, val)  {
  //   if (dynamicAttr === 'firstIndex') {
  //     this.setState({
  //       firstKey: key,
  //       firstValue: val,
  //       secondChild: [],
  //       thirdChild: [],
  //       fourChild: [],
  //       fiveChild: [],
  //       secondKey: '',
  //       secondValue: '',
  //       thirdKey: '',
  //       thirdValue: '',
  //       fourKey: '',
  //       fourValue: '',
  //       fiveKey: '',
  //       fiveValue: ''
  //     }, () => {
  //       this.totalResult(index, dynamicAttr, null);
  //     })
  //   }else if (dynamicAttr === 'secondIndex') {
  //     let {dataSource} = this.props;
  //     let { firstIndex } = this.state;
  //     let firstKey = dataSource[firstIndex][this.labelKey()];
  //     let firstValue = dataSource[firstIndex][this.valueKey()];
  //     this.setState({
  //       firstKey,
  //       firstValue,
  //       secondKey: key,
  //       secondValue: val,
  //       thirdChild: [],
  //       fourChild: [],
  //       fiveChild: [],
  //       thirdKey: '',
  //       thirdValue: '',
  //       fourKey: '',
  //       fourValue: '',
  //       fiveKey: '',
  //       fiveValue: ''
  //       }, () => {
  //         this.totalResult(index, dynamicAttr, null)
  //     })
  //   } else if(dynamicAttr === 'thirdIndex') {
  //     let {firstIndex, secondIndex} = this.state;
  //     this.setState({
  //       thirdKey: key,
  //       thirdValue: val,
  //       fourChild: [],
  //       fiveChild: [],
  //       fourKey: '',
  //       fourValue: '',
  //       fiveKey: '',
  //       fiveValue: ''
  //     }, () => {
  //         this.totalResult(index, dynamicAttr, null)
  //     })
  //   } else if(dynamicAttr === 'fourIndex') {
  //     this.setState({
  //       fourKey: key,
  //       fourValue: val,
  //       fiveChild: [],
  //       fiveKey: '',
  //       fiveValue: ''
  //     }, () => {
  //       this.totalResult(index, dynamicAttr, null)
  //     })
  //   } else {
  //     this.setState({
  //       fiveKey: key,
  //       fiveValue: val
  //     }, () => {
  //       this.totalResult(index, dynamicAttr, null)
  //     })
  //   }
  //       // if (!this.props.isEnableSeachData && this.props.handleEvent) {
  //       //   this.props.handleEvent()
  //       // }
  // }
  
  buildSecondDom  (item) {
    return (
      item && item.length > 0 ?
      <ul className="el-cascader-menu">
      {
        item.map((childItem ,index) => {
          return (
            <div>
              {
                !this.props.isEnableSeachData ?
                childItem[this.childrenKey()] && childItem[this.childrenKey()].length > 0 ? 
                <li key={index} className={this.state.secondIndex === index ? "el-cascader-menu__item el-cascader-menu__item--extensible is-active" : "el-cascader-menu__item el-cascader-menu__item--extensible"} onMouseOver={this.buildThridChildren.bind(this, index, childItem[this.labelKey()], childItem[this.valueKey()], childItem[this.childrenKey()])} onClick={this.submitSelect.bind(this, index, 'secondIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
                :
                <li key={index} className={this.state.secondIndex === index ? "el-cascader-menu__item is-active" : "el-cascader-menu__item"} onClick={this.submitSelect.bind(this, index, 'secondIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
                :
                childItem[this.childrenKey()] && childItem[this.childrenKey()] > 0 ?
                <li key={index} className={this.state.secondIndex === index ? "el-cascader-menu__item el-cascader-menu__item--extensible is-active" : "el-cascader-menu__item el-cascader-menu__item--extensible"} onMouseOver={this.buildThirdArr.bind(this, index, childItem[this.labelKey()], childItem[this.valueKey()])} onClick={this.submitSelect.bind(this, index, 'secondIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
                :
                <li key={index} className={this.state.secondIndex === index ? "el-cascader-menu__item is-active" : "el-cascader-menu__item"} onClick={this.submitSelect.bind(this, index, 'secondIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
              }
            </div>
          )
        })
      }
    </ul> :''
    )
  }
  buildThidDom  (item)  {
      return (
        item && item.length > 0 ?
        <ul className="el-cascader-menu">
        {
          item.map((childItem ,index) => {
            return (
              <div>
                {
                  !this.props.isEnableSeachData ?
                  childItem[this.childrenKey()] && childItem[this.childrenKey()].length > 0 ? 
                  <li key={index} className={this.state.thirdIndex === index ? "el-cascader-menu__item el-cascader-menu__item--extensible is-active" : "el-cascader-menu__item el-cascader-menu__item--extensible"} onMouseOver={this.buildFourChildren.bind(this, index, childItem[this.labelKey()], childItem[this.valueKey()], childItem[this.childrenKey()])} onClick={this.submitSelect.bind(this, index, 'thirdIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
                  :
                  <li key={index} className={this.state.thirdIndex === index ? "el-cascader-menu__item is-active" : "el-cascader-menu__item"} onClick={this.submitSelect.bind(this, index, 'thirdIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
                  :
                  childItem[this.childrenKey()] && childItem[this.childrenKey()] > 0 ? 
                  <li key={index} className={this.state.thirdIndex === index ? "el-cascader-menu__item el-cascader-menu__item--extensible is-active" : "el-cascader-menu__item el-cascader-menu__item--extensible"} onMouseOver={this.buildFourArr.bind(this, index, childItem[this.labelKey()], childItem[this.valueKey()])} onClick={this.submitSelect.bind(this, index, 'thirdIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
                  :
                  <li key={index} className={this.state.thirdIndex === index ? "el-cascader-menu__item is-active" : "el-cascader-menu__item"} onClick={this.submitSelect.bind(this, index, 'thirdIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
                }
              </div>
            )
          })
        }
      </ul> :''
      )
  }
  buildFourDom  (item) {
    return (
      item && item.length > 0 ?
      <ul className="el-cascader-menu">
      {
        item.map((childItem ,index) => {
          return (
            <div>
              {
                !this.props.isEnableSeachData ?
                childItem[this.childrenKey()] && childItem[this.childrenKey()].length > 0 ? 
                <li key={index} className={this.state.fourIndex === index ? "el-cascader-menu__item el-cascader-menu__item--extensible is-active" : "el-cascader-menu__item el-cascader-menu__item--extensible"} onMouseOver={this.buildFiveChildren.bind(this, index, childItem[this.labelKey()], childItem[this.valueKey()], childItem[this.childrenKey()])} onClick={this.submitSelect.bind(this, index, 'fourIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
                :
                <li key={index} className={this.state.fourIndex === index ? "el-cascader-menu__item is-active" : "el-cascader-menu__item"} onClick={this.submitSelect.bind(this, index, 'fourIndex', childItem)}>{childItem[this.labelKey()]}</li>
                :
                childItem[this.childrenKey()] && childItem[this.childrenKey()] > 0 ? 
                <li key={index} className={this.state.fourIndex === index ? "el-cascader-menu__item el-cascader-menu__item--extensible is-active" : "el-cascader-menu__item el-cascader-menu__item--extensible"} onMouseOver={this.buildFiveArr.bind(this, index, childItem[this.labelKey()], childItem[this.valueKey()])} onClick={this.submitSelect.bind(this, index, 'fourIndex', childItem)}>{childItem[this.labelKey()]}</li>
                :
                <li key={index} className={this.state.fourIndex === index ? "el-cascader-menu__item is-active" : "el-cascader-menu__item"} onClick={this.submitSelect.bind(this, index, 'fourIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
              }
            </div>
          )
        })
      }
    </ul> :''
    )
  }

  buildSecondArr  (index, key, val)  {
    this.setState({
      secondChild: this.props.getSecondData(),
      firstIndex: index,
      thirdChild: [],
      fourChild: [],
      fiveChild: [],
      firstKey: key,
      firstValue: val,
      secondKey: '',
      secondValue: '',
      thirdKey: '',
      thirdValue: '',
      fourKey: '',
      fourValue: '',
      fiveKey: '',
      fiveValue: ''
    }, () => {
        console.log('test:',this.state.secondKey);
    })
  }
  buildThirdArr  (index, key, val)  {
    this.setState({
      thirdChild: this.props.getThirdData(),
      fourChild: [],
      fiveChild: [],
      secondIndex: index,
      secondKey: key,
      secondValue: val,
      thirdKey: '',
      thirdValue: '',
      fourKey: '',
      fourValue: '',
      fiveKey: '',
      fiveValue: ''
    }, () => {
        console.log(this.state.thirdKey)
    })
  }
  buildFourArr  (index, key, val)  {
    this.setState({
      fourChild: this.props.getFourData(),
      thirdIndex: index,
      fiveChild: [],
      thirdKey: key,
      thirdValue: val,
      fourKey: '',
    fourValue: '',
    fiveKey: '',
    fiveValue: ''
    }, () => {
        console.log(this.state.fourKey)
    })
  }
  buildFiveArr  (index, key, val)  {
    this.setState({
      fiveChild: this.props.getFiveData(),
      fourIndex: index,
      fourKey: key,
      fourValue: val,
      fiveKey: '',
      fiveValue: ''
  })
  }
  clearValue() {
    this.setState({ selectKeyResult: '', selectValResult: '', inputValue: ''});
    let emptyObj = {allKey:'', allVal:''};
    this.setState({caseResult: emptyObj},
      () => {
        this.onEvent = {newValue: this.state.caseResult, oldValue: this.onEvent.newValue};
        if (!this.props.isEnableSeachData && this.props.handleEvent) {
          this.props.handleEvent(new OnChangeEvent(this, null, null, this.state.caseResult, this.onEvent.oldValue));
        }
      }  
    );
    if (Util.parseBool(this.props.searchabled) && this.state.isShowSearchListContent) {
      this.inputSearch({target:{value:''}});
    }
    
  }

  // ------------------输入框搜索事件------------------
  inputSearch(event) {
    let val = event.target.value;
    let result = this.res.filter((item) => {
      if (item.label.indexOf(val) !== -1) {
        return item;
      }
    });
    this.setState({
      searchOptionsList: result,
      inputValue: val,
      isShowSearchListContent: true,
      isShow: false
    });
  }
  // -----------------搜索列表点击事件-----------------
  searchListLiClick(item) {
    this.setState({
      selectKeyResult: item.label,
      selectValResult: item.value,
      caseResult: {allKey: item.label, allVal: item.value},
      inputValue: item.label,
      // searchOptionsList: [],
      isShowSearchListContent: false
    },
      () => {
        this.reverseLoadMainFunction(this.props.dataSource, item.label.split('/'));
        this.onEvent = {newValue: this.state.caseResult, oldValue: this.onEvent.newValue};
        if (!this.props.isEnableSeachData && this.props.handleEvent) {
          this.props.handleEvent(new OnChangeEvent(this, null, null, this.state.caseResult, this.onEvent.oldValue));
        }
      }
    );
    if (this.props.model && this.props.property) {
      this.props.model[this.props.property] = item.value;
    }
    
  }


  renderInput() {
      const inputProps = {
        ref: ref => this.input = ref
      }
      const { ...divProps } = this.props.inputProps;
      if (this.props.io == 'in') {
          if (!this.props.validator) {
            if (!Util.parseBool(this.props.required)) {
                this.clearValidationInfo()
                ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);   
                ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId + '-input');                 
            } else {
                this.initValidator();
            }
        }
      }

      let language = I18nUtil.getSystemI18N();
      let placeholder = language == "zh_CN" ? '请选择': "Please select";
      return (
          <div className="input-group adv-cascader"  {...divProps} ref={ref => this.input = ref} id={this.componentId+ '-input'} name={this.componentId+ '-input'} required={this.props.required} caseResult={this.state.selectKeyResult}>
          <span className="is-opened" style={{width: '100%'}} >
              <div className="" onClick={this.changeStatus.bind(this)}>
            <i className={this.state.isShow ? "rainbow SingArrowDown is-reverse" :  "rainbow SingArrowDown"}
              style={{paddingTop: '4px'}}></i>
              {
                Util.parseBool(this.props.searchabled) ?
                  <input type="text" id={this.componentId} placeholder={placeholder} style={{border: 'none', width: '100%', outline: 'none', paddingLeft: '10px'}}
                    autocomplete = "off" 
                    onChange = {() => {this.inputSearch(event)}} value={this.state.inputValue} onClick={this.changeStatus.bind(this)}/>
                :
                  <input type="text" id={this.componentId} placeholder={placeholder} style={{border: 'none', width: '100%', outline: 'none', paddingLeft: '10px'}} 
                    autocomplete="off" readonly="readonly" 
                    value={this.state.inputValue} onClick={this.changeStatus.bind(this)}/>
              }
              {
                this.getDisabled() != "disabled" && (this.state.isShow || this.state.isShowSearchListContent) && this.state.inputValue?
                <span className="el-deleclose rainbow Close" onClick={this.clearValue.bind(this)}></span>
                : null
              }
            </div>
            
            {
              // this.state.selectKeyResult !== '' ?
              // <span className="el-cascader__label" onClick={this.changeStatus.bind(this)}>{this.state.selectKeyResult}</span>
              // :
              // <span className="el-cascader__label" style={{display:'none'}}></span>
              
            }  
            {/* </span> */}
            {
                this.state.isShowSearchListContent? (
                  <div className = "searchListContent" id={this.componentId+"-search-list-content"} style={{}}>
                    <ul>
                      {
                        this.state.searchOptionsList.map( (item) => {
                          return (
                            <li onClick={() => { this.searchListLiClick(item)}}>{item.label}</li>
                          );
                        })
                      }
                    </ul>
                  </div>
                ): 
                (
                  this.state.isShow ?
                  <div className={this.state.isShow ? "el-cascader-menus" : "el-cascader-menus el-zoom-in-top-leave-active el-zoom-in-top-leave-to"} id={this.componentId+"-el-cascader-menus"} style={{}}>
                    <ul className="el-cascader-menu">
                    {
                      !this.props.isEnableSeachData ?
                      this.props.dataSource.map((item, index) => {
                        return (
                          <div>
                            {
                              item[this.childrenKey()] && item[this.childrenKey()].length > 0 ? 
                              <li key={index} className={this.state.firstIndex === index ? "el-cascader-menu__item el-cascader-menu__item--extensible is-active" : "el-cascader-menu__item el-cascader-menu__item--extensible"} onMouseOver={this.buildSecondChildren.bind(this,  index, item[this.labelKey()], item[this.valueKey()], item[this.childrenKey()])} onClick={this.submitSelect.bind(this, index, 'firstIndex', item[this.labelKey()], item[this.valueKey()])}>{item[this.labelKey()]}</li>
                              :
                              <li key={index} className={this.state.firstIndex === index ? "el-cascader-menu__item is-active" : "el-cascader-menu__item"} onClick={this.submitSelect.bind(this, index, 'firstIndex', item[this.labelKey()], item[this.valueKey()])}>{item[this.labelKey()]}</li>
                            }
                          </div>
                        )
                      })
                      :
                      this.props.getFirstData.map((item, index) => {
                        return (
                          <div>
                            {
                              item[this.childrenKey()] && item[this.childrenKey()] > 0 ?
                              <li key={index} className={this.state.firstIndex === index ? "el-cascader-menu__item el-cascader-menu__item--extensible is-active" : "el-cascader-menu__item el-cascader-menu__item--extensible"} onMouseOver={this.buildSecondArr.bind(this,  index, item[this.labelKey()], item[this.valueKey()])} onClick={this.submitSelect.bind(this, index, 'firstIndex', item[this.labelKey()], item[this.valueKey()])}>{item[this.labelKey()]}</li>
                              :
                              <li key={index} className={this.state.firstIndex === index ? "el-cascader-menu__item is-active" : "el-cascader-menu__item"} onClick={this.submitSelect.bind(this, index, 'firstIndex', item[this.labelKey()], item[this.valueKey()])}>{item[this.labelKey()]}</li>
                            }
                          </div>
                        )
                      })
                    }
                    </ul>
                    { this.buildSecondDom(this.state.secondChild) }
                    { this.buildThidDom(this.state.thirdChild) }
                    { this.buildFourDom(this.state.fourChild) }
                    {
                      this.state.fiveChild && this.state.fiveChild.length > 0 ? 
                      <ul className="el-cascader-menu">
                      {
                        this.state.fiveChild.map((childItem, index) => {
                          return (
                            <div>
                              <li key={index} className={this.state.fiveIndex === index ? "el-cascader-menu__item is-active" : "el-cascader-menu__item"} onClick={this.submitSelect.bind(this, index, 'fiveIndex', childItem[this.labelKey()], childItem[this.valueKey()])}>{childItem[this.labelKey()]}</li>
                            </div>
                          )
                        })
                      }
                      </ul>
                      : ''
                    }
                  </div>
                  : null
                )
              }
          </span>     
          
      </div>
      )
  }
  showValueTooltip() {
    let inputDiv = $("#for-input-" + this.componentId);
    let text = $("#for-input-" + this.componentId).find('.el-cascader__label').text();
    if (text && text.length > 0 && Util.parseBool(this.props.showValueTooltip)) {
      inputDiv.attr("data-original-title", text);
    }
  }
}

AdvCascade.propTypes = $.extend({}, UIInput.propTypes, {
  showValueToolTip: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
});
AdvCascade.defaultProps = $.extend({}, UIInput.defaultProps, {
  showValueToolTip: false,
});