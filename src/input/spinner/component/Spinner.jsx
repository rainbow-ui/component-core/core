// import {Param,OnChangeEvent,UIInput} from "rainbowui-core";
import Param from '../../../basic/Param';
import OnChangeEvent from '../../../event/OnChangeEvent';
import UIInput from '../../../basic/Input';
import {Util} from "rainbow-desktop-tools"
import PropTypes from 'prop-types';

import "../plugin/bootstrap-touchspin/src/jquery.bootstrap-touchspin.css";
import "../plugin/bootstrap-touchspin/src/jquery.bootstrap-touchspin.js";



export default class Spinner extends UIInput {

	renderInput(){
		let inputRef = this.getInputRefProperty();
		return (
			<div className="textani">
				<input id={this.componentId} name={this.getName()} type="text" ref={inputRef} data-auto-test={this.getNameForTest()} data-bts-decimal="0" data-bts-step-interval="100"  data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-default" data-bts-button-up-class="btn btn-default"/>      
            </div>
		);
	}

// add animate effort Start for spinner underline. also add above'<div className="textani"> container ----by York
	componentDidMount() {
		super.componentDidMount();
		
		if (Util.parseBool(this.props.disabledUp)) {
			$("#" + this.componentId).nextAll().filter('.input-group-btn-vertical').children().eq(0).attr('disabled', 'disabled');
		}
		if (Util.parseBool(this.props.disabledDown)) {
			$("#" + this.componentId).nextAll().filter('.input-group-btn-vertical').children().eq(1).attr('disabled', 'disabled');
		}

		$("#" + this.componentId).after("<span class='focus-border'></span>");
	}
	// add animate effort End

	initComponent(){
		let _self = this;

		let initValue = _self.props.min == undefined ? "" : _self.props.min
		$("#" + this.componentId).TouchSpin({
			//initval: initValue,
			step: parseInt(_self.props.step),
			verticalbuttons: true,
			min: parseInt(_self.props.min),
			max: parseInt(_self.props.max),
			verticalupclass: "glyphicon glyphicon-chevron-up",
			verticaldownclass: "glyphicon glyphicon-chevron-down",

			//decimals: 2,
			//boostat: 5,
			//maxboostedstep: 10,
			//postfix: '%'
			//prefix: '$'
    	});

		if(this.props.io != "out"){
			// handle onchange event
			$("#" + this.componentId).unbind("change");
			$("#" + this.componentId).bind("change", (event) => {
				let value = _self.getInputValue(event);
				if (_self.formatDigit) {
					value = _self.formatDigit(value);
				}
				if (_self.props.onChange) {
					_self.props.onChange(new OnChangeEvent(_self, event, Param.getParameter(_self), value, _self.onEvent.newValue));
				}
				_self.onEvent = {newValue: value, oldValue: _self.onEvent.newValue};
				_self.setComponentValue(event);
                _self.onChangeColorByEndorsement();
			});
		}
	}

};


/**
 * Spinner component prop types
 */
Spinner.propTypes = $.extend({}, UIInput.propTypes, {
    disabledUp:PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
	disabledDown:PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
	min: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
	max: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
	step: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
});

/**
 * Get Spinner component default props
 */
Spinner.defaultProps = $.extend({}, UIInput.defaultProps, {
	//min: 0,
	//max: 100,
	step: 1
});
