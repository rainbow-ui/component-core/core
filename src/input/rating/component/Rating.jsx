// import { UIInput, Param, OnChangeEvent } from "rainbowui-core";
import Param from "../../../basic/Param";
import UIInput from "../../../basic/Input";
import OnChangeEvent from "../../../event/OnChangeEvent";
import { Util } from "rainbow-desktop-tools";
import "../js/star-rating";
import PropTypes from 'prop-types';


export default class Rating extends UIInput {

    constructor(props) {
        super(props)
    }

    renderInput() {
        let inputRef = this.getInputRefProperty();

        return (
            <input id={this.componentId} name={this.getName()} type="number"
                className="rb-rating" data-rtl="0" ref={inputRef} data-auto-test={this.getNameForTest()}/>
        );
    }

    componentDidUpdate() {
        if (this.props.io != "out") {
            $("#" + this.componentId).rating('destroy');
            this.initComponent();
        }
    }
    initComponent() {
        let _self = this;
        // if (!this.props.model[this.props.property] || !$("#" + this.componentId)[0]) {
        //     $("#" + this.componentId).rating('destroy');
        //     $("#" + this.componentId).hide();
        //     // console.log('need destory',  this.componentId, this.props.model[this.props.property]);
        //     $('#' + this.componentId).val(this.props.model[this.props.property])

        //     return;
        // }
        $('#' + this.componentId).val(this.props.model[this.props.property])
        // console.log('rating...', this.componentId, this.props.model[this.props.property], $("#" + this.componentId).val());
        $("#" + this.componentId).show();
        $("#" + this.componentId).unbind("change");
        $("#" + this.componentId).rating({
            min: this.props.min,
            max: this.props.max,
            step: this.props.step,
            size: this.props.size,
            stars: this.props.stars,
            showClear: Util.parseBool(this.props.showClear),
            showCaption: Util.parseBool(this.props.showCaption),

            // hover attribute
            hoverEnabled: true,
            hoverChangeCaption: true,
            hoverChangeStars: true,
            hoverOnClear: true,

            // unicode character -> Stars��&#xf005; Thumbs Up: &#xf164; Coffee: &#xf0f4;
            symbol: String.fromCharCode("0x" + this.props.symbol),
            glyphicon: false,
            ratingClass: "rating-fa",
            containerClass: "text-left",
            defaultCaption: "{rating} hearts",

            starCaptions: function (val) {
                if (val < 3) {
                    return val;
                } else {
                    return "high";
                }
            },

            starCaptionClasses: function (val) {
                if (val < 3) {
                    return "label label-danger";
                } else {
                    return "label label-success";
                }
            }
        })

            // Rating clear event
            .on("rating.clear", function (event) {
                console.log("Your rating is reset");
                _self.setComponentValue(event);

                if (_self.props.onClear) {
                    _self.props.onClear();
                }
            })

            // Rating change event
            .on("rating.change", function (event, value, caption) {
                console.log("You rated: " + value + " = " + $(caption).text());
                _self.setComponentValue(event);

                if (_self.props.onChange) {
                    _self.props.onChange(new OnChangeEvent(_self, event, Param.getParameter(_self)));
                }
            })

            // Rating reset event
            .on("rating.reset", function (event) {
                console.log("rating.reset");

                if (_self.props.onReset) {
                    _self.props.onReset();
                }
            })

            // Rating change event
            .on("rating.hover", function (event, value, caption, target) {
                //console.log("rating.hover");

                if (_self.props.onHover) {
                    _self.props.onHover();
                }
            })

            .on('rating.hoverleave', function (event, target) {
                //console.log("==rating.hoverleave==");

                if (_self.props.onHoverLeave) {
                    _self.props.onHoverLeave();
                }
            })

            .on("rating.refresh", function (event) {
                //console.log("rating.refresh");

                if (_self.props.onRefresh) {
                    _self.props.onRefresh();
                }
            });
    }

};


/**
 * Rating component prop types
 */
Rating.propTypes = $.extend({}, UIInput.propTypes, {
    min: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    max: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    step: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    stars: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    size: PropTypes.oneOf(["xl", "lg", "md", "sm", "xs"]),
    symbol: PropTypes.string,
    showClear: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    showCaption: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),

    // not support onFocus & onBlur event
    onChange: PropTypes.func,
    onClear: PropTypes.func,
    onReset: PropTypes.func,
    onHover: PropTypes.func,
    onHoverLeave: PropTypes.func,
    onRefresh: PropTypes.func,
});

/**
 * Get Rating component default props
 */
Rating.defaultProps = $.extend({}, UIInput.defaultProps, {
    symbol: "e006", // fa-star
    showClear: true,
    showCaption: true,
    min: 0,
    max: 5,
    step: 1,
    stars: 5,
    size: "xs",
});

