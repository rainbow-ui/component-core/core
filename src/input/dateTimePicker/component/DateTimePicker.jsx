import Param from '../../../basic/Param';
import UIInput from '../../../basic/Input';
import DateTimePickerConvertor from '../../../convertor/DateTimePickerConvertor';
import UIMessageHelper from '../../../dialog/MessageHelper';
import I18nUtil from '../../../i18n/I18NUtil';
import OnBlurEvent from '../../../event/OnBlurEvent';
import OnClickEvent from '../../../event/OnClickEvent';
import OnChangeEvent from '../../../event/OnChangeEvent';
import Convertor from '../../../convertor/Convertor';
import ConvertorConstant from '../../../convertor/ConvertorConstant';
import config from "config";
import PropTypes from 'prop-types';
import { StringUtil, Util } from 'rainbow-desktop-tools';
import { ValidatorContext, SessionContext } from 'rainbow-desktop-cache';
import r18n from '../../../i18n/reactjs-tag.i18n.jsx';


export default class DateTimePicker extends UIInput {

    constructor(props) {
        super(props);
    }

    renderInput() {
        
        if (Util.parseBool(this.props.isShow)) {
            if (!this.props.validator) {
                if (!Util.parseBool(this.props.required)) {
                    ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
                    this.clearChangeValidationInfo(this.props)
                    // this.props.required = false;           
                } else {
                    // this.initValidator();
                }
            }
            const _tempClasss = this.getDisabled() ? "input-group jedate jedate-disabled" : "input-group jedate"
            // const _tempClasss = Util.parseBool(this.props.enabled) ? "input-group jedate" : "input-group jedate jedate-disabled"
            
            return (
                <div className={_tempClasss}>
                    <div className="jeinpbox">
                        {
                            this.props.isCanEdit || this.props.isCanEdit == undefined ?
                                <input type="text" autocomplete="off" className="form-control jeinput " title={this.getI18n(this.props.title)} style={this.props.style}
                                    id={this.componentId} name={this.getName()} placeholder={this.props.placeholder ? this.props.placeholder : ""} data-auto-test={this.getNameForTest()} />
                                :
                                <input type="text" autocomplete="off" readOnly="readOnly" className="form-control jeinput " title={this.getI18n(this.props.title)} style={this.props.style}
                                    id={this.componentId} name={this.getName()} placeholder={this.props.placeholder ? this.props.placeholder : ""} data-auto-test={this.getNameForTest()} />
                        }
                    </div>
                    <span className={this.getDisabled() ? "input-group-addon pickerposition disabled" : "input-group-addon pickerposition"}>
                        <span class="rainbow Calendar"
                            onClick={this.onClickIcon.bind(this)} />
                    </span>
                </div>
            );
        } else {
            return (
                <div id={this.componentId}></div>
            )
        }
    }

    onClickIcon(event) {
        // event.preventDefault();
        if (!this.getDisabled()) {
            const inputObj = $("#" + this.componentId);
            inputObj.focus();
            inputObj.click();
        }
    }


    deleteInputValue(event) {
        // event.preventDefault();
        const { model, property } = this.props;
        if (model && property) {
            model[property] = null;
        }
        $("#" + this.componentId).val(null);
    }

    setComponentValue(event) {
        let inputValue = this.getInputValue(event);
        if (StringUtil.isEmpty(inputValue)) {
            this.deleteInputValue(event);
        }

        let format = this.props.format ? this.props.format : config.DEFAULT_DATETIME_SUBMIT_FORMATER;
        if (format.indexOf("y") !== -1 || format.indexOf("Y") !== -1) {
            format = this.props.bindFormat ? this.props.bindFormat : format;
        }
        if (format.indexOf("ii") >= 0) {
            format = format.replace('ii', 'mm');
        }

        let tempInputValue = inputValue.replace('HH', '00').replace('ii', '00').replace('ss', '00'); // inputValue.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        // if(Util.parseBool(this.props.showTime)){
        //     const _tempInputValue = tempInputValue.substring(8,14);
        //     format = `${format.substring(0,11)}${_tempInputValue.substring(0,2)}:${_tempInputValue.substring(2,4)}:${_tempInputValue.substring(4,6)}`;
        // }

        if (Util.parseBool(this.props.forceParse24) || Util.parseBool(this.props.alwaysParse24)) {
            tempFormat = tempFormat.substring(0, 8) + '240000';
            format = format.substring(0, 11) + '24:00:00';
        } else {
            const _tempFormat = tempFormat.substring(8, 14);
            if (_tempFormat && 'hhmmss' == _tempFormat.toLowerCase()) {
                format = format.substring(0, 11) + '00:00:00';
            }
        }
        //let convertorValue = dayjs(tempInputValue, tempFormat).format(format);
        // if (tempInputValue.length == tempFormat.length) {
        //     convertorValue = 
        // }
        let returnDate = tempInputValue;
        if (format.indexOf('YYYY') > -1) {
            let split = this.getFormatSplit(this);
            if (this.props.bindFormat && this.props.bindFormat.length > 5) {
                split = this.props.bindFormat.slice(4, 5);
            }
            tempInputValue = inputValue.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
            const yyyy = tempInputValue.substr(0, 4);
            const mm = tempInputValue.substr(4, 2);
            const dd = tempInputValue.substr(6, 2);
            returnDate = `${yyyy}${split}${mm}${split}${dd}`;
            if (Util.parseBool(this.props.showTime)) {
                const hh = tempInputValue.substr(8, 2);
                const ii = tempInputValue.substr(10, 2);
                const ss = tempInputValue.substr(12, 2);
                if (hh.length > 0){
                    if (ii.length > 0){
                        if (ss.length > 0){
                            returnDate = `${yyyy}${split}${mm}${split}${dd} ${hh}:${ii}:${ss}`;
                        } else {
                            returnDate = `${yyyy}${split}${mm}${split}${dd} ${hh}:${ii}`;
                        }
                    } else {
                        returnDate = `${yyyy}${split}${mm}${split}${dd} ${hh}`;
                    }
                }
            }
        }
        let convertorValue = DateTimePickerConvertor.getAsObject(this, returnDate);
        if (!Util.parseBool(convertorValue)) {
            if (event&&event.target && event.target.className && event.target.className != 'clear') {
                UIMessageHelper.error(r18n.DateFormat)
            }
        }
        this.setValue(this.props.value, convertorValue);

    }

    setInputValue(event, _self, dateComponent) {
        _self.setComponentValue(event);
        let value = _self.getInputValue(event);
        if (_self.getDigitValue) {
            value = _self.getDigitValue(value);
        }

        //用于校验设置最大日期和最小日期后，仍然可以通过点击今天时间的问题
        if (!_self.checkMaxDateAndMinDate(this.getComponentValue())) {
            $("#" + this.componentId).val("");
            _self.setComponentValue(event);
            return
        }
        if (value !== "0") {
            $("#" + this.componentId).val(this.getComponentValue());
        }
        if (_self.props.onChange && (event.val != _self.onEvent.newValue)) {
            let valueChangeEvent = new OnChangeEvent(_self, event, Param.getParameter(_self), this.getComponentValue(), _self.onEvent.newValue);
            _self.props.onChange(valueChangeEvent);
            let newInputValue = _self.getInputValue(event);
            if (_self.getDigitValue) {
                newInputValue = _self.getDigitValue(value);
            }
            _self.onEvent = { newValue: newInputValue, oldValue: _self.onEvent.newValue };
        }

        if (Util.parseBool(_self.props.required)) {
            const helpObject = $("#for-input-" + this.componentId).next(".help-block");
            if (value && value != "0") {
                const inputObject = $("#" + this.componentId);
                const errorInputGroupObject = inputObject.closest(".form-group").parent().parent().parent().parent().parent();
                const errorInputObject = inputObject.closest(".form-group");
                if (errorInputObject.hasClass("has-error")) {
                    inputObject.parent().parent().next().remove();
                    errorInputObject.removeClass("has-feedback has-error").addClass("has-success");
                    helpObject.hide();

                }
                if (errorInputGroupObject.hasClass("has-error")) {
                    inputObject.parent().parent().parent().parent().parent().parent().parent().next().remove();
                    inputObject.parent().parent().parent().parent().parent().parent().parent().next().remove();
                    errorInputGroupObject.removeClass("has-feedback has-error").addClass("has-success");
                    helpObject.hide();
                }
                dateComponent.parent().parent().removeClass("input-required");
            } else {
                const inputObject = $("#" + this.componentId);
                const errorInputObject = inputObject.closest(".form-group");
                const errorInputGroupObject = inputObject.closest(".form-group").parent().parent().parent().parent().parent();
                if (!_.isEmpty(helpObject)) {
                    errorInputObject.addClass("has-feedback has-error");
                    errorInputGroupObject.addClass("has-feedback has-error");
                    helpObject.show();
                }
                dateComponent.parent().parent().addClass("input-required");
            }
        }
        const findClass = this.props.findClass ? "." + this.props.findClass : ".input-group";
        dateComponent.closest(findClass).removeClass("focusborder");
    }

    initComponent() {
        if (this.props.isCanEdit !== undefined) {
            if (this.props.io == "out" || !this.props.isCanEdit) {
                return
            }
        } else {
            if (this.props.io == "out") {
                return
            }
        }

        const dateComponent = $("#" + this.componentId);

        let _self = this;
        if (this.props.property && this.props.model != null && !this.props.model[this.props.property] && Util.parseBool(this.props.rebind)) {
            jeDate("#" + this.componentId, {
                // isinitVal:true,
                format: this.getFormat(),
                language: this.getLanguage(),
                // format: this.getFormat(),
                minDate: this.getMinDate() || "1900-01-01 00:00:00",
                maxDate: this.getMaxDate() || "2099-12-31 24:59:59",
                minutes: this.props.minutes ? this.props.minutes : ['00', '10', '20', '30', '40', '50'],
                seconds: this.props.seconds ? this.props.seconds : ['00', '10', '20', '30', '40', '50'],
                isShow: Util.parseBool(this.props.isShow),
                trigger: this.props.trigger,
                onClose: Util.parseBool(this.props.showTime) ? true : false,
                isinitVal: this.props.defaultValue ? true : false,
                initDate: this.props.initDate ? this.props.initDate : this.getDate(),
                showFlat:  this.props.showFlat,
                rebuildall: this.props.rebuild, // 空不处理，0 点击日期时删除第1个，1，删除第二个
                isTime: Util.parseBool(this.props.showTime),
                isClear: Util.parseBool(this.props.showClear),
                isToday: Util.parseBool(this.props.isShowToday) ? true : false,
                festival: false,
                fixed: true,
                marks: this.props.marks ? this.props.marks : this.getMarks(),
                works: this.props.works ? this.props.works : this.getWorks(),
                clearval: true,
                isShowTimeBtn: Util.parseBool(this.props.isShowTimeBtn),
                isShow24Hour: Util.parseBool(this.props.isShow24Hour),
                isNowTime: Util.parseBool(this.props.isNowTime) ? true : false,                
                donefun: function (event) {

                },
                success: function (elem) {

                },
                clearfun: function (elem, val) {
                    const { model, property } = _self.props;
                    if (model && property) {
                        model[property] = null;
                    }
                    _self.props.onClean ? _self.props.onClean(elem, val) : null;
                },
            });
        } else {
            jeDate("#" + this.componentId, {
                // isinitVal:true,
                format: this.getFormat(),
                language: this.getLanguage(),
                // format: this.getFormat(),
                minDate: this.getMinDate() || "1900-01-01 00:00:00",
                maxDate: this.getMaxDate() || "2099-12-31 24:59:59",
                minutes: this.props.minutes ? this.props.minutes : ['00', '10', '20', '30', '40', '50'],
                seconds: this.props.seconds ? this.props.seconds : ['00', '10', '20', '30', '40', '50'],
                isShow: Util.parseBool(this.props.isShow),
                trigger: this.props.trigger,
                onClose: Util.parseBool(this.props.showTime) ? true : false,
                isinitVal: this.props.defaultValue ? true : false,
                initDate: this.props.initDate ? this.props.initDate : this.getDate(),
                isTime: Util.parseBool(this.props.showTime),
                isClear: Util.parseBool(this.props.showClear),
                isShowTimeBtn: Util.parseBool(this.props.isShowTimeBtn),
                isShow24Hour: Util.parseBool(this.props.isShow24Hour),
                showFlat:  this.props.showFlat,
                rebuildall: this.props.rebuild, // 空不处理，0 点击日期时删除第1个，1，删除第二个
                isToday: Util.parseBool(this.props.isShowToday) ? true : false,
                festival: false,
                fixed: true,
                clearval: false,
                marks: this.props.marks ? this.props.marks : this.getMarks(),
                works: this.props.works ? this.props.works : this.getWorks(),
                isYearOffset: Util.parseBool(this.props.isYearOffset) ? true : false,
                yearOffset: this.props.yearOffset ? parseInt(this.props.yearOffset) : 1911,
                isNowTime: Util.parseBool(this.props.isNowTime) ? true : false,
                donefun: function (event) {
                    if (event.date.hh == "24" && Util.parseBool(_self.props.showTime)) {
                        event.date.mm = "00"
                        event.date.ss = "00"
                        const split = _self.getFormatSplit(_self);
                        const value = `${event.date.YYYY}${split}${event.date.MM}${split}${event.date.DD} ${event.date.hh}:${event.date.mm}:${event.date.ss}`
                        event.val = value
                        $(event.elem[0]).val(value)
                    }
                    if (_self.props.onSetVal && _self.props.showFlat) {
                        _self.props.onSetVal(event);
                    } else {
                        _self.setInputValue(event, _self, dateComponent);
                        _self.props.onOk ? _self.props.onOk(event) : null;
                        _self.onChangeColorByEndorsement();
                    }
                },
                success: function (elem) {
                    _self.props.onShow ? _self.props.onShow(elem) : null;
                    if (_self.props.onShow) {
                        const findClass = _self.props.findClass ? "." + _self.props.findClass : ".input-group";
                        dateComponent.closest(findClass).addClass("focusborder");
                        { !Util.parseBool(_self.props.isOk) ? $(".setok").hide() : null };
                    }
                },
                clearfun: function (elem, val) {
                    const { model, property } = _self.props;
                    if (model && property) {
                        model[property] = null;
                    }
                    _self.setInputValue(event, _self, dateComponent);
                    _self.props.onClean ? _self.props.onClean(elem, val) : null;
                },
            });
        }
        if (Util.parseBool(this.props.isEnableInputEvent)) {
            dateComponent.unbind("keyup");
            dateComponent.keyup(function (event) {
                let inputValue = _self.getInputValue(event);

                if (inputValue) {

                    $("#" + _self.componentId).val(inputValue);
                    _self.setComponentValue(event);
                    if (_self.props.onChange) {
                        let valueChangeEvent = new OnChangeEvent(_self, event, Param.getParameter(_self), '', _self.onEvent.newValue);
                        // _self.props.model[_self.props.property] = convertorValue;
                        _self.props.onChange(valueChangeEvent);
                    }
                    _self.onEvent = { newValue: inputValue, oldValue: _self.onEvent.newValue };

                }
            })
        }
        dateComponent.unbind("blur");
        dateComponent.blur(function (event) {
            // $(".ymsbox").click(function (event) {
            //     let value = $(".arthead").attr("data-val");
            //     $(".setok").trigger('click');
            // });
            let componentValue = _self.getComponentValue();
            let value = $(this).val();
            if (value != componentValue) {
                const split = _self.getFormatSplit(_self);
                const findClass = _self.props.findClass ? "." + _self.props.findClass : ".input-group";
                $(this).closest(findClass).removeClass("focusborder");
                if (_self.checkValue(value, split)) {
                    value = value.replace(/[^0-9]/ig, "");
                    const yyyy = value.slice(0, 4);
                    const mm = value.slice(4, 6);
                    const dd = value.slice(6, 8);
                    if (Util.parseBool(_self.props.showTime)) {
                        value = value.padEnd(14, '0');
                        const hh = value.slice(8, 10);
                        const ii = value.slice(10, 12);
                        const ss = value.slice(12, 14);
                        const returnDate = `${yyyy}${split}${mm}${split}${dd} ${hh}:${ii}:${ss}`;
                        if (_self.checkDate(`${yyyy}${split}${mm}${split}${dd}`) && _self.checkTime(`${hh}:${ii}:${ss}`) && _self.checkMaxDateAndMinDate(returnDate)) {
                            $(this).val(returnDate);
                            _self.setInputValue(event, _self, dateComponent);
                        } else {
                            _self.deleteInputValue(event)
                        }
                    } else {
                        const returnDate = `${yyyy}${split}${mm}${split}${dd}`;
                        if (_self.checkDate(returnDate) && _self.checkMaxDateAndMinDate(returnDate)) {
                            $(this).val(returnDate);
                            _self.setInputValue(event, _self, dateComponent);
                        } else {
                            _self.deleteInputValue(event)
                        }
                    }
                } else {
                    _self.deleteInputValue(event)
                }
            }
            dateComponent.parent().parent().removeClass("focusborder");
            _self.props.onBlur ? _self.props.onBlur(new OnBlurEvent(_self, event, Param.getParameter(_self), null, null)) : null;
        });
        dateComponent.click(function (event) {
            let inputValue = _self.getInputValue(event);
            if (StringUtil.isEmpty(inputValue)) {
                _self.deleteInputValue(event);
                // if (_self.props.onChange) {
                //     let valueChangeEvent = new OnChangeEvent(_self, event, Param.getParameter(_self), inputValue, _self.onEvent.newValue);
                //     _self.props.onChange(valueChangeEvent);
                // }
                // _self.onEvent = { newValue: inputValue, oldValue: _self.onEvent.newValue };
            }

            let format = config.DEFAULT_DATETIME_FORMATER;
            if (_self.props.format) {
                format = _self.props.format;
            }
            if (format.indexOf("ii") >= 0) {
                format = format.replace('ii', 'mm');
            }

            if (format.indexOf("hh") >= 0) {
                format = format.replace('hh', 'HH');
            }

            if (inputValue) {
                let tempInputValue = inputValue.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
                let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
                if (tempInputValue.length == tempFormat.length) {
                    // let convertorValue = dayjs(tempInputValue, tempFormat).format(format);
                    let returnDate = tempInputValue;
                    if (format.indexOf('YYYY') > -1) {
                        const split = _self.getFormatSplit(_self);
                        let yyyy = tempInputValue.substr(0, 4);
                        const mm = tempInputValue.substr(4, 2);
                        const dd = tempInputValue.substr(6, 2);
                        if(_self.props.isYearOffset){
                            let yearOffset = _self.props.yearOffset?parseInt(_self.props.yearOffset):1911;                            
                            yyyy = (parseInt(yyyy) + yearOffset).toString();
                        }
                        returnDate = `${yyyy}${split}${mm}${split}${dd}`;
                        if (Util.parseBool(_self.props.showTime)) {
                            const hh = tempInputValue.substr(8, 2);
                            const ii = tempInputValue.substr(10, 2);
                            const ss = tempInputValue.substr(12, 2)&&tempInputValue.substr(12, 2)!=""?tempInputValue.substr(12, 2):"00";
                            returnDate = `${yyyy}${split}${mm}${split}${dd} ${hh}:${ii}:${ss}`;
                        }
                    }

                    if (_self.checkDate(returnDate) && _self.checkMaxDateAndMinDate(returnDate)) {
                        inputValue = dayjs(returnDate).format(format);
                        if(Util.parseBool(_self.props.isYearOffset)&&format.indexOf('YYYY')>-1){
                            let yearOffset = _self.props.yearOffset?parseInt(_self.props.yearOffset):1911;
                            let year = parseInt(inputValue.substr(0,4));
                            let yearToStr = (year-yearOffset).toString();
                            if(yearToStr.length<3){
                                for (let i = yearToStr.length; i < 3; i++) {
                                    yearToStr = "0" + yearToStr
                                }
                            }
                            let newDate = yearToStr + inputValue.substr(4);
                            inputValue = newDate;
                        }
                        $("#" + _self.componentId).val(inputValue);
                        // _self.setInputValue(event, _self, dateComponent);
                        _self.setComponentValue(event);
                    } else {
                        _self.deleteInputValue(event)
                    }
                }else{
                    _self.deleteInputValue(event)
                    UIMessageHelper.error(r18n.DateFormat)                    
                }
            }
        });
    }

    checkDate(date) {
        let dateArr = date.match(/\w+|d+/g);
        // if (isNaN(dateArr[0]) || isNaN(dateArr[1]) || isNaN(dateArr[2])) return false;
        if (dateArr) {
            if (dateArr[0].length > 4) return false;
            if (dateArr[1] > 12 || dateArr[1] < 1) return false;
            if (dateArr[2] < 1 || dateArr[2] > 31) return false;
            if ((dateArr[1] == 4 || dateArr[1] == 6 || dateArr[1] == 9 || dateArr[1] == 11) && dateArr[2] > 30) return false;
            if (dateArr[1] == 2) {
                if (dateArr[2] > 29) return false;
                let yearToStr = dateArr[0];
                let format = this.props.format?this.props.format:config.DEFAULT_DATETIME_SUBMIT_FORMATER;                
                if(Util.parseBool(this.props.isYearOffset)&&format.indexOf('YYYY')>-1){
                    let yearOffset = this.props.yearOffset?parseInt(this.props.yearOffset):1911;
                    let year = parseInt(yearToStr.substr(0,4));
                    if(year<1000){
                        yearToStr = (year+yearOffset).toString();
                    }
                }
                if ((yearToStr % 100 == 0 && yearToStr % 400 != 0 || yearToStr % 4 != 0) && dateArr[2] > 28) return false;
            }
            return true;
        } else {
            return false;
        }
    }

    checkMaxDateAndMinDate(date) {
        if (date) {
            let maxDate = this.getMaxDate();
            let minDate = this.getMinDate();
            let format = config.DEFAULT_DATETIME_FORMATER;
            if (this.props.format) {
                format = this.props.format;
            }
            if (format.indexOf("ii") >= 0) {
                format = format.replace('ii', 'mm');
            }

            if (format.indexOf("hh") >= 0) {
                format = format.replace('hh', 'HH');
            }
            let tempInputValue = date.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
            let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
            tempInputValue = this.transYearOffSet(tempInputValue);
            if (tempInputValue.length == tempFormat.length) {
                // let convertorValue = dayjs(tempInputValue, tempFormat).format(format);
                let returnDate = tempInputValue;
                if (format.indexOf('YYYY') > -1) {
                    const split = this.getFormatSplit(this);
                    const yyyy = tempInputValue.substr(0, 4);
                    const mm = tempInputValue.substr(4, 2);
                    const dd = tempInputValue.substr(6, 2);
                    returnDate = `${yyyy}${split}${mm}${split}${dd}`;
                    if (Util.parseBool(this.props.showTime)) {
                        const hh = tempInputValue.substr(8, 2);
                        const ii = tempInputValue.substr(10, 2);
                        const ss = tempInputValue.substr(12, 2);
                        returnDate = `${yyyy}${split}${mm}${split}${dd} ${hh}:${ii}:${ss}`;
                    }
                }
                let nowDate = DateTimePickerConvertor.getAsObject(this, returnDate);
                if (Util.parseBool(this.props.isYearOffset)) {
                    maxDate = this.addYearOffset(this.getMaxDate(), "maxDate");
                    minDate = this.addYearOffset(this.getMinDate());
                    // date = this.addYearOffset(this.transYearOffSet(date));
                }
                if ((maxDate && dayjs(nowDate).isAfter(maxDate))) {
                    UIMessageHelper.error(r18n.DateTimePicker.maxDateErrorMessage + dayjs(this.getMaxDate()).format(this.props.format));
                    return false;
                }
                if ((minDate && dayjs(nowDate).isBefore(minDate))) {
                    UIMessageHelper.error(r18n.DateTimePicker.minDateErrorMessage + dayjs(this.getMinDate()).format(this.props.format));
                    return false;
                }
            }
        }
        return true;
    }

    addYearOffset(value, dateType) {
        let format = this.props.format ? this.props.format : config.DEFAULT_DATETIME_SUBMIT_FORMATER;
        if (Util.parseBool(this.props.isYearOffset) && format.indexOf('YYYY') > -1) {
            let yearOffset = this.props.yearOffset ? parseInt(this.props.yearOffset) : 1911;
            let year = parseInt(value.substr(0, 4));
            if (year < 1000) {
                let yearToStr = (year + yearOffset).toString();
                let newDate = yearToStr + value.substr(4);
                value = newDate;
            }
        }
        let tempFormat = format;
        let temp = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        if(temp=="YYYYMMDD"){
            tempFormat = "YYYYMMDD 00:00:00";
            if (dateType == "maxDate") {
                tempFormat = "YYYYMMDD 23:59:59"
            }
            return dayjs(value).format(tempFormat);
        }
        // value = value.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        // if (value.length > 8) {
        //     value = value.substr(0, 8)
        // }
        return value;
    }

    checkTime(time) {
        let dateArr = time.match(/\w+|d+/g);
        if (isNaN(dateArr[0]) || isNaN(dateArr[1]) || isNaN(dateArr[2])) return false;
        if (dateArr[0] > 24 || dateArr[0] < 0) return false;
        if (dateArr[1] < 0 || dateArr[1] > 59) return false;
        if (dateArr[2] < 0 || dateArr[2] > 59) return false;
        return true;
    }

    getFormatSplit(_self) {
        return _self.getFormat().slice(4, 5);
    }

    checkValue(val, split) {
        if (val === "" || val == null) {
            return false;
        }
        val = val.replace(/[^0-9]/ig, "");
        if (!isNaN(val)) {
            return true;
        } else {
            return false;
        }
    }

    getWorks() {
        const _config = SessionContext.get("project_config");
        return _config ? _config.DEFAULT_DATETIME_WORK ? _config.DEFAULT_DATETIME_WORK.split(",") : null : null;
    }

    getMarks() {
        const _config = SessionContext.get("project_config");
        return _config ? _config.DEFAULT_DATETIME_MARKER ? _config.DEFAULT_DATETIME_MARKER.split(",") : null : null;
    }

    getLanguage() {
        let lang = localStorage.getItem('system_i18nKey');
        const Language = {
            name: lang,
            month: r18n.FullCalendar.MonthNamesShort,
            weeks: r18n.DateRangePicker.DaysOfWeek,
            times: r18n.DateRangePicker.times,
            timetxt: [r18n.DateRangePicker.timeChoose, r18n.DateRangePicker.DateRangePicker, r18n.DateRangePicker.ToLabel],
            backtxt: r18n.DateRangePicker.BackDate,
            clear: r18n.DateRangePicker.Clear,
            today: r18n.DateRangePicker.Today,
            yes: r18n.DateRangePicker.ApplyLabel,
            close: r18n.DateRangePicker.Close
        }

        return Language;
    }

    componentDidMount() {
        if (Util.parseBool(this.props.isShow)) {
        super.componentDidMount();
        }
        if (!this.isDynamicProduct()) {
            this.initComponent();
            if (this.props.io != "out") {
                this.initValidator();
            }
        }
    }

    componentDidUpdate(nextProps, nextState) {
        super.componentDidUpdate(nextProps, nextState);
        if (!this.isDynamicProduct()) {
            this.clearValidationInfo(nextProps);
            this.initComponent();
            let _self = this;
            let newInputValue = _self.getInputValue(event);
            _self.onEvent = { newValue: newInputValue, oldValue: _self.onEvent.newValue };
        }
    }

    componentWillUpdate(nextProps, nextState) {
        super.componentWillUpdate(nextProps, nextState);
    }

    clearValidationInfo(nextProps) {
        const inputObject = $("#" + this.componentId);
        if ($("form").data('bootstrapValidator') && !$("form").data('bootstrapValidator').isValidField(this.componentId) && inputObject.val() != '') {
            $("form").data('bootstrapValidator').enableFieldValidators(this.componentId,false);
            ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
        }
        if (Util.parseBool(nextProps.required) && inputObject.val() != '' && !Util.parseBool(this.props.enabled) && inputObject.closest(".form-group").hasClass("has-error")) {
            inputObject.parent().parent().next().remove();
            const errorInputObject = inputObject.closest(".form-group");
            if (errorInputObject.hasClass("has-error")) {
				errorInputObject.removeClass("has-error").addClass("has-success");
                // inputObject.parent().parent().parent().next().remove();
                inputObject.parents('.datetimepicker-textalign').find('small').remove()
                inputObject.closest(".input-group").css("border", "1px solid #cbcbcb");
            };

        }
    }

    clearChangeValidationInfo(nextProps) {
        const inputObject = $("#" + this.componentId);
        // inputObject.data('bootstrapValidator').validateField('notEmpty');

        if (nextProps && nextProps.required != undefined) {
            inputObject.parent().parent().next().remove();
            inputObject.parent().parent().next().remove(); // small
            if (inputObject.data('bv-field')) {
                // inputObject.removeAttr('data-bv-field');
                // inputObject.attr('data-bv-field', '');
            }

            if (inputObject.parents('.has-feedback').length > 0 || inputObject.parents('.has-error').length > 0) {
                $(inputObject.parents('.has-feedback')[0]).removeClass('has-feedback');
                $(inputObject.parents('.has-error')[0]).removeClass('has-error')
            }
            const errorInputObject = inputObject.closest(".form-group");
            if (errorInputObject.hasClass("has-error")) {
                inputObject.closest(".input-group").css("border", "2px solid #E1E8EE");
            }

        }
    }


    getInputValue(event) {
        //let inputRef = this.getInputRefProperty();
        if (Util.parseBool(this.props.inline)) {
            return DateTimePickerConvertor.getAsString(this, event.date);
        }
        //return React.findDOMNode(this.refs[inputRef]).value;
        let format = config.DEFAULT_DATETIME_FORMATER;
        if (this.props.format) {
            format = this.props.format;
        }
        if (format.indexOf("ii") >= 0) {
            format = format.replace('ii', 'mm');
        }
        let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        let val = $('#' + this.componentId).val().replace('HH', '00').replace('ii', '00').replace('ss', '00'); //;$('#' + this.componentId).val()
        let valCheck = val.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        let isInputNum = /^[0-9]+.?[0-9]*$/;
        if(valCheck&&!isInputNum.test(valCheck)){
            return val = '';
        }
        if (tempFormat != 'hhmmss') {
            val = valCheck;
        }
        if (Util.parseBool(this.props.isYearOffset)) {
            if(val){
                val = val.padEnd(tempFormat.length - 1, '00');
                if (val.length < tempFormat.length) {
                    for (let i = val.length; i < tempFormat.length; i++) {
                        val = "0" + val
                    }
                }
            }
            return val;
        } else {
            if (val && val.length < tempFormat.length && tempFormat.length == 8) {
                let value = val.substring(0, 6) + "0" + val.substring(6, 7);
                return value;
            } else {
                return val;
            }
        }
    }


    getFormat() {
        let format = this.props.format;
        if (format) {
            if (this.props.showTime && format.indexOf("HH") >= 0) {
                format = format.replace("HH", "hh");
            }
        } else {
            format = config.DEFAULT_DATETIME_FORMATER;
        }
        return format;
    }

    getMinDate() {
        if (this.props.minDate) {
            let value = null;
            if (this.props.minDate == "TODAY") {
                const today = window.ServerDate ? new ServerDate() : new Date();
                let minToday = dayjs(dayjs(today).format("YYYYMMDD 00:00:00")); //dayjs(dayjs(today).format("YYYYMMDD") + " 00:00:00", "YYYYMMDD HH:mm:ss");
                value = DateTimePickerConvertor.getAsObject(this, minToday.format('YYYY-MM-DD[T]HH:mm:ss'));
            } else {
                let minDate = this.props.minDate;
                if(Util.parseBool(this.props.isYearOffset)){
                    minDate = this.transYearOffSet(this.props.minDate)
                }
                value = DateTimePickerConvertor.getAsObject(this, minDate.format('YYYY-MM-DD[T]HH:mm:ss'));
            }

            if (value != undefined) {
                let format = this.getFormat();
                if (format == "YYYY") {
                    return dayjs(value + "0101 00:00:00", "YYYYMMDD HH:mm:ss");
                }
                if (Util.parseBool(this.props.isYearOffset) && format.indexOf('YYYY') > -1) {
                    let yearOffset = this.props.yearOffset ? parseInt(this.props.yearOffset) : 1911;
                    let year = parseInt(value.substr(0, 4));
                    if (year > 1000) {
                        let yearToStr = (year - yearOffset).toString();
                        let newDate = yearToStr + value.substr(4);
                        value = newDate;
                    }
                }
                if (value.indexOf('T') > -1) {
                    let splitValue = value.split('T');
                    let strValue = splitValue[0].toString();
                    if (strValue && strValue.length < 10) {
                        for (let i = strValue.length; i < 10; i++) {
                            strValue = "0" + strValue
                        }
                    }
                    let strTime = splitValue[1].toString();
                    value = strValue + ' ' + strTime;
                    // value = value.replace(/T/g, ' ');
                }
                return value;
            }
        } else if (Util.parseBool(this.props.isYearOffset)) {
            return "0001-01-01 00:00:00";
        }
        return null;
    }

    getMaxDate() {
        if (this.props.maxDate) {
            let value = null;
            if (this.props.maxDate == "TODAY") {
                const today = window.ServerDate ? new ServerDate() : new Date();
                let maxToday = dayjs(dayjs(today).format("YYYYMMDD 23:59:59")); //dayjs(dayjs(today).format("YYYYMMDD") + " 23:59:59", "YYYYMMDD HH:mm:ss");
                value = DateTimePickerConvertor.getAsObject(this, maxToday.format('YYYY-MM-DD[T]HH:mm:ss'));
            } else {
                let maxDate = this.props.maxDate;
                if(Util.parseBool(this.props.isYearOffset)){
                    maxDate = this.transYearOffSet(this.props.maxDate)
                }
                value = DateTimePickerConvertor.getAsObject(this, maxDate.format('YYYY-MM-DD[T]HH:mm:ss'));
            }

            if (value != undefined) {
                let format = this.getFormat();
                if (format == "YYYY") {
                    return dayjs(value + "1231 23:59:59", "YYYYMMDD HH:mm:ss");
                }
                if (Util.parseBool(this.props.isYearOffset) && format.indexOf('YYYY') > -1) {
                    let yearOffset = this.props.yearOffset ? parseInt(this.props.yearOffset) : 1911;
                    let year = parseInt(value.substr(0, 4));
                    if (year > 1000) {
                        let yearToStr = (year - yearOffset).toString();
                        let newDate = yearToStr + value.substr(4);
                        value = newDate;
                    }
                }
                if (value.indexOf('T') > -1) {
                    let splitValue = value.split('T');
                    let strValue = splitValue[0].toString();
                    if (strValue && strValue.length < 10) {
                        for (let i = strValue.length; i < 10; i++) {
                            strValue = "0" + strValue
                        }
                    }
                    let strTime = splitValue[1].toString();
                    value = strValue + ' ' + strTime;
                    // value = value.replace(/T/g,' ');
                }
                return value;
            }
        } else if (Util.parseBool(this.props.isYearOffset)) {
            return "0999-12-31 23:59:59";
        }
        return "2099-12-31 24:59:59";
    }

    transYearOffSet(date) {
        let format = config.DEFAULT_DATETIME_FORMATER;
        if (this.props.format) {
            format = this.props.format;
        }
        if (format.indexOf("ii") >= 0) {
            format = format.replace('ii', 'mm');
        }
        let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        let val = date.replace('HH', '00').replace('ii', '00').replace('ss', '00');
        if (tempFormat != 'hhmmss') {
            val = val.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        }
        if (Util.parseBool(this.props.isYearOffset)) {
            if (val && val.length < tempFormat.length) {
                for (let i = val.length; i < tempFormat.length; i++) {
                    val = "0" + val
                }
            }
        }
        return val;
    }

    getDefaultDate() {
        let defaultDate = DateTimePickerConvertor.getAsObject(this, this.props.defaultDate);
        if (defaultDate) {
            let maxDate = this.getMaxDate();
            let minDate = this.getMinDate();
            const dayjsDefaultDate = dayjs(defaultDate);
            if ((maxDate && dayjsDefaultDate.isAfter(maxDate)) || (minDate && dayjsDefaultDate.isBefore(minDate))) {
                defaultDate = "";
                const { model, property } = this.props;
                if (model && property) {
                    model[property] = null;
                }
            } else {
                return [{ YYYY: dayjsDefaultDate.get('year'), MM: dayjsDefaultDate.get('month') + 1, DD: dayjsDefaultDate.get('date'), hh: dayjsDefaultDate.get('hour'), mm: dayjsDefaultDate.get('minute'), ss: dayjsDefaultDate.get('second') }, false]
            }
        } else {
            return []
        }
    }

    getDate() {
        let date = DateTimePickerConvertor.getAsObject(this, this.getComponentValue());
        if (date) {
            // let maxDate = this.getMaxDate();
            // let minDate = this.getMinDate();
            if (!this.checkMaxDateAndMinDate(date)) {
                date = "";
                const { model, property } = this.props;
                if (model && property) {
                    model[property] = null;
                }
                $("#" + this.componentId).val(date);
            }
            // if ((maxDate && dayjs(date).isAfter(maxDate)) || (minDate && dayjs(date).isBefore(minDate))) {
            //     date = "";
            //     const { model, property } = this.props;
            //     if (model && property) {
            //         model[property] = null;
            //     }
            //     $("#" + this.componentId).val(date);
            // }
        }
        return date;
    }

    getConvertorId() {
        return ConvertorConstant.DATETIMEPICKER_CONVERTOR;
    }

    onChangeCallback(_self) {
        let inputValue = this.getInputValue(event);
        if (StringUtil.isEmpty(inputValue)) {
            this.deleteInputValue(event);
            if (this.props.onChange) {
                let valueChangeEvent = new OnChangeEvent(this, event, Param.getParameter(this), inputValue, this.onEvent.newValue);
                this.props.onChange(valueChangeEvent);
            }
            this.onEvent = { newValue: inputValue, oldValue: this.onEvent.newValue };
        }

        let format = config.DEFAULT_DATETIME_FORMATER;
        if (this.props.format) {
            format = this.props.format;
        }
        if (format.indexOf("ii") >= 0) {
            format = format.replace('ii', 'mm');
        }
        if (inputValue) {
            let tempInputValue = inputValue.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
            let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
            if (tempInputValue.length == tempFormat.length) {
                // let convertorValue = dayjs(tempInputValue, tempFormat).format(format);
                let returnDate = tempInputValue;
                if (format.indexOf('YYYY') > -1) {
                    const split = _self.getFormatSplit(_self);
                    const yyyy = tempInputValue.substr(0, 4);
                    const mm = tempInputValue.substr(4, 2);
                    const dd = tempInputValue.substr(6, 2);
                    returnDate = `${yyyy}${split}${mm}${split}${dd}`;
                    if (Util.parseBool(this.props.showTime)) {
                        const hh = tempInputValue.substr(8, 2);
                        const ii = tempInputValue.substr(10, 2);
                        const ss = tempInputValue.substr(12, 2);
                        returnDate = `${yyyy}${split}${mm}${split}${dd} ${hh}:${ii}:${ss}`;
                    }
                }
                let convertorValue = DateTimePickerConvertor.getAsObject(this, returnDate);
                //比较日期
                let maxDate = this.getMaxDate();
                let minDate = this.getMinDate();
                if (Util.parseBool(this.props.isYearOffset)) {
                    maxDate = this.addYearOffset(this.getMaxDate(), "maxDate");
                    minDate = this.addYearOffset(this.getMinDate());
                }
                if (maxDate && dayjs(convertorValue).isAfter(maxDate)) {
                    convertorValue = "";
                }
                if (minDate && dayjs(convertorValue).isBefore(minDate)) {
                    convertorValue = "";
                }
                $("#" + this.componentId).val(inputValue);

                this.setComponentValue(event);
                if (this.props.onChange) {
                    let valueChangeEvent = new OnChangeEvent(this, event, Param.getParameter(this), convertorValue, this.onEvent.newValue);
                    this.props.model[this.props.property] = convertorValue;
                    this.props.onChange(valueChangeEvent);
                }
                this.onEvent = { newValue: convertorValue, oldValue: this.onEvent.newValue };
            }
        }
    }

    formatDateofManualInput(event) {
        let inputValue = this.getInputValue(event);
        if (StringUtil.isEmpty(inputValue)) {
            this.deleteInputValue(event);
        }

        let format = config.DEFAULT_DATETIME_FORMATER;
        if (this.props.format) {
            format = this.props.format;
        }
        if (format.indexOf("ii") >= 0) {
            format = format.replace('ii', 'mm');
        }
        if (format.indexOf("hh") >= 0) {
            format = format.replace('hh', 'HH');
        }
        if (inputValue) {
            let tempInputValue = inputValue.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
            let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
            if (tempInputValue.length == tempFormat.length) {
                let convertorValue = dayjs(tempInputValue, tempFormat).format(format);
                //比较日期
                let maxDate = this.getMaxDate();
                let minDate = this.getMinDate();
                if (maxDate && dayjs(convertorValue).isAfter(maxDate)) {
                    convertorValue = "";
                }
                if (minDate && dayjs(convertorValue).isBefore(minDate)) {
                    convertorValue = "";
                }
                $("#" + this.componentId).val(convertorValue);
                this.setComponentValue(event);
                if (this.props.onChange) {
                    let valueChangeEvent = new OnChangeEvent(this, event, Param.getParameter(this), convertorValue, this.onEvent.newValue);

                    this.props.onChange(valueChangeEvent);
                }
                this.onEvent = { newValue: convertorValue, oldValue: this.onEvent.newValue };
            }
        }
    }


};


/**@ignore
 * DateTimePicker component prop types
 */
DateTimePicker.propTypes = $.extend({}, UIInput.propTypes, {
    minDate: PropTypes.string,
    maxDate: PropTypes.string,
    format: PropTypes.string,
    trigger: PropTypes.string,
    defaultValue: PropTypes.string,
    showToday: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    showClear: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    showTime: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    isOk: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    componentType: PropTypes.string,
    onOk: PropTypes.function,
    onClean: PropTypes.function,
    onShow: PropTypes.function,
    autoClose: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    forceParse24: PropTypes.bool,
    alwaysParse24: PropTypes.bool,
    manualInput: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    offSet: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    rebind: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    isEnableInputEvent: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    initParse24: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]), // 初始成24:00:00,值是23:59:59
    isShowTimeBtn: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]), // 是否显示时间选择按钮
    isShow24Hour: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]), // 时间选择是否显示24 hour
    isYearOffset: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]), // 年差量（为实现民国日历那样的）
    isShow: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    isShowToday: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    bindFormat: PropTypes.string,
    yearOffset: PropTypes.string,
    isNowTime: PropTypes.oneOfType([PropTypes.string, PropTypes.bool])    
});

/**@ignore
 * Get DateTimePicker component default props
 */
DateTimePicker.defaultProps = $.extend({}, UIInput.defaultProps, {
    autoClose: true,
    showTime: false,
    showToday: true,
    showClear: true,
    isOk: true,
    trigger: "click",
    format: config.DEFAULT_DATETIME_FORMATER,
    bindFormat: config.DEFAULT_DATETIME_SUBMIT_FORMATER,
    componentType: "datetimepicker",
    forceParse24: false,
    alwaysParse24: false,
    manualInput: true,
    rebind: false,
    sEnableInputEvent: false,
    initParse24: false, // 初始成24:00:00,值是23:59:59
    isShowTimeBtn: true, // 是否显示时间选择按钮
    isShow24Hour: true, // 时间选择是否显示24 hour
    isYearOffset: SessionContext.get("project_config")&&SessionContext.get("project_config").telentConfig&&SessionContext.get("project_config").telentConfig.isYearOffset&&Util.parseBool(SessionContext.get("project_config").telentConfig.isYearOffset)?true:false,// 年差量（为实现民国日历那样的）
    yearOffset: "1911",
    isShowToday: true,
    isShow: true,
    isNowTime: false
});
