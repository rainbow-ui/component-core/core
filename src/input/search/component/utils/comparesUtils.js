export class compareUtils {

    static hashcode(str) {
        let hash = 0, i, chr, len;
        if (str.length === 0) return hash;
        for (i = 0, len = str.length; i < len; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }

    static compareObjects(objA, objB) {
        if (objA == null) {
            objA = {}
        }
        if (objB == null) {
            objB = {}
        }
        return this.hashcode(JSON.stringify(objA)) == this.hashcode(JSON.stringify(objB))
    }
}
