import UIText from '../../Text';
import { UrlUtil, Util } from 'rainbow-desktop-tools';
import PropTypes from "prop-types";
import { SessionContext } from 'rainbow-desktop-cache';
import r18n from '../../../i18n/reactjs-tag.i18n';
export default class Search extends UIText {
    constructor(props) { 
        super(props);
        this.idField = this.props.idField;
        this.orgin_keyField = this.props.keyField;
        this.keyField = this.props.keyField;
        this.effectiveFields = this.props.effectiveFields;
        this.icon = "rainbow Search";
        this.url = this.props.url;
        this.clear = this.props.clear;
        this.init = true;
        this.select = false;
        this.down = false;
        this.option = this.buildOption();
        this.value = "";
        this.previousComponentValue = this.props.model && this.props.property ? this.props.model[this.props.property] : null;
        this.componentDisplayValue = '';
        this.data = {};
    }

    componentWillMount() {
        super.componentWillMount();
    }

    renderComponent() {
        return (
            <div className="searchGroup">
                <UIText className="search" id={this.componentId} {...this.props} unablePaste="true" onPaste={this.onPaste.bind(this)} onBlur={this.onBlur.bind(this)} suffixIcon={this.icon}
                    onSuffixIconClick={this.onSuffixIconClick.bind(this)} onDeleteIconClick={this.deleteValue.bind(this)} showDeleteIcon={this.clear}/>
            </div>
        );
    }
    deleteValue() {
        this.value = "";
        if (this.props.model && this.props.property) {
            this.props.model[this.props.property] = null;
        }
        let inputDiv = $("#for-input-" + this.componentId);
        inputDiv.attr("data-original-title", '');
        let deleteFunc = this.props.onDeleteIconClick;
        deleteFunc ? deleteFunc() : null;
        this.props.autoDelete ? this.data = "" : null;
    }

    buildUrl() {
        if (this.props.codetableName) {
            let url = UrlUtil.getConfigUrl('UI_API_GATEWAY_PROXY', 'DEFINE_CODETABLE_SEARCH_API');
            url = this.props.queryMap ? url + '?codeTableName=' + this.props.codetableName + '&queryMap=' + encodeURI(this.props.queryMap, "UTF-8") : url + '?codeTableName=' + this.props.codetableName;
            return url;
        } else {
            return this.url
        }
    }

    buildOption() {
        const self = this;
        const _displayField = this.props.displayField;
        const effectiveFieldsAlias = this.props.effectiveFieldsAlias;
        if (effectiveFieldsAlias) {
            for (let key in effectiveFieldsAlias) {
                effectiveFieldsAlias[key] = this.getI18n(effectiveFieldsAlias[key]);
            }
        }
        if (_displayField) {
            if (Util.isString(_displayField)) {
                this.idField = this.props.idField ? this.props.idField : _displayField;
                this.keyField = this.props.keyField ? this.props.keyField : _displayField;
                this.effectiveFields = [_displayField];
            } else if (Util.isArray(_displayField)) {
                this.idField = this.props.idField;
                this.keyField = this.props.keyField;
                this.effectiveFields = [_displayField[0], _displayField[1]];
            }
        } else {
            this.keyField = this.keyField ? this.keyField : "Display";
        }
        return {
            effectiveFields: this.effectiveFields,
            effectiveFieldsAlias: effectiveFieldsAlias,
            idField: this.idField,
            keyField: this.keyField,
            searchFields: [this.idField, this.keyField, "Display"],
            allowNoKeyword: true,
            separator: ",",
            getDataMethod: "url",
            url: this.buildUrl(),
            showHeader: this.props.showHeader,
            showBtn: true,
            twoWayMatch: false,
            autoDropup: true,
            delayUntilKeyup: true,
            multiWord: true,
            searchingTip: r18n.Searching,
            separator: " ",
            inputWarnColor: 'rgba(255,255,255)',
            delay: 300,
            listStyle: {
                "transition": "none", "-webkit-transition": "none", "-moz-transition": "none", "-o-transition": "none"
            },
            fnPreprocessKeyword: function (keyword) {
                if (Util.parseBool(self.props.readOnly)) {
                    return;
                }
                return self.props.paramKey ? self.props.paramKey + "=" + encodeURI(keyword, "UTF-8") : self.keyField + "=" + encodeURI(keyword, "UTF-8");
            },
            fnProcessData: function (json) {
                let data = !_.isEmpty(json) ? { value: json } : { value: [] };
                if (self.props.codetableName) {
                    data = !_.isEmpty(json) ? { value: json['BusinessCodeTableValueList'] } : { value: [] };
                }
                _.each(data["value"], (j) => {
                    j["Display"] = j[self.idField] + " " + j[self.orgin_keyField];
                });
                return data;
            },
            fnAdjustAjaxParam: function (keyword, opts) {
                const config = JSON.parse(sessionStorage.getItem("project_config"));
                const param = {
                    type: self.props.codeTableName ? 'POST' : self.props.method?self.props.method:'GET' ,
                    data: self.props.postData?JSON.stringify(self.props.postData):null,
                    timeout: 10000,
                    beforeSend: function (jqXHR) {
                        const setionToken = sessionStorage.getItem("Authorization");
                        if (setionToken) {
                            jqXHR.setRequestHeader("Authorization", 'Bearer ' + setionToken.substr(13).split("&")[0]);
                            if(config&&config.UI_CAS){
                                jqXHR.setRequestHeader('x-ebao-auth-protocal',config.UI_CAS);
                            }
                        }
                        if(!_.isEmpty(self.props.requestHeader)){
                            let hearerKeys = Object.keys(self.props.requestHeader);
                            if(hearerKeys.length>0){
                                _.each(hearerKeys, (key)=>{
                                    jqXHR.setRequestHeader(key,self.props.requestHeader[key]);                                                                
                                })
                            }                          
                        }
                    },
                };
                if (!_.isEmpty(self.url)) {
                    param.url = self.url;
                }
                return param;
            }

        }
    }

    onSuffixIconClick() {
        if (this.props.onSuffixIconClick){
            this.props.onSuffixIconClick()
        } else {
            const input = $("#" + this.componentId);
            input.focus();
            input.click();
        }
    }

    onPaste() {
        const self = this;
        const inputObject = $("#" + this.componentId);

        const value = event.clipboardData.getData('Text'); //inputObject.val();
        this.value = value;
        if (!_.isEmpty(value) && !self.down) {
            this.autoSelectFirstRow(inputObject);
        }

        self.handleDispalyField(inputObject);
    }

    onBlur() {
        const self = this;
        const inputObject = $("#" + this.componentId);
        const _displayField = this.props.displayField;
        let inputGroup = inputObject.parent('.input-group');
        if (inputGroup && inputGroup.hasClass("focusborder")) {
            inputGroup.removeClass('focusborder');
        }
        if (!_.isEmpty(this.value)) {
            inputObject.val(this.value);
            inputObject.attr("title",this.value);
        }
        if (this.props.model && this.value) {
            if (_displayField) {
                if (Util.isString(_displayField)) {
                    this.props.model[this.props.property] = inputObject.val();
                } else if (Util.isArray(_displayField) && _displayField.length > 0) {
                    this.props.model[this.props.property] = this.data[this.idField];
                }
            } else {
                this.props.model[this.props.property] = this.data[this.idField];
            }
        }

        // if (this.select) {
        //     this.props.model[this.props.property] = inputObject.val();
        // }

        if (this.select && this.props.autoDelete) {
            if (Object.keys(this.data).length != 0 && _displayField) {
                let displayId = _displayField[0];
                let displayValue = _displayField[1];
                inputObject.val(this.data[displayId] + " " + this.data[displayValue])
            } else {
                inputObject.val("")
            }
        }
        if (this.props.onBlur) {
            this.props.onBlur();
        }
        if ($('#' + self.componentId).parents('td') && $('#' + self.componentId).parents('td').length > 0) {
            // let l = $('#' + this.componentId).parents('div [needreposition=1]').position().left;
            let l = $('#for-input-' + self.componentId).position().left;
            let w = $('#' + self.componentId).parents('td').attr('realwidth');
            let marginTop = self.props.overflowMarginTop;
            
            // $('#' + this.componentId).parents('div [needreposition=1]').css({ position: 'absolute', left: l + 'px', width: w + 'px', marginTop: this.props.overflowMarginTop });
            let curObjH = $('#for-input-' + self.componentId).height() / 2;
            $('#for-input-' + self.componentId).css({ position: 'relative' ,left:'0px',marginTop: '0px'});
            
        }

        this.select = false;
    }


    componentDidUpdate() {
        let self = this;
        const inputObject = $("#" + this.componentId);
        this.removeFeedBack();
        let nextPropsValue = this.props.model[this.props.property];
        if (this.previousComponentValue !== nextPropsValue || (self.componentDisplayValue == '' && nextPropsValue != '')) {
            this.previousComponentValue = nextPropsValue;
            self.handleDispalyField(inputObject);
        } else {
            inputObject.val(self.componentDisplayValue);
        }
        this.showValueTooltip();

    }

    componentWillReceiveProps(nextProps) {
        this.url = nextProps.url;
        this.queryMap = nextProps.queryMap;
    }

    componentDidMount() {
        const self = this;
        self.removeFeedBack();
        const inputObject = $("#" + this.componentId);
        if (self.props.io == "in") {
            inputObject.parent().addClass("input-group");
            inputObject.parent().addClass("search-group");
            inputObject.after('<ul class="dropdown-menu dropdown-menu-right" role="menu"></ul>');
        }
        if (!this.getDisabled()) {
            this.buildSuggest(this.option, inputObject);
        }

        inputObject.focus(() => {
            this.value = inputObject.val();
            // this.value = '';
            inputObject.val('');
            self.removeFeedBack();
            this.autoSelectFirstRow(inputObject);
        });

        inputObject.keyup(() => {
            const self = this;
            const value = inputObject.val();
            this.value = value;
            if (!_.isEmpty(value) && !self.down) {
                this.autoSelectFirstRow(inputObject);
            }
        });

        // inputObject.focus(() => {
        //     this.autoSelectFirstRow(inputObject);
        // });

        self.handleDispalyField(inputObject);
        if (!Util.parseBool(self.props.enabled)) {
            $("#" + self.componentId).attr("disabled", self.getDisabled());
        }

        this.showValueTooltip();
    }

    autoSelectFirstRow(inputObject) {
        setTimeout(() => {
            const dom = inputObject.next().find("tr:first-child");
            const hover = inputObject.next().find('.jhover');
            if (hover && hover.length == 1) {
                self.down = true;
            } else {
                self.down = false;
            }
            if (!self.down) {
                let trObj = null;
                if (dom.length > 1) {
                    trObj = $(dom[1]);
                } else if (dom.length == 1) {
                    trObj = $(dom[0]);
                }
                if (trObj && !this.down) {
                    trObj.addClass("jhover");
                }
            }

        }, 500);
    }

    handleDispalyField(inputObject) {
        let self = this;
        const _displayField = this.props.displayField;
        if (this.props.model[this.props.property]) {
            if (!_displayField) {
                self.setDisplayField(inputObject);
            } else {
                if (Util.isString(_displayField)) {
                    if (Util.isString(_displayField) == this.props.property) {
                        this.componentDisplayValue = this.props.model[this.props.property];
                        inputObject.val(this.props.model[this.props.property]);
                    } else {
                        self.setDisplayField(inputObject);
                    }
                } else if (Util.isArray(_displayField) && _displayField.length > 0) {
                    self.setDisplayField(inputObject);
                }
            }
        } else {
            this.componentDisplayValue = "";
            inputObject.val("");
        }
    }

    setDisplayField(inputObject) {
        const self = this;
        const _displayField = this.props.displayField;
        let displayId = "";
        let displayValue = "";
        if (_displayField && Util.isArray(_displayField)) {
            displayId = _displayField[0];
            displayValue = _displayField[1] ? _displayField[1] : '';
        } else {
            displayId = this.props.idField ? this.props.idField : '';
            displayValue = this.props.keyField ? this.props.keyField : '' ;
        }
        let tempUrl = "";
        if (this.props.optionUrl && this.props.optionParamKey) {
            tempUrl = this.props.optionUrl + self.props.optionUrlConnect + self.props.optionParamKey + "=" + encodeURI(self.props.model[self.props.property], "UTF-8");
            let para = self.props.optionUrlConnect + self.props.optionParamKey + "=" + encodeURI(self.props.model[self.props.property], "UTF-8");
            if (this.props.optionUrl && this.props.optionUrl.indexOf('?') != -1) {
                tempUrl = this.props.optionUrl + para.replace('?', '&');
            } else {
                tempUrl = this.props.optionUrl + para;
            }
        } else {
            let para = self.props.optionUrlConnect + self.props.paramKey + "=" + encodeURI(self.props.model[self.props.property], "UTF-8");
            if (self.url && self.url.indexOf('?') != -1) {
                tempUrl = self.url + para.replace('?', '&');
            } else {
                tempUrl = self.url + para;
            }
        }
        if (this.props.optionFunction) {
            const value = this.props.optionFunction();
            self.componentDisplayValue = value;
            inputObject.val(value);
            inputObject.attr("title",value);
        } else if (this.props.optionUrl) {
            const value = SessionContext.get('search_' + this.props.model[this.props.property] + (_.isArray(this.props.effectiveFields) ? this.props.effectiveFields.join('') : this.props.effectiveFields));
            if (value) {
                self.value = value;
                inputObject.val(value);
                inputObject.attr("title",value);
                self.componentDisplayValue = value;
                if (this.props.io == "out") {
                    $("#" + this.componentId).text(value);
                }
            } else {
                const config = JSON.parse(sessionStorage.getItem("project_config"));
                $.ajax({
                    method: "GET",
                    url: tempUrl,
                    async: false,
                    contentType: "application/text;charset=UTF-8",
                    xhrFields: {withCredentials: true},
                    crossDomain: true,
                    beforeSend: function (xhr) {
                        let authorization = SessionContext.get("Authorization");
                        if (authorization == null || authorization == undefined || authorization == "") {
                        } else {
                            xhr.setRequestHeader("Authorization", 'Bearer ' + authorization.substr(13).split("&")[0])
                            if(config&&config.UI_CAS){
                                xhr.setRequestHeader('x-ebao-auth-protocal',config.UI_CAS);
                            }
                        }
                        if(!_.isEmpty(self.props.requestHeader)){
                            let hearerKeys = Object.keys(self.props.requestHeader);
                            if(hearerKeys.length>0){
                                _.each(hearerKeys, (key)=>{
                                    jqXHR.setRequestHeader(key,self.props.requestHeader[key]);                                                                
                                })
                            }                          
                        }
                    },
                    success: function (rsdata) {
                        let data = rsdata.BusinessCodeTableValueList ? rsdata.BusinessCodeTableValueList : rsdata;
                        if (!_.isEmpty(data)) {
                            if (Util.isArray(data)) {
                                if (_displayField && Util.isArray(_displayField)) {
                                    self.data = data[0];
                                    if (data[0][displayId] && data[0][displayValue]) {
                                        self.componentDisplayValue = data[0][displayId] + " " + data[0][displayValue];
                                    } else {
                                        self.componentDisplayValue = data[0][displayId]
                                    }
                                } else {
                                    self.data = data[0];
                                    self.componentDisplayValue = data[0][_displayField];
                                }
                            } else if (Util.isObject(data)) {
                                if (_displayField && Util.isArray(_displayField)) {
                                    self.data = data;
                                    if (data[displayId] && data[displayValue]) {
                                        self.componentDisplayValue = data[displayId] + " " + data[displayValue];
                                    } else {
                                        self.componentDisplayValue = data[displayId]
                                    }
                                } else {
                                    self.data = data;
                                    self.componentDisplayValue = data[_displayField];
                                }
                            }
                            self.value = self.componentDisplayValue;
                            inputObject.val(self.value);
                            inputObject.attr("title",self.value);
                            SessionContext.put('search_' + self.props.model[self.props.property] + (_.isArray(self.props.effectiveFields) ? self.props.effectiveFields.join('') : self.props.effectiveFields), self.value);
                            if (self.props.io == "out") {
                                $("#" + self.componentId).text(self.value);
                            }
                        }
                    },
                    error: function () {
                        
                    }
                });
            }
        } else {
            AjaxUtil.call(tempUrl).then((data) => {
                if (!_.isEmpty(data)) {
                    if (Util.isArray(data)) {
                        if(data.length>1){
                        _.each(data,(item)=>{
                            if(item[self.props.property]==self.props.model[self.props.property]){
                                self.data = item;
                            }
                        })
                        }else{
                            self.data = data[0];
                        }
                        if ( self.data[displayId] && self.data[displayValue]) {
                            self.componentDisplayValue = self.data[displayId] + " " + self.data[displayValue];
                        } else {
                            self.componentDisplayValue = self.data[displayId]
                        }
                    } else if (Util.isObject(data)) {
                        self.data = data;
                        if ( data[displayId] && data[displayValue]) {
                            self.componentDisplayValue = data[displayId] + " " + data[displayValue];
                        } else {
                            self.componentDisplayValue = data[displayId]
                        }
                    }
                    self.value = self.componentDisplayValue;
                    inputObject.val(self.value);
                    inputObject.attr("title",self.value);
                }
            });
        }
    }

    showValueTooltip() {
        let inputObj = $("#" + this.componentId);
        let inputDiv = $("#for-input-" + this.componentId);
        const value = SessionContext.get('search_' + this.props.model[this.props.property] + (_.isArray(this.props.effectiveFields) ? this.props.effectiveFields.join('') : this.props.effectiveFields));
        if (value && Util.parseBool(this.props.showValueTooltip)) {
            inputDiv.attr("data-original-title", inputObj.val());
        }
    }

    buildSuggest(option, inputObject) {
        const self = this;
        const _displayField = self.props.displayField;
        inputObject.bsSuggest(option).on('onDataRequestSuccess', function (e, result) {
            self.select = true;
            inputObject.closest(".input-group").addClass("focusborder");
            if ($('#' + self.componentId).parents('td') && $('#' + self.componentId).parents('td').length > 0) {
                // let l = $('#' + this.componentId).parents('div [needreposition=1]').position().left;
                let l = $('#for-input-' + self.componentId).position().left;
                let w = $('#' + self.componentId).parents('td').attr('realwidth');
                let marginTop = self.props.overflowMarginTop;
                
                // $('#' + this.componentId).parents('div [needreposition=1]').css({ position: 'absolute', left: l + 'px', width: w + 'px', marginTop: this.props.overflowMarginTop });
                let curObjH = $('#for-input-' + self.componentId).height() / 2;
                $('#for-input-' + self.componentId).css({ position: 'absolute' ,left: l + 'px',marginTop: '-' + curObjH + 'px'});
                
            }
        }).on('onSetSelectValue', function (e, keyword, data) {
            self.data = data;
            if (self.props.model) {
                self.props.model[self.props.property] = data[self.idField];
                self.previousComponentValue = data[self.idField];
                const _displayField = self.props.displayField;
                let displayId = "";
                let displayValue = "";
                if (_displayField && Util.isArray(_displayField)) {
                    displayId = _displayField[0];
                    displayValue = _displayField[1] ? _displayField[1] : '';
                    if (_displayField && _displayField.length > 1) {
                        self.componentDisplayValue = data[self.idField] + " " + data[self.keyField];
                    } else {
                        self.componentDisplayValue = data[displayId];
                    }
                } else {
                    displayId = self.props.idField ? self.props.idField : '';
                    displayValue = self.props.keyField ? self.props.keyField : '' ;
                    self.componentDisplayValue = data[displayValue];
                }
                // self.componentDisplayValue = data[self.idField] + " " + data[self.keyField];
            }
            if (self.props.onSelectValue) {
                self.props.onSelectValue(data);
                self.value = self.props.model[self.props.property];
                inputObject.val(self.value);
                inputObject.attr("title",self.value);
            }
            self.select = false;
            if (_displayField) {
                if (Util.isString(_displayField)) {
                    self.value = self.props.model[self.props.property];
                    inputObject.val(self.value);
                    inputObject.attr("title",self.value);
                } else if (Util.isArray(_displayField) && _displayField.length > 1) {
                    self.setDisplayField(inputObject);
                }
            } else {
                self.value = data["Display"] ? data["Display"] : data["Description"];
                inputObject.val(self.value);
                inputObject.attr("title",self.value);
            }
            setTimeout(() => {
                if (inputObject.next().is(":hidden")) {
                    inputObject.blur();
                }
            }, 100);
            self.clearValidationInfo();
        }).on('onUnsetSelectValue', function () {
            self.select = true;
        });
    }

    removeFeedBack() {
        const inputObject = $("#" + this.componentId);
        const inputGroup = inputObject.parent();
        const feedBackIcon = inputGroup.next();
        if (feedBackIcon) {
            feedBackIcon.remove();
        }
    }

    clearValidationInfo() {
        const inputObject = $("#" + this.componentId);
        const inputGroup = inputObject.parent();
        const inputDiv = inputGroup.parent();
        const formGroup = inputDiv.parent();
        if (formGroup && formGroup.hasClass("has-feedback") && formGroup.hasClass("has-error")) {
            formGroup.removeClass("has-feedback");
            formGroup.removeClass("has-error");
            inputDiv.next().remove();
        }
    }

};


Search.propTypes = $.extend({}, UIText.propTypes, {
    componentType: PropTypes.string,
    enabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    clear: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    showHeader: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    url: PropTypes.string,
    codetableName: PropTypes.string,
    queryMap: PropTypes.object,
    postData: PropTypes.object,
    paramKey: PropTypes.string,
    optionUrl: PropTypes.string,
    optionFunction: PropTypes.function,
    onSuffixIconClick: PropTypes.function,
    optionUrlConnect: PropTypes.string,
    optionParamKey: PropTypes.string,
    keyField: PropTypes.string,
    idField: PropTypes.string,
    displayField: PropTypes.string,
    effectiveFields: PropTypes.array,
    effectiveFieldsAlias: PropTypes.object,
    readOnly: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    autoDelete: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    overflowMarginTop: PropTypes.string,
    method: PropTypes.string,
    showValueTooltip: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    requestHeader: PropTypes.object
});

Search.defaultProps = $.extend({}, UIText.defaultProps, {
    componentType: "uisearch",
    readOnly: false,
    method:'GET',
    postData:null,
    clear: false,
    showHeader: false,
    optionUrlConnect: "?",
    autoDelete: false,
    overflowMarginTop: '-14px',
    showValueTooltip: false
});
