import classNames from 'classnames';
import PropTypes from 'prop-types';
import Component from '../../../basic/Component';
import blockEvent from './utils/blockEvent';

export default class Option extends Component {

    constructor(props) {
        super(props);

        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.handleMouseEnter = this.handleMouseEnter.bind(this);
        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.handleTouchStart = this.handleTouchStart.bind(this);
        this.handleTouchEnd = this.handleTouchEnd.bind(this);
        this.handleTouchMove = this.handleTouchMove.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.handleMouseOver = this.handleMouseOver.bind(this);
        this.handleMouseOut = this.handleMouseOut.bind(this);
    }

    handleMouseDown(event) {
        event.preventDefault();
        event.stopPropagation();
        this.props.onSelect(this.props.option, event);
    }

    handleMouseOver(event) {
        event.preventDefault();
        event.stopPropagation();
        let _toolTipObj = $(".tooltip.fade.bs-tooltip-top-" + event.target.id);
        setTimeout(() => {
            _toolTipObj.show();
            _toolTipObj.addClass("show");
        }, 700)
    }

    handleMouseOut(event) {
        event.preventDefault();
        event.stopPropagation();
        let _toolTipObj = $(".tooltip.fade.bs-tooltip-top-" + event.target.id);
        setTimeout(() => {
            _toolTipObj.hide();
            _toolTipObj.removeClass("show");
        }, 700)
    }

    handleMouseEnter(event) {
        this.onFocus(event);
    }

    handleMouseMove(event) {
        this.onFocus(event);
    }

    handleTouchEnd(event) {
        // Check if the view is being dragged, In this case
        // we don't want to fire the click event (because the user only wants to scroll)
        if (this.dragging) return;

        this.handleMouseDown(event);
    }

    handleTouchMove() {
        // Set a flag that the view is being dragged
        this.dragging = true;
    }

    handleTouchStart() {
        // Set a flag that the view is not being dragged
        this.dragging = false;
    }

    onFocus(event) {
        if (!this.props.isFocused) {
            this.props.onFocus(this.props.option, event);
        }
    }

   render() {
        const { option, instancePrefix, optionIndex } = this.props;
        const className = classNames(this.props.className, option.className);

        return option.disabled ? (
            <div className={className}
                onMouseDown={blockEvent}
                onClick={blockEvent}>
                {this.props.children}
            </div>
        ) : (
                <div className={className}
                    style={option.style}
                    role="option"
                    aria-label={option.label}
                    onMouseDown={this.handleMouseDown}
                    onMouseEnter={this.handleMouseEnter}
                    onMouseMove={this.handleMouseMove}
                    onTouchStart={this.handleTouchStart}
                    onTouchMove={this.handleTouchMove}
                    onTouchEnd={this.handleTouchEnd}
                    id={`${instancePrefix}-option-${optionIndex}`}
                    title={this.props.children}
                    onMouseOver={this.handleMouseOver}
                    onMouseOut={this.handleMouseOut}
                >
                    {this.props.children}
                    {this.renderToolTip()}
                </div>
            );

    }

    renderToolTip() {
        const { option, instancePrefix, optionIndex } = this.props;
        return (
            this.props.showValueToolTip ? <div class={`tooltip fade bs-tooltip-top-${instancePrefix}-option-${optionIndex}`} role="tooltip" x-placement="top">
                <div class="arrow"></div>
                <div class="tooltip-inner"> {this.props.children}</div></div> : <noscript />
        )
    }
}

Option.propTypes = $.extend({}, Component.propTypes, {
    children: PropTypes.node,
    className: PropTypes.string,             // className (based on mouse position)
    instancePrefix: PropTypes.string.isRequired,  // unique prefix for the ids (used for aria)
    isDisabled: PropTypes.bool,              // the option is disabled
    isFocused: PropTypes.bool,               // the option is focused
    isSelected: PropTypes.bool,              // the option is selected
    isShowValueToolTip: PropTypes.bool,           // the option is ShowValueToolTip
    onFocus: PropTypes.func,                 // method to handle mouseEnter on option element
    onSelect: PropTypes.func,                // method to handle click on option element
    onUnfocus: PropTypes.func,               // method to handle mouseLeave on option element
    option: PropTypes.object.isRequired,     // object that is base for that option
    optionIndex: PropTypes.number,           // index of the option, used to generate unique ids for aria
});

