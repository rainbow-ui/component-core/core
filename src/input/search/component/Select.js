import config  from 'config';
import UIInput from '../../../basic/Input';
import UIMessageHelper from '../../../dialog/MessageHelper';
import Param from '../../../basic/Param';
import OnChangeEvent from '../../../event/OnChangeEvent';
import I18nUtil from '../../../i18n/I18NUtil';
import { ValidatorContext, SessionContext, LocalContext ,ComponentContext} from "rainbow-desktop-cache";
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { CodeTableService } from "rainbow-desktop-codetable";
import { Util, DateUtil } from 'rainbow-desktop-tools';
import r18n from "../../../i18n/reactjs-tag.i18n";
import defaultArrowRenderer from './utils/defaultArrowRenderer';
import defaultClearRenderer from './utils/defaultClearRenderer';
import defaultFilterOptions from './utils/defaultFilterOptions';
import defaultMenuRenderer from './utils/defaultMenuRenderer';
import { compareUtils } from './utils/comparesUtils'
import Option from './Option';
import Value from './Value';
import UniqueId from '../../../basic/UniqueId';
// import { config } from 'webpack';

const stringifyValue = value =>
    typeof value === 'string'
        ? value
        : (value !== null && JSON.stringify(value)) || '';
 
const stringOrNode = PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
]);
const stringOrNumber = PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
]);
 
let instanceId = 1;

let selectBoxWidth = 0;

const shouldShowValue = (state, props) => {
    const { inputValue, isPseudoFocused, isFocused } = state;
    const { onSelectResetsInput } = props;
 
    if (!inputValue) return true;
 
    if (!onSelectResetsInput) {
        return !(!isFocused && isPseudoFocused || isFocused && !isPseudoFocused);
   }
 
    return false;
};
 
const shouldShowPlaceholder = (state, props, isOpen) => {
    const { inputValue, isPseudoFocused, isFocused } = state;
    const { onSelectResetsInput } = props;
 
    return !inputValue || !onSelectResetsInput && !isOpen && !isPseudoFocused && !isFocused;
};
 
/**
* Retrieve a value from the given options and valueKey
* @param {String|Number|Array} value   - the selected value(s)
* @param {Object}       props  - the Select component's props (or nextProps)
*/
const expandValue = (value, props) => {
    const valueType = typeof value;
    if (valueType !== 'string' && valueType !== 'number' && valueType !== 'boolean') return value;
    let { options, valueKey } = props;
    
    if (!options) return;

    let findObj = false;
    let curOptions;
    for (let i = 0; i < options.length; i++) {
        if (options[i][valueKey] == value) {
            findObj = true;
            curOptions = options[i];
            break;
            // return options[i];
        }
    }
    if (findObj) {
        return curOptions;
    } else {
        let migrationData = SessionContext.get('migrationData');
        let obj = {};
        let key = 'C_'+props.codeTableName+'{'+'code:'+props.model[props.property]+'}';

        let returnCodeTable = SessionContext.get(key)
        if (returnCodeTable) {
            return returnCodeTable;
        }
       
        if (migrationData && props.model[props.property] && props.codeTableName) {
            let url = UrlUtil.getConfigUrl("UI_API_GATEWAY_PROXY", "DD_API", "GET_VALUE_BY_CODEANDCODETABLENAME");
            let newObj = {codes:[]};
            $.ajax({
                method: "GET",
                url: url + '?codeTableName='+props.codeTableName+'&code='+ props.model[props.property],
                async: false,
                contentType: "application/json;charset=UTF-8",
                xhrFields: { withCredentials: true },
                crossDomain: true,
                beforeSend: function (xhr) {
                    let authorization = SessionContext.get("Authorization");
                    if (authorization) {
                        xhr.setRequestHeader("Authorization", 'Bearer ' + authorization.substr(13).split("&")[0])
                    }
                },
                success: function (data) {
                    if (data) {
                        obj.ConditionFields = data.ConditionFields;
                        obj.Status = data.Status;
                        obj.id = data.Id;
                        obj.text = data.Description; 
                        if (window.makePyCCIC) {
                            obj['pinyin'] = makePyCCIC(obj[props.labelKey] ? obj[props.labelKey] + '' : "")
                        }
                        SessionContext.put(key,obj)              
                    }
                }
            });
            return obj;
        }
        
    }
};
 
const handleRequired = (value, multi) => {
    if (!value) return true;
    return (multi ? value.length === 0 : Object.keys(value).length === 0);
};

const searchValue = (url,key,param) => {
    let result = [];
    $.ajax({
        method: "GET",
        url: url + '?'+ key + '=' + param,
        async: false,
        contentType: "application/json;charset=UTF-8",
        xhrFields: { withCredentials: true },
        crossDomain: true,
        beforeSend: function (xhr) {
            let authorization = SessionContext.get("Authorization");
            if (authorization) {
                xhr.setRequestHeader("Authorization", 'Bearer ' + authorization.substr(13).split("&")[0])
            }
        },
        success: (data) => {
            if(data && data.length && data.length > 0){
                data.forEach((item,index,arr)=>{
                    if(item && item.name && !item.text){
                        let temp = item.name;
                        delete arr[index].name;
                        arr[index].text = temp;
                    }
                })
            }
            result = data;
        }
    });
    return result;
}
 
export default class Select extends UIInput {
    constructor(props) {
        super(props);
        [
            'clearValue',
            'focusOption',
            'getOptionLabel',
            'handleInputBlur',
            'handleInputChange',
            'handleInputFocus',
            'handleInputValueChange',
            'handleKeyDown',
            'handleMenuScroll',
            'handleMouseDown',
            'handleMouseDownOnArrow',
            'handleMouseDownOnMenu',
            'handleTouchEnd',
            'handleTouchEndClearValue',
            'handleTouchMove',
            'handleTouchOutside',
            'handleTouchStart',
            'handleValueClick',
            'onOptionRef',
            'removeValue',
            'selectValue',
        ].forEach((fn) => this[fn] = this[fn].bind(this));
        this.updateSign = false
        this.state = {
            options: props.options,
            inputValue: '',
            isFocused: false,
            isOpen: false,
            isPseudoFocused: false,
            autoChooseFirstFlg: true,
            url:props.url,
            language:"zh_CN",
            hasRenderMenu: false
        };
        this.props.id = String(this.props.id);
        this.props.id = this.props.id && (this.props.id).indexOf('undefined') == -1 ? this.props.id : 'select_' + UniqueId.generateId();
        // this.autoChooseFirstFlg = true;
    }
 
    propsMap(props) {
        if (props.io == 'out') {
            props.enabled = false;
            props.disabled = true;
        }
        if(!props.options && props.optionUrl){
            let array = props.model ? props.property ? props.model[props.property] : [] : [];
            let param = '';
            if(typeof array == 'string'){
                param = array;
            }else if(typeof array == 'object'){
                param = array.join(',');
            }
            if('' !== param){
                props.options = searchValue(props.optionUrl,props.paramKey,param);
            }
        }
        if (props.codeTable) {
            props.options=[];
            if (Util.parseBool(this.props.showBlankOption)) {
                if (this.props.blankOptionId != '') {
                    props.options.push({ "id": props.blankOptionId, "text": props.blankOption });
                } else {
                    props.options.push({ "id": "", "text": props.blankOption });
                }
            }
            _.each(props.codeTable["codes"],option => {
                props.options.push(option);
            });
            if (this.props.processOption && !this.state.hasRenderMenu) {
               const options = processOptionData( props.options, this.props);
               props.options = [];
               props.options = props.options.concat(options);
            }
            this.state.hasRenderMenu = true;
        }
        else if (props.valueOption) {
            let vkey = props.valueKey
            let vlabel = props.labelKey
            props.options = _.map(props.valueOption, (e) => {
                let obj = {}
                obj[vkey] = e;
                obj[vlabel] = e;
                return obj
            })
        }
    }
 
    loadCodeTable(callback, nextProps) {
        let result
        let self = this;
        let props = this.props
        if (nextProps) {
            if (compareUtils.compareObjects(nextProps.codeTableName, self.props.codeTableName) && compareUtils.compareObjects(nextProps.conditionMap, self.props.conditionMap) && compareUtils.compareObjects(nextProps.options, self.options)) {
                nextProps.options = self.props.options
                self.props.isLoading = false
                callback(self, nextProps, self.props)
            } else {
                if (nextProps.options) self.props.options = nextProps.options
                this.handleOption(nextProps, props, true, callback);
            }
        } else {
            this.handleOption(nextProps, props, false, callback);
        }
    }
 
    handleOption(param1, param2, flag, callback) {
        let self = this;
        let param = flag ? param1 : param2;
        if (param.codeTableName||param.url) {
            self.getCodetable(param1, param2, flag, callback);
        } else if (param.options) {
            if (window.makePyCCIC) {
                param.options.forEach(element => {
                    element['pinyin'] = makePyCCIC(element[param.labelKey] ? element[param.labelKey] + '' : "")
                });
                self.choseFirst(self);
            }
        }
    }
 
    checkConditionMap(ConditionMap) {
        let flag = [];
        _.each(_.keys(ConditionMap), (key) => {
            if (!ConditionMap[key]) {
                flag.push(key);
            }
        });
        return !_.isEmpty(flag);
    }
 
    countCodetable(param, data) {
        const project_config = SessionContext.get("project_config");
        if (project_config) {
            const isCountCodetable = project_config["ui_count_codetable"];
            const countCodetableUrl = project_config["ui_count_codetable_url"];
            const resetCountCodetable = project_config["ui_reset_count_codetable"];
            if (Util.parseBool(resetCountCodetable)) {
                LocalContext.remove(param.codeTableName);
            }
 
            if (Util.parseBool(isCountCodetable) && countCodetableUrl) {
                const localCodetable = LocalContext.get(param.codeTableName);
                if (!localCodetable) {
                    const type = param["model"] ? param["model"]["@type"] : null;
                    let ModelName = null;
                    let ObjectCode = null;
                    if (!_.isEmpty(type)) {
                        const spType = type.split("-");
                        ModelName = spType[0];
                        ObjectCode = spType[1];
                    }
                    const _param = [
                        {
                            "CodeTableName": param.codeTableName,
                            "ModelName": ModelName ? ModelName : "",
                            "ObjectCode": ObjectCode ? ObjectCode : "",
                            "PageInfo": window.location.href,
                            "FieldName": param.property ? param["property"] : "",
                            "UseTime": DateUtil.getCurrentDateTime()
                        }
                    ];
                    AjaxUtil.call(countCodetableUrl, _param, { 'method': 'POST' }).then((data) => {
                        if (LocalContext.check(3)) {
                            LocalContext.put(param.codeTableName, '1');
                        } else {
                            LocalContext.clear();
                        }
                    });
                }
            }
        }
 
 
    }
 
    getCodetable(param1, param2, flag, callback) {
        let self = this;
        let param = flag ? param1 : param2;
        self.props.isLoading = true
        let postParam = { "CodeTableName": param.codeTableName };
        if (!this.checkConditionMap(param.conditionMap)) {
            postParam["ConditionMap"] = param.conditionMap;
            if(self.state.url){
                postParam["CodeTableUrl"] = {"url":self.state.url};
            }
            
            CodeTableService.getCodeTable(postParam).then(function (data) {
                // self.countCodetable(param, data);
                self.handleCodetable(data,self,param,param1, param2,callback);
            }, (err) => {
                UIMessageHelper.error(err, "Error Message", UIMessageHelper.POSITION_TOP_FULL_WIDTH);
            });
 
        } else {
            self.props.isLoading = false;
        }
 
 
    }
 
    handleCodetable(data,self,param,param1, param2,callback){
        const codeList = data.codes?data.codes:data;
        if (window.makePyCCIC) {
            if (codeList) {
                codeList.forEach(element => {
                    element['pinyin'] = makePyCCIC(element[param.labelKey] ? element[param.labelKey] + '' : "")
                });
            }
        }
        let commonData = data.common;
        if (commonData && commonData.length > 0) {
            codeList.unshift({ "id": "", "text": r18n.All, "disabled": true });
            _.each(commonData, (item) => {
                // item['pinyin'] = makePyCCIC(item[param.labelKey] ? item[param.labelKey] + '' : "")
                codeList.unshift(item);
            });
            codeList.unshift({ "id": "", "text": r18n.Common, "disabled": true });
        }
        if (Util.parseBool(self.props.showBlankOption)) {
            // codeList.unshift({ "id": "", "text": self.props.blankOption });
            if (self.props.blankOptionId != '') {
                codeList.unshift({ "id": self.props.blankOptionId, "text": self.props.blankOption });
            } else {
                codeList.unshift({ "id": "", "text": self.props.blankOption });
            }
        }
        self.props.options = codeList;
          if (self.props.processOption && !self.state.hasRenderMenu) {
               self.props.options = processOptionData(self.props.options, self.props);
            }
            self.state.hasRenderMenu = true;
        self.props.isLoading = false;
        self.setStatePromise({
            options: self.props.options
        }).then(() => {
            self.choseFirst(self);
            callback(self, param1, param2);
        })
    }
 
    componentWillMount() {
        // this.componentId = this.uuid()
        super.componentWillMount();
        this.propsMap(this.props);
        // this.autoChooseFirstFlg = true;
        const self = this;
        this._instancePrefix = `react-select-${(this.props.instanceId || ++instanceId)}-`;
        if (this.props.model) {
            if (!this.props.model[this.props.property] && this.props.defaultValue) {
                this.props.model[this.props.property] = this.props.defaultValue
            }
            if (this.props.model[this.props.property] && this.props.willChange) {
                this.props.onChange(new OnChangeEvent(self, null, Param.getParameter(this), this.props.model ? this.props.model[this.props.property] : {}, this.onEvent.newValue));
            }
            this.onEvent = { newValue: this.props.model[this.props.property], oldValue: null };
        }
        this.loadCodeTable(this.defaultValueFun, null)
        this.state.language = I18nUtil.getSystemI18N();
    }

    getNameForTest() {
        return this.componentName;
    }
 
    defaultValueFun(self, nextProps, props) {
        if (!nextProps) {
            if (props) {
                if (props.model) {
                    if (props.model[props.property]) {
                        const valueArray = self.getValueArray(props.model[props.property]);
                        if (!props.multi) {
                            if (!_.isEmpty(valueArray)) {
                                self.selectValue(valueArray[0])
                            }
                        }
                    }
                    if (props.model[props.property]) {
                        $('#' + self.componentId).val(props.model[props.property])
                    } else {
                        self.choseFirst(self)
                    }
                }
                else {
                    self.choseFirst(self)
                }
            }
        } else {
            if (nextProps.model) {
                if (nextProps.model[nextProps.property]) {
                    $('#' + self.componentId).val(nextProps.model[nextProps.property])
                }
            }
            else {
                self.choseFirst(self)
            }
        }
    }
    
    // mark
    choseFirst(self) {
        // if (Util.parseBool(self.props.autoChooseFirst) && (self.props.model[self.props.property].length === 0 || self.props.model[self.props.property] === '') ) {
        //     if (self.props.options) {
        //         // if (self.props.options.length == 1) {
        //             self.selectValue(self.props.options[0]);
        //             if(self.props.onChange&&self.state.autoChooseFirstFlg){
        //                 // self.state.autoChooseFirstFlg = false;
        //                 self.setState({autoChooseFirstFlg: false});
        //                 if(self.props.model&&self.props.model[self.props.property]!=self.props.options[0].id || self.props.changenext){
        //                     self.props.changenext = false;
        //                     if (!self.props.multi) {
        //                         self.props.model[self.props.property]=self.props.options[0].id;
        //                     }
                            
        //                     self.props.onChange(new OnChangeEvent(self, null, Param.getParameter(self), self.props.model ? self.props.model[self.props.property] : {}, self.onEvent.newValue));
        //                 }                    
        //             }  
        //         // }
        //     }
        // }
        if (self.props.options.length === 0 || !self.state.autoChooseFirstFlg) {return;}
        // 当选项数量大于1时，设置过自动选择第一个时，便不在进入此函数
        self.setState({autoChooseFirstFlg: false});
        let componentValue = self.getComponentValue();
        if (Util.parseBool(self.props.autoChooseFirst) && _.isEmpty(componentValue)) {
            // 设置未true，可以使用setValue中的onChange事件
            self.setState({isFocused: true},
                () => { self.selectValue(self.props.options[0]);}
            );            
        }
    }
    
    componentDidMount() {
        // super.componentDidMount();
        super.showLabelTooltip();
         // mark
        // if (['SearchCodeTable', 'uisearch'].indexOf(this.props.componentType) != -1) {
        //     console.log("come in")
        //     let selectObj = $('#for-input-' + this.componentId);
        //     let selectHeight = selectObj.height();
        //     selectObj.parent().css("height", selectHeight);
        //     selectBoxWidth = selectHeight;
        // }
        //-------------
        // if (['SearchCodeTable', 'uisearch'].indexOf(this.props.componentType) != -1) {
        //     let selectObj = $('#for-input-' + this.componentId);
        //     let selectDivHeight = $('#selectcontrol' + this.componentId).height()
        //     // 设置select初始高度,解决不居中问题
        //     selectObj.css("height", selectDivHeight + 2);
        // }
        
        $("[data-toggle='tooltip']").tooltip();
        if (typeof this.props.autofocus !== 'undefined' && typeof console !== 'undefined') {
            console.warn('Warning: The autofocus prop has changed to autoFocus, support will be removed after react-select@1.0');
        }
        if (this.props.autoFocus || this.props.autofocus) {
            this.focus();
        }
        if (this.props.io != "out") {
            this.initValidator();
            this.onChangeColorByEndorsement();
            $('#for-input-'+this.componentId).attr({'data-auto-test':this.getNameForTest()});
        }
        this.showValueTooltip();
        this.forceUpdate();
    }
 
    componentWillReceiveProps(nextProps) {
        this.propsMap(nextProps)
 
        let preValue = this.props.model ? this.props.property ? this.props.model[this.props.property] : '' : ''
        let nextValue = nextProps.model ? nextProps.property ? nextProps.model[nextProps.property] : '' : ''
        this.state.url=nextProps.url;
        if(nextProps.options&&this.props.options&&this.props.options.length==1&&nextProps.options.length==1){
            const nextOption = nextProps.options[0].id;
            const prevOption = this.props.options[0].id;
            if(nextOption!=prevOption){
                this.state.autoChooseFirstFlg=true;
            }
        }
        if (this.state.inputValue && preValue !== nextValue && nextProps.onSelectResetsInput) {
            if (this.updateSign) {
                this.props.model[this.props.property] = nextValue
                this.updateSign = false
            }
            this.setState({ inputValue: this.handleInputValueChange('')});
        }
 
        this.loadCodeTable(this.defaultValueFun, nextProps)
 
        // if (this.props.model) {
        //     if (nextValue && nextProps.resetInput) {
        //         this.props.model[this.props.property] = nextValue;
        //     }
        //     if (this.props.model[this.props.property]) {
        //         $('#' + this.componentId).val(this.props.model[this.props.property])
        //     }
        // }
 
    }
 
   
    componentDidUpdate(prevProps, prevState) {
        $("[data-toggle='tooltip']").tooltip();

        // mark
        // let selectObj = $('#for-input-' + this.componentId);
        // let multSelect = $("#" + this.componentId);
        // if (multSelect.height() > selectBoxWidth) {
        //     selectObj.parent().css("height",multSelect.height());
        //     selectBoxWidth = multSelect.height()
        // } else {
        //     selectObj.parent().css("height", selectObj.height());
        // }

        // focus to the selected option
        if (this.menu && this.focused && this.state.isOpen && !this.hasScrolledToOption) {
            const focusedOptionNode = ReactDOM.findDOMNode(this.focused);
            let menuNode = ReactDOM.findDOMNode(this.menu);
 
            const scrollTop = menuNode.scrollTop;
            const scrollBottom = scrollTop + menuNode.offsetHeight;
            const optionTop = focusedOptionNode.offsetTop;
            const optionBottom = optionTop + focusedOptionNode.offsetHeight;
 
            if (scrollTop > optionTop || scrollBottom < optionBottom) {
                menuNode.scrollTop = focusedOptionNode.offsetTop;
            }
 
            // We still set hasScrolledToOption to true even if we didn't
            // actually need to scroll, as we've still confirmed that the
            // option is in view.
            this.hasScrolledToOption = true;
        } else if (!this.state.isOpen) {
            this.hasScrolledToOption = false;
        }
 
        if (this._scrollToFocusedOptionOnUpdate && this.focused && this.menu) {
            this._scrollToFocusedOptionOnUpdate = false;
            const focusedDOM = ReactDOM.findDOMNode(this.focused);
            let menuDOM = ReactDOM.findDOMNode(this.menu);
            const focusedRect = focusedDOM.getBoundingClientRect();
            const menuRect = menuDOM.getBoundingClientRect();
            if (focusedRect.bottom > menuRect.bottom) {
                menuDOM.scrollTop = (focusedDOM.offsetTop + focusedDOM.clientHeight - menuDOM.offsetHeight);
            } else if (focusedRect.top < menuRect.top) {
                menuDOM.scrollTop = focusedDOM.offsetTop;
            }
        }
        if (this.props.scrollMenuIntoView && this.menuContainer) {
            const menuContainerRect = this.menuContainer.getBoundingClientRect();
            if (window.innerHeight < menuContainerRect.bottom + this.props.menuBuffer) {
                window.scrollBy(0, menuContainerRect.bottom + this.props.menuBuffer - window.innerHeight);
            }
        }
        if (prevProps.enabled !== this.props.enabled || prevProps.disabled !== this.props.disabled) {
            this.updateSign = true
            this.setState({ isFocused: false }); // eslint-disable-line react/no-did-update-set-state
            this.closeMenu();
        }
        if (prevState.isOpen !== this.state.isOpen) {
            this.toggleTouchOutsideEvent(this.state.isOpen);
            const handler = this.state.isOpen ? this.props.onOpen : this.props.onClose;
            handler && handler();
        }
        // if (this.props.model && this.props.property) {
        //     if($('#' + this.componentId).find('.Select-value').length==0&&this.props.io!='out'){
        //         if(_.isEmpty(this.props.defaultValue)){
        //             //this.props.model[this.props.property]=null;
        //         }
        //     };
        //     // $('#' + this.componentId).val(this.props.model[this.props.property])
        // }
        if (this.props.io != "out") {
            this.showValueTooltip();
            let object = $("#" + this.componentId);
            if (object.length != 0) {
                if (object.length > 0 && $._data(object[0], "events") == undefined) {
                    this.onChangeColorByEndorsement();
                }
            }
        }
        ComponentContext.put(this.props.id, this);
        this.clearValidationInfo();
        this.onEvent = { newValue: this.props.model?this.props.model[this.props.property]:null, oldValue: this.onEvent.newValue };
    }
 
    showValueTooltip() {
        let inputObj = $("#" + this.componentId);
        let inputDiv = $("#for-input-" + this.componentId);
        let text = $("#" + this.componentId + ' .Select-value-label').text();
        if ( Util.parseBool(this.props.isShowValueTooltip) || Util.parseBool(this.props.showValueTooltip)) {
            inputDiv.attr("data-original-title", text);
        }
        // if (this.props.componentType == "text"
        //     || this.props.componentType == "number"
        //     || this.props.componentType == "textarea") {
        //     if (inputObj.val() && Util.parseBool(this.props.showValueTooltip)) {
        //         inputDiv.attr("data-original-title", inputObj.val());
        //     } else {
        //         if (this.props.title) {
        //             inputDiv.attr("data-original-title", this.getI18n(this.props.title));
        //         } else {
        //             inputDiv.attr("data-original-title", "");
        //         }
        //     }
        // }
    }
 
    componentWillUnmount() {
        this.toggleTouchOutsideEvent(false);
    }
 
 
 
    componentWillUpdate(nextProps){
        this.propsMap(nextProps);
 
        if (this.props.model && this.props.property) {
            if(nextProps&&nextProps.model&&nextProps.model[nextProps.property]==null){
                // this.autoChooseFirstFlg = true;
                this.state.autoChooseFirstFlg = true;
            }
          
        }
        this.state.language = I18nUtil.getSystemI18N();
    }
 
    toggleTouchOutsideEvent(enabled) {
        if (enabled) {
            if (!document.addEventListener && document.attachEvent) {
                document.attachEvent('ontouchstart', this.handleTouchOutside);
            } else {
                document.addEventListener('touchstart', this.handleTouchOutside);
            }
        } else {
            if (!document.removeEventListener && document.detachEvent) {
                document.detachEvent('ontouchstart', this.handleTouchOutside);
            } else {
                document.removeEventListener('touchstart', this.handleTouchOutside);
            }
        }
    }
 
    setStatePromise(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        })
 
    }
 
    handleTouchOutside(event) {
        // handle touch outside on ios to dismiss menu
        if (this.wrapper && !this.wrapper.contains(event.target)) {
            this.closeMenu();
        }
    }
 
    focus() {
        if (!this.input) return;
        this.input.focus();
        // let value = this.getComponentValue();
        let value = this.onEvent.newValue;
        if ($('#' + this.componentId + ' .Select-clear-zone') && value && value.length != 0) {
            $('#' + this.componentId + ' .Select-clear-zone').css({ display: '' });
        }
    }
 
    blurInput() {
        if (!this.input) return;
        this.input.blur();
    }
 
    handleTouchMove() {
        // Set a flag that the view is being dragged
        this.dragging = true;
    }
 
    handleTouchStart() {
        // Set a flag that the view is not being dragged
        this.dragging = false;
    }
 
    handleTouchEnd(event) {
        // Check if the view is being dragged, In this case
        // we don't want to fire the click event (because the user only wants to scroll)
        if (this.dragging) return;
 
        // Fire the mouse events
        this.handleMouseDown(event);
    }
 
    handleTouchEndClearValue(event) {
        // Check if the view is being dragged, In this case
        // we don't want to fire the click event (because the user only wants to scroll)
        if (this.dragging) return;
 
        // Clear the value
        this.clearValue(event);
    }
 
    handleMouseDown(event) {
        // if the event was triggered by a mousedown and not the primary
        // button, or if the component is enable, ignore it.
        if (this.getDisabled() || (event.type === 'mousedown' && event.button !== 0)) {
            return;
        }
 
        // if (event.target.tagName === 'INPUT') {
        //     if (!this.state.isFocused) {
        //         this._openAfterFocus = this.props.openOnClick;
        //         this.focus();
        //     } else if (!this.state.isOpen) {
        //         this.setState({
        //             isOpen: true,
        //             isPseudoFocused: false,
        //         });
        //     }
 
        //     return;
        // }
        if (event.target.tagName == 'DIV' && event.target.className == 'Select-value' ||
            event.target.tagName == 'DIV' && event.target.className == 'Select-placeholder' ||
            event.target.tagName == 'INPUT' && event.target.className == 'Select-input') {
            this.focus();
            this.state.isFocused = true;
        }
 
        // prevent default event handlers
        event.preventDefault();
 
        // for the non-searchable select, toggle the menu
        if (!this.props.searchable) {
            // This code means that if a select is searchable, onClick the options menu will not appear, only on subsequent click will it open.
            this.focus();
            return this.setState({
                isOpen: !this.state.isOpen,
            });
        }
 
        if (this.state.isFocused) {
            // On iOS, we can get into a state where we think the input is focused but it isn't really,
            // since iOS ignores programmatic calls to input.focus() that weren't triggered by a click event.
            // Call focus() again here to be safe.
            this.focus();
 
            let input = this.input;
            let toOpen = true;
 
            if (typeof input.getInput === 'function') {
                // Get the actual DOM input if the ref is an <AutosizeInput /> component
                input = input.getInput();
            }
 
            // clears the value so that the cursor will be at the end of input when the component re-renders
            input.value = '';
 
            if (this._focusAfterClear) {
                toOpen = false;
                this._focusAfterClear = false;
            }
 
            // if the input is focused, ensure the menu is open
            this.setState({
                isOpen: toOpen,
                isPseudoFocused: false,
                focusedOption: null,
            });
        } else {
            // otherwise, focus the input and open the menu
            this._openAfterFocus = this.props.openOnClick;
            this.focus();
            this.setState({ focusedOption: null });
        }
    }
 
    handleMouseDownOnArrow(event) {
        // if the event was triggered by a mousedown and not the primary
        // button, or if the component is enable, ignore it.
        if (this.getDisabled() || (event.type === 'mousedown' && event.button !== 0)) {
            return;
        }
 
        if (this.state.isOpen) {
            // prevent default event handlers
            event.stopPropagation();
            event.preventDefault();
            // close the menu
            this.closeMenu();
        } else {
            // If the menu isn't open, let the event bubble to the main handleMouseDown
            this.setState({
                isOpen: true,
                isFocused: true,
            });
        }
    }
 
    handleMouseDownOnMenu(event) {
        // if the event was triggered by a mousedown and not the primary
        // button, or if the component is enable, ignore it.
        if (this.getDisabled() || (event.type === 'mousedown' && event.button !== 0)) {
            return;
        }
 
        event.stopPropagation();
        event.preventDefault();
 
        this._openAfterFocus = true;
        this.focus();
    }
 
    closeMenu() {
        if (this.props.onCloseResetsInput) {
            this.setState({
                inputValue: this.handleInputValueChange(''),
                isOpen: false,
                isPseudoFocused: this.state.isFocused && !this.props.multi,
            });
        } else {
            this.setState({
                isOpen: false,
                isPseudoFocused: this.state.isFocused && !this.props.multi
            });
        }
        this.hasScrolledToOption = false;
    }
 
    async handleInputFocus(event) {
        //-----------------------start 在获取焦点的时候加载数据-------------------------
        //这里改成await去获取codeTable内容，在willMount里面的方法注释掉，如果以后要换，注释这部分代码，打开willMount的内容
        // if (!this.props.options) {
        // await this.loadCodeTable(this.props.codeTableName, this.defaultValueFun, null)
        // }
        //-----------------------end 在获取焦点的时候加载数据---------------------------
 
        if (this.getDisabled()) return;
 
        let toOpen = this.state.isOpen || this._openAfterFocus || this.props.openOnFocus;
        toOpen = this._focusAfterClear ? false : toOpen; //if focus happens after clear values, don't open dropdown yet.
 
        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
 
        this.setState({
            isFocused: true,
            isOpen: !!toOpen,
        });
 
        this._focusAfterClear = false;
        this._openAfterFocus = false;
        // mark
        // let value = $('#' + this.componentId).val();
        // if ($('#' + this.componentId + ' .Select-clear-zone') && value.length !== 0) {
        //     $('#' + this.componentId + ' .Select-clear-zone').css({ display: '' });
        // }
    }
 
    handleInputBlur(event) {
        // The check for menu.contains(activeElement) is necessary to prevent IE11's scrollbar from closing the menu in certain contexts.
        if (this.menu && (this.menu === document.activeElement || this.menu.contains(document.activeElement))) {
            this.focus();
            return;
        }
 
        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
        let onBlurredState = {
            isFocused: false,
            isOpen: false,
            isPseudoFocused: false,
        };
        if (this.props.onBlurResetsInput) {
            onBlurredState.inputValue = this.handleInputValueChange('');
        }
        this.setState(onBlurredState);
        const obj = $('#' + this.componentId + ' .Select-clear-zone');
        if (obj) {
            obj.css({ display: 'none' });
        }
        this.showValueTooltip()
    }
 
    handleInputChange(event) {
        let newInputValue = event.target.value;
 
        if (this.state.inputValue !== event.target.value) {
            newInputValue = this.handleInputValueChange(newInputValue);
        }
      
        this.setState({
            inputValue: newInputValue,
            isOpen: true,
            isPseudoFocused: false,
        });
        const obj = $('#' + this.componentId + ' .Select-clear-zone');
 
        if (obj) {
            obj.css({ display: '' });
        }
    }
 
    setInputValue(newValue) {
        if (this.props.onInputChange) {
            let nextState = this.props.onInputChange(newValue);
            if (nextState != null && typeof nextState !== 'object') {
                newValue = '' + nextState;
            }
        }
        this.updateSign = true
        this.setState({
            inputValue: newValue
        });
    }
 
    handleInputValueChange(newValue) {
       
        if (this.props.onInputChange) {
            let nextState = this.props.onInputChange(newValue);
            // Note: != used deliberately here to catch undefined and null
            if (nextState != null && typeof nextState !== 'object') {
                newValue = '' + nextState;
            }
            const obj = $('#' + this.componentId + ' .Select-clear-zone');
 
            if (obj) {
                obj.css({ display: '' });
            }
        }
        return newValue;
    }
 
    handleKeyDown(event) {
        if (this.getDisabled()) return;
 
        if (typeof this.props.onInputKeyDown === 'function') {
            this.props.onInputKeyDown(event);
            if (event.defaultPrevented) {
                return;
            }
        }
 
        switch (event.keyCode) {
            case 8: // backspace
                if (!this.state.inputValue && this.props.backspaceRemoves) {
                    event.preventDefault();
                    this.popValue();
                }
                break;
            case 9: // tab
                if (event.shiftKey || !this.state.isOpen || !this.props.tabSelectsValue) {
                    break;
                }
                event.preventDefault();
                this.selectFocusedOption();
                break;
            case 13: // enter
                event.preventDefault();
                event.stopPropagation();
                if (this.state.isOpen) {
                    this.selectFocusedOption();
                } else {
                    this.focusNextOption();
                }
                break;
            case 27: // escape
                event.preventDefault();
                if (this.state.isOpen) {
                    this.closeMenu();
                    event.stopPropagation();
                } else if (this.props.clearable && this.props.escapeClearsValue) {
                    this.clearValue(event);
                    event.stopPropagation();
                }
                break;
            case 32: // space
                if (this.props.searchable) {
                    break;
                }
                event.preventDefault();
                if (!this.state.isOpen) {
                    this.focusNextOption();
                    break;
                }
                event.stopPropagation();
                this.selectFocusedOption();
                break;
            case 38: // up
                event.preventDefault();
                this.focusPreviousOption();
                break;
            case 40: // down
                event.preventDefault();
                this.focusNextOption();
                break;
            case 33: // page up
                event.preventDefault();
                this.focusPageUpOption();
                break;
            case 34: // page down
                event.preventDefault();
                this.focusPageDownOption();
                break;
            case 35: // end key
                if (event.shiftKey) {
                    break;
               }
                event.preventDefault();
                this.focusEndOption();
                break;
            case 36: // home key
                if (event.shiftKey) {
                    break;
                }
                event.preventDefault();
                this.focusStartOption();
                break;
            case 46: // delete
                if (!this.state.inputValue && this.props.deleteRemoves) {
                    event.preventDefault();
                    this.popValue();
                }
                break;
        }
    }
 
    handleValueClick(option, event) {
        if (!this.props.onValueClick) return;
        this.props.onValueClick(option, event);
    }
 
    handleMenuScroll(event) {
        if (!this.props.onMenuScrollToBottom) return;
        let { target } = event;
        if (target.scrollHeight > target.offsetHeight && (target.scrollHeight - target.offsetHeight - target.scrollTop) <= 0) {
            this.props.onMenuScrollToBottom();
        }
    }
 
    //todo:此处修改显示内容
    getOptionLabel(op) {
        if (Util.parseBool(this.props.showCode)) {
            return op[this.props.valueKey] + ' - ' + op[this.props.labelKey];
        }
        else {
            return op[this.props.labelKey];
        }
    }
 
    /**
    * Turns a value into an array from the given options
    * @param {String|Number|Array} value       - the value of the select input
    * @param {Object}      nextProps   - optionally specify the nextProps so the returned array uses the latest configuration
    * @returns {Array} the value of the select represented in an array
    */
    getValueArray(value, nextProps = undefined) {
        /** support optionally passing in the `nextProps` so `componentWillReceiveProps` updates will function as expected */
        const props = typeof nextProps === 'object' ? nextProps : this.props;

        if (props.multi) {
            if (typeof value === 'string') {
                value = value.split(props.delimiter);
            }
            if (!Array.isArray(value)) {
                if (value === null || value === undefined) return [];
                value = [value];
            }
            return value.map(value => expandValue(value, props)).filter(i => i);
        }
        const expandedValue = expandValue(value, props);

        return expandedValue ? [expandedValue] : [];
    }
 
    setValue(value) {
        if (this.props.autoBlur) {
            this.blurInput();
        }
        const self = this;
        
        // if (this.props.required) {
        // const required = handleRequired(value, this.props.multi);
        // this.setState({ required });
        // }
        if (this.props.simpleValue && value) {
            value = this.props.multi ? value.map(i => i[this.props.valueKey]).join(this.props.delimiter) : value[this.props.valueKey];
        }
        //todo:**********************
        // this.props.model[this.props.property] = value ? value[this.props.valueKey] ? value[this.props.valueKey] : value : value
        // $('#' + this.componentId).val(value ? value[this.props.valueKey] : '')
        this.updateSign = true
        this.setState({ inputValue: this.handleInputValueChange('') });
 
        if (this.props.model && this.props.property) {
            this.props.model[this.props.property] = []
 
            // let temp = _.cloneDeep(value)
            if (Array.isArray(value)) {
                value.forEach(element => {
                    // delete element["pinyin"]
                    if ( Util.parseBool(this.props.valueTostring)) {
                        this.props.model[this.props.property] += (element[this.props.valueKey]) + ',';
                    } else {
                        this.props.model[this.props.property].push(element[this.props.valueKey])
                    }
                });
            } else {
                this.props.model[this.props.property] = value ? value[this.props.valueKey] ? value[this.props.valueKey] : value[this.props.valueKey] == 0 ? value[this.props.valueKey] : value : value
            }
            $('#' + this.componentId).val(this.props.model[this.props.property])
        }
        this.clearValidationInfo();
        if (this.props.onChange && this.state.isFocused) {
            let valueArray = [];
            if (this.props.model) {
                valueArray = this.getValueArray(this.props.model[this.props.property]);
            }
            let options = this._visibleOptions = this.filterOptions(this.props.multi && this.props.removeSelected && this.props.io != 'out' ? valueArray : null);
            let isOpen = this.state.isOpen;
            if (this.props.multi && !options.length && valueArray.length && !this.state.inputValue) isOpen = false;
            const focusedOptionIndex = this.getFocusableOptionIndex(valueArray[0]);
     
            if (focusedOptionIndex !== null) {
                this._focusedOption = options[focusedOptionIndex];
            } else {
                this._focusedOption = null;
            }

            if (Util.parseBool(this.props.valueNoChangeNeedCallChangeEvent) && this.props.model[this.props.property] != this.onEvent.newValue) {
                this.props.onChange(new OnChangeEvent(self, null, Param.getParameter(this), this.props.model ? this.props.model[this.props.property] : {}, this.onEvent.newValue));
                this.onEvent = { newValue: this.props.model?this.props.model[this.props.property]:null, oldValue: this.onEvent.newValue };
                super.onChangeColorByEndorsement();
                this.setState({ isFocused: false });
            }
        }
    }
 
    clearValidationInfo() {
        const self = this;
        const selectObj = $("#" + self.componentId);
        
        if ($("form").data('bootstrapValidator') && !$("form").data('bootstrapValidator').isValidField(self.componentId) && selectObj.val() != '') {
            $("form").data('bootstrapValidator').enableFieldValidators(self.componentId,false);
            ValidatorContext.removeValidator(this.getValidationGroup(), self.componentId);
        }
        if (!Util.parseBool(self.props.required) && !Util.parseBool(this.props.enabled) && selectObj.closest(".form-group").hasClass("has-error")) {
            // selectObj.parent().parent().next().remove();
            selectObj.parents('.SearchCodeTable-textalign').find('i').remove();
            const errorInputObject = selectObj.closest(".form-group");
            if (errorInputObject.hasClass("has-error")) {
				errorInputObject.removeClass("has-error").addClass("has-success");
                // inputObject.parent().parent().parent().next().remove();
                selectObj.parents('.SearchCodeTable-textalign').find('small').remove();
                selectObj.closest(".input-group").css("border", "1px solid #cbcbcb");
            };

        }
    }

    selectValue(value) {
        // NOTE: we actually add/set the value in a callback to make sure the
        // input value is empty to avoid styling issues in Chrome
        if(this.props.searchUrl){
            this.props.options = this.props.options || [];
            this.props.options.push(value);
        }
        if (this.props.closeOnSelect) {
            this.hasScrolledToOption = false;
        }
        const updatedValue = this.props.onSelectResetsInput ? '' : this.state.inputValue;
        // const updatedValue = this.props.onSelectResetsInput ? '' : value.value;
        if (this.props.multi) {
            this.setState({
                focusedIndex: null,
                inputValue: this.handleInputValueChange(updatedValue),
                isOpen: !this.props.closeOnSelect,
            }, () => {
                const valueArray = this.getValueArray(this.props.model[this.props.property]);
                if (valueArray.some(i => i[this.props.valueKey] === value[this.props.valueKey])) {
                    this.removeValue(value);
                } else {
                    this.addValue(value);
                }
            });
        } else {
            this.setState({
                inputValue: this.handleInputValueChange(updatedValue),
                isOpen: !this.props.closeOnSelect,
                isPseudoFocused: this.state.isFocused,
            }, () => {
                this.setValue(value);
            });
        }
    }
 
    addValue(value) {
        let tempvalue = null
        if (this.props.model) {
            if (this.props.property) {
                tempvalue = this.props.model[this.props.property]
            }
        }
        let valueArray = this.getValueArray(tempvalue);
        const visibleOptions = this._visibleOptions.filter(val => !val.disabled);
        const lastValueIndex = visibleOptions.indexOf(value);
        this.setValue(valueArray.concat(value));
        if (visibleOptions.length - 1 === lastValueIndex) {
            // the last option was selected; focus the second-last one
            this.focusOption(visibleOptions[lastValueIndex - 1]);
        } else if (visibleOptions.length > lastValueIndex) {
            // focus the option below the selected one
            this.focusOption(visibleOptions[lastValueIndex + 1]);
        }
    }
 
    popValue() {
        let tempvalue = null
        if (this.props.model) {
            if (this.props.property) {
                tempvalue = this.props.model[this.props.property]
            }
        }
        let valueArray = this.getValueArray(tempvalue);
        if (!valueArray.length) return;
        if (valueArray[valueArray.length - 1].clearableValue === false) return;
        this.setValue(this.props.multi ? valueArray.slice(0, valueArray.length - 1) : null);
    }
    
    removeValue(value) {
        let tempvalue = null
        if (this.props.model) {
            if (this.props.property) {
                tempvalue = this.props.model[this.props.property]
            }
        }
        let valueArray = this.getValueArray(tempvalue);
        this.focus();
        this.state.isFocused = true;
        this.setValue(valueArray.filter(i => i[this.props.valueKey] !== value[this.props.valueKey]));
    }
 
    clearValue(event) {
        // if the event was triggered by a mousedown and not the primary
        // button, ignore it.
        if (event && event.type === 'mousedown' && event.button !== 0) {
            return;
        }
 
        event.preventDefault();
        if (this.props.onChange) {
            this.state.isFocused = true;
            if ($('#' + this.componentId + ' .Select-clear-zone')) {
                $('#' + this.componentId + ' .Select-clear-zone').css({ display: 'none' });
            }
        }
        this.setValue(this.getResetValue());
        this.setState({
            inputValue: this.handleInputValueChange(''),
            isOpen: false,
        }, this.focus);
 
        this._focusAfterClear = true;
    }
 
    getResetValue() {
        if (this.props.resetValue !== undefined) {
            return this.props.resetValue;
        } else if (this.props.multi) {
            return [];
        } else {
            return null;
        }
    }
 
    focusOption(option) {
        this.setState({
            focusedOption: option
        });
    }
 
    focusNextOption() {
        this.focusAdjacentOption('next');
    }
 
    focusPreviousOption() {
        this.focusAdjacentOption('previous');
    }
 
    focusPageUpOption() {
        this.focusAdjacentOption('page_up');
    }
 
    focusPageDownOption() {
        this.focusAdjacentOption('page_down');
    }
 
    focusStartOption() {
        this.focusAdjacentOption('start');
    }
 
    focusEndOption() {
        this.focusAdjacentOption('end');
    }
 
    focusAdjacentOption(dir) {
        const options = this._visibleOptions
            .map((option, index) => ({ option, index }))
            .filter(option => !option.option.disabled);
        this._scrollToFocusedOptionOnUpdate = true;
        if (!this.state.isOpen) {
            const newState = {
                focusedOption: this._focusedOption || (options.length ? options[dir === 'next' ? 0 : options.length - 1].option : null),
                isOpen: true,
            };
            if (this.props.onSelectResetsInput) {
                newState.inputValue = '';
            }
            this.setState(newState);
            return;
        }
        if (!options.length) return;
        let focusedIndex = -1;
        for (let i = 0; i < options.length; i++) {
            if (this._focusedOption === options[i].option) {
                focusedIndex = i;
                break;
            }
        }
        if (dir === 'next' && focusedIndex !== -1) {
            focusedIndex = (focusedIndex + 1) % options.length;
        } else if (dir === 'previous') {
            if (focusedIndex > 0) {
                focusedIndex = focusedIndex - 1;
            } else {
                focusedIndex = options.length - 1;
            }
        } else if (dir === 'start') {
            focusedIndex = 0;
        } else if (dir === 'end') {
            focusedIndex = options.length - 1;
        } else if (dir === 'page_up') {
            const potentialIndex = focusedIndex - this.props.pageSize;
            if (potentialIndex < 0) {
                focusedIndex = 0;
            } else {
                focusedIndex = potentialIndex;
            }
        } else if (dir === 'page_down') {
            const potentialIndex = focusedIndex + this.props.pageSize;
            if (potentialIndex > options.length - 1) {
                focusedIndex = options.length - 1;
            } else {
                focusedIndex = potentialIndex;
            }
        }
 
        if (focusedIndex === -1) {
            focusedIndex = 0;
        }
 
        this.setState({
            focusedIndex: options[focusedIndex].index,
            focusedOption: options[focusedIndex].option
        });
    }
 
    getFocusedOption() {
        return this._focusedOption;
    }
 
    selectFocusedOption() {
        if (this._focusedOption) {
            return this.selectValue(this._focusedOption);
        }
    }
 
    renderLoading() {
        if (!this.props.isLoading) return;
        return (
            <span className="Select-loading-zone" aria-hidden="true">
                <span className="Select-loading" />
            </span>
        );
    }
 
    renderValue(valueArray, isOpen) {
        let renderLabel = this.props.valueRenderer || this.getOptionLabel;
        let ValueComponent = this.props.valueComponent;
        let option;
        // if(this.props.placeholder!="请选择"){
        //     option = this.getI18n(this.props.placeholder);
        // }else{
        //     option = this.state.language=="zh_CN"?'请选择':"Please select";
        // }
        if (this.props.placeholder){
            option = this.props.placeholder // 如果要自己翻译，placeholder要传翻译好的数值
        } else {
            option = r18n.BlankOption
        }
        if (!valueArray.length) {
            const showPlaceholder = shouldShowPlaceholder(this.state, this.props, isOpen);
            $('#' + this.componentId).val([]);
            return showPlaceholder ? <div className="Select-placeholder">{option}</div> : null;
        }
        let onClick = this.props.onValueClick ? this.handleValueClick : null;
        
        // console.log('renderValue', renderLabel(valueArray[0]))
        if (this.props.multi) {
            return valueArray.map((value, i) => {
                return (
                    <ValueComponent
                        disabled={this.getDisabled() ? true : false || value.clearableValue === false}
                        id={`${this._instancePrefix}-value-${i}`}
                        instancePrefix={this._instancePrefix}
                        key={`value-${i}-${value[this.props.valueKey]}`}
                        onClick={onClick}
                        onRemove={this.removeValue}
                        placeholder={option}
                        value={value}
                    >
                        {renderLabel(value, i)}
                        <span className="Select-aria-only">&nbsp;</span>
                    </ValueComponent>
                );
            });
        } else if (shouldShowValue(this.state, this.props)) {
            if (isOpen) onClick = null;
 
            return (
                <ValueComponent
                    disabled={this.getDisabled() ? true : false}
                    id={`${this._instancePrefix}-value-item`}
                    instancePrefix={this._instancePrefix}
                    onClick={onClick}
                    placeholder={option}
                    value={valueArray[0]}
                >
                    {
                        renderLabel(valueArray[0])}
                </ValueComponent>
            );
        }
    }
 
    renderInput(valueArray, focusedOptionIndex) {
        const className = classNames('Select-input', this.props.inputProps.className);
        const isOpen = this.state.isOpen;
        const ariaOwns = classNames({
            [`${this._instancePrefix}-list`]: isOpen,
            [`${this._instancePrefix}-backspace-remove-message`]: this.props.multi
                && this.getDisabled() ? false : true
                && this.state.isFocused
                && !this.state.inputValue
        });
 
        let value = this.state.inputValue;
        if (value && !this.props.onSelectResetsInput && !this.state.isFocused) {
            // it hides input value when it is not focused and was not reset on select
            value = '';
        }
      
        const inputProps = {
            ...this.props.inputProps,
            'aria-activedescendant': isOpen ? `${this._instancePrefix}-option-${focusedOptionIndex}` : `${this._instancePrefix}-value`,
            'aria-describedby': this.props['aria-describedby'],
            'aria-expanded': '' + isOpen,
            'aria-haspopup': '' + isOpen,
            'aria-label': this.props['aria-label'],
            'aria-labelledby': this.props['aria-labelledby'],
            'aria-owns': ariaOwns,
            keyDownType: 'noShieldEnter',
            className: className,
            onBlur: this.handleInputBlur,
            onChange: this.handleInputChange,
            onFocus: this.handleInputFocus,
            ref: ref => this.input = ref,
            role: 'combobox',
            // required: this.state.required,
            tabIndex: this.props.tabIndex,
            value,
        };
 
        if (this.props.inputRenderer) {
            return this.props.inputRenderer(inputProps);
        }
 
        if (this.getDisabled() || (this.props.multi ? !this.props.multiSearchable : !this.props.searchable)) {
            const { ...divProps } = this.props.inputProps;
 
            const ariaOwns = classNames({
                [`${this._instancePrefix}-list`]: isOpen,
            });
            return (
 
                <div
                    {...divProps}
                    aria-expanded={isOpen}
                    aria-owns={ariaOwns}
                    aria-activedescendant={isOpen ? `${this._instancePrefix}-option-${focusedOptionIndex}` : `${this._instancePrefix}-value`}
                    aria-disabled={'' + this.getDisabled() ? true : false}
                    aria-label={this.props['aria-label']}
                    aria-labelledby={this.props['aria-labelledby']}
                    className={className}
                    onBlur={this.handleInputBlur}
                    onFocus={this.handleInputFocus}
                    ref={ref => this.input = ref}
                    role="combobox"
                    style={{ border: 0, width: 1, display: 'inline-block' }}
                    tabIndex={this.props.tabIndex || 0}
                />
            );
        }
 
        return (
            <div className={className} key="input-wrap" style={{ display: 'inline-block' }}>
                <input {...inputProps} />
 
            </div>
        );
    }
 
    renderClear() {
        let valueArray = [];
        if (this.props.model) {
            valueArray = this.getValueArray(this.props.model[this.props.property]);
        }
        if (!this.props.clearable
            // || !valueArray.length
            || this.getDisabled() ? true : false
            || this.props.isLoading) return;
        const ariaLabel = this.props.multi ? this.props.clearAllText : this.props.clearValueText;
        // const clear = this.props.clearRenderer();
 
        return (
            <span style={{ display: 'none' }}
                aria-label={ariaLabel}
                className={this.componentId + " Select-clear-zone"}
                onMouseDown={this.clearValue}
                onTouchEnd={this.handleTouchEndClearValue}
                onTouchMove={this.handleTouchMove}
                onTouchStart={this.handleTouchStart}
                title={ariaLabel}
            >
                {/* {clear} */}
                <span class="rainbow Clear deleteIcon" id={this.componentId + "_deleteIcon"} 
                    style={{color: '#BFBFBF', fontSize: '16px', lineHeight: '32px', marginRight: '10px'}}></span>
            </span>
        );
    }
 
    renderArrow() {
        if (!this.props.arrowRenderer) return;
 
        const onMouseDown = this.handleMouseDownOnArrow;
        const isOpen = this.state.isOpen;
        const arrow = this.props.arrowRenderer({ onMouseDown, isOpen });
 
        if (!arrow) {
            return null;
        }
 
        return (
            <span
                className="Select-arrow-zone"
                onMouseDown={onMouseDown}
            >
                {arrow}
            </span>
        );
    }
 
    filterOptions(excludeOptions) {
        const filterValue = this.state.inputValue;
        if (filterValue != '') {
            this.state.isFocused = true
        }
        const options = this.props.options || [];
        
        // search api
        if(this.props.searchUrl){
            let result = [];
            let valueArray = [];
            if (this.props.model) {
                valueArray = this.getValueArray(this.props.model[this.props.property]);
            }

            result = searchValue(this.props.searchUrl,this.props.paramKey,filterValue);
            // 筛选
            result = result.filter((item) => {
                let flag = true;
                if(valueArray.length && valueArray.length > 0){
                    valueArray.forEach((temp)=>{
                        flag = flag && temp.id !== item.id 
                    })
                }
                return flag;
            })
            return result;
        }

        // let t = options.find(c => c.id == '-1aa');
        // 全部
        // if (Util.parseBool(this.props.showBlankOption) && !t) {
        //     options.unshift({id: '-1aa', text: this.props.blankOption, pinyin:{}});
        // }
        if (this.props.filterOptions) {
            // Maintain backwards compatibility with boolean attribute
            const filterOptions = typeof this.props.filterOptions === 'function'
                ? this.props.filterOptions
                : defaultFilterOptions;
 
            return filterOptions(
                options,
                filterValue,
                excludeOptions,
                {
                    filterOption: this.props.filterOption,
                    ignoreAccents: this.props.ignoreAccents,
                    ignoreCase: this.props.ignoreCase,
                    labelKey: this.props.labelKey,
                    matchPos: this.props.matchPos,
                    matchProp: this.props.matchProp,
                    trimFilter: this.props.trimFilter,
                    valueKey: this.props.valueKey,
                }
            );
        } else {
            return options;
        }
    }
 
    onOptionRef(ref, isFocused) {
        if (isFocused) {
            this.focused = ref;
        }
    }
 
    renderMenu(options, valueArray, focusedOption, isOpen) {
        if (options && options.length) {
            let idx = options.findIndex((item) => {
                return item.Code == '999' || item.Status == 'invalid'
            });
            if (idx != -1) {
                options.splice(idx,1);
            }
            return this.props.menuRenderer({
                focusedOption,
                focusOption: this.focusOption,
                inputValue: this.state.inputValue,
                instancePrefix: this._instancePrefix,
                showValueToolTip: this.props.showValueToolTip,
                labelKey: this.props.labelKey,
                onFocus: this.focusOption,
                onOptionRef: this.onOptionRef,
                onSelect: this.selectValue,
                optionClassName: this.props.optionClassName,
                optionComponent: this.props.optionComponent,
                optionRenderer: this.props.optionRenderer || this.getOptionLabel,
                options,
                removeValue: this.removeValue,
                selectValue: this.selectValue,
                valueArray,
                valueKey: this.props.valueKey,
            });
        } else if (this.props.noResultsText) {
            return (
                <div className="Select-noresults">
                    {this.props.noResultsText}
                </div>
            );
        } else {
            return null;
        }
    }
 
    renderHiddenField(valueArray) {
        if (!this.props.name) return;
        if (this.props.joinValues) {
            let value = valueArray.map(i => stringifyValue(i[this.props.valueKey])).join(this.props.delimiter);
            return (
                <input
                    disabled={this.getDisabled() ? true : false}
                    name={this.props.name}
                    ref={ref => this.value = ref}
                    type="hidden"
                    value={value}
                />
            );
        }
        return valueArray.map((item, index) => (
            <input
                disabled={this.getDisabled() ? true : false}
                key={`hidden.${index}`}
                name={this.props.name}
                ref={`value${index}`}
                type="hidden"
                value={stringifyValue(item[this.props.valueKey])}
            />
        ));
    }
 
    getFocusableOptionIndex(selectedOption) {
        const options = this._visibleOptions;
        if (!options.length) return null;
 
        const valueKey = this.props.valueKey;
        let focusedOption = this.state.focusedOption || selectedOption;
        if (focusedOption && !focusedOption.disabled) {
            let focusedOptionIndex = -1;
            options.some((option, index) => {
                const isOptionEqual = option[valueKey] === focusedOption[valueKey];
                if (isOptionEqual) {
                    focusedOptionIndex = index;
                }
                return isOptionEqual;
            });
            if (focusedOptionIndex !== -1) {
                return focusedOptionIndex;
            }
        }
 
        for (let i = 0; i < options.length; i++) {
            if (!options[i].disabled) return i;
        }
        return null;
    }
 
    hideDrop() {
        // if ($('#' + this.componentId).parents('td') && $('#' + this.componentId).parents('td').length > 0) {
        //      $('#for-input-' + this.componentId).css({ position: 'relative',left: '0px',  marginTop:'0px' });          
        // }
    }
    // mark
    renderOuter(options, valueArray, focusedOption, isOpen) { 

        // if ($('#' + this.componentId).parents('td') && $('#' + this.componentId).parents('td').length > 0) {
        //     // let l = $('#for-input-' + this.componentId).position().left;
        //     // let w = $('#' + this.componentId).parents('td').attr('realwidth');
        //     // let marginTop = this.props.overflowMarginTop;
        //     // let curObjH = $('#for-input-' + this.componentId).height() / 2;
        //     // $('#for-input-' + this.componentId).css({ position: 'absolute' ,left: l + 'px', marginTop: '-' + (curObjH / 2) + 'px'});
        //     // $('#for-input-' + this.componentId).css({ position: 'absolute', left: l + 'px'});
        //     // let l = $('#selectcontrol').position().left;
        //     // $(`#${this._instancePrefix}-list`).css({ width: '400px'})
        // }
 
        let menu = this.renderMenu(options, valueArray, focusedOption, isOpen);
        if (!menu) {
            return null;
        }
        // 挡住cascade时要关掉组件，并改掉cascade z-index
        if (isOpen && $('.cascade-textalign') &&  $('.cascade-textalign').children('div') &&  $('.cascade-textalign').children('div').length > 0 && $('.cascade-textalign').children('div').attr('needreposition')) {
            $($('.cascade-textalign').children('div')[0]).css({zIndex:'unset',position: 'relative', left: '0px'});
            $($('.cascade-textalign .content')[0]).hide()
        }
        let inputDiv = $("#for-input-" + this.componentId);
        inputDiv.tooltip('hide');
        return (
            // mark
            <div ref={ref => this.menuContainer = ref} className="Select-menu-outer" style={{display:isOpen ? '': 'none', position: 'absolute', minWidth: 'auto'}, this.getLocation()}>
                <div
                    className="Select-menu"
                    id={`${this._instancePrefix}-list`}
                    onMouseDown={this.handleMouseDownOnMenu}
                    onScroll={this.handleMenuScroll}
                    ref={ref => this.menu = ref}
                    role="listbox"
                    style={this.props.menuStyle, this.getOptionWidth()}
                    tabIndex={-1}
                >
                    {menu}
                </div>
            </div>
        );
    }
    // 获取下拉框的宽度
    getOptionWidth() {
        let obj = $('#selectcontrol' + this.componentId);
        let width = obj.width();
        // 下拉框的宽度
        return {width: width};
    }
    // 获取正确位置
    getLocation() {
        
        let obj = $('#selectcontrol' + this.componentId);
        let left = obj.position().left;
        let top = obj.position().top;
        let height = obj.height();
        
        return {left: left + 'px', top: (top + height) + 'px', minWidth: 'auto'}
    }
    renderInputComponent() {
        let valueArray = [];
        if (this.props.model) {
            valueArray = this.getValueArray(this.props.model[this.props.property]);
        }
        let options = this._visibleOptions = this.filterOptions(this.props.multi && this.props.removeSelected && this.props.io != 'out' ? valueArray : null);
        if (options && options.length > 0) {
            let idx = options.findIndex((item) => {
                return item.Code == '999' || item.Status == 'invalid'
            });
            if (idx != -1) {
                options.splice(idx,1);
            }
        } 
        let isOpen = this.state.isOpen;
        if (this.props.multi && !options.length && valueArray.length && !this.state.inputValue) isOpen = false;
        const focusedOptionIndex = this.getFocusableOptionIndex(valueArray[0]);
 
        let focusedOption = null;
        if (focusedOptionIndex !== null) {
            focusedOption = this._focusedOption = options[focusedOptionIndex];
        } else {
            focusedOption = this._focusedOption = null;
        }
        // options.unshift({id:-1,text:'全部'})
        let className = classNames('Select', 'input-group', this.props.className, {
            'has-value': valueArray.length,
            'is-clearable': this.props.clearable,
            'is-disabled': this.getDisabled() ? true : false,
            'is-focused': this.state.isFocused,
            'is-loading': this.props.isLoading,
            'is-open': isOpen,
            'is-pseudo-focused': this.state.isPseudoFocused,
            'is-searchable': this.props.multi ? this.props.multiSearchable : this.props.searchable,
            'Select--multi': this.props.multi,
            'Select--rtl': this.props.rtl,
            'Select--single': !this.props.multi,
        });
 
        let removeMessage = null;
        if (this.props.multi &&
            this.getDisabled() ? false : true &&
            valueArray.length &&
            !this.state.inputValue &&
            this.state.isFocused &&
            this.props.backspaceRemoves) {
            removeMessage = (
                <span id={`${this._instancePrefix}-backspace-remove-message`} className="Select-aria-only" aria-live="assertive">
                    {this.props.backspaceToRemoveMessage.replace('{label}', valueArray[valueArray.length - 1][this.props.labelKey])}
                </span>
            );
        }
 
        if (this.props.io == 'out') {
            if (this.props.options) {
                let showLabel = []
                let ids = []
                if (this.props.model && this.props.property) {
                    if (Array.isArray(this.props.model[this.props.property])) {
 
                        ids = this.props.model[this.props.property]
                    }
                    else {
                        if (this.props.model[this.props.property] != null && this.props.model[this.props.property] != undefined) {
                            ids.push(this.props.model[this.props.property])
                        }
                    }
                }
                let self = this
                if (ids.length > 0) {
                    ids.forEach(eleid => {
                        options.forEach(eleOption => {
                            if (eleOption[self.props.valueKey] == eleid) {
                                if (Util.parseBool(this.props.showCode)) {
                                    showLabel.push(eleOption[self.props.valueKey] + '-' + eleOption[self.props.labelKey])
                                }
                                else {
                                    showLabel.push(eleOption[self.props.labelKey])
                                }
                                return
                            }
                        });
                    });
                }
                if(Util.parseBool(this.props.isShowValueTooltip)){
                    return (
                        <div data-toggle="tooltip" data-placement="bottom" data-original-title={showLabel.join("\n")} style={{textOverflow: (this.props.needellipsis ? 'ellipsis' : ''), overflow: this.props.needellipsis ? 'hidden' : '' }}>
                            {showLabel.join(",")}
                        </div>
                    )
                }else{
                    return (
                        <div style={{textOverflow: (this.props.needellipsis ? 'ellipsis' : ''), overflow: this.props.needellipsis ? 'hidden' : '' }}>
                            {showLabel.join(",")}
                        </div>
                    )
                }
            }
        }
        else {
            // 自带验证
            if (!this.props.validator) {
                if (!Util.parseBool(this.props.required)) {
                    this.clearValidationInfo()
                    ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);                   
                } else {
                    this.initValidator();
                }
            }
            return (
                <div ref={ref => this.wrapper = ref} className={className} style={this.props.wrapperStyle, {position: 'static'}} id={this.componentId } name={this.getName()} required={this.props.required} data-component="select">
                    {this.renderHiddenField(valueArray)}
                    <div ref={ref => this.control = ref}
                        className="Select-control"
                        id={`selectcontrol${this.componentId}`}   
                        onKeyDown={this.handleKeyDown}
                        onMouseDown={this.handleMouseDown}
                        onTouchEnd={this.handleTouchEnd}
                        onTouchMove={this.handleTouchMove}
                        onTouchStart={this.handleTouchStart}
                        style={this.props.style, {position: 'relative'}}
                    >
                        <span className="Select-multi-value-wrapper" id={`${this._instancePrefix}-value`}>
                            {this.renderValue(valueArray, isOpen)}
                            {this.renderInput(valueArray, focusedOptionIndex)}
                        </span>
                        {removeMessage}
                        {this.renderLoading()}
                        {this.renderClear()}
                        {this.renderArrow()}
                    </div>
                    {/* {this.renderOuter(options, valueArray, focusedOption, isOpen)} */}
                    {isOpen ? this.renderOuter(options, valueArray, focusedOption, isOpen) : this.hideDrop()}
                </div>
            );
        }
    }
 
    uuid() {
        let s = [];
        let hexDigits = "0123456789abcdef";
        for (let i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";
 
        let uuid = s.join("");
        return uuid;
    }
}
 
Select.propTypes = $.extend({}, UIInput.propTypes, {
    'aria-describedby': PropTypes.string, // html id(s) of element(s) that should be used to describe this input (for assistive tech)
    'aria-label': PropTypes.string, // aria label (for assistive tech)
    'aria-labelledby': PropTypes.string, // html id of an element that should be used as the label (for assistive tech)
    arrowRenderer: PropTypes.func, // create the drop-down caret element
    autoBlur: PropTypes.bool, // automatically blur the component when an option is selected
    autoFocus: PropTypes.bool, // autofocus the component on mount
    autofocus: PropTypes.bool, // deprecated; use autoFocus instead
    autosize: PropTypes.bool, // whether to enabled autosizing or not
    autoChooseFirst: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    backspaceRemoves: PropTypes.bool, // whether backspace removes an item if there is no text input
    backspaceToRemoveMessage: PropTypes.string, // message to use for screenreaders to press backspace to remove the current item - {label} is replaced with the item label
    className: PropTypes.string, // className for the outer element
    clearAllText: stringOrNode, // title for the "clear" control when multi: true
    clearRenderer: PropTypes.func, // create clearable x element
    clearValueText: stringOrNode, // title for the "clear" control
    clearable: PropTypes.bool, // should it be possible to reset value
    closeOnSelect: PropTypes.bool, // whether to close the menu when a value is selected
    codeTable: PropTypes.any,
    codeTableName: PropTypes.string,
    itemIndex: PropTypes.string,
    conditionMap: PropTypes.object,
    deleteRemoves: PropTypes.bool, // whether delete removes an item if there is no text input
    delimiter: PropTypes.string, // delimiter to use to join multiple values for the hidden field value
    enabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]), // whether the Select is enabled or not
    escapeClearsValue: PropTypes.bool, // whether escape clears the value when the menu is closed
    filterOption: PropTypes.func, // method to filter a single option (option, filterString)
    filterOptions: PropTypes.any, // boolean to enabled default filtering or function to filter the options array ([options], filterString, [values])
    ignoreAccents: PropTypes.bool, // whether to strip diacritics when filtering
    ignoreCase: PropTypes.bool, // whether to perform case-insensitive filtering
    inputProps: PropTypes.object, // custom attributes for the Input
    inputRenderer: PropTypes.func, // returns a custom input component
    instanceId: PropTypes.string, // set the components instanceId
    isLoading: PropTypes.bool, // whether the Select is loading externally or not (such as options being loaded)
    joinValues: PropTypes.bool, // joins multiple values into a single form field with the delimiter (legacy mode)
    labelKey: PropTypes.string, // path of the label value in option objects
    matchPos: PropTypes.string, // (any|start) match the start or entire string when filtering
    matchProp: PropTypes.string, // (any|label|value) which option property to filter on
    menuBuffer: PropTypes.number, // optional buffer (in px) between the bottom of the viewport and the bottom of the menu
    menuContainerStyle: PropTypes.object, // optional style to apply to the menu container
    menuRenderer: PropTypes.func, // renders a custom menu with options
    menuStyle: PropTypes.object, // optional style to apply to the menu
    model: PropTypes.object.isRequired,
    multi: PropTypes.bool, // multi-value input
    name: PropTypes.string, // generates a hidden <input /> tag with this field name for html forms
    noResultsText: stringOrNode, // placeholder displayed when there are no matching search results
    onBlur: PropTypes.func, // onBlur handler: function (event) {}
    onBlurResetsInput: PropTypes.bool, // whether input is cleared on blur
    onChange: PropTypes.func, // onChange handler: function (newValue) {}
    onClose: PropTypes.func, // fires when the menu is closed
    onCloseResetsInput: PropTypes.bool, // whether input is cleared when menu is closed through the arrow
    onFocus: PropTypes.func, // onFocus handler: function (event) {}
    onInputChange: PropTypes.func, // onInputChange handler: function (inputValue) {}
    onInputKeyDown: PropTypes.func, // input keyDown handler: function (event) {}
    onMenuScrollToBottom: PropTypes.func, // fires when the menu is scrolled to the bottom; can be used to paginate options
    onOpen: PropTypes.func, // fires when the menu is opened
    onSelectResetsInput: PropTypes.bool, // whether input is cleared on select (works only for multiselect)
    resetInput: PropTypes.bool, // whether input is cleared on select (works only for multiselect)
    onValueClick: PropTypes.func, // onClick handler for value labels: function (value, event) {}
    openOnClick: PropTypes.bool, // boolean to control opening the menu when the control is clicked
    openOnFocus: PropTypes.bool, // always open options menu on focus
    valueOption: PropTypes.array,
    optionClassName: PropTypes.string, // additional class(es) to apply to the <Option /> elements
    optionComponent: PropTypes.func, // option component to render in dropdown
    optionRenderer: PropTypes.func, // optionRenderer: function (option) {}
    options: PropTypes.array, // array of options
    pageSize: PropTypes.number, // number of entries to page when using page up/down keys
    placeholder: stringOrNode, // field placeholder, displayed when there's no value
    property: PropTypes.string.isRequired,
    removeSelected: PropTypes.bool, // whether the selected option is removed from the dropdown on multi selects
    resetValue: PropTypes.any, // value to use when you clear the control
    readOnly: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    rtl: PropTypes.bool,                                    // set to true in order to use react-select in right-to-left direction
    scrollMenuIntoView: PropTypes.bool, // boolean to enable the viewport to shift so that the full menu fully visible when engaged
    searchable: PropTypes.bool, // whether to enable searching feature or not
    multiSearchable: PropTypes.bool,
    showCode: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    simpleValue: PropTypes.bool, // pass the value to onChange as a simple value (legacy pre 1.0 mode), defaults to false
    style: PropTypes.object, // optional style to apply to the control
    tabIndex: stringOrNumber, // optional tab index of the control
    tabSelectsValue: PropTypes.bool, // whether to treat tabbing out while focused to be value selection
    trimFilter: PropTypes.bool, // whether to trim whitespace around filter value
    defaultValue: PropTypes.any, // initial field value
    valueComponent: PropTypes.func, // value component to render
    valueKey: PropTypes.string, // path of the label value in option objects
    valueRenderer: PropTypes.func, // valueRenderer: function (option) {}
    wrapperStyle: PropTypes.object, // optional style to apply to the component wrapper
    showValueToolTip: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    showBlankOption: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]), // 是否显示空白选项
    overflowMarginTop: PropTypes.string,
    blankOption: PropTypes.string,// 空白选项label
    willChange: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]), // 绑值时触发onchange事件
    needellipsis: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    processOption: PropTypes.string,// 特别处理options事件
    blankOptionId: PropTypes.string,
    changenext: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]), // 是否刷新下一个
    valueNoChangeNeedCallChangeEvent: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    searchUrl: PropTypes.string,
    optionUrl: PropTypes.string,
    paramKey: PropTypes.string,
});
 
Select.defaultProps = $.extend({}, UIInput.defaultProps, {
    arrowRenderer: defaultArrowRenderer,
    autosize: false,
    autoChooseFirst: config.DEFAULT_SELECT_AUTOCHOOSEFIRST? Util.parseBool(config.DEFAULT_SELECT_AUTOCHOOSEFIRST): false,
    backspaceRemoves: true,
    backspaceToRemoveMessage: 'Press backspace to remove {label}',
    clearable: true,
    clearAllText: 'Clear all',
    clearRenderer: defaultClearRenderer,
    clearValueText: 'Clear value',
    closeOnSelect: true,
    deleteRemoves: true,
    delimiter: ',',
    enabled: true,
    escapeClearsValue: true,
    filterOptions: defaultFilterOptions,
    ignoreAccents: true,
    ignoreCase: true,
    inputProps: {},
    isLoading: false,
    joinValues: false,
    labelKey: 'text',
    matchPos: 'any',
    matchProp: 'any',
    menuBuffer: 0,
    menuRenderer: defaultMenuRenderer,
    multi: false,
    itemIndex: null,
    noResultsText: r18n.NotEmpty,
    onBlurResetsInput: true,
    onCloseResetsInput: true,
    onSelectResetsInput: true,
    resetInput: true,
    openOnClick: true,
    optionComponent: Option,
    pageSize: 5,
    placeholder: r18n.BlankOption,
    removeSelected: true,
    rtl: false,
    scrollMenuIntoView: true,
    searchable: true,
    showCode: "false",
    simpleValue: false,
    tabSelectsValue: true,
    trimFilter: true,
    valueComponent: Value,
    valueKey: 'id',
    componentType: 'SearchCodeTable',
    showValueToolTip: false, // Select下拉框里选项的tooltip
    isShowValueTooltip: config.SHOW_VALUE_TOOLTIP ? Util.parseBool(config.SHOW_VALUE_TOOLTIP) : false,
    showBlankOption: false,
    blankOption: r18n.BlankOption,
    overflowMarginTop: '-14px',
    multiSearchable: true,
    willChange:false,
    needellipsis: true,
    blankOptionId: '',
    changenext: false,
    valueNoChangeNeedCallChangeEvent: true,
    paramKey: 'id'
});
