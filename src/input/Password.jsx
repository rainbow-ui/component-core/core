import Input from "../basic/Input";
import { Util } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';
import { ValidatorContext } from 'rainbow-desktop-cache';

export default class Password extends Input {

    constructor(props) {
        super(props);

        this.state = {
            showPassword: false
        };
    }

    renderInput() {
        if (!Util.parseBool(this.props.showPassword)) {
            return (
                <div className="input-group">
                    {this.renderPrefixIcon()}
                    {this.renderInputPassword()}
                    {this.renderDeleteIcon()}
                    {this.renderSuffixIcon()}
                </div>
            );
        } else if (this.state.showPassword) {
            return (
                <div className="input-group">
                    {this.renderInputText()}
                    {this.renderDeleteIcon()}
                    {this.renderIcon()}
                </div>
            );
        } else {
            return (
                <div className="input-group">
                    {this.renderInputPassword()}
                    {this.renderDeleteIcon()}
                    {this.renderIcon()}
                </div>);
        }
    }
    
    renderDeleteIcon() {
        if (Util.parseBool(this.props.enabled) && Util.parseBool(this.props.showDeleteIcon)) {
            return (
                // <button id={this.componentId + "_deleteIcon"} class="input-remove" type="button"
                //     style={{ display: "none", right:'34px' }} >x</button>
                <span class="rainbow Clear deleteIcon" id={this.componentId + "_deleteIcon"} 
                style={{display: 'none'}}></span>
            )
        }
    }

    renderInputText() {
        return (
            <input id={this.componentId} name={this.getName()} type="text" className={this.props.className + " form-control"} style={this.props.style}
                placeholder={this.props.placeHolder} title={this.getI18n(this.props.title)} data-auto-test={this.getNameForTest()} />
        );
    }

    renderInputPassword() {
        return (

            <input id={this.componentId} name={this.getName()} type="password" className={this.props.className + " form-control"}
                placeholder={this.props.placeHolder} title={this.getI18n(this.props.title)} data-auto-test={this.getNameForTest()} />
        );
    }

    renderIcon() {
        if (this.state.showPassword) {
            return (
                <span className="input-group-addon pickerposition">
                    <span className="glyphicon glyphicon-eye-open" onClick={this.onCloseShowPassword.bind(this)}
                        style={{ cursor: "pointer" }} />
                </span>
            );
        } else {
            return (
                <span className="input-group-addon pickerposition">
                    <span className="glyphicon glyphicon-eye-close" onClick={this.onOpenShowPassword.bind(this)}
                        style={{ cursor: "pointer" }} />
                </span>
            );
        }
    }

    renderPrefixIcon() {
        if (this.props.prefixIcon != undefined) {
            return (
                <span className="input-group-addon fixleftposition">
                    <span id={this.componentId + "_prefixIcon"} className={this.props.prefixIcon} style={{ cursor: "pointer" }} />
                </span>
            );
        }
        return <noscript />;
    }

    renderSuffixIcon() {
        if (this.props.suffixIcon != undefined) {
            return (
                <span className="input-group-addon fixalliconposition">
                    <span id={this.componentId + "_suffixIcon"} className={this.props.suffixIcon} style={{ cursor: "pointer" }} />
                </span>
            );
        }
        return <noscript />;
    }

    onOpenShowPassword() {
        this.setState({ showPassword: true });
    }

    onCloseShowPassword() {
        this.setState({ showPassword: false });
    }

    componentDidUpdate(nextProps, nextState) {
        super.componentDidUpdate(nextProps, nextState);
        this.clearValidationInfo(nextProps);
    }

    clearValidationInfo(nextProps) {
        const inputObject = $("#" + this.componentId);
		if ($("form").data('bootstrapValidator') && !$("form").data('bootstrapValidator').isValidField(this.componentId) && inputObject.val() != '') {
            $("form").data('bootstrapValidator').enableFieldValidators(this.componentId,false);
            ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
        }
    }

};


/**@ignore
 * Password component prop types
 */
Password.propTypes = $.extend({}, Input.propTypes, {
    enabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    componentType: PropTypes.string,
    showPassword: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    showDeleteIcon: PropTypes.bool,
    prefixIcon: PropTypes.string,
	suffixIcon: PropTypes.string,
});

/**@ignore
 * Get Password component default props
 */
Password.defaultProps = $.extend({}, Input.defaultProps, {
    showPassword: false,
    showDeleteIcon: true,
    componentType: "password",
});
