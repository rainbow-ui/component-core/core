import {ValidatorContext} from "rainbow-desktop-cache";
import r18n from "../i18n/reactjs-tag.i18n";

let EmailValidator = {
	
	validate: function(component){
		ValidatorContext.putValidator(component.getValidationGroup(), component.componentId, {
			emailAddress: {
				message: r18n.EmailValidator
			}
		});
	}
	
};

module.exports = EmailValidator;
