import { Spinner } from '../basic/Spin';

module.exports = {
	

    show: function(id){
        new Spinner().spin(document.getElementById(id));
    },

    hide: function(id){
        if ($('#'+id).prev('.spinner').length > 0){
            $('#'+id).prev('.spinner').remove();
        } else if ($('#'+id).children('.spinner').length > 0){
            $('#'+id).children('.spinner').remove();
        }
    }


}
