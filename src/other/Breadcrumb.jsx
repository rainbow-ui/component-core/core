import Component from "../basic/Component";
export default class Breadcrumb extends Component {
   constructor(props) {
        super(props);
    }

    renderComponent() {
        return (
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb" id={this.props.id} style={{listStyle:'none', padding:'4px 0px 0px 15px',marginBottom:'0px', background:'none', borderRadius:'0px'}}>
                    {this.renderBreadcrumbItem(this)}
                </ol>
            </nav>
        );
    }
    renderBreadcrumbItem(component) {
         let children = component.props.children;
        if(!$.isArray(children)){
            children = [children];
        }
        if ( children != null) {
            return children.map(function(item){
                return item;
            });
        }
    }
};


/**
 * Breadcrumb component prop types
 */
Breadcrumb.propTypes = $.extend({}, Component.propTypes, {

});

/**
 * Get Breadcrumb component default props
 */
Breadcrumb.defaultProps = $.extend({}, Component.defaultProps, {
    
});
