import Component from "../basic/Component";
import PropTypes from "prop-types";
export default class BreadcrumbItem extends Component {

    render() {
         if(this.props.value==null){
             return(
                  <li class="breadcrumb-item active" id={this.props.id} aria-current="page" style={{display:'flex'}}><p>{this.getI18n(this.props.title)}</p></li>
             );
        }else{
            return (
                <li class="breadcrumb-item" id={this.props.id} style={{display:'flex'}}><a href={this.props.value}>{this.getI18n(this.props.title)}</a>
                </li>
            );
         }
    }
};

BreadcrumbItem.propTypes = {
    value: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    badgeClass: PropTypes.string,
    disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    visibled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    onClick: PropTypes.func
};

BreadcrumbItem.defaultProps = {
    disabled: false,
    visibled: true,
    badgeClass: 'default'
};
