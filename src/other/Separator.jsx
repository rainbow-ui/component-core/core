import PropTypes from 'prop-types';
import Component from "../basic/Component";
export default class Separator extends Component {
	
	renderComponent(){
		return (
			<li role="presentation" className="dropdown-divider"/>
		)
	}
	
};

/**@ignore
 * Separator component prop types
 */
Separator.propTypes = $.extend({}, Component.propTypes, {
	id: PropTypes.string
});

/**@ignore
 * Get separator component default props
 */
Separator.defaultProps = $.extend({}, Component.defaultProps, {
	componentType: "Separator"
});
