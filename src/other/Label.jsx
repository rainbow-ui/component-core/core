import Component from "../basic/Component";
import PropTypes from 'prop-types';
import {Util, StringUtil, ELUtil, UrlUtil} from 'rainbow-desktop-tools';

export default class Label extends Component { 
    renderComponent() {
        switch (this.props.size) {
            case '6x':
                switch (this.props.type) {
                    case 'legend':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <legend className={this.props.styleClass} title={this.getI18n(this.props.label)}><h1>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </h1></legend>
                        </div>);
                    case 'span':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h1><span className={"nspan "+ this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </span></h1>
                        </div>);
                    default:
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h1><label className={this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </label></h1>
                        </div>);
                }
                break;
            case '5x':
                switch (this.props.type) {
                    case 'legend':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <legend className={this.props.styleClass} title={this.getI18n(this.props.label)}><h2>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </h2></legend>
                        </div>);
                    case 'span':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h2><span className={"nspan "+ this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </span></h2>
                        </div>);
                    default:
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h2><label className={this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </label></h2>
                        </div>);
                }
                break;
            case '4x':
                switch (this.props.type) {
                    case 'legend':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <legend className={this.props.styleClass} title={this.getI18n(this.props.label)}><h3>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </h3></legend>
                        </div>);
                    case 'span':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h3><span className={"nspan "+ this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </span></h3>
                        </div>);
                    default:
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h3><label className={this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </label></h3>
                        </div>);
                }
                break;
            case '3x':
                switch (this.props.type) {
                    case 'legend':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <legend className={this.props.styleClass} title={this.getI18n(this.props.label)}><h4>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </h4></legend>
                        </div>);
                    case 'span':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h4><span className={"nspan "+ this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </span></h4>
                        </div>);
                    default:
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h4><label className={this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </label></h4>
                        </div>);
                }
                break;
            case '2x':
                switch (this.props.type) {
                    case 'legend':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <legend className={this.props.styleClass} title={this.getI18n(this.props.label)}><h5>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </h5></legend>
                        </div>);
                    case 'span':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h5><span className={"nspan "+ this.props.styleClass} title={this.getI18n(this.props.label)}> 
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </span></h5>
                        </div>);
                    default:
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h5><label className={this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </label></h5>
                        </div>);
                }
                break;
            case '1x':
                switch (this.props.type) {
                    case 'legend':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <legend className={this.props.styleClass} title={this.getI18n(this.props.label)}><h6>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </h6></legend>
                        </div>);
                    case 'span':
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h6><span className={"nspan "+ this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </span></h6>
                        </div>);
                    default:
                        return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                            <h6><label className={this.props.styleClass} title={this.getI18n(this.props.label)}>
                                <span>{this.getI18n(this.props.label)}</span>
                                {this.renderRequired()}
                            </label></h6>
                        </div>);
                }
                break;
            default:
                return (<div className={(Util.parseBool(this._required)) ? 'r-label input-required' : 'r-label'}>
                    <label className={this.props.styleClass} title={this.getI18n(this.props.label)}>
                        <span>{this.getI18n(this.props.label)}</span>
                        {this.renderRequired()}
                    </label>
                </div>);
        }

    }


    setProperty() {
        super.setProperty();
        this._required = this.getProperty("required");
    }

     /**@ignore
     * render children comonent required
     */
    renderRequired() {
        
            if (Util.parseBool(this._required)) {
                return (
                    <span className={"glyphicon glyphicon-asterisk"}
                          data-toggle="tooltip" data-placement="top"
                          style={{paddingLeft: "5px", color: "#ff5555", transform: "scale(0.7)"}}>
                    </span>
                );
            }else{
                <noscript/>
            }
        
    }

};


/**@ignore
 * Label component prop types
 */
Label.propTypes = $.extend({}, Component.propTypes, {
    id: PropTypes.string,
    label: PropTypes.string,
    size: PropTypes.string,
    styleClass: PropTypes.string,
    type: PropTypes.string,
    required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
});

/**@ignore
 * Get Label component default props
 */
Label.defaultProps = $.extend({}, Component.defaultProps, {
    componentType: 'Label',
    required: false,
});