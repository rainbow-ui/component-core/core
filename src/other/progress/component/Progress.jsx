import Component from "../../../basic/Component";
import {Util} from "rainbow-desktop-tools";
import PropTypes from "prop-types";

export default class Progress extends Component {
    render() {
        let tempClass = 'progress-bar ';
        if (this.props.styleClass) {
            tempClass += 'bg-' + this.props.styleClass;
        }
        if (Util.parseBool(this.props.striped)) {
            tempClass += ' progress-bar-striped';
        }
        if (Util.parseBool(this.props.animated)) {
            tempClass += ' progress-bar-animated';
        }

        let tempStyle = {
            width: this.getValue(this.props.value) + '%',
            height: this.props.height
        };

        return (
            <div className="progress">
                <div className={tempClass} style={tempStyle}>{this.getValue(this.props.value)}%</div>
            </div>
        );
    }

    getValue(value) {
        let inputValue = 0;
        const model = this.props.model;
        const property = this.props.property;
        if (Util.isFunction(value)) {
            inputValue = value();
        } else if (value != 0) {
            inputValue = value;
        } else if (model != null && property != null) {
            inputValue = model[property];
        }
        return inputValue;
    }
};

Progress.propTypes = {
    id: PropTypes.string,
    styleClass: PropTypes.oneOf(["default", "primary", "success", "warning", "danger", "info"]),
    striped: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    animated: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    model: PropTypes.object,
    property: PropTypes.string
};

Progress.defaultProps = {
    styleClass: 'default',
    striped: true,
    animated: true,
    value: 0,
    height: '1rem'
};