﻿import { NumberUtil, Util } from 'rainbow-desktop-tools';
import ConvertorConstant from './ConvertorConstant';
let PercentConvertor = {

    getAsObject: function (component, value) {
        if (value != "" && value != null && value != undefined) {
            //let formatValue = component.formatDigit(component.canonicalDigit(value));
            value = value.replace(/[^0-9\.\-]/g, "");
            //eg: 33.55->0.3355, 5.55->0.0555
            //let result = NumberUtil.divide(value,  parseInt(component.props.limit));
            //let result = parseFloat(value / parseInt(component.props.limit));
            let result;
            if (component.getConvertorId() == ConvertorConstant.PERMILLAGE_CONVERTOR) {
                result = value > 0 ? this.numScale(value, -3) : value / 1000;
            } else {
                result = value > 0 ? this.numScale(value, -2) : value / 100;
            }
            if (Util.parseBool(component.props.isRounding)) {
                return parseFloat(result.toFixed(parseInt(component.decimalPrecision) + 2));
            } else {
                if (result > 0) {
                    return parseFloat(result.toString().substr(0, parseInt(component.decimalPrecision) + 5));
                } else {
                    return parseFloat(result.toString().substr(0, parseInt(component.decimalPrecision) + 6));
                }

            }


        }
        return null;
    },

    getAsString: function (component, value) {
        let result;
        if (value !== "" && value !== null && value !== undefined) {
            if (component.getConvertorId() == ConvertorConstant.PERMILLAGE_CONVERTOR) {
                if (value.toString().indexOf('e') != -1) {
                    result = parseFloat(this.numTransfer(value, 3))
                } else {
                    result = NumberUtil.multiply(value, 1000);
                }
            } else {
                if (value.toString().indexOf('e') != -1) {
                    result = parseFloat(this.numTransfer(value, 2))
                } else {
                    result = NumberUtil.multiply(value, 100);
                }
            }
            if (result.toString().indexOf('e') != -1) {
                return this.numTransfer(result, 0)
            } else {
                return component.formatDigit(component.canonicalDigit(result));
            }


        }

        return value;
    },

    numScale: function (value, m) {
        let parts = value.toString().split('.');
        const integerLen = parts[0].length;
        const decimalLen = parts[1] ? parts[1].length : 0;

        if (m > 0) {
            let zeros = m - decimalLen;
            while (zeros > 0) {
                zeros -= 1;
                parts.push(0);
            }
        } else {
            let zeros = Math.abs(m) - integerLen;
            while (zeros > 0) {
                zeros -= 1;
                parts.unshift(0);
            }
        }
        let index = integerLen + m;
        parts = parts.join('').split('');
        parts.splice(index > 0 ? index : 0, 0, '.');

        return parseFloat(parts.join(''));
    },
    numTransfer: function (value, digit) {
        let regExp = new RegExp('^(((\\d+.?\\d+)|(\\d+))[Ee]{1}((-(\\d+))|(\\d+)))$', 'ig');
        let newResult = regExp.exec(value);
        let resultValue = "";
        let power = "";
        if (newResult != null) {
            if (newResult[2].indexOf('.') != -1) {
                let newArr = newResult[2].trim().split(".");
                resultValue = newArr[0] + newArr[1];
            } else {
                resultValue = newResult[2];
            }
            power = newResult[7];
            newResult = regExp.exec(value);
        }
        let string = "0.";
        if (resultValue != "") {
            if (power != "") {
                for (let i = 0; i < power - digit - 1; i++) {
                    string += "0"
                }
                string += resultValue
                value = string;
            }
        }
        return value
    }

};

module.exports = PercentConvertor;