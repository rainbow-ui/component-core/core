import config from 'config';
import { Util } from 'rainbow-desktop-tools';
import MessageHelper from '../dialog/MessageHelper';
import r18n from '../i18n/reactjs-tag.i18n.jsx';

const DateTimePickerConvertor = {

    getAsObject: function (component, value) {
        if (value == undefined || value == null) {
            return null;
        }
        if (value == '') {
            return ''
        }
        value = value.toString();        
        try {
            let format = component.props.format?component.props.format:config.DEFAULT_DATETIME_SUBMIT_FORMATER;
            if(Util.parseBool(component.props.isYearOffset)&&format.indexOf('YYYY')>-1){
                let yearOffset = component.props.yearOffset?parseInt(component.props.yearOffset):1911;
                let year = parseInt(value.substr(0,4));
                if(year<1000){
                    let yearToStr = (year+yearOffset).toString();
                    let newDate = yearToStr + value.substr(4);
                    let fourth = value.substr(4,1);
                    if(Object.prototype.toString.call(value) === "[object Number]"){
                        newDate = yearToStr + '-' + value.substr(4);
                    }
                    value = newDate;                    
                }
            }
            let submitFormat = config.DEFAULT_DATETIME_SUBMIT_FORMATER;

            if (format == "YYYY") {
                value = value.substr(0,4);
                return value;
            }
            if (format.indexOf("ii") >= 0) {
                format = format.replace('ii', 'mm');
            }
            if (format.indexOf("hh") >= 0) {
                format = format.replace("hh", "HH");
            }

            let alwaysParse24 = Util.parseBool(component.props.alwaysParse24);
            let forceParse24 = Util.parseBool(component.props.forceParse24);
            let initParse24 = Util.parseBool(component.props.initParse24);

            if (alwaysParse24) {
                let hour = dayjs(value).hour(); //dayjs(value,'YYYYMMDDHHmmss').get("hour");
                let minute = dayjs(value).minute(); //dayjs(value,'YYYYMMDDHHmmss').get('minute');
                let second = dayjs(value).second(); //dayjs(value,'YYYYMMDDHHmmss').get('second');
                if (value && hour == "0" && minute == "0" && second == '0') {
                    value = dayjs(value).add('day', 1);
                    value = dayjs(value).subtract('second', 1);
                    format = format.replace("HH:mm:ss", "23:59:59");
                } 
            }

            if (forceParse24) {
                let hour = dayjs(value).hour(); //dayjs(value,'YYYYMMDDHHmmss').get("hour");
                let minute = dayjs(value).minute(); //dayjs(value,'YYYYMMDDHHmmss').get('minute');
                let second = dayjs(value).second(); //dayjs(value,'YYYYMMDDHHmmss').get('second');
                const hh = value.slice(8, 10);
                const ii = value.slice(10, 12);
                const ss = value.slice(12, 14);
                if (hh=='24'){
                    value = dayjs(value).add('day', 1);
                    value = dayjs(value).subtract('second', 1);
                    format = format.replace("HH:mm:ss", "23:59:59");
                }
                if (hh=='00'){
                    format = format.replace("HH:mm:ss", hh+':'+ii+':'+ss);
                    submitFormat = config.DEFAULT_DATETIME_SUBMIT_FORMATER.replace("HH:mm:ss", hh+':'+ii+':'+ss);

                }
                if (value && hour == "23" && minute == "59" && second == '59') {
                    value = dayjs(value).add('day', 1);
                    value = dayjs(value).subtract('second', 1);
                    format = format.replace("HH:mm:ss", "23:59:59");
                }
            }
        
            let offSet = component.props.offSet;
            if (Util.isString(offSet)) {

                // let hour = dayjs(value).get("hour");
                // let minute = dayjs(value).get('minute');
                // let second = dayjs(value).get('second');
                if (value) {
                    value = dayjs(value).add('second', offSet)
                }
            } else if (offSet) {
                // let hour = dayjs(value).get("hour");
                // let minute = dayjs(value).get('minute');
                // let second = dayjs(value).get('second');
                if (value) {
                    value = dayjs(value).add(offSet[0], offSet[1])
                }
            }
            
            let convertorValue ;
            if (!initParse24) {
                if (alwaysParse24) {
                    convertorValue = dayjs(value).format(config.DEFAULT_DATETIME_SUBMIT_FORMATER.replace("HH:mm:ss", "23:59:59"));//dayjs(value, format).format(config.DEFAULT_DATETIME_SUBMIT_FORMATER.replace("HH:mm:ss", "23:59:59"));
                } else if (forceParse24 && !initParse24) {
                    convertorValue = dayjs(value).format(submitFormat);//dayjs(value, format).format(submitFormat);
                }else{
                    let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
                    if (tempFormat == "HHmmss") { // 00:00:00 only
                        convertorValue = dayjs(dayjs().format('YYYY-MM-DD')+value).format(config.DEFAULT_DATETIME_SUBMIT_FORMATER);
                    } else {
                        if (tempFormat.endsWith("HH")) {
                            value = value + ":00:00";
                        }
                        if (component.props.bindFormat) {
                            convertorValue = dayjs(value).format(component.props.bindFormat);
                        } else {
                            convertorValue = dayjs(value).format(config.DEFAULT_DATETIME_SUBMIT_FORMATER); //dayjs(value, format).format(config.DEFAULT_DATETIME_SUBMIT_FORMATER);
                        }
                    }
                }
            } else {
                let hour = dayjs(value).hour(); //dayjs(value,'YYYYMMDDHHmmss').get("hour");
                let minute = dayjs(value).minute(); //dayjs(value,'YYYYMMDDHHmmss').get('minute');
                let second = dayjs(value).second();//dayjs(value,'YYYYMMDDHHmmss').get('second');
                if (hour == 0 && minute == 0 && second == 0) {
                    convertorValue = dayjs(value).format(config.DEFAULT_DATETIME_SUBMIT_FORMATER.replace("HH:mm:ss", "23:59:59"));//dayjs(value, format).format(config.DEFAULT_DATETIME_SUBMIT_FORMATER.replace("HH:mm:ss", "23:59:59"));
                } else {
                    convertorValue = dayjs(value).format(config.DEFAULT_DATETIME_SUBMIT_FORMATER);//dayjs(value, format).format(config.DEFAULT_DATETIME_SUBMIT_FORMATER);
                }
            }
            if (convertorValue == "Invalid date" || format.length > value.length) {
                if (convertorValue == "Invalid date") {
                    return '';
                } else {
                    return convertorValue;
                }
            }
            return convertorValue;
        } catch (e) {}
    },

    getAsString: function (component, value) {
        if (value == undefined || value == null || value == '') {
            return null;
        }
        value = value.toString();
        try {
            let format = this.getFormat(component);
            if(Util.parseBool(component.props.isYearOffset)&&format.indexOf('YYYY')>-1){
                let yearOffset = component.props.yearOffset?parseInt(component.props.yearOffset):1911;
                let year = parseInt(value.substr(0,4));
                if(year<1000){
                    let yearToStr = (year+yearOffset).toString();
                    let newDate = yearToStr + value.substr(4);
                    value = newDate;
                }
            }
            if (format.indexOf("ii") >= 0) {
                format = format.replace('ii', 'mm');
            }
            if (format.indexOf("hh") >= 0) {
                format = format.replace("hh", "HH");
            }
            if (format == "YYYY") {
                let year = parseInt(value.substr(0,4));                
                if(Util.parseBool(component.props.isYearOffset)&&format.indexOf('YYYY')>-1){
                    let yearOffset = component.props.yearOffset?parseInt(component.props.yearOffset):1911;
                    if(year>1000){
                        let yearToStr = (year-yearOffset).toString();
                        if(parseInt(yearToStr)<0){
                            MessageHelper.error(r18n.EffectiveDateError);
                            value = '';                       
                        }else{
                            value = yearToStr;
                        }
                    }
                }else{
                    value = year.toString();
                }
                return value;
            }
            let alwaysParse24 = Util.parseBool(component.props.alwaysParse24);
            if (alwaysParse24) {
                // let hour = dayjs(value).get("hour");
                // let minute = dayjs(value).get('minute');
                // let second = dayjs(value).get('second');
                if (value) {
                    // value = dayjs(value).subtract('days', 1);
                    format = format.replace("HH:mm:ss", "24:00:00");
                }
            }
            let forceParse24 = Util.parseBool(component.props.forceParse24);
            let initParse24 = Util.parseBool(component.props.initParse24);
            if (forceParse24) {
                let hour = dayjs(value).hour(); //dayjs(value).get("hour");
                let minute = dayjs(value).minute();//dayjs(value).get('minute');
                let second = dayjs(value).second();//dayjs(value).get('second');
                if (hour == "23" && minute == "59" && second == '59') {
                    // value = dayjs(value).subtract('second', 1);
                    format = format.replace("HH:mm:ss", "24:00:00");
                }
            }

            if (initParse24 ) {
                let hour = dayjs(value).hour(); //dayjs(value).get("hour");
                let minute = dayjs(value).minute();//dayjs(value).get('minute');
                let second = dayjs(value).second();//dayjs(value).get('second');
                if (hour == 0 && minute == 0 && second == 0) {
                    format = format.replace("HH:mm:ss", "24:00:00");
                }
            }

            let offSet = component.props.offSet;
            if (Util.isString(offSet)) {

                // let hour = dayjs(value).get("hour");
                // let minute = dayjs(value).get('minute');
                // let second = dayjs(value).get('second');
                if (value) {
                    value = dayjs(value).subtract('second', offSet)
                }
            } else if (offSet) {
                // let hour = dayjs(value).get("hour");
                // let minute = dayjs(value).get('minute');
                // let second = dayjs(value).get('second');
                if (value) {
                    value = dayjs(value).subtract(offSet[0], offSet[1])
                }
            }
            let convertorValue = dayjs(value).format(format); //dayjs(value, config.DEFAULT_DATETIME_SUBMIT_FORMATER).format(format);
            if (convertorValue == "Invalid date" || convertorValue == null || convertorValue == undefined) {
                return value;
            }
            if(Util.parseBool(component.props.isYearOffset)&&format.indexOf('YYYY')>-1){
                let yearOffset = component.props.yearOffset?parseInt(component.props.yearOffset):1911;
                let year = parseInt(convertorValue.substr(0,4));
                if(year>1000){
                    let yearToStr = (year-yearOffset).toString();
                    if(yearToStr.length<3){
                        for (let i = yearToStr.length; i < 3; i++) {
                            yearToStr = "0" + yearToStr
                        }
                    }
                    let newDate = yearToStr + convertorValue.substr(4);
                    convertorValue = newDate;
                }
            }
            return convertorValue;
        } catch (e) {}
    },


    getFormat: function (component) {
        let format = config.DEFAULT_DATETIME_FORMATER;
        if (component.props.format != undefined) {
            format = component.props.format;
        }

        return format;
    }

};

module.exports = DateTimePickerConvertor;