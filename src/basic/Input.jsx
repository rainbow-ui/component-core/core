import config from "config";
import Component from "./Component";
import Param from "./Param";
import Event from "./Event";
import OnChangeEvent from '../event/OnChangeEvent';
import OnBlurEvent from '../event/OnBlurEvent';
import OnFocusEvent from '../event/OnFocusEvent';
import OnClickEvent from '../event/OnClickEvent';
import { ComponentContext, ValidatorContext, PageContext, LocalContext } from "rainbow-desktop-cache";
import { Util, StringUtil, ELUtil, UrlUtil } from 'rainbow-desktop-tools';
import Convertor from '../convertor/Convertor';
import Validator from '../validator/Validator';
import r18n from "../i18n/reactjs-tag.i18n";
import PropTypes from 'prop-types';
import { PolicyStore, EndorsementStore, ObjectStore, SubmissionStore } from "rainbow-desktop-sdk";
import { CodeTableService } from "rainbow-desktop-codetable";

export default class Input extends Component {

    constructor(props) {
        super(props)
        this.temp = "",
            this.tempClass = "inputClass";
    }

    renderComponent () {

        this.tempClass = this.props.io == 'in' ? "inputClass" : "outputClass";

        if (this.props.layout == "vertical") {
            return this.renderVerticalLayout();
        }

        else if (this.props.layout == "horizontal") {
            return this.renderHorizontalLayout();
        }
    }

    renderVerticalLayout () {
        const componentType = this.props.componentType;
        let classGroup = "form-group " + this.tempClass + " " + this.props.inputClass + " " + componentType + "-textalign";
        let styleClassWidth = this.getStyleClassWidth();
        let labelWidth = null, inputWidth = null;
        if (styleClassWidth.length != 0) {
            //labelWidth = styleClassWidth[0];
            inputWidth = styleClassWidth[1];
        }
        if (this.componentId == undefined) {
            this.componentId = this.props.id;
        }
        let inTable = $("#" + this.componentId).parents('table') && $("#" + this.componentId).parents('table').length > 0 && this.props.componentType && (this.props.componentType == 'SearchCodeTable' || this.props.componentType == 'InputTreeSelect' || this.props.componentType == 'InputTableSelect' || this.props.componentType == 'cascade' || this.props.componentType == 'uisearch');
        let mgTop = this.props.needoverflow ? this.props.overflowMarginTop : '';
        // mgTop = '';

        if (this.props.label) {
            let hasOpenId = this.props.openId ? true : false;
            return (
                <div className={classGroup} style={{ width: this.props.width }}>
                    {
                        !Util.parseBool(this.props.isShowLabelTitle) ?
                            hasOpenId ?
                                <label id={this.componentId} onClick={this.openSrc.bind(this)} hasevent="yes" style={{ display:'flex' }}>
                                    {this.renderLabel()}
                                    {this.renderHelpIdText()}
                                    {this.renderRequired()}
                                </label>
                                :
                                <label htmlFor={this.componentId} style={{ display:'flex' }}>
                                    {this.renderLabel()}
                                    {this.renderHelpText()}
                                    {this.renderRequired()}
                                </label>
                            :
                            hasOpenId ?
                                <label id={this.componentId} title={this.renderLabelTile()} onClick={this.openSrc.bind(this)} hasevent="yes" style={{ display:'flex' }}>
                                    {this.renderLabel()}
                                    {this.renderHelpIdText()}
                                    {this.renderRequired()}
                                </label>
                                :
                                <label htmlFor={this.componentId} title={this.renderLabelTile()} style={{ display:'flex' }}>
                                    {this.renderLabel()}
                                    {this.renderHelpText()}
                                    {this.renderRequired()}
                                </label>
                    }

                    {
                        inTable ?
                            <div style={{ width: this.getDropWidth(this), position: (this.props.needoverflow ? this.getPosition(this) : '') }} needreposition={this.props.needoverflow ? '1' : ''} className={(Util.parseBool(this._required) && this.props.io != "out") ? 'input-required' : null} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="bottom" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                {this.renderInputComponent()}
                            </div>
                            :
                            <div style={{ width: inputWidth }} className={(Util.parseBool(this._required) && this.props.io != "out") ? 'input-required' : null} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="bottom" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                {this.renderInputComponent()}
                            </div>
                    }
                </div>
            );
        } else {
            return (
                <div className={classGroup} style={{ width: this.props.width }}>
                    <label htmlFor={this.componentId} />
                    {
                        inTable ?
                            <div style={{ width: this.getDropWidth(this), position: (this.props.needoverflow ? this.getPosition(this) : '') }} needreposition={this.props.needoverflow ? '1' : ''} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="bottom" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                {this.renderInputComponent()}
                            </div>
                            :
                            (this.props.io == 'in' ?
                                <div style={{ width: inputWidth }} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="bottom" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                    {this.renderInputComponent()}
                                </div>
                                :
                                <div style={{ width: inputWidth, textOverflow: (this.props.needellipsis ? 'ellipsis' : ''), overflow: this.props.needellipsis ? 'hidden' : '' }} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="bottom" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                    {this.renderInputComponent()}
                                </div>)
                    }
                </div>
            );
        }
    }

    getPosition (obj) {
        // if ($(obj).find('.react-dropdown-tree-select') && $(obj).find('.react-dropdown-tree-select').length > 0) {
        //     return 'absolute';
        // } else {
        //     return 'relative'
        // }
    }

    /*** Render horizontal layout */
    renderHorizontalLayout () {
        let styleClassArray = this.getStyleClassArray();
        let styleClassWidth = this.getStyleClassWidth();
        let labelWidth = null, inputWidth = null;
        let inputStyleClass = styleClassArray[1];
        if (Util.parseBool(this._required) && this.props.io != "out") {
            inputStyleClass += " input-required"
        }
        if (styleClassWidth.length != 0) {
            labelWidth = styleClassWidth[0];
            inputWidth = styleClassWidth[1];
        }
        // console.log(this.componentId, this.props.id);
        if (this.componentId == undefined) {
            this.componentId = this.props.id;
        }

        // (event && event.target && $(event.target).attr('class').indexOf('common-add') != -1) || 
        let inTable = $("#" + this.componentId).parents('table') && $("#" + this.componentId).parents('table').length > 0 && this.props.componentType && (this.props.componentType == 'SearchCodeTable' || this.props.componentType == 'InputTreeSelect' || this.props.componentType == 'InputTableSelect' || this.props.componentType == 'cascade' || this.props.componentType == 'uisearch');
        let mgTop = this.props.needoverflow ? this.props.overflowMarginTop : '';
        // if ($("#" + this.componentId).parents('tr:last').attr('id') != $("#" + this.componentId).parents('tr').attr('id')) {
        mgTop = '';
        // }

        if (!inTable) {
            let isintable = Util.parseBool(this.props.isintable);
            inTable = isintable;
            inputWidth = this.props.width;
        }

        const componentType = this.props.componentType;
        let classGroup = "form-group row line-horizontal " + this.tempClass + " " + this.props.inputClass + " " + componentType + "-textalign";
        if (this.props.label) {
            let hasOpenId = this.props.openId ? true : false;
            return (
                <div className={classGroup} style={{ width: this.props.width }}>
                    {
                        !Util.parseBool(this.props.isShowLabelTitle) ?
                            hasOpenId ?
                                <label id={this.componentId} className={styleClassArray[0] + " col-form-label"} onClick={this.openSrc.bind(this)} hasevent="yes"
                                    style={{ width: labelWidth, display:'flex'}}>
                                    {this.renderLabel()}
                                    {this.renderHelpIdText()}
                                    {this.renderRequired()}
                                </label>
                                :
                                <label htmlFor={this.componentId} className={styleClassArray[0] + " col-form-label"}
                                    style={{ width: labelWidth, display:'flex' }}>
                                    {this.renderLabel()}
                                    {this.renderHelpText()}
                                    {this.renderRequired()}
                                </label>
                            :
                            hasOpenId ?
                                <label id={this.componentId} className={styleClassArray[0] + " col-form-label"} onClick={this.openSrc.bind(this)} hasevent="yes"
                                    style={{ width: labelWidth, display:'flex' }} title={this.renderLabelTile()}>
                                    {this.renderLabel()}
                                    {this.renderHelpIdText()}
                                    {this.renderRequired()}
                                </label>
                                :
                                <label htmlFor={this.componentId} className={styleClassArray[0] + " col-form-label"}
                                    style={{ width: labelWidth, display:'flex' }} title={this.renderLabelTile()}>
                                    {this.renderLabel()}
                                    {this.renderHelpText()}
                                    {this.renderRequired()}
                                </label>
                    }

                    {
                        inTable ?
                            <div className={inputStyleClass + " input-required"} style={{ width: this.getDropWidth(this), position: (this.props.needoverflow ? this.getPosition(this) : '') }} needreposition={this.props.needoverflow ? '1' : ''} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="bottom" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                {this.renderInputComponent()}
                            </div>
                            :
                            <div className={inputStyleClass} style={{ width: inputWidth }} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="bottom" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                {this.renderInputComponent()}
                            </div>
                    }
                </div>
            );
        } else {
            return (
                <div className={classGroup} style={{ width: this.props.width }}>
                    <label htmlFor={this.componentId} className={styleClassArray[0] + " col-form-label"}
                        style={{ width: labelWidth }} />
                    {
                        inTable ?
                            <div className={inputStyleClass + " input-required"} style={{ width: this.getDropWidth(this), position: (this.props.needoverflow ? this.getPosition(this) : '') }} needreposition={this.props.needoverflow ? '1' : ''} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="bottom" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                {this.renderInputComponent()}
                            </div>
                            :
                            (this.props.io == 'in' ?
                                <div className={inputStyleClass} style={{ width: inputWidth }} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="top" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                    {this.renderInputComponent()}
                                </div>
                                :
                                <div className={inputStyleClass} style={{ width: inputWidth, textOverflow: (this.props.needellipsis ? 'ellipsis' : ''), overflow: this.props.needellipsis ? 'hidden' : '' }} id={"for-input-" + this.componentId} data-toggle="tooltip" data-placement="bottom" data-trigger="hover" data-original-title={this.getI18n(this.props.title)}>
                                    {this.renderInputComponent()}
                                </div>)
                    }

                </div>
            );
        }
    }

    getDropWidth (obj) {
        if (this.props.io == 'out') {
            return '';
        }
        let table = $("#" + obj.componentId).parents('table')[0];
        if ($(table).length > 0) {
            if ($(table).find('#' + obj.componentId).parents('td') && $(table).find('#' + obj.componentId).parents('td').length > 0) {
                // var theadtr = $(table).find('thead tr');
                // let itemPos = $($(table).find('#'+obj.componentId).parents('td')[0]).attr('tdindex');
                // if ($($(table).find('#' + obj.componentId).parents('td')[0]).prevAll('[class=details-control]').length > 0) {
                //     itemPos = parseInt(itemPos) + $($(table).find('#' + obj.componentId).parents('td')[0]).prevAll('[class=details-control]').length;
                // }
                if ($(table).find('#' + obj.componentId).parents('td').attr('realwidth')) {
                    let tdindex = $(table).find('#' + obj.componentId).parents('td').attr('tdindex');
                    let thwidth = $($(table).find('thead th')[tdindex]).css('min-width'); //$(table).find('thead th')[tdindex]
                    let tdwidth = $(table).find('#' + obj.componentId).parents('td').attr('realwidth');
                    let width = tdwidth;
                    if (thwidth && tdwidth) {
                        thwidth = parseInt(thwidth.replace('px', ''));
                        tdwidth = parseInt(tdwidth.replace('px', ''));
                        if (thwidth > tdwidth) {
                            width = thwidth + 'px'
                        }
                    }
                    return width;
                } else {
                    return '100px';
                }
                // console.log('getdropwidth...')
                // if ($(theadtr[theadtr.length - 1]).text() !== '') {
                //     var width = $($(theadtr[theadtr.length - 1]).find('th')[itemPos]).width();
                //     return width + 'px';
                // } else {
                //     var width = $($(theadtr[0]).find('th')[itemPos]).width();
                //     return width + 'px';
                // }

            }

        } else {
            if (Util.parseBool(this.props.isintable) && this.props.width) {
                return this.props.width;
            }
        }
    }

    renderLabel () {
        if (Util.parseBool(this.props.lableShowDiv) && this.props.labelDivStyle) {
            return (
                <div style={this.props.labelDivStyle}></div>
            )
        }
        if (this.props.label && Util.isObject(this.props.label)) {
            this.label = this.props.label;
            // if(Util.parseBool(this.props.isShowLableToolTip)){
            //     return (
            //         <div data-toggle="tooltip" data-placement="bottom" data-original-title={this.props.label} 
            //             class="input-label" style={{ maxWidth: Util.parseBool(this.props.required) ? "80%" : '100%' }}>
            //             {this.props.label}
            //         </div>
            //     );
            // }else{
                return (
                    <div id= {"input-label-" + this.componentId} class="input-label" style={{ maxWidth: Util.parseBool(this.props.required) ? "80%" : '100%' }}>
                        {this.props.label}
                    </div>
                );
            // }
        } else {
            this.label = super.getI18n(this.props.label);
            // if(Util.parseBool(this.props.isShowLableToolTip)){
            //     return (
            //         <div data-toggle="tooltip" data-placement="bottom" data-original-title={super.getI18n(this.props.label)} 
            //             class="input-label" style={{ maxWidth: Util.parseBool(this.props.required) ? "80%" : '100%' }}>
            //             {super.getI18n(this.props.label)}
            //         </div>
            //     );
            // }else{
                return (
                    <div id= {"input-label-" + this.componentId} class="input-label" style={{ maxWidth: Util.parseBool(this.props.required) ? "80%" : '100%' }}>
                        {super.getI18n(this.props.label)}
                    </div>
                );
            // }
        }
    }

    renderLabelTile () {
        if (this.props.label && Util.isObject(this.props.label)) {
            return this.props.label

        } else {
            return super.getI18n(this.props.label)
        }
    }

    /*** Render input component */
    renderInputComponent () {
        if (this.props.io == "in" || this.props.io == null) {
            return this.renderInput();
        } else if (this.props.io == "out") {
            return this.renderOutput();
        }

        return <noscript />;
    }

    /*** Render input */
    renderInput () {
        return <noscript />;
    }

    /*** Render output */
    renderOutput () {
        const output = this.getOutputValue();
        const outPutClass = "outPutText";
        if (output && output instanceof Array) {
            return (
                <span id={this.componentId} className={outPutClass} style={this.props.style}>
                    {
                        output.map(item => {
                            if(Util.parseBool(this.props.isShowValueTooltip)){
                                return <span className='outPutTextItem' data-toggle="tooltip" data-placement="bottom" data-original-title={item}>{item}</span>;
                            }else{
                                return <span className='outPutTextItem'>{item}</span>;                                
                            }
                        })
                    }
                </span>
            );
        } else {
            if (this.props.onClick && Util.parseBool(this.props.haslink)) {
                if(Util.parseBool(this.props.isShowValueTooltip)){
                    return (
                        <a href="javascript: void(0);" onClick={this.onClick.bind(this)}>
                            <span id={this.componentId} className={outPutClass} data-toggle="tooltip" data-placement="bottom" data-original-title={output} style={this.props.style}>
                                {output}
                            </span>
                        </a>
                    )
                }else{
                    return (
                        <a href="javascript: void(0);" onClick={this.onClick.bind(this)}>
                            <span id={this.componentId} className={outPutClass} style={this.props.style}>
                                {output}
                            </span>
                        </a>
                    ) 
                }
            } else {
                if(Util.parseBool(this.props.isShowValueTooltip)){
                    return (
                        <span id={this.componentId} className={outPutClass} data-toggle="tooltip" data-placement="bottom" data-original-title={output} style={this.props.style}>
                            {output}
                        </span>
                    );
                }else{
                    return (
                        <span id={this.componentId} className={outPutClass} style={this.props.style}>
                            {output}
                        </span>
                    );
                }
            }
        }
    }

    // 有时要忽略disabled, 如UITab  PageContext.put("PAGE_READONLY", true)时也要能切换
    onClick (cancelDisable = true, event) {
        if (arguments.length > 1) {
            event.preventDefault();
            if (cancelDisable && this.getDisabled() == "disabled") {
                return;
            }
        } else {
            event = cancelDisable;
            event.preventDefault();
            if (this.getDisabled() == "disabled") {
                return;
            }
        }

        if (!ValidatorContext.validate(this.props.causeValidation, this.props.validationGroup, this.props.exceptValidationGroup)) {
            return;
        }

        // handler onClick
        let clickEvent = new OnClickEvent(this, event, Param.getParameter(this));
        if (this.props.onClick) {
            this.props.onClick(new OnClickEvent(this, event, Param.getParameter(this)));
        }

        // handler onComplete
        if (this.props.onComplete != undefined) {
            eval(this.props.onComplete);
        }

        // handler update
        if (this.props.update != undefined) {
            ComponentContext.forceUpdate(this.props.update);
        }
    }

    /*** Get output value */
    getOutputValue () {
        let value = this.getComponentValue();

        if (this.props.mask != null && this.props.mask != undefined && this.props.mask != "") {
            value = StringUtil.mask(value, this.props.mask);
        }

        return value;
    }

    /*
     *init MaxLength
     */
    initMaxLength (val) {
        if (this.props.maxLength || ["number", "currency"].indexOf(this.props.componentType) !== -1) {
            let maxLength;
            // if (this.props.componentType == "textarea") {
            //     maxLength = parseInt(this.props.maxLength * 3) < 0 ? 0 : parseInt(this.props.maxLength * 3);
            // } else {
            maxLength = parseInt(this.props.maxLength) < 0 ? 0 : parseInt(this.props.maxLength);
            // }
            maxLength = maxLength < 0 ? 0 : maxLength;
            if (val && val.toString().indexOf("-") == 0) {
                maxLength += 1;
            }

            if (maxLength >= 0 || ["number", "currency"].indexOf(this.props.componentType) !== -1) {
                if (this.props.subType == "UINumber" || this.props.subType == "UICurrency") {
                    let deciPre = 0;
                    if (this.props.allowDecimal && Util.parseBool(this.props.allowDecimal)) {
                        deciPre = this.props.decimalPrecision;
                    }
                    this.decimalPrecision = parseInt(this.decimalPrecision) ? (this.decimalPrecision >=16 ? 15 : parseInt(this.decimalPrecision)) : 2;
                    maxLength = parseInt(this.props.maxLength) ? 
                                    (parseInt(this.props.maxLength) + parseInt(this.decimalPrecision) > 16 ?
                                    16 - this.decimalPrecision : this.props.maxLength) : 16 - this.decimalPrecision; 
                    let needCutOne = parseInt(deciPre) == 0 ? parseInt(deciPre) : (parseInt(deciPre) + 1);
                    if (this.props.model && this.props.property) {
                        let propsVal = this.props.model[this.props.property];

                        if (propsVal != null && propsVal != undefined && parseInt(propsVal).toString().length > (maxLength - needCutOne)) {
                            this.temp = propsVal.toString().substr(0, (maxLength - (parseInt(deciPre) + 1)));
                        } else {
                            this.temp = this.props.model[this.props.property];
                        }
                    }
                    if (val != null && val != undefined && parseInt(val).toString().length <= (maxLength - needCutOne)) {
                        this.temp = val;
                        return val;
                    }
                    else {
                        if (!val) {
                            this.temp = null;
                        }
                        $('#' + this.componentId).val(this.temp);
                        return $('#' + this.componentId).val();
                    }

                } else {
                    if (this.props.componentType == "textarea") {
                        if (val != null && val != undefined) {
                            val = val.replace(new RegExp("<br/>", "gm"), "\n");
                        }
                    }
                    let valLength = this.getStrLeng(val)
                    if (val != null && val != undefined && valLength <= maxLength) {
                        this.temp = val;
                        return val;
                    } else {
                        if (!val) {
                            this.temp = null;
                        }
                        $('#' + this.componentId).val(this.temp);
                        return $('#' + this.componentId).val();
                    }
                }
            }
            return val;
        }
        return val;
    }

    // UTF8字符集实际长度计算 
    getStrLeng (str) {
        if (str == '' || str == null) { return 0; }
        let realLength = 0;
        let len = str.length;
        let charCode = -1;
        let chn_chatLength = config["CHN_CHATLEN"] || 1;
        for (let i = 0; i < len; i++) {
            charCode = str.charCodeAt(i);
            if (charCode >= 0 && charCode <= 128) {
                realLength += 1;
            } else {
                // 如果是中文则长度加3 
                realLength += chn_chatLength;
            }
        }
        return realLength;
    }

    /*** Set component value */
    setComponentValue (event) {
        let inputValue = Convertor.getAsObject(this.getConvertorId(), this, this.getInputValue(event));
        if (Util.parseBool(this.props.maxLength) || ["number", "currency"].indexOf(this.props.componentType) !== -1) {
            inputValue = this.initMaxLength(inputValue);
        }
        /*
         * UIPercent componet limit error handler
         */
        if (this.props.limit && !this.props.ignoreLimit) {
            if (inputValue >= parseFloat(this.props.limit) / 100) {
                inputValue = parseFloat(this.props.limit) / 100;
            }
        }

        this.setValue(this.props.value, inputValue);

    }

    getEventNewValue (event) {
        let inputValue = Convertor.getAsObject(this.getConvertorId(), this, this.getInputValue(event));

        /*
         * UIPercent componet limit error handler
         */
        if (this.props.limit && !this.props.ignoreLimit) {
            if (inputValue >= parseFloat(this.props.limit) / 100) {
                inputValue = parseFloat(this.props.limit) / 100;
            }
        }

        return inputValue;
    }

    /**@ignore
     * Get component value
     * If value is not null, it get value. else it get default value.
     */
    getComponentValue () {
        let value = null, { defaultValue } = this.props;
        value = this.getValue(this.props.value);
        // if value is null or undefined, and default value is not null or not defined, set default value to component value
        if ((value == null || value == undefined) && (defaultValue != null && defaultValue != undefined)) {
            value = defaultValue;
            this.setValue(this.props.value, Convertor.getAsObject(this.getConvertorId(), this, value));
        }
        if (Util.parseBool(this.props.valueTostring) && !_.isEmpty(value) && (value.indexOf(':') != -1 || this.props.componentType=='checkbox')) {
            value = JSON.parse(value);
        }
        return Convertor.getAsString(this.getConvertorId(), this, value);
    }

    /**@ignore
     * Set value
     */

    setValue (value, inputValue) {
        const model = this.props.model;
        const property = this.props.property;
        if (model != null && property != null) {
            if(Util.parseBool(this.props.valueTostring)){
                model[property] = JSON.stringify(inputValue);
            }else if(inputValue && Util.parseBool(this.props.isTrim) && typeof(inputValue) == 'string'){
                model[property] = this.inputValueTrim(inputValue);
            }else{
                model[property] = inputValue;
            }
        } else if (value != null) {
            value = inputValue;
        }
    }

    inputValueTrim(inputValue) {
        return inputValue.replace(/^\s+|\s+$/gm,'');
    }

    /**@ignore
     * Get value
     */

    getValue (value) {
        let inputValue = null;
        const model = this.props.model;
        const property = this.props.property;
        let componentType = this.props.componentType ? this.props.componentType.toLowerCase() : null;
        if (Util.isFunction(value)) {
            inputValue = value();
        } else if (value != null) {
            inputValue = value;
        } else if (model != null && property != null) {
            inputValue = model[property];
        }
        if (this.props.mask != null && this.props.mask != undefined && this.props.mask != "") {
            if (componentType == "password") {
                inputValue = StringUtil.mask(inputValue, this.props.mask);
            }else{
                if(Util.parseBool(this.props.disabled)||!Util.parseBool(this.props.enabled)||this.props.io=="out"){
                    inputValue = StringUtil.mask(inputValue, this.props.mask);                
                }
            }
        }
        return inputValue;

    }

    /**@ignore
     * Get prefix and suffix json from value
     */
    getPrefixSuffixJson (value) {
        let psJson = {};
        let index = value.indexOf('.');
        psJson.prefix = value.substr(0, index);

        //model[0].a.b || model.a.b
        if (psJson.prefix.indexOf("[") != -1) {
            let leftIndex = psJson.prefix.indexOf("[");
            let rightIndex = psJson.prefix.indexOf("]");
            psJson.count = psJson.prefix.substr(leftIndex + 1, rightIndex - leftIndex - 1); // 0

            psJson.prefix = value.substr(0, leftIndex);//model
        }
        psJson.suffix = value.substr(index + 1, value.length);//a.b

        return psJson;
    }

    getValidatorId () {
        return null;
    }

    getConvertorId () {
        return null;
    }

    getComponent () {
        return $("#" + this.componentId);
    }

    getInputValue (event) {
        // let inputRef = this.getInputRefProperty();
        // if (React.findDOMNode(this.refs[inputRef])) {
        //     return React.findDOMNode(this.refs[inputRef]).value;
        // } else {
        //     return "";
        // }
        let value = $('#' + this.componentId).val();
        let deleteIcon = $("#" + this.componentId + "_deleteIcon");
        if (value){
            deleteIcon.show();
        } else {
            deleteIcon.hide();
        }
        return value;
    }

    initDeleteEvent () {
        let _self = this;
        let inputObj = $("#" + _self.componentId);
        let _deleteIcon = $("#" + _self.componentId + "_deleteIcon");
        _deleteIcon.unbind();
        _deleteIcon.mousedown(function () {
            inputObj.val('');
            _self.setComponentValue();
            _self.onChangeCallback(_self);
            _deleteIcon.hide();
            //for UISearchCodeTable and UISearch
            let deleteFunc = _self.props.onDeleteIconClick;
            deleteFunc ? deleteFunc() : null;
            inputObj.parent().addClass("input-required");
        })
    }

    initEvent () {
        let _self = this;
        let me = $("#" + this.componentId);
        let commponentType = this.props.componentType ? this.props.componentType.toLowerCase() : null;
        if (commponentType === 'textarea') {
            $("label[for^='" + this.componentId + "']").click(function () {
                return true;
            });

        } else {
            $("label[for^='" + this.componentId + "']").click(function () {
                return false;
            });
        }
        // handler input propertychange
        me.bind("input propertychange", (event) => {
            //排除DateTimePicker在手动输入时超限的问�?
            if (!(_self.props.manualInput && Util.parseBool(_self.props.manualInput))) {
                _self.setComponentValue(event);
            }
            // if (_self.props.componentType == "datetimepicker" && Util.parseBool(_self.props.manualInput)) {
            //     _self.formatDateofManualInput(event);
            // }
            if (Util.parseBool(_self.props.required)) {
                const value = me.val();
                if (value) {
                    me.closest(".input-required").removeClass("input-required");
                } else {
                    me.parent().addClass("input-required");
                }
            }
        });

        // handle onchange event
        me.bind("change", (event) => {
            _self.onChangeCallback(_self);
            _self.onChangeColorByEndorsement();
        });

        // handle onblur event
        me.bind("blur", (event) => {
            _self.handleBlurEvent();
            if (_self.props.onBlur) {
                _self.props.onBlur(new OnBlurEvent(_self, event, Param.getParameter(_self), null, null));
            }
        });

        // handle onfocus event
        me.bind("focus", (event) => {
            _self.handleFocusEvent();
            if (_self.props.onFocus) {
                _self.props.onFocus(new OnFocusEvent(_self, event, Param.getParameter(_self), null, null));
            }
        });

        me.bind("keydown", (event) => {
            if (_self.props.onKeyDown) {
                _self.props.onKeyDown(new OnFocusEvent(_self, event, Param.getParameter(_self), null, null));
            }
        });

        me.bind("keyup", (event) => {
            if (this.props.componentType == "textarea") {
                this.checkMaxInput();
            }
            if (_self.props.onKeyUp) {
                _self.props.onKeyUp(new OnFocusEvent(_self, event, Param.getParameter(_self), null, null));
            }
        });

        me.bind("contextmenu", (event) => {
            if (Util.parseBool(this.props.unableContext)) {
                return false;
            }
        });

        me.bind("paste", (event) => {
            if (Util.parseBool(this.props.unablePaste)) {
                // console.log('paste')
                if (_self.props.onPaste) {
                    _self.props.onPaste(new OnFocusEvent(_self, event, Param.getParameter(_self), null, null));
                }
            }
        });

        me.bind("click", (event) => {});
    }

    handleBlurEvent () {
        let _self = this;
        let inputObj = $('#' + _self.componentId);
        let deleteIcon = $("#" + _self.componentId + "_deleteIcon");
        deleteIcon.hide();
        _self.showValueTooltip();
        let inputGroup = inputObj.parent('.input-group');
        if (inputGroup && inputGroup.hasClass("focusborder")) {
            inputGroup.removeClass('focusborder');
        }
    }

    handleFocusEvent () {
        let _self = this;
        let inputObj = $('#' + _self.componentId);
        let deleteIcon = $("#" + _self.componentId + "_deleteIcon");
        if (inputObj.val()) {
            deleteIcon.show();
        }   
        inputObj.tooltip('hide');
        let inputGroup = inputObj.parent('.input-group');
        if (inputGroup && !inputGroup.hasClass("focusborder")) {
            inputGroup.addClass('focusborder');
        }
    }

    /*** Get input ref property  */
    getInputRefProperty () {
        let refProperty = this.getRefProperty();
        return refProperty + "_ref";
    }

    onChangeCallback (_self) {
        let value = _self.getInputValue(event);
        if (_self.getDigitValue) {
            value = _self.getDigitValue(value);
        }
        let valueChangeEvent = new OnChangeEvent(_self, event, Param.getParameter(_self), value, _self.onEvent.newValue);
        if (_self.props.onChange) {
            _self.props.onChange(valueChangeEvent);
        }

        _self.onEvent = { newValue: _self.getComponentValue(), oldValue: _self.onEvent.newValue };
    }

    /**  Endorsement color onChange **/
    onChangeColorByEndorsement () {
        let endorsementId = PageContext.get("endorsementId");
        let endorsementOldPolicyId = PageContext.get("endorsementOldPolicyId");

        let oldApplication = PageContext.get("_Old_Application");
        let oldPolicyCheck = PageContext.get("_Old_Policy");

        if (oldApplication || oldPolicyCheck) {
            const endoStore = oldApplication ? oldApplication : oldPolicyCheck;
            let oldPolicy = endoStore && endoStore.OldPolicy ? endoStore.OldPolicy : null;
            if (endoStore) {
                this.setColor(oldPolicy);
            }
        } else if (endorsementId) {// if endo type
            const endoStore = EndorsementStore.getEndorsement(endorsementId);
            let oldPolicy = null;
            if (endoStore) {//非车&通用
                oldPolicy = endoStore.OldPolicy;
                this.setColor(oldPolicy);
            } else {//车险&多保单通用
                const submissionEndoStore = SubmissionStore.getSubmission(endorsementId);
                if (submissionEndoStore && submissionEndoStore.EndorsementSubmissionProductList) {
                    submissionEndoStore.EndorsementSubmissionProductList.forEach((product) => {
                        oldPolicy = product.Endorsement.OldPolicy;
                        this.setColor(oldPolicy);
                    });
                }
            }
        } else if (endorsementOldPolicyId) {//意健�?
            const oldPolicy = PolicyStore.getPolicy(endorsementOldPolicyId);
            this.setColor(oldPolicy);
        }
    }

    setColor (oldPolicy) {
        const model = this.props.model;
        let property = this.props.property;
        let commponentType = this.props.componentType ? this.props.componentType.toLowerCase() : null;
        if (model && property && model["@pk"]) {
            let me = $("#" + this.componentId);
            //find old policy by @pk
            const oldModel = ObjectStore.findModelByPK(oldPolicy, model["@pk"]);
            let newPropsVal = model[property];
            if (oldModel) {
                let oldPropsVal = oldModel[property];
                newPropsVal = newPropsVal === "" ? undefined : newPropsVal;
                if (oldPropsVal && newPropsVal == undefined) {
                    //changeOldValue
                    if (commponentType === 'checkbox') {
                        me.parent().parent().parent().parent().addClass("percent-color");
                        this.getAlertText(oldPropsVal, me);
                    } else if (commponentType === 'radio' || commponentType === 'select' || commponentType === 'searchcodetable' || commponentType === 'search') {//SEARCHCODETABLE
                        me.parent().parent().addClass("percent-color");
                        this.getAlertText(oldPropsVal, me);
                    } else if (commponentType === 'switch') {
                        me.parent().parent().parent().addClass("percent-color");
                        this.getAlertText(oldPropsVal, me);
                    } else {
                        me.parent().parent().addClass("percent-color");
                        if (oldPropsVal != undefined && oldPropsVal != null) {
                            me.attr("title", oldPropsVal);
                        } else {
                            me.attr("title", null);
                        }
                    }
                    return;
                }
                if ((newPropsVal == oldPropsVal) || (newPropsVal == undefined && oldPropsVal == undefined) || (newPropsVal == null && oldPropsVal == null)) {
                    //noChangeOldValue
                    if (commponentType === 'checkbox') {
                        me.parent().parent().parent().parent().removeClass("percent-color");
                    } else {
                        me.parent().parent().removeClass("percent-color");
                    }
                    me.attr("title", null);
                } else {
                    //changeOldValue
                    if (commponentType === 'checkbox') {
                        me.parent().parent().parent().parent().addClass("percent-color");
                        this.getAlertText(oldPropsVal, me);
                    } else if (commponentType === 'radio' || commponentType === 'select' || commponentType === 'searchcodetable' || commponentType === 'search') {//SEARCHCODETABLE
                        me.parent().parent().addClass("percent-color");
                        this.getAlertText(oldPropsVal, me);
                    } else if (commponentType === 'switch') {
                        me.parent().parent().parent().addClass("percent-color");
                        this.getAlertText(oldPropsVal, me);
                    } else {
                        me.parent().parent().addClass("percent-color");
                        if (oldPropsVal != undefined && oldPropsVal != null) {
                            me.attr("title", oldPropsVal);
                        } else {
                            me.attr("title", null);
                        }
                    }

                }
            }
        }
    }

    getAlertText (value, me) {
        let { conditionMap, codeTableName, url } = this.props;
        let getCode = codeTableName ? { "CodeTableName": codeTableName, "ConditionMap": conditionMap } : {};
        if (codeTableName) {
            CodeTableService.getCodeTable(getCode).then((data) => {
                this.getCodeTableSoure(data, value, me);
            });
        } else if (url) {
            CodeTableService.fetchCodeTable(url).then((data) => {
                this.getCodeTableSoure(data, value, me);
            });
        } else {
            if (!_.isEmpty(this.codeTable)) {
                me.attr("title", this.codeTable.getValue(value));
            }
        }

    }

    getCodeTableSoure (data, value, me) {
        const dataArray = [];
        let codetable_key = config["DEFAULT_CODETABLE_KEYVALUE"]["KEY"];
        let codetable_value = config["DEFAULT_CODETABLE_KEYVALUE"]["VALUE"];
        let source = null;
        if (data && !_.isEmpty(data.codes)) {
            source = data.codes;
        } else if (data && !_.isEmpty(data.BusinessCodeTableValueList)) {
            codetable_key = config["DEFAULT_API_CODETABLE_KEYVALUE"]["KEY"];
            codetable_value = config["DEFAULT_API_CODETABLE_KEYVALUE"]["VALUE"];
            source = data.BusinessCodeTableValueList;
        } else {
            console.error("codetable  can't find data.");
        }
        if (source) {
            let valueList = "";
            source.forEach((codeItem) => {
                if (value && value instanceof Array) {
                    for (let i = 0; i < value.length; i++) {
                        if (codeItem[codetable_key] == value[i]) {
                            valueList += codeItem[codetable_value] + " ";
                        }
                    }
                } else if (codeItem[codetable_key] == value) {
                    valueList = codeItem[codetable_value];
                }

            });
            me.attr("title", valueList);
        }

    }

    /*** Init value */
    initValue () {
        $("#" + this.componentId).val(this.getComponentValue());
    }

    /*** Init disabled */
    initDisabled () {
        if (this.getDisabled() == "disabled") {
            let inputObj = $('#' + this.componentId);
            let inputGroup = inputObj.parent('.input-group');
            inputGroup.addClass("disabled");
        } else {
            let inputObj = $('#' + this.componentId);
            let inputGroup = inputObj.parent('.input-group');
            inputGroup.removeClass("disabled");
        }
        $("#" + this.componentId).attr("disabled", this.getDisabled());
    }

    initReadOnly () {

    }

    /*** Init property */
    initProperty () {
        this.initValue();

        this.initDisabled();

        this.initReadOnly();
    }

    /*** Init component */
    initComponent () {
    }

    /*** Init validator */
    initValidator () {
        ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
        if (this.props.validator) {
            this.props.validator.validate(this);
        } else if (this.props.io == "in" || this.props.io == null) {
            if (this.props.required) {
                Validator.validate("InputValidator", this);

                // call component validator
                Validator.validate(this.getValidatorId(), this);
            } else {
                ValidatorContext.removeValidator(this.getValidationGroup(), this.componentId);
            }
        }
    }

    getValidationGroup () {
        let validationGroupList = ValidatorContext.getValidatorList();
        let validationGroup = 'commonValidation';
        if (this.props.validationGroup == null || this.props.validationGroup == undefined) {
            for (let i = 0; i < validationGroupList.length; i++) {
                let item = validationGroupList[i];
                if (item != undefined && item.validator) {
                    let validate = item.validator[this.componentId];
                    if (validate != undefined) {
                        validationGroup = item.validatorId;
                        break;
                    }
                }
            }
            return validationGroup;
        } else {
            return this.props.validationGroup;
        }
    }

    /**@ignore
     * render children comonent required
     */
    renderRequired () {
        if (this._required && Util.isObject(this._required)) {
            return (
                <span>
                    {this._required}
                </span>
            )
        } else {
            if (Util.parseBool(this._required) && this.props.io != "out") {
                return (
                    <div id={this.componentId + "_required"} className={this.props.requiredText == null ?
                        "glyphicon glyphicon-asterisk" : "requiredTextClass"}
                        data-toggle="tooltip" data-placement="top" title={r18n.Required}
                        style={{ paddingLeft: "5px", color: "#ff5555", transform: "scale(0.7)"}}>
                        {this.props.requiredText != null ? this.props.requiredText : ""}
                    </div>
                );
            }
        }
    }

    /**@ignore
     * render children comonent helpText 
     */
    renderHelpText () {
        let doesUseHelpMessage = LocalContext.get('DOSE_USE_HELP_MESSAGE');
        if (doesUseHelpMessage == null || doesUseHelpMessage == undefined || Util.parseBool(doesUseHelpMessage)) {
            let propsHelpText = this.props.helpText;
            let propsHelpImage = this.props.helpImage;
            let propsHelpHtml = this.props.helpHtml;
            if (propsHelpText || propsHelpImage || propsHelpHtml) {
                let helpMessage = '';
                if (propsHelpText) {
                    helpMessage = this.getI18n(propsHelpText);
                } else if (propsHelpImage) {
                    let imageUrl = UrlUtil.getConfigUrl("STATIC_RESOURCE") + propsHelpImage;
                    helpMessage = imageUrl ? '<img src=' + imageUrl + ' width=100%/>' : '';
                } else if (propsHelpHtml) {
                    helpMessage = propsHelpHtml;
                }
                return (<span id={this.componentId + "_helpText"} className="glyphicon glyphicon-question-sign"
                    data-html={true} data-toggle="tooltip" data-placement="top"
                    title={helpMessage} data-original-title={helpMessage} style={{ paddingLeft: "5px", color: "#ed9c28" }}>
                </span>);
            }
        }
    }

    renderHelpIdText () {
        let doesUseHelpMessage = LocalContext.get('DOSE_USE_HELP_MESSAGE');
        if (doesUseHelpMessage == null || doesUseHelpMessage == undefined || Util.parseBool(doesUseHelpMessage)) {
            let propsHelpText = this.props.helpText;
            let propsHelpImage = this.props.helpImage;
            let propsHelpHtml = this.props.helpHtml;
            if (propsHelpText || propsHelpImage || propsHelpHtml) {
                let helpMessage = '';
                if (propsHelpText) {
                    helpMessage = this.getI18n(propsHelpText);
                } else if (propsHelpImage) {
                    let imageUrl = UrlUtil.getConfigUrl("STATIC_RESOURCE") + propsHelpImage;
                    helpMessage = imageUrl ? '<img src=' + imageUrl + ' width=100%/>' : '';
                } else if (propsHelpHtml) {
                    helpMessage = propsHelpHtml;
                }
                return (<span id={this.componentId + "_helpText"} className="glyphicon glyphicon-question-sign"
                    style={{ paddingLeft: "5px", color: "#ed9c28", cursor: 'pointer' }}>
                </span>);

            }
        }
    }

    openSrc () {
        event.stopPropagation();
        openWinTip(this.props.openId)
    }

    getWidthAllocation () {
        let allocation = this.props.widthAllocation.split(",");
        let widthAllocation = [];
        widthAllocation[0] = StringUtil.trim(allocation[0]);
        widthAllocation[1] = StringUtil.trim(allocation[1]);

        return widthAllocation;
    }

    getStyleClassArray () {
        let allocation = this.getWidthAllocation();
        let styleClass = [];
        styleClass[0] = "col-sm-" + allocation[0] + " col-md-" + allocation[0] + " col-lg-" + allocation[0];
        styleClass[1] = "col-sm-" + allocation[1] + " col-md-" + allocation[1] + " col-lg-" + allocation[1];

        return styleClass;
    }

    getStyleClassWidth () {
        let styleClassWidth = [];
        let colspan = this.props.colspan;
        
        if(this.props.layout && this.props.layout==='vertical'){
            styleClassWidth[0] = "0%";
            styleClassWidth[1] = "100%";
        }else{
            if (colspan != null && colspan != undefined) {
                let allocation = this.getWidthAllocation();
                styleClassWidth[0] = ((100 * allocation[0]) / (12 * colspan)).toFixed(8) + "%";
                styleClassWidth[1] = 100 - ((100 * allocation[0]) / (12 * colspan)).toFixed(8) + "%";
            }
        }

        return styleClassWidth;
    }

    setProperty () {
        super.setProperty();

        this._label = this.getProperty("label");
        this._required = this.getProperty("required");
    }

    componentWillMount () {
        super._componentWillMount();

        this.onEvent = { newValue: this.getComponentValue(), oldValue: null };
    }

    componentDidMount () {
        super._componentDidMount();
        $("[data-toggle='tooltip']").tooltip();
        if (this.props.io != "out") {
            this.initEvent();
            this.initProperty();
            this.initValidator();
            this.initComponent();
            this.onChangeColorByEndorsement();
            Event.initEventListener(this);

            ComponentContext.put(this.componentId, this);
        }else if(this.props.componentType=="newDatetimepicker"){
            this.initComponent();
        }
        this.showLabelTooltip();
        this.showValueTooltip();
        this.initDeleteEvent();
    }

    componentWillUpdate (nextProps, nextState) {
        super._componentWillUpdate();
    }

    componentDidUpdate (prevProps, prevState) {
        $("[data-toggle='tooltip']").tooltip();
        if (this.props.io != "out") {
            let object = $("#" + this.componentId);
            if (object.length != 0) {
                if (object.length > 0 && $._data(object[0], "events") == undefined) {
                    this.initEvent();
                }
            } else {
                object = $("input[name=" + this.componentId + "]");
                if (object.length > 0 && $._data(object[0], "events") == undefined) {
                    this.initCodeTableEvent();
                }
            }

            //if($("#" + this.componentId).length > 0 && $._data($("#" + this.componentId)[0], "events") == undefined){
            //  this.initEvent();
            //}

            if (document.activeElement != null && document.activeElement.id != this.componentId && this.props.componentType != "password") {
                this.initProperty();
            }

            this.initValidator();
            if (prevProps.io == "out" || !Util.parseBool(prevProps.visibled)) {
                this.initComponent();
            }
            this.showValueTooltip();
            this.initDeleteEvent();
            this.onChangeColorByEndorsement();
        }
        this.onEvent = { newValue: this.getComponentValue(), oldValue: this.onEvent.newValue };
    }

    initCodeTableEvent () {}

    showLabelTooltip () {
        let inputObj = $("#input-label-" + this.componentId);
        let labelElement = inputObj.parent();
        if (inputObj && inputObj[0] && inputObj[0].offsetWidth < inputObj[0].scrollWidth) {
            labelElement.attr("title", this.label);
        }
    }

    showValueTooltip () {
        let inputObj = $("#" + this.componentId);
        let inputDiv = $("#for-input-" + this.componentId);
        if (this.props.componentType == "text"
            || this.props.componentType == "number"
            || this.props.componentType == "textarea") {
            // if (inputObj.val() && !Util.parseBool(this.props.showValueTooltip) && this.getDisabled() == "disabled") {
            //     // inputObj.attr("title", inputObj.val());                
            //     inputDiv.attr("data-original-title", inputObj.val());
            // }
            let val = inputObj.val()?inputObj.val():"";
            if (Util.parseBool(this.props.isShowValueTooltip)) {
                inputDiv.attr("data-original-title", val);
            } else {
                if (this.props.title) {
                    inputDiv.attr("data-original-title", this.getI18n(this.props.title));
                } 
                // else {
                //     inputDiv.attr("data-original-title", "");
                // }
            }
        }
    }

};

// Minxin
//reactMixin.onClass(Input, BindToMixin);

/**@ignore
 * Input component prop types
 */
Input.propTypes = $.extend({}, Component.propTypes, {
    label: PropTypes.string,
    hasLabel: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    validationLabel: PropTypes.string,
    defaultValue: PropTypes.string,
    io: PropTypes.oneOf(["in", "out"]),
    placeHolder: PropTypes.string,
    //pattern: PropTypes.string,
    format: PropTypes.string,
    mask: PropTypes.string,
    helpText: PropTypes.string,
    helpImage: PropTypes.string,
    helpHtml: PropTypes.string,
    layout: PropTypes.oneOf(["horizontal", "vertical"]),
    itemIndex: PropTypes.string,
    widthAllocation: PropTypes.string,
    styleClassAllocation: PropTypes.string,
    validationGroup: PropTypes.string,
    title: PropTypes.string,
    needoverflow: PropTypes.bool,
    // 是否需要...显示
    needellipsis: PropTypes.bool,
    iconEnabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),

    required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    unableContext: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    unablePaste: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),

    minLength: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    maxLength: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    minLengthMessage: PropTypes.string,
    maxLengthMessage: PropTypes.string,
    requiredMessage: PropTypes.string,
    //componentType: PropTypes.string,
    inputClass: PropTypes.string,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    onKeyDown: PropTypes.func,
    onKeyUp: PropTypes.func,
    isValidation: PropTypes.string,
    validator: PropTypes.object,
    // showValueTooltip: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    overflowMarginTop: PropTypes.string,
    width: PropTypes.string,
    isShowLabelTitle: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    isintable: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    haslink: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    // isShowLableToolTip:PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    isShowValueTooltip:PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    // openSrc: PropTypes.func
    isTrim: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]) //是否去除前后空格
});

/**@ignore
 * Get Input component default props
 */
Input.defaultProps = $.extend({}, Component.defaultProps, {
    io: "in",
    //causeValidation: true,
    defaultValue: null,
    required: false,
    layout: config.DEFAULT_INPUT_LAYOUT,
    itemIndex: null,
    widthAllocation: "4,8",
    componentType: "INPUT",
    unableContext: "false",
    unablePaste: "false",
    inputClass: "inputClass",
    onChange: () => {
    },
    onBlur: () => {
    },
    onFocus: () => {
    },
    isValidation: "true",
    iconEnabled: "false",
    requiredText: null,
    needoverflow: true,
    needellipsis: true,
    // showValueTooltip: false,
    overflowMarginTop: '0px',
    isShowLabelTitle: config.SHOW_LABEL_TITLE ? config.SHOW_LABEL_TITLE : false,
    haslink: false,
    isintable: false,
    // isShowLableToolTip: false,
    isShowValueTooltip: config.SHOW_VALUE_TOOLTIP ? config.SHOW_VALUE_TOOLTIP : false,
    isTrim: true
});