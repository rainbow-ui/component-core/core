import config from "config";
import { PageContext,SessionContext } from "rainbow-desktop-cache";
import UniqueId from './UniqueId';
import { Util } from "rainbow-desktop-tools";
import PropTypes from 'prop-types';
import { CodeTableService } from "rainbow-desktop-codetable";

export default class Component extends React.Component {

    constructor(props) {
        super(props);
        //this.state = {};
    }

    render() {
        this.setProperty();
        if (Util.parseBool(this._visibled) && this.isShowComponent()) {
            return this.renderComponent();
        }
        return <noscript />;
    }

    renderComponent() {
        return <noscript />;
    }

    isShowComponent() {
        if (this.isDynamicProduct() && this.props.dynamicInformation && JSON.stringify(this.props.dynamicInformation.schema) != '{}') {
            let curSchema = this.props.dynamicInformation;
            return this.isInSchema(curSchema.schema, curSchema.schemaField, curSchema.policyField);
        }
        return true
    }

    isDynamicProduct() {
        let config = JSON.parse(sessionStorage.getItem("project_config")) || {};
        let dynamicProducts = config.UI_DYNAMIC_PRODUCTS;
        if (dynamicProducts && dynamicProducts.length > 0) {
            let curProduct = SessionContext.get('curProduct');
            let index = dynamicProducts.findIndex(item => item == curProduct);
            return index != -1;
        }
        return false
    }

    isInSchema(curSchema, node, field) {
        let config = JSON.parse(sessionStorage.getItem("project_config"))|| {};
        let dynamicProducts = config.UI_DYNAMIC_PRODUCTS;
        if (dynamicProducts && dynamicProducts.length > 0) {           
            if (this.isDynamicProduct()) {
                if (curSchema && curSchema.schemaField 
                    && curSchema.schemaField[node]) {
                        let allfield = this.getKeysArr(curSchema.schemaField[node]);
                        return allfield.indexOf(field.toLowerCase()) != -1
                }
            } else {
                return true;
            }
            
        } else {
            return true
        }
    }

    getKeysArr(arr) {
		if (arr) {
			return _.map(arr,'ProductElementName').join(',').toLowerCase()
		}
		return ''
    }

    getProperty(propertyName, component) {
        let _self = component ? component : this;
        let property = _self.props[propertyName];

        if (Util.isFunction(property)) {
            return property(_self);
        }

        return property;
    }

    getId() {
        return this.componentId;
    }


    getRefProperty() {
        // for ref is the key word in react,use refName to define ref.
        if (this.props.refName) {
            return this.props.refName;
        } else {
            return this.getId();
        }
    }


    getComposedRefProperty() {
        let refProperty = this.getRefProperty();
        return refProperty + "_composed";
    }

    getName() {
        if (this.props.name) {
            return this.props.name;
        } else {
            return this.componentId;
        }
    }

    getNameForTest() {
        return this.componentName;
    }

    getDisabled() {
        const pageReadOnly = Util.parseBool(PageContext.get("PAGE_READONLY"));
        if(pageReadOnly){
            const ignorePageReadOnly = Util.parseBool(this.props.ignorePageReadOnly);
            if(ignorePageReadOnly){
                if (Util.parseBool(this.getProperty("disabled")) || !Util.parseBool(this.getProperty("enabled"))) {
                    return "disabled";
                } else {
                    return null;
                }
            }else{
                return "disabled";
            }
        }else{
            if (Util.parseBool(this.getProperty("disabled")) || !Util.parseBool(this.getProperty("enabled"))) {
                return "disabled";
            } else {
                return null;
            }
        }
    }

    getReadOnly() {
        if (Util.parseBool(this.getProperty("readonly"))) {
            return "disabled";
        }
        return null;
    }

    getURL() {
        let url = this.getProperty("url")
        // if (url.indexOf("?") > -1) {
        //     url = url + "&" + sessionStorage.getItem("Authorization");
        // } else {
        //     url = url + "?" + sessionStorage.getItem("Authorization");
        // }

        return url;
    }

    setProperty() {
        this._disabled = this.getProperty("disabled");
        this._enabled = this.getProperty("enabled");
        // this._visibled = this.getProperty("visibled");
        this._visibled = this.checkVisibled();

    }

    checkVisibled(){
        const p_code = SessionContext.get("PERMISSION-CODE",false);
        let  permissionCodes  = PageContext.get(p_code);
        if(!permissionCodes){
            const _code = SessionContext.get(p_code);
            if(_code){
                permissionCodes = JSON.parse( Util.uncompile(_code));
                PageContext.put(p_code,permissionCodes);
            }
        }
        if(permissionCodes){
            const noI18n = this.getProperty("noI18n");
            const permissionCode = this.getProperty("permissionCode");
            const i18nCode = this.getProperty("label")||this.getProperty("value");
            let checkCode = permissionCode;
            if(!Util.parseBool(noI18n)&&_.isEmpty(permissionCode)){
                    checkCode = i18nCode;
            }
            const check_code = _.find(permissionCodes,(code)=>{
                return code==checkCode;
            });
            if(_.isEmpty(check_code)){
                return Util.parseBool(this.getProperty("visibled"));
            }else{
                return false;
            }
        }else{
            return Util.parseBool(this.getProperty("visibled"));
        }
    }

    countdown() {
        let end = new Date();
        end.setFullYear(2018);
        end.setMonth(0);
        end.setDate(15);
        end.setHours(0);
        end.setMinutes(0);
        end.setSeconds(0);
        let now = new Date();

        let m = Math.round((end - now) / 1000);
        let day = parseInt(m / 24 / 3600);
        let hours = parseInt((m % (3600 * 24)) / 3600);
        let minutes = parseInt((m % 3600) / 60);
        let seconds = m % 60;

        if (m > 0) {
            return "距离废弃 codetabe Id 还剩" + day + "天" + hours + "小时" + minutes + "分钟" + seconds + "秒";
        }

    }

    getI18n(code) {
        if (config.DOES_USE_I18N == undefined) {
            return code;
        } else if (config.DOES_USE_I18N&&i18n) {
            if (Util.parseBool(this.getProperty("noI18n"))) {
                return code;
            } else {
                let lowI18n = {};
                for(let key in i18n){
                    let lowKey = key.toLowerCase();
                    lowI18n[lowKey] = i18n[key]
                }
                let lowCode = code?code.toLowerCase():undefined;
                const value = lowI18n[lowCode];
                if (!_.isEmpty(lowCode)) {
                    if (_.isEmpty(value)) {
                        return config.DEFAULT_I18N_CONFIGURATION_GROUP + "." + lowCode;

                    } else {
                        return value;
                    }
                }
            }
        } else {
            if (Util.parseBool(this.getProperty("noI18n"))) {
                return code;
            } else if(i18n&&i18n[code]) {
               return i18n[code]
            }else{
                return code
            }
        }
    }

    componentWillMount() {
        this._componentWillMount();
    }

    _componentWillMount() {
        this.componentId = (this.props.id) ? this.props.id : UniqueId.generateId();

        this.componentName = "";
        if (this.props.id) {
            this.componentName = this.props.id;
        } else {
            const props = this.props;
            const arr = [props.label, props.value, props.valueLink, props.model, props.property];

            for (let i = 0; i < arr.length; i++) {
                if (arr[i]) {
                    this.componentName += arr[i];
                }
            }
            this.componentName = this.componentName.replace(/[#\{\}\.\[\] ]/g, "_").replace(/_+/g, "_");
            if (this.componentName[this.componentName.length - 1] == "_") {
                this.componentName = this.componentName.substring(0, this.componentName.length - 1);
            }
        }

        //this.setProperty();
    }

    componentDidMount() {
        this._componentDidMount();
    }

    _componentDidMount() {
        UniqueId.addComponentId();
    }

    componentWillUpdate() {
        this._componentWillUpdate();
    }

    _componentWillUpdate() {
        //this.setProperty();
    }

    componentDidUpdate() {
        this._componentDidUpdate();
    }

    _componentDidUpdate() {

    }



}


/**  
 * @ignore
 * Component component prop types
 */
Component.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    style: PropTypes.object,
    dynamicInformation: PropTypes.object,
    styleClass: PropTypes.oneOf(["default", "primary", "success", "warning", "danger", "info"]),
    className: PropTypes.string,
    enabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string, PropTypes.func]),
    disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string, PropTypes.func]),
    visibled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    onClick: PropTypes.func,
    ignorePageReadOnly: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};

/**
 * Get component component default props
 */
Component.defaultProps = {
    enabled: true,
    disabled: false,
    visibled: true,
    style: {},
    styleClass: config.DEFAULT_STYLE_CLASS,
    onClick: () => { },
    ignorePageReadOnly: false
};

