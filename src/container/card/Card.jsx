import Component from '../../basic/Component';
import Cell from '../Cell';
import OnClickEvent from '../../event/OnClickEvent';
import Param from "../../basic/Param";
import { Util, ELUtil } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';
import config from "config";

export default class Card extends Component {

    componentDidMount(){
        super.componentDidMount()
        this.initCollapseEvent();
        if ($('#' + this.componentId).parents('td.hastddetail') && $('#' + this.componentId).parents('td.hastddetail').length > 0 && $('#'+this.componentId).attr('class') == 'card-block collapse show') {
            let height = $('#'+this.componentId).height();
            if (height < 40) {
                height = 40;
            }
            $('#'+this.componentId).attr({'realheight':height})
        }
    }
    
    componentDidUpdate(){
        if ($('#' + this.componentId).parents('td.hastddetail') && $('#' + this.componentId).parents('td.hastddetail').length > 0 && $('#'+this.componentId).attr('class') == 'card-block collapse show') {
            let height = $('#'+this.componentId).height();
            if (height < 40) {
                height = 40;
            }
            $('#'+this.componentId).attr({'realheight':height})
        }
    }
    renderComponent() {
        let tempClass = this.props.className + ' card';
        const { children, enabled, outline, styleClass, title, style ,titleContent} = this.props;

        if (!Util.parseBool(enabled)) {
            this.recursChildren(children);
        }

        if (outline) {
            tempClass += ' card-outline-' + outline;
        }
        if (styleClass && styleClass != 'default') {
            tempClass += ' card-inverse card-' + styleClass;
        }

        return (
            <div className={tempClass} style={style}>
                {this.props.anchor ? this.renderAnchor() : null}
                {title ? this.renderHeader() : null}
                {this.renderImage()}
                {this.renderBlock()}
                {this.renderFooter()}
            </div>
        );
    }

    renderAnchor() {
        return (
            <a id={this.props.anchor} anchor='true' style={this.props.anchorTop?{position: 'absolute', top:'-' + this.props.anchorTop}:{}}></a>
        )
    }
    renderHeader() {
        const { title, functions, subTitle, titleContent} = this.props;
        return (
            <div className="card-header" >
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div className='cardHeaderFunction'>
                        {this.renderCardTitle()}
                        {this.props.titleFunctions ? this.props.titleFunctions : <noscript />}
                    </div>
                    <div className="cardHeaderContentFunction">
                        <div className="cardContentFunction">
                            {this.renderCardTitleContent()}
                        </div>
                        <div className='cardFunction'>
                            {functions}
                            {this.renderCollapse()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderCardTitle() {
        if (Util.isObject(this.props.title)) {
            return (
                <span>
                    <a onClick={this.showCardContent.bind(this)} id={this.componentId+"-collapse"} data-toggle="collapse" href={"#" + this.componentId} aria-expanded="false" aria-controls={this.componentId}>
                        {this.props.title}
                    </a>
                </span>
            );
        } else {
            return (
                <a onClick={this.showCardContent.bind(this)} id={this.componentId+"-collapse"} data-toggle="collapse" href={"#" + this.componentId} aria-expanded="false" aria-controls={this.componentId}>
                    <h4 className="card-title" tabindex="-1">
                        {this.getI18n(this.getProperty("title"))}
                        {
                            Util.parseBool(this.props.icon) ?
                                <span className={this.props.icon}/> :
                            null
                        }
                    </h4>
                </a>
            )
        }
    }

    initCollapseEvent(){
        let collapseObj = $("#" + this.componentId +"-collapse");
        collapseObj.bind("keydown", (event)=>{
            if(event.keyCode == "13"){
                $("#"+this.componentId).collapse("toggle");
            }
        })
    }

    onClickTitle(){

    }

    renderCardTitleContent(){
        if (Util.isObject(this.props.titleContent)) {
            return (
                <span>{this.props.titleContent}</span>
            );
        } else {
            return (
                <span>{this.getI18n(this.getProperty("titleContent"))}</span>
            )
        }
    }


    renderImage() {
        const { imageSrc } = this.props;

        return (
            imageSrc ?
                <img className="card-img-top" width="100%" src={imageSrc} /> :
                null
        );
    }

    renderBlock() {
        const { collapse, children } = this.props;

        return (
            <div className={Util.parseBool(collapse) ? 'card-block collapse show' : 'card-block collapse'} id={this.componentId}>
                {children}
            </div>
        );
    }

    renderCollapse() {
        const { collapseIcon } = this.props;

        return (
            Util.parseBool(collapseIcon) ?
                <a className="showCardContent" data-toggle="collapse" href={"#" + this.componentId} aria-expanded="false" aria-controls={this.componentId} tabindex='-1'>
                    <span className={this.icon()} onClick={this.onClick.bind(this)}></span>
                </a> :
                null
        );
    }
    icon(){
        const { collapse } = this.props;
        return (
            Util.parseBool(collapse) ? 'glyphicon glyphicon-chevron-down' : 'glyphicon glyphicon-chevron-up'
        )
    }
    showCardContent(event) {
        let childNode = $(event.target).parents(".cardHeaderFunction").siblings('.cardHeaderContentFunction').children('.cardFunction').children('.showCardContent').children('span');
        let icon = childNode.attr("class");
        let up = 'glyphicon glyphicon-chevron-up';
        let down = 'glyphicon glyphicon-chevron-down';
        if (icon == up) {
            childNode.removeClass(icon);
            childNode.addClass(down);
        }
        if (icon == down) {
            childNode.removeClass(icon);
            childNode.addClass(up);
        }
    }
    onClick(event) {
        let icon = $(event.target).attr("class");
        let up = 'glyphicon glyphicon-chevron-up';
        let down = 'glyphicon glyphicon-chevron-down';
        if (icon == up) {
            $(event.target).removeClass(icon);
            $(event.target).addClass(down);
        }
        if (icon == down) {
            $(event.target).removeClass(icon);
            $(event.target).addClass(up);
        }
        event.preventDefault();
        let clickEvent = new OnClickEvent(this, event, Param.getParameter(this));
        if (this.props.onClick) {
            this.props.onClick(new OnClickEvent(this, event, Param.getParameter(this)));
        }
    }
    renderFooter() {
        const { footer } = this.props;

        return (
            footer ?
                <div className="card-footer">
                    <small className="text-muted">{footer}</small>
                </div> :
                null
        );
    }

    recursChildren(children) {
        if (children.props) {
            React.Children.forEach(children, item => {
                if (item.props.enabled !== 'true' && item.props.enabled !== true) {
                    item.props.enabled = 'false';
                }
                this.recursChildren(item.props.children);
            });
        }
    }
}

/**@ignore
 * Panel component prop types
 */
Card.propTypes = $.extend({}, Component.propTypes, {
    enabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    outline: PropTypes.oneOf(["none", "default", "primary", "success", "warning", "danger", "info"]),
    footer: PropTypes.string,
    imageSrc: PropTypes.string,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    titleContent: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    subTitle: PropTypes.string,
    functions: PropTypes.func,
    titleFunctions: PropTypes.func,
    collapse: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    collapseIcon: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    icon: PropTypes.oneOfType([PropTypes.string, PropTypes.bool])
});

/**@ignore
 * Get dynamicSection component default props
 */
Card.defaultProps = $.extend({}, Component.defaultProps, {
    styleClass: '',
    className: '',
    outline: 'none',
    collapse: 'true',
    collapseIcon: config.DEFAULT_CARD_COLLAPSEICON ? config.DEFAULT_CARD_COLLAPSEICON :"false",
    icon: 'false'
});