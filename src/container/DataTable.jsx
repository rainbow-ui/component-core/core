import Data from "../basic/Data";
import SelectionMode from "./SelectionMode";
import OnRowSelectEvent from '../event/OnRowSelectEvent';
import config from "config";
import UniqueId from '../basic/UniqueId';
import r18n from "../i18n/reactjs-tag.i18n";
import { PageContext, ValidatorContext, ComponentContext, LocalContext } from 'rainbow-desktop-cache';
import { Util, ELUtil, UrlUtil } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';
import Spinner from "../other/Spinner";
import image from "../image/table-empty01.png";
import I18nUtil from '../i18n/I18NUtil';

export default class DataTable extends Data {

    constructor(props) {
        super(props);
        this.isClick = true;
        this.showPagination = true;
        this.timer = {};
        this.arrFixedTh = [],
            this.arrFixedRightTH = [], // 锁定的右边列头
            this.arrFixedBody = [],
            this.arrFixedRightBody = [] // 锁定的右边列
    }

    /**@ignore
     * Get selected record
     */
    static getSelectedRecord(dataTableId) {
        let recordArray = [];

        // handler single select
        $("input:radio[name=" + dataTableId + "_single" + "]").each(function (index, element) {
            if (element.checked) {
                let tableRow = $(element).closest("tr");
                recordArray.push(JSON.parse($(tableRow).attr("data-value")));
            }
        });

        // handler multiple select
        $("input:checkbox[name=" + dataTableId + "_multiple" + "]").each(function (index, element) {
            if (element.checked) {
                let tableRow = $(element).closest("tr");
                recordArray.push(JSON.parse($(tableRow).attr("data-value")));
            }
        });

        return recordArray;
    }

    renderData() {
        let tableClass = "scrollTable " + this.props.className;
        let bodyWidht = $('#' + this.componentId).width();
        if (bodyWidht && bodyWidht < $('#' + this.componentId).parent('div').width()) {
           return (
                <div className={tableClass} style={{ width: "auto" }}>
                    {this.renderBeforeData()}
                    {this.renderDropList()}
                    {this.renderTable()}
                </div>
            );
        } else {
            return (
                // mark
                <div className={tableClass} style={{ width: "auto"}}>
                    {/* 注释掉这两层，是因为在dd中的searchtable中，输入框搜索问题 */}
                {/* <div style={{position: 'relative'}}> */}
                {/* <div className={tableClass} style={{ width: "auto"}}> */}
                    {this.renderBeforeData()}
                    {this.renderDropList()}
                    {(Util.parseBool(this.props.isNeedLockColumns) || Util.parseBool(this.props.isNeedLockRightColumns)) && this.pageValue && this.pageValue.length > 0 ? this.renderFixedTable() : this.renderTable()}
                {/* </div> */}
                {/* </div> */}
                </div>
            );
        }
    }

    renderDropList() {
        if (!Util.parseBool(this.props.isColumnCustomDisp)) return;
        if (this.props.children.length <= 0) return;
        return (
            <div class="fixed-table-toolbar">
                <div class="columns columns-right btn-group pull-right">
                    <div class="keep-open btn-group" title="Columns">
                        <button type="button" aria-label="columns" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-th icon-th"></i>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            {
                                this.props.children.filter(e => e.props.value).map(e => (
                                    <li role="menuitem"><label><input type="checkbox" value={e.props.headTitle} onClick={this.handleClick} checked={e.state.display} />{e.props.headTitle}</label></li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            </div>
        )
    }

    renderFixedTable() {
        return (
            <div className="fixed-table-box row-col-fixed">
                {this.renderFixedTableHeaderWraper()}
                {this.renderFixedTableBodyWraper()}
                {Util.parseBool(this.props.isNeedLockColumns) ? this.renderFixedTableFixedLeft() : ''}
                {Util.parseBool(this.props.isNeedLockRightColumns) ? this.renderFixedTableFixedRight() : ''}
                {Util.parseBool(this.props.isNeedLockRightColumns) ? this.renderFixedTableRightPatch() : ''}
            </div>

        )

    }

    /**1 */
    renderFixedTableHeaderWraper() {
        return (
            <div className="fixed-table_header-wraper">
                <table id={this.componentId + 'header-wraper'} className="fixed-table_header">
                    <thead ><tr>{this.renderTableHeader()}</tr></thead>
                </table>
            </div>
        )

    }

    /**2 */
    renderFixedTableBodyWraper() {
        return (
            <div className="fixed-table_body-wraper ">
                <table id={this.componentId} className="fixed-table_body table dttable table-responsive">
                    <tbody>{this.renderTableBody()}</tbody>
                </table>
            </div>
        )
    }

    /**3 */
    renderFixedTableFixedLeft() {
        return (
            <div className="fixed-table_fixed fixed-table_fixed-left">
                <div className="fixed-table_header-wraper">
                    <table className="fixed-table_header" id={this.componentId + '-fixed-table_fixed-left'}>
                        <thead>
                            {this.arrFixedTh}
                        </thead>
                    </table>
                </div>
                <div className="fixed-table_body-wraper">
                    <table id={this.componentId + '-fixed-table_body'} className="fixed-table_body table dttable table-responsive">
                        <tbody>
                            {this.arrFixedBody}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

    /**4 */
    renderFixedTableFixedRight() {
        return (
            <div className="fixed-table_fixed fixed-table_fixed-right">
                <div className="fixed-table_header-wraper">
                    <table className="fixed-table_header" id={this.componentId + '-fixed-table_fixed-right'}>
                        <thead>
                            {this.arrFixedRightTH}
                        </thead>
                    </table>
                </div>
                <div className="fixed-table_body-wraper">
                    <table id={this.componentId + '-fixed-table_body-right'} className="fixed-table_body table dttable table-responsive">
                        <tbody>
                            {this.arrFixedRightBody}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

    /**5 */
    renderFixedTableRightPatch() {
        return (
            <div className="fixed-table-box_fixed-right-patch" style={{ width: '5px', height: '30px' }}>

            </div>
        )
    }

    /**@ignore
     * Render table
     */
    renderTable() {

        //<caption />
        let table_class = "table dttable table-responsive";
        if (this.props.thBreakAll == 'true' && this.props.tdBreakAll == 'false') {
            table_class = "table dttable table-responsive thBreakAll";
        }
        if (this.props.thBreakAll == 'true' && this.props.tdBreakAll == 'true') {
            table_class = "table dttable table-responsive thBreakAll tdBreakAll";
        }
        if (this.props.thBreakAll == 'false' && this.props.tdBreakAll == 'true') {
            table_class = "table dttable table-responsive tdBreakAll";
        }
        // let thNoResult = '';
        // if (_.isEmpty(this.pageValue)) {
        //     thNoResult = 'no-result'
        // }
        return (
            <table id={this.componentId} className={table_class} data-auto-test={this.getNameForTest()}>

                {this.props.renderTableHeader ? this.props.renderTableHeader() : <thead ><tr>{this.renderTableHeader()}</tr></thead>}

                <tbody>{this.renderTableBody()}</tbody>
            </table>
        )
    }

    /**@ignore
     * Render table header
     */
    renderTableHeader() {
        let _self = this, tableHeaderArray = [], index = 0;
        const arr_fix = this.props.fixedColumns.split(',')

        // handler page index
        if (Util.parseBool(this.props.indexable)) {
            let lang = I18nUtil.getSystemI18N();
            let indexTitle = this.props.indexTitle;
            if (!this.props.indexTitle) {
                indexTitle = lang == 'zh_CN' ? '序号' : 'Order'
            }
            tableHeaderArray.push(<th style={{ width: this.props.indexTitleWidth }} >{indexTitle}</th>);
        }

        // handler single and multiple selected
        if (this.props.selectionMode == "single") {
            tableHeaderArray.push(<th vis></th>);
        } else if (this.props.selectionMode == "multiple") {
            tableHeaderArray.push(
                <th style={{width:this.props.width,minWidth: this.props.width}}>
                    <input type="checkbox" id={this.componentId + "_multiple_selectall"}
                        name={this.componentId + "_multiple_selectall"} />
                </th>
            );
        }

        React.Children.forEach(this.props.children, function (child) {
            if (child.props.selectionMode == "single") {
                tableHeaderArray.push(<th></th>);
            } else if (child.props.selectionMode == "multiple") {
                tableHeaderArray.push(
                    <th style={{width:child.props.width,minWidth: child.props.width}}>
                        <input type="checkbox" id={_self.componentId + "_multiple_selectall"}
                            name={_self.componentId + "_multiple_selectall"} disabled={_self.getDisabledStyleClass()} />
                    </th>
                );
            } else {
                let style = {
                    width: child.props.width,
                    display: child.props.display ? '' : 'none',
                    position: _.indexOf(arr_fix, index.toString()) ? 'fixed' : 'static'
                };
                if (child && child.props) {
                    Util.parseBool(child.props.visibled) ?
                        tableHeaderArray.push(
                            <th data-field={child.props.value ? child.props.value : ""} style={{ width: child.props.width, textAlign: child.props.align, minWidth: child.props.width }}>
                                {child.props.headerTitle ? !Util.isString(child.props.headerTitle) || Util.parseBool(child.props.noI18n) ? child.props.headerTitle : _self.getI18n(child.props.headerTitle) : ""}
                                {_self.renderTableHeaderSortIcon(child)}
                                {_self.renderRequired(child)}
                                {_self.renderHelpText(child)}
                            </th>
                        ) : null;
                }
                index++;
            }
        });

        // handler row detail
        if (Util.parseBool(this.props.detailable)) {
            if (this.props.detailSecquence) {
                tableHeaderArray.splice(Number(this.props.detailSecquence), 0, <th />);
            } else {
                tableHeaderArray.push(<th />);
            }
        }
        if (Util.parseBool(this.props.isNeedLockColumns)) {
            this.arrFixedTh = [];
            let num = parseInt(this.props.lockNumColumns);
            this.arrFixedTh = tableHeaderArray.slice(0, num); // 锁定的列th数组
        }
        if (Util.parseBool(this.props.isNeedLockRightColumns)) {
            let numright = parseInt(this.props.lockNumRightColumns);
            this.arrFixedRightTH = tableHeaderArray.slice(tableHeaderArray.length - numright, tableHeaderArray.length);  // 右边默认锁定1列
        }
        return tableHeaderArray;
    }

    renderHelpText(column) {
        let doesUseHelpMessage = LocalContext.get('DOSE_USE_HELP_MESSAGE');
        if (doesUseHelpMessage == null || doesUseHelpMessage == undefined || Util.parseBool(doesUseHelpMessage)) {
            let propsHelpText = column.props.helpText;
            let propsHelpImage = column.props.helpImage;
            let propsHelpHtml = column.props.helpHtml;
            let propsHelpImageUrl = column.props.helpImageUrl;
            if (propsHelpText || propsHelpImage || propsHelpHtml || propsHelpImageUrl) {
                let helpMessage = '';
                if (propsHelpText) {
                    helpMessage = this.getI18n(propsHelpText);
                } else if (propsHelpImage) {
                    let imageUrl = UrlUtil.getConfigUrl("STATIC_RESOURCE") + propsHelpImage;
                    helpMessage = imageUrl ? '<img src=' + imageUrl + ' width=100%/>' : '';
                } else if (propsHelpImageUrl) {
                    helpMessage = '<img src=' + propsHelpImageUrl + ' width=100%/>';
                } else if (propsHelpHtml) {
                    helpMessage = propsHelpHtml;
                }
                return (<span id={this.componentId + "_helpText"} className="glyphicon glyphicon-question-sign"
                    data-html={true} data-toggle="tooltip" data-placement="top"
                    title={helpMessage} data-original-title={helpMessage} style={{ paddingLeft: "5px", color: "#ed9c28" }}>
                </span>);
            }
        }
    }

    renderRequired(column) {
        if (Util.parseBool(column.props.required)) {
            return (
                <span className="glyphicon glyphicon-asterisk" style={{ paddingLeft: "5px", color: "#ff5555", transform: "scale(0.7)" }}>
                </span>
            );
        }
    }

    /**@ignore
     * Render table header sort icon
     */
    renderTableHeaderSortIcon(column) {
        if (Util.parseBool(column.props.sortable) || (column.props.sortable == undefined && Util.parseBool(this.props.sortable))) {
            let value = column.props.value || column.props.sortValue;

            if (this.state.sortColumn == value) {
                if (this.state.sortWay == "desc") {
                    // 降序 => 从大到小
                    return (<a href="javascript:void(0);" data-value={value} onClick={this.onSort.bind(this)}
                        className="ui-table-header-sort pull-right glyphicon glyphicon-triangle-bottom datatable-sort-icon-position" />);
                } else if (this.state.sortWay == "asc") {
                    // 升序 => 从小到大
                    return (<a href="javascript:void(0);" data-value={value} onClick={this.onSort.bind(this)}
                        className="ui-table-header-sort pull-right glyphicon glyphicon-triangle-top datatable-sort-icon-position" />);
                }
            }
            return (<a href="javascript:void(0);" data-value={value} onClick={this.onSort.bind(this)}
                className="ui-table-header-sort pull-right glyphicon glyphicon-sort datatable-sort-icon-position" />);
        }
    }
    /**@ignore
     * Render table body
     */
    renderTableBody() {
        let _self = this;
        let tableBodyArray = [], tableTD = [];

        const comTable = $('#' + _self.componentId);
        const lastTh = comTable.find('thead tr:last th')
        const findTable = comTable.find('table');
        const tableTh = comTable.find('thead tr th')
        const tableNoImg = $('.' + this.componentId + "table-no-result-image");
        const tableNoMessage = $('.' + this.componentId + "table-no-result-msg")
        //let value = this.pageValue;
        if (this.pageValue && this.pageValue instanceof Array) {
            let selectState = this.getProperty("selectState");
            let pageIndex = this.currentPageIndex;
            let pageSize = this.state.pageSize;

            if (this.pageValue && this.pageValue.length == 0 && pageIndex != '1' && parseInt(pageIndex) > 0) {
                this.onToPage(parseInt(pageIndex) - 1); 
            }

            $.each(this.pageValue, function (index, element) {
                if (!element) { return; }
                tableTD = [];
                let count = 0;
                if (Util.parseBool(config.DEFAULT_DATATABLE_IS_INDEX)) {
                    //element.dataIndex = index;
                    element.dataIndex = (pageIndex - 1) * pageSize + index;
                }

                // handler page index
                if (Util.parseBool(_self.props.indexable)) {
                    //tableTD.push(<td>{index + 1}</td>);
                    tableTD.push(<td style={{ minWidth: '50px' }}>{(pageIndex - 1) * pageSize + index + 1}</td>);
                    count++;
                }

                // handler single and multiple select
                if (_self.props.selectionMode == "single") {
                    let value = (selectState != null && selectState != undefined && $.inArray(index, selectState) != -1) ? 1 : 0;
                    tableTD.push(<td style={{ minWidth: '50px' }}><SelectionMode id={_self.componentId + "_single" + index}
                        name={_self.componentId + "_single"} value={value}
                        selectionMode="single" enabled={_self.props.enabled} /></td>);
                    count++;
                } else if (_self.props.selectionMode == "multiple") {
                    let value = (selectState != null && selectState != undefined && $.inArray(index, selectState) != -1) ? 1 : 0;
                    tableTD.push(<td style={{ minWidth: '50px' }}><SelectionMode id={_self.componentId + "_multiple" + index}
                        name={_self.componentId + "_multiple"} value={value}
                        selectionMode="multiple" enabled={_self.props.enabled} /></td>);
                    count++;
                }



                React.Children.forEach(_self.props.children, function (child, itemindex) {
                    let value = _self.getColumnValue(element, child.props.value);
                    value = (value == null || value == undefined) ? "" : value;
                    let realw = $(lastTh[itemindex + count]).css('min-width');
                    if (findTable && findTable.length > 0) {
                        realw = $(tableTh[itemindex + count]).css('min-width');
                    }
                    if (!realw) {
                        realw = child.props.width && child.props.width.indexOf('px') > -1 ? child.props.width : '100%';
                    }else{
                        realw = child.props.width;
                    }

                    // if (realw && realw.indexOf('%') == -1) {
                    //     realw = realw ? parseInt(realw.replace('px', '')) : 70;
                    // } else {
                    //     realw = $(lastTh[itemindex + count]).width();
                    // }
                    // if (realw > 70) {
                    //     realw = realw;
                    // } else {
                    //     realw = 70;
                    // }

                    if (React.isValidElement(child.props.children)) {
                        let cloneChildren = null; 
                        if (child.props.cuzObjectRelation) {
                            let modelObj = _self.findModelByCuzObjectRelation(element, child.props.cuzObjectRelation, child.props.value);
                            value = modelObj ? modelObj[child.props.value] : "";
                            if (child.props.children.props.componentType == "codingComponent") {
                                child.props.children.props.model = element;
                                child.props.children.props.property = child.props.value;
                            }
                            if (child.props.children.props.io == "in") {
                                if (Util.parseBool(child.props.visibled)) {
                                    cloneChildren = React.cloneElement(child.props.children, {
                                        id: child.props.children.props.id + "_" + index + parseInt(Math.random() * 100),
                                        model: modelObj,
                                        property: child.props.value,
                                        value: (_self.props.var != undefined) ? "#{" + _self.props.var + "[" + index + "]." + child.props.value + "}" : value
                                    });
                                }
                            } else {
                                cloneChildren = React.cloneElement(child.props.children, {
                                    id: child.props.children.props.id + "_" + index + parseInt(Math.random() * 100),
                                    model: modelObj,
                                    property: child.props.value,
                                    value: value
                                });
                            }
                        } else {
                            value = _self.getColumnValue(element, child.props.value) || _self.getColumnValue(element, child.props.value) == 0 ? _self.getColumnValue(element, child.props.value) : child.props.children.props.defaultValue;
                            if (child.props.children.props.componentType == "codingComponent") {
                                child.props.children.props.model = element;
                                child.props.children.props.property = child.props.value;
                            }
                            if (child.props.children.props.io == "in") {
                                cloneChildren = React.cloneElement(child.props.children, {
                                    id: child.props.children.props.id + "_" + index,
                                    model: element,
                                    property: child.props.value,
                                    value: (_self.props.var != undefined) ? "#{" + _self.props.var + "[" + index + "]." + child.props.value + "}" : value
                                });
                            } else {
                                cloneChildren = React.cloneElement(child.props.children, {
                                    id: child.props.children.props.id + "_" + index,
                                    model: element,
                                    property: child.props.value,
                                    value: value
                                });
                            }
                        }
                        if (child && child.props) {
                            Util.parseBool(child.props.visibled) ?
                                tableTD.push(<td style={{ minWidth: '50px' }} realWidth={realw} className="tddefault" tdIndex={itemindex + count}>{cloneChildren}</td>) : null
                        };

                    } else if (child.props.selectionMode == "single") {
                        //tableTD.push(<td><input type="radio" name={_self.props.id + "_single"} checked={(1 == value || "1" == value) ? "checked" : null} /></td>);
                        value = (selectState != null && selectState != undefined && $.inArray(index, selectState) != -1) ? 1 : value;
                        if (child && child.props) {
                            Util.parseBool(child.props.visibled) ?
                                tableTD.push(<td style={{ minWidth: '50px' }} realWidth={realw} tdIndex={itemindex + count}><SelectionMode id={_self.componentId + "_single" + index}
                                    name={_self.componentId + "_single"} value={value}
                                    selectionMode="single" enabled={child.props.enabled} /></td>) : null
                        };
                    } else if (child.props.selectionMode == "multiple") {
                        //tableTD.push(<td><input type="checkbox" name={_self.props.id + "_multiple"} checked={(1 == value || "1" == value) ? "checked" : null} /></td>);
                        value = (selectState != null && selectState != undefined && $.inArray(index, selectState) != -1) ? 1 : value;
                        if (child && child.props) {
                            Util.parseBool(child.props.visibled) ?
                                tableTD.push(<td style={{ minWidth: '50px' }} realWidth={realw} tdIndex={itemindex + count}><SelectionMode id={_self.componentId + "_multiple" + index}
                                    name={_self.componentId + "_multiple"} value={value}
                                    selectionMode="multiple" enabled={child.props.enabled} /></td>) : null
                        };
                    } else if (child.props.render != undefined) {
                        let render = child.props.render;
                        if (typeof render == "function") {
                            if (child && child.props) {
                                Util.parseBool(child.props.visibled) ?
                                    tableTD.push(<td style={{ minWidth: '50px' }} realWidth={realw} tdIndex={itemindex + count} className="tddefault">{render(element)}</td>) : null
                            };
                        } else {
                            if (child && child.props) {
                                Util.parseBool(child.props.visibled) ?
                                    tableTD.push(<td style={{ minWidth: '50px' }} realWidth={realw} tdIndex={itemindex + count} className="tddefault">==None==</td>) : null
                            };
                        }
                    } else {
                        if (child && child.props) {
                            Util.parseBool(child.props.visibled) ?
                                tableTD.push(<td style={{ minWidth: '50px' }} realWidth={realw} tdIndex={itemindex + count} className="tddefault">{String(value)}</td>) : null
                        };
                    }
                });
                const strValue = JSON.stringify(element);
                if (_self.props.highlight) {
                    const _index = $.inArray(index, _self.props.highlight);
                    if (_index >= 0) {
                        tableBodyArray.push(<tr onDoubleClick={_self.onDoubleClick.bind(_self, strValue, index)}
                            onClick={_self.onClick.bind(_self, strValue, index)} className="highlight" id={_self.props.id + "_tr_" + index} 
                            data-value={strValue}>{tableTD}</tr>);
                    } else {
                        tableBodyArray.push(<tr onDoubleClick={_self.onDoubleClick.bind(_self, strValue, index)}
                            onClick={_self.onClick.bind(_self, strValue, index)} id={_self.componentId + "_tr_" + index} 
                            data-value={strValue}>{tableTD}</tr>);
                    }
                } else {
                    tableBodyArray.push(<tr onDoubleClick={_self.onDoubleClick.bind(_self, strValue, index)}
                        onClick={_self.onClick.bind(_self, strValue, index)} className={_self.initCellByEndorsementColor(element)} id={_self.componentId + "_tr_" + index} 
                        data-value={strValue}>{tableTD}</tr>);
                }

                // handler row detail
                if (_self.props.rowDetailRender) {
                    let rowDetailContent = _self.getRowDetail(element);
                    let detailControlTD = rowDetailContent ? <td style={{ minWidth: '50px' }} className="details-control" /> : <td style={{ minWidth: '50px' }} className="tddefault" />;
                    if (_self.props.detailSecquence) {
                        tableTD.splice(Number(_self.props.detailSecquence), 0, detailControlTD);
                    } else {
                        tableTD.push(detailControlTD);
                    }
                    let colspan = tableTD.length;//$(tr).find("td").size();
                    tableBodyArray.push(<tr style={{ width: '100%', display: 'none' }} className='ui-datatable-detail'><td style={{ minWidth: '50px' }} colSpan={colspan} className="hastddetail" detailid={index}>{rowDetailContent}</td></tr>);
                }
            });
        }
        let showMsg
        if (this.props.showMsg) {
            showMsg = this.getI18n(this.props.showMsg)
        } else {
            showMsg = r18n.DataTable.NoResult
        }

        if (tableBodyArray && tableBodyArray.length == 0) {
            let tableBodyWidht = tableNoImg.parent().parent().parent().parent().parent().parent().width();
            let imageWidht = tableNoImg.width();
            let msgWidht = tableNoMessage.width();
            if (Util.parseBool(this.props.showImage) && Util.parseBool(this.props.isOnSearch) && (!Util.parseBool(this.props.isNeedLockColumns) )) {
                tableBodyArray.push(<tr id={"no-result" + this.componentId} className="table-no-result">
                    <td colSpan={this.props.children.length} disabled>
                        <p align="center" style={{marginTop:'20px'}}>
                            <img src={image} />
                        </p>
                        <p align="center">
                            {showMsg}
                        </p>
                    </td> </tr>);
            }
            if (Util.parseBool(this.props.showImage) && Util.parseBool(this.props.isOnSearch) && (Util.parseBool(this.props.isNeedLockColumns) )) {
                tableBodyArray.push(<tr id={"no-result" + this.componentId} className="table-no-result">
                    <td colSpan={this.props.children.length} disabled>
                        <p style={{ marginLeft: (tableBodyWidht - imageWidht) / 2 + "px", marginTop:'20px' }}>
                            <img className={this.componentId + "table-no-result-image"} src={image} />
                        </p>
                        <p style={{ marginLeft: (tableBodyWidht - msgWidht) / 2 + "px" }}>
                            <span className={this.componentId + "table-no-result-msg"}>{showMsg}</span>
                        </p>
                    </td> </tr>);
            }


            this.showPagination = false;
        }

        if (Util.parseBool(this.props.showBlankRow)) {
            if (this.pageCount == this.state.currentPageIndex) {
                let tableLength = tableBodyArray.length;
                let pagesize = this.state.pageSize;
                if (this.props.rowDetailRender) {
                    pagesize = pagesize * 2;
                }
                const trSize = pagesize - tableLength;
                if (trSize > 0) {
                    for (let i = 0; i < trSize; i++) {
                        tableBodyArray.push(<tr style={{ height: '31px' }}>{this.renderBlankTd()}</tr>);
                    }
                }
            }
        }

        if (Util.parseBool(this.props.isNeedLockColumns) || Util.parseBool(this.props.isNeedLockRightColumns)) {
            this.reGetFixedBody(tableBodyArray);
        }
        return tableBodyArray;
    }

    reGetFixedBody(arrBody) {
        this.arrFixedBody = [];
        this.arrFixedRightBody = [];
        let arr = [];
        let rightArr = [];
        if (!_.isEmpty(arrBody)) {
            if (Util.parseBool(this.props.isNeedLockColumns) && arrBody[0].props.className != "table-no-result") {
                for (let i = 0; i < arrBody.length; i++) {
                    if (arrBody[i].props.children) {
                        let arrChildren = $.extend(true, {}, arrBody[i]); // _.cloneDeep(arrBody[i]); // arrBody[i].props.children.slice(0, 2);
                        // let obj = arrBody[i];
                        // obj.props.children = arrChildren;
                        let num = parseInt(this.props.lockNumColumns);
                        let arrObj = arrChildren.props.children.slice(0, num);
                        arrChildren.props.children = [];
                        arrChildren.props.children = arrObj;
                        arrChildren.props.id = arrChildren.props.id + '_copy'
                        arr.push(arrChildren);


                    }

                }
                this.arrFixedBody = arr;
            }
            if (Util.parseBool(this.props.isNeedLockRightColumns) && arrBody[0].props.className != "table-no-result") {
                for (let i = 0; i < arrBody.length; i++) {
                    if (arrBody[i].props.children) {
                        let arrChildren2 = $.extend(true, {}, arrBody[i]);
                        // let obj = arrBody[i];
                        // obj.props.children = arrChildren;
                        // right
                        let arrObj2 = arrChildren2.props.children.slice(arrChildren2.props.children.length - 1, arrChildren2.props.children.length);
                        arrChildren2.props.children = [];
                        arrChildren2.props.children = arrObj2;
                        arrChildren2.props.id = arrChildren2.props.id + '_copy-right'
                        rightArr.push(arrChildren2);
                    }
                }
                this.arrFixedRightBody = rightArr;
            }
        }
    }

    findModelByCuzObjectRelation(cuzObject, cuzObjectRelation, cuzField) {
        let tempObject = cuzObject;
        let relationArray = cuzObjectRelation.split('.');
        for (let i = 0; i < relationArray.length; i++) {
            let relation = relationArray[i];
            if (tempObject) {
                if (relation.indexOf("[") != -1 && relation.indexOf("]") != -1) {
                    let relationName = relation.substring(0, relation.indexOf("["));
                    let tempIndex = relation.substring(relation.indexOf("[") + 1, relation.indexOf("]"));
                    if ((tempObject[relationName] instanceof Array) && tempObject[relationName][tempIndex]) {
                        tempObject = tempObject[relationName][tempIndex];
                    } else {
                        console.error(`UIDataTable: Error in hanle relation of customization object and field, customizationObject: ${cuzObject}, customizationField: ${cuzField}, Customization Object Relation: ${relationArray.join(".")}`);
                    }
                } else {
                    tempObject = tempObject[relation];
                }
            } else {
                tempObject = null;
                break
            }
            if (i == relationArray.length - 1) {
                break
            }
        }
        return tempObject;
    }

    onToPage(currentIndex) {
        super.onToPage(currentIndex);
        $("#" + this.componentId + " tbody tr.ui-datatable-detail").css({ 'width': '100%', 'display': 'none' })
    }

    refresh() {
        this.setState({});
        let allheadTh = $('#'+this.componentId+'header-wraper thead th');
        if (allheadTh && allheadTh.length > 0) {
            let lastTH = allheadTh[allheadTh.length -1];
            let lastLeft = $(lastTH).offset();
            let rightObj = $('.fixed-table_fixed-right').offset();
            if (lastLeft && rightObj) {
                if (lastLeft.left <= rightObj.left || rightObj.left == 0) {
                    $('.fixed-table_fixed-right').hide();
                } else {
                    $('.fixed-table_fixed-right').show();
                }
            }
        }
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.refresh.bind(this))
    }
    componentDidMount() {
        window.addEventListener('resize', this.refresh.bind(this))
        
        super.componentDidMount();
        if (/rainbow-ui-\d+/g.test(this.componentId)) {
            console.warn("Please define id for UIDataTable component!");
        }

        let _self = this;
        this.initEvent();
        this.initDetailEvent();

        if (Util.parseBool(this.props.detailable) && this.props.detailVisible) {
            $("#" + this.props.id + " tbody tr td.details-control").each(function (index, data) {
                if (Util.isArray(_self.props.detailVisible)) {
                    if (_self.props.detailVisible.indexOf(index + 1) >= 0) {
                        $(data).click();
                    }
                } else {
                    if (Util.isString(_self.props.detailVisible) && _self.props.detailVisible.toLowerCase() == "all") {
                        $(data).click();
                    } else if (index + 1 == Number(_self.props.detailVisible)) {
                        $(data).click();
                    }
                }
            });
        }

        if (!this.showPagination) {
            $("#pagination" + this.componentId).hide();
        } else {
            $("#pagination" + this.componentId).show();
        }

        ComponentContext.put(this.componentId, this);

        if (Util.parseBool(this.props.isNeedLockColumns) || Util.parseBool(this.props.isNeedLockRightColumns)) {
            $(".fixed-table-box").fixedTable();
            this.recalTableWidth();
        }
    }

    renderBlankTd() {
        const tdArray = [];
        for (let i = 0; i < this.props.children.length; i++) {
            tdArray.push(<td style={{ minWidth: '50px' }}></td>);
        }
        return tdArray;
    }

    componentWillUpdate() {
        if (/rainbow-ui-\d+/g.test(this.componentId)) {
            console.warn("Please define id for UIDataTable component!");
        }
    }

    componentDidUpdate() {
        let _self = this;
        this.initEvent();
        this.initDetailEvent();
        this.isClick = true;
        if (Util.parseBool(_self.props.detailable)) {
            $("#" + _self.componentId + " tbody tr.ui-datatable-detail").each(function (index, data) {
                if ($(this).css("display") != "none") {
                    // 注销，引起detail table展开第二次回调问题
                    // _self.isClick = false;
                    $(this).prev().find(".details-control").click();
                }
            });
        }

        if (Util.parseBool(_self.props.detailable) && _self.props.detailVisible) {
            $("#" + _self.componentId + " tbody tr td.details-control").each(function (index, data) {
                if (Util.isArray(_self.props.detailVisible)) {
                    if (_self.props.detailVisible.indexOf(index + 1) >= 0) {
                        $(data).click();
                    }
                } else {
                    if (Util.isString(_self.props.detailVisible) && _self.props.detailVisible.toLowerCase() == "all") {
                        $(data).click();
                    } else if (index + 1 == Number(_self.props.detailVisible)) {
                        $(data).click();
                    }
                }
            });
        }

        if (!Util.parseBool($('#' + this.componentId).find('tbody tr').length > 0)) {
            $("#pagination" + this.componentId).hide();
        } else {
            $("#pagination" + this.componentId).show();
        }

        let arrLastRowTd = $('#' + this.componentId).find('tbody tr:last td');
        if (arrLastRowTd && arrLastRowTd.length > 0 && this.props.io == 'in') {
            let arrTH = $('#' + this.componentId).find('thead tr:last th');

            for (let i = 0; i < arrLastRowTd.length; i++) {
                const root = $(arrLastRowTd[i])
                const childObj = root.children('div').attr('class');
                const childDiv = root.children('div').children('div')
                if (childObj && (childObj.indexOf('SearchCodeTable') != -1 ||
                    childObj.indexOf('InputTreeSelect') != -1 ||
                    childObj.indexOf('InputTableSelect') != -1 ||
                    childObj.indexOf('uisearch') != -1 ||
                    childObj.indexOf('cascade') != -1)) {
                        root.children('div').children('div').attr({ 'needreposition': '1' });
                    let tdindex = root.attr('tdindex');
                    let realw = $($(arrTH)[tdindex]).css('min-width')
                    if (realw && realw.indexOf('%') == -1) {
                        realw = realw ? parseInt(realw.replace('px', '')) : 70;
                    } else {
                        realw = $($(arrTH)[tdindex]).width();
                    }
                    if (realw > 70) {
                        realw = realw;
                    } else {
                        realw = 70;
                    }
                    childDiv.css({ 'width': realw });
                    let arrClassName = childDiv.attr('class') ? childDiv.attr('class').split(' ') : [];
                    let lastClass = arrClassName && arrClassName.length > 0 ? arrClassName[arrClassName.length - 1] : childDiv.attr('class');
                    if (lastClass) {
                        childDiv.removeClass();
                        childDiv.addClass(lastClass);
                    }

                }
            }
        }
        
        if ((Util.parseBool(this.props.isNeedLockColumns) || Util.parseBool(this.props.isNeedLockRightColumns)) && this.pageValue && this.pageValue.length > 0) {
            $(".fixed-table-box").fixedTable();
            this.recalTableWidth();
            const flexTable = $('.fixed-table_fixed-right')
            if (Util.parseBool(this.props.isNeedLockRightColumns)) {
                let allheadTh = $('#'+this.componentId+'header-wraper thead th');
                if (allheadTh && allheadTh.length > 0) {
                    let lastTH = allheadTh[allheadTh.length -1];
                    let lastLeft = $(lastTH).offset();
                    let rightObj = flexTable.offset();
                    if (lastLeft && rightObj) {
                        if (lastLeft.left <= rightObj.left) {
                            flexTable.hide();
                        } else {
                            flexTable.show();
                        }
                    }
                }
            }
        }
    }

    recalTableWidth() {
        // 1 获取锁定列的宽度
        // let arrlockTD = $('#' + this.componentId + '-fixed-table_body').find('tr:first-child td');

        // 2 获取tale各列宽度
        let arrTD = $('#' + this.componentId).find('tr:first-child td');

        // // 3 更改table里lock列宽
        // for (let i = 0; i < arrlockTD.length; i++) {
        //     let iwidth = $(arrlockTD[i]).width();
        //     $(arrTD[i]).css({width: iwidth + 'px'});
        // }
        // 4 更改th宽度
        let arrTH = $('#' + this.componentId + 'header-wraper').find('th');
        for (let i = 0; i < arrTH.length; i++) {
            let ithwdith = $(arrTH[i]).width();
            $(arrTD[i]).css({ width: ithwdith + 'px' });
        }
    }

    /**@ignore
     * Init event
     */
    initEvent() {
        let _self = this;

        // Clear single & multiple change event
        $("input:radio[name=" + this.componentId + "_single" + "]").unbind("change");
        $("input:checkbox[name=" + this.componentId + "_multiple" + "]").unbind("change");
        $("input:checkbox[name=" + this.componentId + "_multiple_selectall" + "]").unbind("change").attr("checked", null);
        this.setMultipleSelectAll();

        // handler single select event
        $("input:radio[name=" + this.componentId + "_single" + "]").change((event) => {
            _self.onSingleChange(event);
        });

        // handler multiple select event
        $("input:checkbox[name=" + this.componentId + "_multiple" + "]").change((event) => {
            _self.onMultipleChange(event);
        });

        // handler multiple select all event
        $("input:checkbox[name=" + this.componentId + "_multiple_selectall" + "]").change((event) => {
            _self.onSelectAll(event);
        });
    }

    /**@ignore
     * Init detail event
     */
    initDetailEvent() {

        let _self = this, detailRows = [];
        const { detailDisVisible, rowDetailRender } = this.props;

        if (rowDetailRender) {
            const tableDetail = $("#" + this.componentId + " tbody").find("tr td.details-control");
            tableDetail.unbind();
            tableDetail.bind("click", function () {
                let tr = $(this).closest("tr");
                let trIndex = parseInt(tr.attr('id').substr(tr.attr('id').length - 1, 1)) + 1 + "";
                if (detailDisVisible && detailDisVisible.toString().indexOf(trIndex) >= 0) {
                    return;
                }
                const element = JSON.parse(tr.attr("data-value"));
                tr.toggleClass("details");
                tr.next().toggle();
                if (_self.props.rowDetailCallBack && _self.isClick) {
                    if (event && $(event.target).attr('class') == 'details-control') {
                        _self.props.rowDetailCallBack(tr.next().is(':hidden'), element);
                    }
                }
                let countHeight = 0;
                let arr = [...$('#' + _self.componentId).find('.hastddetail')];
                let aCount = arr.length;


                let datatabledetail = [...$('#' +  _self.componentId  + " tbody").find('.ui-datatable-detail > .hastddetail .tab-pane > p')];
                datatabledetail.forEach((item,posIndex) => {
                    for (let i = 0; i < datatabledetail.length; i++) {
                        let item = datatabledetail[i];
                        const itemCard = (item).children(".card").find('.card-block')
                        let ldatatabledetailheight = $(item).children(".card").height();
                        let cardheigth = ldatatabledetailheight;
                        if ( itemCard &&  itemCard.length > 0) {
                            cardheigth = $(itemCard[0]).attr('realheight');
                            if (cardheigth > ldatatabledetailheight) {
                                ldatatabledetailheight = cardheigth;
                            }
                        }
                        $(item).attr("style","height:" + ldatatabledetailheight + "px");
                        $(item).children(".card").attr("style","height:" + ldatatabledetailheight + "px;position: absolute;width: calc(100% - 130px);");
                        $(item).children(".card .SearchCodeTable-textalign > div").attr("style","margin-top: 0px!important;");

                    }
                })
                
            });
        }
    }

    /**@ignore
     * Clear table all tr detail
     */
    clearDetail() {
        const obj = $("#" + this.componentId + " tbody")
        obj.find("tr").removeClass("details");
        obj.find("tr.ui-datatable-detail").remove();
    }

    /**@ignore
     * Init cell by datatable in the endorsement
     */
    initCellByEndorsementColor(element) {
        let endorsementId = PageContext.get("endorsementId");
        if (endorsementId) {// if endo type
            //PolicyStatus==4 is delete
            let status = element.operationStatus;
            if ("addRow" == status) {
                return 'table-blue-color';
            } else if ("deleteRow" == status) {
                return 'table-percent-color';
            } else {
                return '';
            }
        }
        return null
    }

    getRowDetail(element) {
        let { rowDetailRender } = this.props;
        if (typeof rowDetailRender == "function") {
            return rowDetailRender(element);
        }
    }

    /**@ignore
     * On select all
     */
    onSelectAll(event) {
        $("input:checkbox[name=" + this.componentId + "_multiple" + "]").each(function (index, element) {
            element.checked = event.target.checked ? "checked" : null
        });

        if (this.props.onSelectAll != undefined) {
            this.props.onSelectAll(new OnRowSelectEvent(this, event, null, null));
        }
    }

    /**@ignore
     * On single change
     */
    onSingleChange(event) {
        this.onRowSelect(event);
    }

    /**@ignore
     * On multiple change
     */
    onMultipleChange(event) {
        this.setMultipleSelectAll();

        this.onRowSelect(event);
    }

    /**@ignore
     * Set multiple select all checkbox
     */
    setMultipleSelectAll() {
        let selectAllFlag = "checked";

        let multiple = $("input:checkbox[name=" + this.componentId + "_multiple" + "]");
        if (multiple && multiple.length > 0) {
            multiple.each(function (index, element) {
                if (!element.checked) {
                    selectAllFlag = null;
                }
            });
        } else {
            selectAllFlag = null;
        }

        $("input:checkbox[name=" + this.componentId + "_multiple_selectall" + "]").each(function (index, element) {
            element.checked = selectAllFlag;
        });
    }

    /**@ignore
     * On row select
     */
    onRowSelect(event) {
        if (this.props.onRowSelect != undefined) {
            let tableRow = $(event.target).closest("tr");
            this.props.onRowSelect(new OnRowSelectEvent(this, event, null, JSON.parse($(tableRow).attr("data-value"))));
        }
    }

    /**@ignore
     * On sort
     */
    onSort(event) {
        let sortWay = this.state.sortWay == "asc" ? "desc" : "asc";
        let sortCol = $(event.target).attr("data-value")
        this.setState({ sortColumn: sortCol, sortWay: sortWay });
        this.updateStatus = "Sort";

        if (this.props.onSort) {
            this.props.onSort(event, sortCol, sortWay)
        }
    }

    handleClick()  {
        this.setState({
            dispaly: !dispaly
        })
    }

    /**@ignore
     * onClick
     */
    onClick(value, index) {
        clearTimeout(this.timer);
        if (this.props.onClick) {
            this.changeHighLight(index);
            this.timer = setTimeout(() => {
                this.props.onClick(value, index);
            }, 200)
        }
    }

    /**@ignore
     *onDoubleClick
     */
    onDoubleClick(value, index) {
        clearTimeout(this.timer);
        if (this.props.onDoubleClick) {
            this.changeHighLight(index);
            this.props.onDoubleClick(value, index);
        }
    }

    /**@ignore
     *changeHighLight
     */
    changeHighLight(index) {
        const obj = $('#' + this.componentId + ' tr[id*=' + this.componentId + '_tr_]')
        const len = obj.length;
        for (let i = 0; i < len; i++) {
            $(obj[i]).removeClass('highlight');
        }
        $(obj[index]).addClass('highlight');
    }
};


/**@ignore
 * DataTable component prop types
 */
DataTable.propTypes = $.extend(Data.propTypes, {
    className: PropTypes.string,
    var: PropTypes.string,
    thBreakAll: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    tdBreakAll: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    // datatable event
    onRowSelect: PropTypes.func,
    onSelectAll: PropTypes.func,
    onClick: PropTypes.func,
    onSort: PropTypes.func,
    onDoubleClick: PropTypes.func,
    detailVisible: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    highlight: PropTypes.array,
    detailSecquence: PropTypes.string,
    rowDetailCallBack: PropTypes.func,
    renderTableHeader: PropTypes.func,
    showBlankRow: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    isColumnCustomDisp: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
    fixedColumns: PropTypes.string.isRequired,
    showPaginationNum: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    showGoToPage: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    indexTitle: PropTypes.string,
    indexTitleWidth: PropTypes.string,
    showImage: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    showMsg: PropTypes.string,
    isNeedLockColumns: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    isNeedLockRightColumns: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    lockNumColumns: PropTypes.string,
    lockNumRightColumns: PropTypes.string,
    visibled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    isOnSearch: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    maxPageCount:PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    showCount: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    tips: PropTypes.string
});

/**@ignore
 * Get DataTable component default props
 */
DataTable.defaultProps = $.extend(Data.defaultProps, {
    className: null,
    thBreakAll: "false",
    tdBreakAll: "false",
    detailVisible: 0,
    indexable: "false",
    showBlankRow: "false",
    isCustomColumn: false,
    fixedColumns: "1",
    onClick: null,
    onDoubleClick: null,
    onSort: null,
    showPaginationNum: true,
    showGoToPage: false,
    indexTitle: '',
    indexTitleWidth: '20px',
    showImage: true,
    isNeedLockColumns: false,
    isNeedLockRightColumns: false,
    lockNumColumns: "2",
    lockNumRightColumns: "1",
    isOnSearch: true,
    visibled: true
});
