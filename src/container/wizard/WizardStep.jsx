import PropTypes from 'prop-types';
import Component from "../../basic/Component";
export default class WizardStep extends Component{
	renderComponent(){
		return this.props.children;
	}
};

/**@ignore
 * WizardStep component prop types
 */
WizardStep.propTypes = $.extend({}, Component.propTypes, {
	id: PropTypes.string,
	title: PropTypes.string,
	description: PropTypes.string,
	stepName: PropTypes.string
});

/**@ignore
 * Get wizardStep component default props
 */
WizardStep.defaultProps = $.extend({}, Component.defaultProps, {

});