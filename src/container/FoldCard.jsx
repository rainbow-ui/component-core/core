import Component from '../basic/Component';
import { Util } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';

export default class FoldCard extends Component {

    renderComponent() {
        let tempClass = Util.parseBool(this.props.isExpand) ? this.props.className + ' fold-card expandCard':this.props.className + ' fold-card closeCard';      
        const { enabled, style, title} = this.props;
        let foldId = this.props.id ? this.props.id : "rainbow_fold_card"
        return (
            <div id={foldId} className={tempClass} style={style}>
                {title ? this.renderHeader() : null}
                {this.renderBlock()}
            </div>
        );
    }

    renderHeader() {
        return (
            <div className ="fold-card-head" onClick={this.ClickCardSwitch.bind(this)}>
                <div id="cardSwitchOn" class="rainbow MenuFolded menu_switch"/>
                <div id="cardSwitchOff" class="rainbow MenuUnfolded menu_switch icon_display"/>
                <h4 className="fold-card-title">
                    {this.getI18n(this.getProperty("title"))}
                </h4>
                
            </div>
        )
    }

    componentDidMount(){
        let foldId = this.props.id ? this.props.id : "rainbow_fold_card";        
        let foldCardElement = $('#'+foldId);
        if(foldCardElement&&foldCardElement[0]){
            foldCardElement[0].style.width = this.props.cardWidth;
        }
    }

    ClickCardSwitch(){
        let cardSwitchOn = $('#cardSwitchOn');
        let cardSwitchOff = $('#cardSwitchOff');
        if(cardSwitchOn&&cardSwitchOn.hasClass("icon_display")){
            cardSwitchOff.addClass('icon_display');
            cardSwitchOn.removeClass('icon_display');
        }else{
            cardSwitchOn.addClass('icon_display');
            cardSwitchOff.removeClass('icon_display');
        }
        let foldId = this.props.id ? this.props.id : "rainbow_fold_card"        
        this.cardSwitch(foldId)
    }

    cardSwitch(foldId) {
        let cardElement = $('#' + foldId);
        let childFoldId = this.props.id ? this.props.id+'-children' : "rainbow_fold_card-children"                
        let childElement = $('.fold-card-block');        
        if (cardElement&&cardElement.hasClass("expandCard")){
            cardElement.addClass('closeCard');
            cardElement.removeClass('expandCard');
            childElement.addClass('close-fold-card')
        }else {
            cardElement.addClass('expandCard');
            cardElement.removeClass('closeCard');
            childElement.removeClass('close-fold-card')                      
        }
    }

    renderBlock() {
        const { isExpand, children } = this.props;
        let childFoldId = this.props.id ? this.props.id+'-children' : "rainbow_fold_card-children"        
        return (
            <div className={Util.parseBool(isExpand) ? 'fold-card-block' : 'fold-card-block close-fold-card'} id={this.componentId}>
                {children}
            </div>
        );
    }

}

/**@ignore
 * Panel component prop types
 */
FoldCard.propTypes = $.extend({}, Component.propTypes, {
    isExpand: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),    
    cardWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
});

/**@ignore
 * Get dynamicSection component default props
 */
FoldCard.defaultProps = $.extend({}, Component.defaultProps, {
    className: '' ,
    isExpand: 'true'
});