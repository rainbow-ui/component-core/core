import Component from "../../basic/Component";
import PropTypes from 'prop-types';

export default class TabItem extends Component {

	renderComponent(){
		return <noscript/>;
	}

};

/**@ignore
 * TabItem component prop types
 */
TabItem.propTypes = $.extend({}, Component.propTypes, {
	enabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
	id: PropTypes.string,
	title: PropTypes.string,
	icon: PropTypes.string,
	badge: PropTypes.string,
	style: PropTypes.string,
	disabled:PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
	causeValidation:PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
	validationGroup: PropTypes.string,
	className: PropTypes.string,
	lazyLoad: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
});

/**@ignore
 * Get tabItem component default props
 */
TabItem.defaultProps = $.extend({}, Component.defaultProps, {
	componentType: "TabItem",
	className: '',
	lazyLoad:true
});
