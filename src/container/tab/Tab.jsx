import { Util, ELUtil } from 'rainbow-desktop-tools';
import { ValidatorContext, ComponentContext } from "rainbow-desktop-cache";
import Component from "../../basic/Component";
import Param from "../../basic/Param";
import TabLink from "./TabLink";
import OnTabChangeEvent from '../../event/OnTabChangeEvent';
import OnClickEvent from '../../event/OnClickEvent';
import PropTypes from 'prop-types';
// import './tabNav.css'
import React, { Children } from 'react';
import ReactDOM from 'react-dom'
export default class Tab extends Component {

    constructor(props) {
        super(props);
        if (Util.parseBool(props.lazyLoad)) {
            this.state = { activeIndex: props.activeIndex };
        } else {
            this.state = {};
        }
        this.myscroll = null;
        this.state = {
            isShowBtn: false,
            isShowLeftShadow: false,
            isShowRightShadow: false
        }
        this.isValidation = false;
        this.navBoxRef = React.createRef();  // 最外层盒子
        this.navContentRef = React.createRef();  // 盒子的ref
        this.tabsRef = React.createRef();  // ul 的ref
        this.rightWidth = 0;
        this.navListLeft = 0;
        this.navListRealWidth = 0; 
        this.translateX = 0;
    }

    componentWillMount() {
        super.componentWillMount();
        let self = this;
        PubSub.subscribe("Validation_tabIndex", async (msg,data)=> {
          if(data){
            let hrefArray = data.split("__");
            let tabItemIndex = parseInt(hrefArray[1]);
            if (hrefArray.length == 3) {
                tabItemIndex = tabItemIndex + parseInt(hrefArray[2]);
            }
            this.isValidation = true;
            self.setState({activeIndex:tabItemIndex})
          }
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps && nextProps.activeIndex) {
            this.setState({ activeIndex: nextProps.activeIndex });
        }
    }

    static getActiveIndex(tabId) {
        let activeIndex = 0;
        $("#" + tabId).find(".nav-item").map(function (index, element) {
            if ($(element).children().eq(0).hasClass("active")) {
                activeIndex = index + 1;
            }
        });
        return activeIndex;
    }

    static deleteTabItem(event) {
        $("#" + event.target.id).parent().hide();
    }

    renderComponent() {
        // let navClassName = " nav nav-" + this.props.type + " " + this.props.className + ' fixedtab';
        let navClassName = "nav-" + this.props.type;

        if (Util.parseBool(this.props.isTabNeedScroll)) {
            return (
                <div style={this.props.style} className="scrollTab">
                <div id={"scrollmask"+this.componentId} style={{height:'40px',position:'absolute',textAlign:'right', width:'200px',right:'10px',paddingRight:'20px',zIndex:999,backgroundImage:'-webkit-gradient(linear,left top,right bottom,from(rgba(255,255,255,0)),color-stop(70%, #FFF))',backgroundImage:'linear-gradient(90deg,rgba(255,255,255,0) 0%,#fff 70%)'}}>
                    <span className="rainbow Start" style={{marginTop:'10px',cursor:'pointer'}} onClick={this.scrollRight.bind(this)}></span>
                </div>
                <div id={"scrollmenu"+this.componentId} style={{overflow:'hidden'}} className={Util.parseBool(this.props.needCrossValidate) ? "PageContainer MultiTab" : ''}>
                    <ul id={this.componentId} className={Util.parseBool(this.props.tabSelfAdaption) ? navClassName + " tab-normal-self-adaption" : navClassName}>
                        {this.renderTab(this.props.children)}
                    </ul>
                    </div>
                    {this.props.tabSelfAdaption ?
                        <div className={navClassName + " tab-self-adaption"}>
                            <div className="btn-group">
                                {this.renderTab(this.props.children.slice(0, 1))}
                                <div className="btn-group" role="group">
                                    <button type="button" className="btn btn-secondary dropdown-toggle split-btn btn-default" data-toggle="dropdown">
                                    </button>
                                    <div className="dropdown-menu split-btn">
                                        <ul id={this.componentId} className={navClassName}>
                                            {this.renderTab(this.props.children.slice(1))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        : null}
                    <div id={this.componentId + "_content"} className="tab-content">
                        {this.renderTabContent()}
                    </div>
                </div >
            );
        } else {
            return (
                <div style={this.props.style} className={Util.parseBool(this.props.needCrossValidate) ? "PageContainer MultiTab" : ''}>
                    {/* <ul id={this.componentId} className={Util.parseBool(this.props.tabSelfAdaption) ? navClassName + " tab-normal-self-adaption" : navClassName}>
                        {this.renderTab(this.props.children)}
                    </ul> */}
                    {/* mark */}
                    {
                        this.state.isShowTab ?
                            <div class="navBox" id={"navBox" + this.componentId} ref = {this.navBoxRef} style={{width: '100%'}}>
                                <div id={"navContent" + this.componentId} className="navContent fixedtab" ref = {this.navContentRef}>
                                    <div className={`leftBtn ${this.state.isShowRightShadow?'showRightShadow':''}`} onClick={() => {this.pullLeft()}} style={this.state.isShowBtn?{display: 'block'}:{}}>
                                        <span>&lsaquo;</span>
                                    </div>
                                    <ul id={this.componentId} className={"navList " + navClassName} ref = {this.tabsRef}>
                                        {this.renderTab(this.props.children)}
                                    </ul>
                                    <div class={`rightBtn ${this.state.isShowLeftShadow?'showLeftShadow':''}`} onClick={() => {this.pullRight()}} style={this.state.isShowBtn?{display: 'block'}:{}}>
                                        <span>&rsaquo;</span>  
                                    </div>
                                </div>
                            </div>
                        : null
                    }
                    
                    {this.props.tabSelfAdaption ?
                        <div className={navClassName + " tab-self-adaption"}>
                            <div className="btn-group">
                                {this.renderTab(this.props.children.slice(0, 1))}
                                <div className="btn-group" role="group">
                                    <button type="button" className="btn btn-secondary dropdown-toggle split-btn btn-default" data-toggle="dropdown">
                                    </button>
                                    <div className="dropdown-menu split-btn">
                                        <ul id={this.componentId} className={navClassName}>
                                            {this.renderTab(this.props.children.slice(1))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        : null}
                    <div id={this.componentId + "_content"} className="tab-content">
                        {this.renderTabContent()}
                    </div>
                </div >
            );
    
        }
    }

    // 点击右键
    pullRight() {
        let navContentWidth = this.navContentRef.current.clientWidth - 120;
        let total = this.tabsRef.current.style.marginLeft?this.tabsRef.current.style.marginLeft : 0
        let surplus = this.navListRealWidth - Math.abs(parseFloat(total)) - navContentWidth; // 剩余宽度
        if (surplus <= 0 ) {return}
        $('.leftBtn span').css({color: '#707070'});
        this.setState({isShowRightShadow: true});
        if (surplus >= navContentWidth) {
            this.tabsRef.current.style.marginLeft = (parseFloat(total) - navContentWidth) + 'px';
        } else {
            this.tabsRef.current.style.marginLeft = (parseFloat(total) - surplus) + 'px';
            $('.rightBtn span').css({color: '#b7b7b7'}); // 按钮变灰
            this.setState({isShowLeftShadow: false});
        } 
    }
    // 点击了左键
    pullLeft() {
        // 120 是左右按钮宽度之和
        let navContentWidth = this.navContentRef.current.clientWidth - 120;
        let total = this.tabsRef.current.style.marginLeft?this.tabsRef.current.style.marginLeft : 0
        if (parseFloat(total) == 0){return}
        $('.rightBtn span').css({color: '#707070'});
        this.setState({isShowLeftShadow: true});
        if (Math.abs(parseFloat(total)) > navContentWidth) {
            this.tabsRef.current.style.marginLeft = (parseFloat(total) + navContentWidth) + 'px';
        }else{
            this.tabsRef.current.style.marginLeft = 0;
            $('.leftBtn span').css({color: '#b7b7b7'});
            this.setState({isShowRightShadow: false});
        }   
    }

    renderDeleteIcon(tabItemContentId, child) {
        return (
            <button id={tabItemContentId} class="tab-remove" onClick={child.props.onClick} disabled={child.props.iconDisabled}>x</button>
        )
    }




    /**@ignore
     * Render tab
     */
    renderTab(children) {
        let activeIndex = this.state.activeIndex || this.props.activeIndex;
        if (this.props.children.length != children.length) {
            activeIndex = this.state.activeIndex - 1 || this.props.activeIndex - 1
        }
        if (children.length == 1) {
            activeIndex = this.state.activeIndex || this.props.activeIndex;
        }
        if (event && event.target && $(event.target)) {
            if ((!$(event.target).attr('class') || $(event.target).attr('class').indexOf('nav-link') == -1) && !$(event.target).attr('nativetype')) {
                for (let i = 0; i < React.Children.map.length; i++) {
                    /** 如果验证没通过自动跳转到指定Tab */
                    let tabItemClass = '.' + this.getTabItemContentId(i);
                    let jumpActiveIndex = null;
                    if ($(tabItemClass + ' a') && $(tabItemClass + ' a').attr('autojump')) {
                        jumpActiveIndex = parseInt($(tabItemClass + ' a').attr('autojump'));
                        // this.setState({activeIndex: jumpActiveIndex})
                        activeIndex = jumpActiveIndex;
                        break;
                    }
                    /** end */
                }
            }
        }
        return React.Children.map(children, (child, index) => {
            let tabItemContentId = "#" + this.getTabItemContentId(index);
            let deleteIconId = this.getTabItemContentId(index);

            if (this.props.children.length != children.length) {
                tabItemContentId = "#" + this.getTabItemContentId(index + 1);
                deleteIconId = this.getTabItemContentId(index + 1);

            }
            if (children.length == 1) {
                tabItemContentId = "#" + this.getTabItemContentId(index);
                deleteIconId = this.getTabItemContentId(index);

            }
            let disabled = child.props.disabled;
            if (Util.parseBool(disabled)) {
                return (
                    <li className="disabled">
                        <a href="javascript:void(0);" style={child.props.style}>
                            {this.renderTabTitle(child)}
                        </a>
                    </li>
                );
            }

            // if children is "TabItem"
            if (child.props.componentType == "TabItem") {
                let flag = false;
                if (activeIndex == index + 1) {
                    flag = true;
                }
                return (
                    // mark
                    <li className={"nav-item " + deleteIconId} style={{whiteSpace: 'nowrap'}} onClick={() => this.getLocation(deleteIconId)}>
                        <TabLink className={`nav-link ${flag?'active':''}`} href={tabItemContentId} style={child.props.style}
                            causeValidation={child.props.causeValidation} validationGroup={child.props.validationGroup}
                            onClick={this.onClickTabItem.bind(this)} id={flag?'activeItem'+this.componentId:''}>
                            {this.renderTabTitle(child)}
                            {child.props.showDeleteIcon ? this.renderDeleteIcon(deleteIconId, child) : <noscript />}
                        </TabLink>
                    </li>
                );
            } else if (child.props.componentType == "TabItemList") {
                return (
                    <li className="nav-item dropdown">
                        <a id={this.componentId + "_dropdown"} href={tabItemContentId} className="nav-link dropdown-toggle"
                            data-toggle="dropdown" style={child.props.style}>
                            {this.renderTabTitle(child)}
                            {child.props.showDeleteIcon ? this.renderDeleteIcon(deleteIconId, child) : <noscript />}
                            <b className="caret" />
                        </a>
                        <ul className="dropdown-menu" role="menu" aria-labelledby={this.componentId + "_dropdown"}>
                            {this.renderTabItemList(child, index)}
                            {child.props.showDeleteIcon ? this.renderDeleteIcon(deleteIconId, child) : <noscript />}
                        </ul>
                    </li>
                );
            }
        });
    }

    /**@ignore
     * Render tab item list
     */
    renderTabItemList(children, index) {
        return React.Children.map(children.props.children, (child, subIndex) => {
            if (child.props.componentType == "TabItem") {
                let tabItemListContentId = "#" + this.getTabItemListContentId(index, subIndex);
                let activeIndex = this.state.activeIndex || this.props.activeIndex;

                if (activeIndex == (index + subIndex + 2)) {
                    return (
                        <TabLink href={tabItemListContentId} style={child.props.style}
                            causeValidation={child.props.causeValidation} validationGroup={child.props.validationGroup}
                            onClick={this.onClickTabItem.bind(this)}>
                            {this.renderTabTitle(child)}
                        </TabLink>
                    );
                }

                return (
                    <li>
                        <TabLink href={tabItemListContentId} style={child.props.style}
                            causeValidation={child.props.causeValidation} validationGroup={child.props.validationGroup}
                            onClick={this.onClickTabItem.bind(this)}>
                            {this.renderTabTitle(child)}
                        </TabLink>
                    </li>
                );
            }
        });
    }

    /**@ignore
     * Render tab content
     */
    renderTabContent() {
        let activeIndex = this.state.activeIndex || this.props.activeIndex;
        if (event && event.target && $(event.target)) {
            if ((!$(event.target).attr('class') || $(event.target).attr('class').indexOf('nav-link') == -1) && !$(event.target).attr('nativetype')) {
                for (let i = 0; i < React.Children.map.length; i++) {
                    /** 如果验证没通过自动跳转到指定Tab */
                    let tabItemClass = '.' + this.getTabItemContentId(i);
                    let jumpActiveIndex = null;
                    if ($(tabItemClass + ' a') && $(tabItemClass + ' a').attr('autojump')) {
                        jumpActiveIndex = parseInt($(tabItemClass + ' a').attr('autojump'));
                        // this.setState({activeIndex: jumpActiveIndex})
                        activeIndex = jumpActiveIndex;
                    }
                    /**end */
                }
            }
        }
        return React.Children.map(this.props.children, (child, index) => {
            // if children is "TabItem"
            if (child.props.componentType == "TabItem") {
                let tabItemContentId = this.getTabItemContentId(index);
                // let activeIndex = this.state.activeIndex || this.props.activeIndex;

                if (activeIndex == index + 1) {
                    return (
                        <div className="tab-pane fade active show" id={tabItemContentId}>
                            <p>{child.props.children}</p>
                        </div>
                    );
                }
                if (!Util.parseBool(this.props.lazyLoad) || !Util.parseBool(child.props.lazyLoad)) {
                    return (
                        <div className="tab-pane fade" id={tabItemContentId}>
                            <p>{child.props.children}</p>
                        </div>
                    );
                }
                return <noscript />;
            }

            // if children is "TabItemList"
            else if (child.props.componentType == "TabItemList") {
                return this.renderTabItemListContent(child, index);
            }
        });
    }

    /**@ignore
     * Render tab item list content
     */
    renderTabItemListContent(children, index) {
        return React.Children.map(children.props.children, (child, subIndex) => {
            if (child.props.componentType == "TabItem") {
                let tabItemListContentId = this.getTabItemListContentId(index, subIndex);
                let activeIndex = this.state.activeIndex || this.props.activeIndex;
                if (activeIndex == (index + subIndex + 2)) {
                    return (
                        <div className="tab-pane fade in active" id={tabItemListContentId}>
                            <p>{child.props.children}</p>
                        </div>
                    );
                }
                if (!Util.parseBool(this.props.lazyLoad)) {
                    return (
                        <div className="tab-pane fade" id={tabItemListContentId}>
                            <p>{child.props.children}</p>
                        </div>
                    );
                }
                return <noscript />;
            }
        });
    }

    getInitTab() {
        // 最外层盒子宽度  // ref获取宽度会四舍五入 例如18.5会获取到19
        // let navBoxWidth = this.navBoxRef.current.clientWidth;
        let navBoxWidth = $("#navBox" + this.componentId).width();
        // let { clientWidth } = this.tabsRef.current;
        let clientWidth = $("#" + this.componentId).width();
        let {current} = this.tabsRef;
        if ( clientWidth > navBoxWidth ) {
            this.navListRealWidth = clientWidth; // 保存ul的真实长度
            current.style.overflowX = 'hidden';
            this.setState({isShowBtn: true});
            this.setState({isShowLeftShadow: true});
        }

        // 控制ul初始位置
        let navObj = document.getElementById("navContent" + this.componentId).getBoundingClientRect();
        let activeItem = document.getElementById("activeItem" + this.componentId).getBoundingClientRect();
        if (activeItem.left - navObj.left > navBoxWidth) {
            this.tabsRef.current.style.marginLeft = - (activeItem.left - navObj.left - 10) + 'px';
            this.setState({isShowRightShadow: true});
        }

        if (this.tabsRef.current.style.marginLeft == 0) {
            $('.leftBtn span').css({color: '#b7b7b7'});
        }
        this.navContentRef.current.style.width = (navBoxWidth) + 'px';
        // if (this.navContentRef.current.style.position == 'fixed') {
        //     this.navContentRef.current.style.width = (navBoxWidth) + 'px'
        // }
    }

    componentDidMount() {
        this.setState({isShowTab: true},
            () => this.getInitTab()
        );
        

        this.onTabChangeEvent = { newActiveIndex: this.getInitIndex(), oldActiveIndex: null };
        ComponentContext.put(this.componentId, this);
        if (Util.parseBool(this.props.isTabNeedScroll)) {
            let all_width = $('#' + this.props.id)[0].scrollWidth + 300;
            $('#' + this.props.id).css({
                'width':all_width + 'px'
            })
            let scollid = 'scrollmenu' + this.componentId;
            let maskid = 'scrollmask' + this.componentId;
            this.myscroll=new IScroll("#"+scollid,{
                scrollX: true,  
                scrollY: false, 
                hScrollbar:false,
            vScrollbar : false,
            vScroll:false,
            //    startX : 0,
            tap: true
            //    scrollStart: function() {
            //        console.log('scroll...')
            //    }
            })

            $('#'+scollid).on('tap',function(){  
                console.log('开始滚动了');  
            }) 
            //自定义事件  
            this.myscroll.on('scrollEnd',function(){  
                if (this.maxScrollX == this.x) {
                    $('#' + maskid).hide()
                } else {
                    $('#' + maskid).show()
                }
                // console.log('滚动结束');  
                // console.log(this.x + '&' + this.y);     //当前位置  
                // console.log(this.directionX + '&' + this.directionY);   //最后的方向  
                // console.log(this.currentPage);      //当前对齐捕获点  
            });
        }
    }

    componentDidUpdate(){
        let isValidation = _.clone(this.isValidation);
        this.isValidation = false; 
        if(isValidation){
            PubSub.publish("Validation_finish","Validation_finish")
        }
    }

    scrollRight() {
        let id = 'scrollmenu' + this.componentId
        // $('#'+id).scrollLeft(document.body.clientWidth)
        this.myscroll.scrollTo(this.myscroll.maxScrollX, 0, 500)
    }

    /**@ignore
     * Render tab title
     */
    renderTabTitle(child) {
        let titleArray = [];

        if (child.props.icon) {
            titleArray.push(<span data-toggle="tooltip" data-placement="top" title={this.getI18n(child.props.helpText)} className={child.props.icon} style={{ padding: "0px 5px 0px 5px" }} />);
        }
        if (child.props.title) {
            titleArray.push(this.getI18n(child.props.title));
        }
        if (child.props.badge) {
            titleArray.push(<span className="badge badge-pill badge-primary" style={{ marginLeft: "10px" }}>{child.props.badge}</span>);
        }

        return titleArray;
    }

    // 获取li正确的位置 mark
    getLocation(className) {
        let obj = document.getElementsByClassName(className)[0].getBoundingClientRect();
        let navObj = document.getElementById("navContent" + this.componentId).getBoundingClientRect();
        let total = this.tabsRef.current.style.marginLeft?this.tabsRef.current.style.marginLeft : 0
        // 60为 单个左右图标的宽度加上左右padding
        if (this.state.isShowBtn) {
            if (obj.right + obj.width > navObj.right) {
                this.tabsRef.current.style.marginLeft =  (parseFloat(total) - (obj.right - navObj.right + 60)) + 'px';
            } else if (obj.left - navObj.left < 60 && this.state.isShowBtn) {
                this.tabsRef.current.style.marginLeft = (parseFloat(total) + (navObj.left - obj.left + 60)) + 'px';
            } 
        }    
    }

    onClickTabItem(event) {
        // event.jsEvent.preventDefault();
        // get tabitem index
        let _self = this;
        // let target = $(event.jsEvent.target).parent("a").attr("href") == undefined ? $(event.jsEvent.target) : $(event.jsEvent.target).parent("a");
        // let target = $(event.target).parent("a").attr("href") == undefined ? $(event.target) : $(event.target).parent("a");
        // let hrefArray = target.attr("href").split("__");
        let hrefArray = event.component.props.href.split("__");
        let tabItemIndex = parseInt(hrefArray[1]);
        if (hrefArray.length == 3) {
            tabItemIndex = tabItemIndex + parseInt(hrefArray[2]);
        }
        // handler before tab change event

        if (_self.props.onBeforeTabChange) {
            if (!_self.props.onBeforeTabChange(new OnTabChangeEvent(_self, this, event, Param.getAttributeParameter(this, tabItemIndex), tabItemIndex, _self.onTabChangeEvent.newActiveIndex))) {
                return;
            }
        }

        // handler tab change event
        let tabChangeEvent = new OnTabChangeEvent(_self, this, event, Param.getAttributeParameter(this, tabItemIndex), tabItemIndex, _self.onTabChangeEvent.newActiveIndex);
        if (_self.props.onTabChange) {
            _self.props.onTabChange(tabChangeEvent);
        }

        // handler after tab change event
        if (_self.props.onAfterTabChange) {
            _self.props.onAfterTabChange(new OnTabChangeEvent(_self, this, event, Param.getAttributeParameter(this, tabItemIndex), null, null));
        }
        // set onTabChangeEvent property
        _self.onTabChangeEvent = { newActiveIndex: tabItemIndex, oldActiveIndex: _self.onTabChangeEvent.newActiveIndex };

        // show click tabitem
        if (Util.parseBool(_self.props.lazyLoad)) {
            _self.setState({ activeIndex: tabItemIndex });
        } else {
            target.tab("show");
        }
    }

    getInitIndex() {
        return this.props.activeIndex == null ? 1 : this.props.activeIndex;
    }

    /**@ignore
     * Get tab item content id
     * @param index
     */
    getTabItemContentId(index) {
        return this.componentId + "__" + (index + 1);
    }

    /**@ignore
     * Get tab item list content id
     * @param index
     * @param subIndex
     */
    getTabItemListContentId(index, subIndex) {
        return this.componentId + "__" + (index + 1) + "__" + (subIndex + 1);
    }

};

/**@ignore
 * Tab component prop types
 */
Tab.propTypes = $.extend({}, Component.propTypes, {
    enabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    type: PropTypes.oneOf(["tabs", "pills"]),
    activeIndex: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    lazyLoad: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    style: PropTypes.object,
    onTabChange: PropTypes.func,
    onBeforeTabChange: PropTypes.func,
    onAfterTabChange: PropTypes.func,
    className: PropTypes.string,
    showDeleteIcon: PropTypes.bool,
    iconDisabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    tabSelfAdaption: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    isTabNeedScroll: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    needCrossValidate: PropTypes.oneOfType([PropTypes.string, PropTypes.bool])
});

/**@ignore
 * Get tab component default props
 */
Tab.defaultProps = $.extend({}, Component.defaultProps, {
    type: "tabs",
    style: {},
    activeIndex: 1,
    lazyLoad: true,
    className: '',
    showDeleteIcon: false,
    iconDisabled: false,
    tabSelfAdaption: false,
    needCrossValidate: false,
    isTabNeedScroll: false
});
