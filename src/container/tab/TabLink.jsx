import Command from "../../basic/Command";
import PropTypes from 'prop-types';

export default class TabLink extends Command {

    renderComponent() {
        const aProps = Object.assign({}, this.props);
        delete aProps.enabled;
        delete aProps.visibled;
        delete aProps.styleClass;
        delete aProps.causeValidation;
        const value = this.getProperty("value");
        const outputValue = value ? this.getI18n(value) : "";
        const title = this.getProperty("title");
        const outputTitle = title ? this.getI18n(title) : "";
        return (
            <a {...aProps} title={this.props.title ? outputTitle : ''} href={this.props.href} 
                onClick={this.onClick.bind(this, false)} 
                style={this.props.style != null && this.props.style != undefined ? this.props.style : null}
                className={this.props.className}>
            </a>
        )
    }

};

/**@ignore
 * Link component prop types
 */
TabLink.propTypes = $.extend({}, Command.propTypes, {
    causeValidation: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    validationGroup: PropTypes.string,
    exceptValidationGroup: PropTypes.string,
    title: PropTypes.string,
});

/**@ignore
 * Get Link component default props
 */
TabLink.defaultProps = $.extend({}, Command.defaultProps, {
    causeValidation: false
});
