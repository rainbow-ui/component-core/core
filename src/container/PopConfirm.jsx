import Command from "../basic/Command";
import PropTypes from 'prop-types';

export default class PopConfirm extends Command {
    constructor(props) {
        super(props)
        this.state = {
        }
        this.popperInstance = null
    }

    render() {
      return (
        <div>
          {this.props.children}
          <div id={this.componentId} class={this.props.className || 'popConfirm '}>
            <div class={this.props.arrowClassName || 'arrow'}></div>
            <div class={this.props.contentClassName || 'popConfirm-content '}>
              {this.props.renderPop}
            </div>
          </div>
        </div>
      );
    }

    static close(popId){
      document.querySelector('#'+popId).removeAttribute('data-show');
      if (this.popperInstance) {
        this.popperInstance.destroy();
        this.popperInstance = null;
      }
    }

    static show(srcId, popId, offsetX = 0, offsetY = 8, direction = 'top'){
      if(this.popperInstance){
        this.popperInstance.destroy();
        this.popperInstance = null;
      }
      const button = document.querySelector('#' + srcId);
      const tooltip = document.querySelector('#' + popId);
      tooltip.setAttribute('data-show', '');
      let offstr = offsetX+','+offsetY
      this.popperInstance = new Popper(button, tooltip, {
        placement: direction,
        modifiers: {
          offset: {
            offset: offstr
          },
          preventOverflow: { enabled: false },
          hide: {enabled: false}
        }
      });

      // 监听滚动
      $('body').bind('mousewheel', onMouseWheel);

      // 监听鼠标点击
      $('body').bind('click', onMouseClick);

      function onMouseWheel (event, delta) {
        if (delta!=0) {
          tooltip.removeAttribute('data-show');
          $('body').unbind('mousewheel', onMouseWheel);
        }
      }

      function onMouseClick (event) {
        let target = $(event.srcElement || event.target);
        if (!target.is(".popConfirm,.popConfirm **")) { 
          tooltip.removeAttribute('data-show');
          $('body').unbind('click', onMouseClick);
        }
      }
    }
};

PopConfirm.propTypes = $.extend({}, Command.propTypes, {
    arrowClassName: PropTypes.string,
    contentClassName: PropTypes.string,
    direction: PropTypes.string,
    custom: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
});

PopConfirm.defaultProps = $.extend({}, Command.defaultProps, {
    direction: 'top',
    custom: false,
});
