module.exports = {
    expandMessage: "Más",
    closeMessage: "Guardar",
    allMessage: "Ilimitado",
    customMessage: "Personalizado",
    expandRowNumMessage: "El número predeterminado de condiciones de expansión 'expandRow' debe ser UN número entero mayor que 0",
    filterMoreNotSupportMessage: "El plugin de consulta fiterMore no soporta el método {0}",
    confirmMessage: "Confirmar",
    multiselectMessage: "múltiple",
    singleSelectMessage: "única",
    Selected:"Seleccionado",
    UnSelected: "No seleccionado",
    AddAll: "Añadir todo",
    Add: "Añadir",
    RemoveAll:"Eliminar todo",
    Remove: "Eliminar",
    Data: {
        All: "Todos",
    },
    All: "Todos",
    Common: "común",
    // DataTable
    DataTable: {
        SelectAll: "múltiple",
        SingleSelect: "única",
        NoResult:"No hay registro."
    },
  
    // Pagination
    DropDownInfo: "Por página {0}",
    Search: "Buscar",
    FirstPage: "Inicio",
    PreviousPage: "Anterior",
    NextPage: "siguiente",
    LastPage: "Último",
    Pages: "P",
    Total: "Todo",
    Items: "Proyecto",
  
    // Input
    // Select
    BlankOption: "Por favor seleccione",
  
    // DateRangePicker
    DateRangePicker: {
        ApplyLabel: "Confirmar",
        CancelLabel: "Cancelar",
        FromLabel: "Comienzo",
        ToLabel: "Fin",
        CustomRangeLabel: "personalizado",
        times: ["horas", "minutos", "segundos"],
          timeChoose: 'Elegir el tiempo',
          BackDate: "Fecha de retorno",
          Clear: "Vacíe",
          Today: "Hoy",
          Close: "Cierre",
        DaysOfWeek: ["Dom", "Lun", "Mar", "Mier", "Juev", "Vier", "Sab"],
        MonthNames: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    },
  
    // TwoText
    TwoText: {
        dialogTitle: "Buscar tabla de códigos",
        tableHeaderTitle: "Buscar tabla de códigos",
        keyColumn: "Clave",
        valueColumn: "Valor",
        confirmButton: "Confirmar",
        cancelButton: "Cancelar",
        error: "Por favor seleccione un registro",
    },
  
    // Data
    // FullCalendar
    FullCalendar: {
        MonthNames: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        MonthNamesShort: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
        DayNames: ["Dom", "Lun", "Mar", "Mier", "Juev", "Vier", "Sab"],
        DayNamesShort: ["Dom", "Lun", "Mar", "Mier", "Juev", "Vier", "Sab"],
        Today: ["Hoy"],
        FirstDay: 1,
        AM:"am",
        PM:"pm",
        ButtonText: {
            //Prev: "<",
            //Next: ">",
            Prev: "mes pasado",
            Next: "próximo mes",
            Today: "Hoy",
            Month: "Mes",
            Week: "Semana",
            Day: "Día"
        }
    },
  
    // Integration
    // DropZone
    DropZone: {
        dictDefaultMessage: "Soltar los archivos o haga clic aquí para cargar",
        dictFallbackMessage: "Su navegador no admite la carga de archivos de arrastrar y soltar.",
        dictFallbackText: "Utilice el formulario de reserva a continuación para cargar sus archivos como en los viejos tiempos.",
        dictInvalidFileType: "No puede cargar el tipo de archivo, el tipo de archivo no es compatible.",
        dictFileTooBig: "El archivo es demasiado grande ({{filesize}}MB). El máximo soporte de carga de archivos: {{maxFilesize}}MB.",
        dictResponseError: "Error en la carga de archivos!",
        dictCancelUpload: "Cancelar la carga",
        dictCancelUploadConfirmation: "¿Estás seguro de que quieres cancelar la publicación?",
        dictRemoveFile: "Eliminación de archivos",
        dictMaxFilesExceeded: "Usted puede cargar hasta {{maxFiles}} archivos a la vez",
    },
  
    MSG_NOT_FOUND: "Mensaje no encontrada",
    MSG_REGULAR_EXPRESSION_ERROR: "Sólo se pueden introducir números",
    MSG_DATACONTEXT_KEY_DUPLICATE: "(Datacontexkey) repita, por favor revise.",
  
    //validator message
    InputValidator: {
      RequiredMessage: "El campo es necesario",
      LengthMessage: "El valor debe tener más de {0} y menos de {1} caracteres de longitud",
      MinLengthMessage: "El valor debe tener más de {0} caracteres de longitud",
      MaxLengthMessage: "El valor debe tener menos de {0} caracteres de longitud",
    },
    DigitValidator: {
      ValueMessage: "El valor debe ser mayor que {0} y menor que {1}",
      MinValueMessage: "El valor debe ser mayor que {0}",
      MaxValueMessage: "El valor debe ser menor que {0}",
    },
    EmailValidator: "La entrada no es una dirección de correo electrónico válida.",
    Page: {
        needContinue: "Continúa la operación?",
        messageFront: "Logout Después de ",
        messageEnd:" segundos.",
        confirm:"Confirmar"
    },
    Required:"necesario.",
    DateTimePicker:{
        maxDateErrorMessage: "La fecha de entrada excede la fecha máxima:",
        minDateErrorMessage: "La fecha de entrada es posterior a la fecha mínima:"
    },
    // UISearch
    Searching: 'En búsqueda...',
  
    //TimeLine
    Online: 'En línea',
    Offline: 'Desconectado'
  };
  