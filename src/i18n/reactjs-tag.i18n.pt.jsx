module.exports = {
    expandMessage: "mais",
    closeMessage: "guarda",
    allMessage: "ilimitada",
    customMessage: "personalização",
    expandRowNumMessage: "O número de condições de expansão por defeito deve ser um número inteiro maior que 0",
    filterMoreNotSupportMessage: "O plugin de consulta fiterMore não suporta o método {0}",
    confirmMessage: "confirmação",
    multiselectMessage: "Escolha múltipla",
    singleSelectMessage: "rádio",
    Selected:"Já foi escolhido.",
    UnSelected: "Não seleccionado",
    AddAll: "Adicionar todos",
    Add: "adicionar",
    RemoveAll:"Remover todos",
    Remove: "Remover",
    Data: {
        All: "Todos os",
    },
    All: "Todos",
    Common: "comum",
    // DataTable
    DataTable: {
        SelectAll: "Escolha múltipla",
        SingleSelect: "rádio",
        NoResult:"Não há registo."
    },

    // Pagination
    DropDownInfo: "Cada página {0}",
    Search: "busca",
    FirstPage: "Ficha técnica",
    PreviousPage: "Página anterior",
    NextPage: "Página seguinte",
    LastPage: "página",
    Pages: "páginas",
    Total: "Total",
    Items: "Itens",


    // Input
    // Select
    BlankOption: "Por favor, escolha",

    // DateRangePicker
    DateRangePicker: {
        ApplyLabel: 'A certeza',
        CancelLabel: 'Cancelar',
        FromLabel: 'Hora de início',
        ToLabel: 'Hora final',
        CustomRangeLabel: 'personalização',
        times: ["Horas", "Minutos", "Segundos"],
        timeChoose: 'Escolha do tempo',
        BackDate: "Data de regress",
        Clear: "Esvaziar",
        Today: "Hoje",
        Close: "Encerramento",
        DaysOfWeek: ['DOM','SEG', 'TER', 'QUA', 'QUI', 'SEX','SAB'],
        MonthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
    },

    // TwoText
    TwoText: {
        dialogTitle: "Pesquisa Code Table",
        tableHeaderTitle: "Pesquisa Code Table",
        keyColumn: "Botão",
        valueColumn: "Valores",
        confirmButton: "Confirmação",
        cancelButton: "Cancelar",
        error: "Por favor selecione um registro",
    },

    // Data
    // FullCalendar
    FullCalendar: {
        MonthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        MonthNamesShort: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
        DayNames: ['DOM','SEG', 'TER', 'QUA', 'QUI', 'SEX','SAB'],
        DayNamesShort: ['DOM','SEG', 'TER', 'QUA', 'QUI', 'SEX','SAB'],
        Today: ["Hoje"],
        AM:"De manhã",
        PM:"À tarde",
        FirstDay: 1,
        ButtonText: {
            //Prev: "<",
            //Next: ">",
            Prev: "No mês passado",
            Next: "No mês que vem",
            Today: "Este mês",
            Month: "mês",
            Week: "Semanas",
            Day: "De outubro"
        }
    },

    // Integration
    // DropZone
    DropZone: {
        dictDefaultMessage: "Arraste o arquivo para lá (ou clique aqui)",
        dictFallbackMessage: "O seu navegador não suporta componentes de upload.",
        dictFallbackText: "Por favor, use o formulário de retorno para carregar os seus arquivos como nos dias de olden.",
        dictInvalidFileType: "Você não pode upload esse tipo de arquivo, o tipo de arquivo não é suportado.",
        dictFileTooBig: "O arquivo é muito grande({{filesize}}MB). Suporte máximo para o upload de arquivos: {{maxFilesize}}MB.",
        dictResponseError: "O carregamento do ficheiro não foi feito!",
        dictCancelUpload: "Cancelar o upload",
        dictCancelUploadConfirmation: "Tens a certeza que queres cancelar o upload?",
        dictRemoveFile: "Remover arquivos",
        dictMaxFilesExceeded: "Você pode carregar até um máximo de {{maxFiles}} arquivos de cada vez",
    },

    MSG_NOT_FOUND: "Informação não encontrada",
    MSG_REGULAR_EXPRESSION_ERROR: "Só podem ser introduzidos números.",
    MSG_DATACONTEXT_KEY_DUPLICATE: "(DataContext key)Repito, exame emocional.",

    //validator message
    InputValidator: {
      RequiredMessage: "O Campo é necessário",
      LengthMessage: "O comprimento do campo deve ser maior que {0} e menor que {1}",
      MinLengthMessage: "O comprimento do campo deve ser maior que {0}",
      MaxLengthMessage: "O comprimento do campo deve ser inferior a {0}",
    },
    DigitValidator: {
      ValueMessage: "O valor deve ser maior que {0}, e menor que {1}",
      MinValueMessage: "O valor deve ser maior que {0}",
      MaxValueMessage: "O valor deve ser inferior a {0}",
    },
    EmailValidator: "Endereço de e-mail não é legal",
    Page: {
        needContinue: "Se a sessão está prestes a esgotar o tempo para continuar ou não a operação?",
        messageFront: "O sistema será",
        messageEnd:"Saída em segundos.",
        confirm:"é"
    },
    Required:"Opções obrigatórias",
    DateTimePicker:{
        maxDateErrorMessage: "A data de entrada excede a data máxima:",
        minDateErrorMessage: "A data de entrada é posterior à data mínima:"
    },
    // UISearch
    Searching: 'Em busca...',

    //TimeLine
    Online: 'Conectados',
    Offline: 'desligada'
};
