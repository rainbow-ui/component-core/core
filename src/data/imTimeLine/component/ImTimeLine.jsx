import Component from "../../../basic/Component";
import PropTypes from 'prop-types';
import UIDateTimePicker from '../../../input/dateTimePicker/component/DateTimePicker'
import r18n from "../../../i18n/reactjs-tag.i18n";

export default class ImTimeLine extends Component {
    constructor(props) {
        super(props);
        this.tempArr = [];
        this.dupArr = []
    }

    // componentDidMount() {
    //     Array.prototype.DuplicateArr=function(){  
    //         var tmp = [];  
    //         this.concat().sort().sort(function(a,b){  
    //             if(a==b && tmp.indexOf(a) === -1) tmp.push(a);  
    //         });  
    //         return tmp;  
    //     }  
    //     this.tempArr.DuplicateArr();
    //     [...$('.listItem .listItemContent-date')].forEach((item,index) => {
    //         console.log(item,index)
    //     })
    // }

    renderComponent() {
        return (
            <div>
                {this.buildItem()}
            </div>
        )
    }

    buildItem() {
        let personPhoto = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAABGdBTUEAALGPC/xhBQAAEgVJREFUeAHtXW1sHMUZntn7SGxiO5/gfDjGIV9ASOBskpQGmqalPxD9AyIVabBpqUQVKn5QqU3/gEr/IH6AhARVKkCQRq1ACv3XH4WiFIgo4KREECAJIYR82PmO7RAnPt9Nn2fthePs253dnd27s+6V7L3bnXnfd57nZndmduYdIWpSQ6CGQA2BGgI1BGoI1BCoIVBDoIZADYEaAgUIyILPE+bjtgOqcbg/uyhhyekqLxuUEg1C5nGUDSyklGpAKGtASjEgLTWQy6uzycbUgY2LZP+EAWG0IFVNsFLK2rZL3JgX2VuVFNdJIZeAzCVCqOZgRMlekL5PCbVPKvGJJVJvb2wXH0op88H0lT9X1RG8dZe6Vonsj1AP1wG+tSB5WpQwgtxz0L8DP5o3pUj9u7NdfhqlPdO6q4LgV/eq5sHLwxuEUp2ooStMg+BHH2r4Htzjt9ZNSv5t/fWy10/ecqStWIJ5+/3rrtzdeal+idvl7bhtJsoBUCmbeBzk8Fh43VLyxfvaE9sr9TZecQR3d6vUxzK3Uar8ZiXE4lIAV9J5gLhfSeuJZSqxraNDZivMt8pwB7fh9OBg9ldCit/hNtxaGV758wK378NCiSfr6lLP4/Y95C93NKkroga/vDv7Y5FXz1ZLjfWigjVaWPKhrkzqDa+0UV8vK8F/363mDKnsU6ixPzNZ0MZJQjQ3WGJavRBNk6SYgr8UnuApa8RKFp2ebE6IC5eV6MPfuYtC9A7kRf9lk16wvy1eScvUI/dm5HGzmvW1lY3gl7uHNikhn0D3wx580Hd5/JQz6qVYMF2Kq6dZoj4drFgXh5T48lxefHFWiTMXcT8xInJACrW5qyP9nBF1PpUEQ8KnkcLkr3arpoty+AV0ee4uPB/085VTpOiYa4lZU0arZ1BFRflOXciL7mN5cfKCIaKl3F6vkg+s75B9RaYi/RorwVt3D3WovHgVkLWFLVXTZCHa5yZEy1SzxBb7deR8Xuw6lhN9l4qv+P8OsA9JS6zvzKS7/ecOliM2gl/eNfQgnkrPoH+bDubqSK4EPF7ZkhALZ0q0Y+JxP49GwuenlXj/SE7kQlZo9JfRulYPd7Wnt4TBQTdvLAi9tDv7R7SSH9V1qlS6+pQQ665JihlXxOL2GDfOfK3EmweHxUUTPV1LPn5/JvXYGCOGT0SKFEejtu7OPadUHrU3nMwEqeuuSYi6VKQuezo5mCXJOXEaZIcVKa0tnZnEpihHwSJD658H1KRT/cN/A8l3hQXi6mlSrLk6IfD6L6wqI/nxelG882UOLW4TJMvXZjUmN9yxSBrupI0UNZIWCmuuKXKvQiv51rbKIZew8YdGn+hbWGEFGMUqEi4iUTpyWw5fc6egObYWt+W4GlN+yKJP9I0+hhWSTMzC6hkvv3GC2aAy8cxNwrN1C5NicjJ8LRmv4CbO0Tf6SF/DCjGzG6NhFRXlN+DatxrtrpCB1jI18pk7ra5yyXVKTR/pqxEBdiPdSSPabCXGCOYgBvu5Jlyb3SBFK4Ycq0XoK302I/KZESzNaDOCIocf7RGqkIMYTpHa5xmqEY7CGI6mfMbzOE0siakJt40QzLFldBjaTDjUhhcGfHFQbUKf6bsJIZb2eL0BZaEJ5lshUy8O2M3NzKm+2uvwQN+NddXxMsbG1lEe8BiKYL7PHXnlF9B6UbZWDGjw3W21Cn1nGUwJsSXGYfSFIhgv65829T6XhbhmRih3wuBgLK/ZMqgGTogI41xgRDnNBi9Z1ocxXph3UlKIOcZaooWa4/3M1jTLYko428We0hRQYSCCOUGOc6gC2hw3G4HBoPu416rpJEe4zHWZRksOrG3MAwARiGDOfkRLb3EAeyWzzMYcqokipstCrO0ZpwEA8o0q5y1zamsAW65ZqrFrVKpAkZQFmNvYlzJa4rxvgjkpHc+F1hL6Ap9uxBSciSJRlIWYE3u/GPkimK8BueLArxGv9JPRKElxLs4EEZaFZTIt9moPcOBHr6/EXCtk+tlLZ022Ov0UPsq0UZSJ2JMDP377IpgLwfwo102bMjb8o2sx+nRRlckvB9oEcwknV/lFAc0E6B2NgSWqMpEDcjHGYIkT2gRzfW5USziHMcdpoklUZSIH9lppTcC0Cebia02dvpNxndBEk0jL5IMLLYLtsAkRrqwfxDxjtNAnDMcsC8sUlUD9CnKio1+LYCWxvDNC4WqBrytiNa2ZQrIsYVdAeHkyEqfEK5UQWgQLJX/orSpcivOXJk4NjqcsdhAaT9A9CebgBrSs9dQUMsEJU6v4QvphIntMZVk7yo2ry54EMw4VFEUaqoge9vZPnBocR1nICblxZRcXPQlmkDEvJSaun8aCay7ArnZhGViWOESHG0+CGUEuDmdp44uzVRtQ7huI4iyDDjeeBDM84DfeR/zhwJl8VXeXcNsULENcosONJ8HwOTaC+7GK/iDiY1Sr0HeWIS7R4caVYEZtxRCE9riniYL9D+ESuDyz2oQ+0/d4RTWPcFTaqivBDMlbOms0V7h6/tOT8d3mTJWCPhtZ+e/TIS+OXAnGAMcMn/aMJP+oNy8uD1dPLaav9Lks4sGRK8GYITilHE4P4U7HFfRstFS60Ef6Sp/LIV4cuRIM340EKQtS8KN9SuxGnKpKF/pIX8slXhy5Esww+OVynHY/PoGoczF2O/yWlb7Rx7KKB0euBDt7HJSzADsP5wSjzlWa0Cf6Vm7x4siV4HI7T/vsMf3rQE4cRgzJShH6Qp+qoTfnSrC9O0kFoDoMbnd8kRN7espfY+gDfaFPlSBeHLnP3sXWM6hDlVAO24cPj+fF+UElvo+YGMmYZ2JyjtVOQ7GxjAJqc1RaoyvBmBk4UGk9FQYfO/31sMggECkDpEW9YI3dINrcjVGqCxU464QclaZXCHeCsWmUKP9dcYz/BPqtQzmx94RExFkEQGl0fdKMya97oqefkWbzBmNH61rWT8eNvdxSuxLMHcHcMpf7GoN2s7EzpzEvls6yxNym8BFoGVn2GPq1n53Ki+NVMAnBiyNXgrnd23BfhNMDDf1CSMTx/py9BGb+VK7PtRDSX2oHLmWA0d4BJXoQ1v+r8wrDpIYci0ENOXIz47ni66XubE/cb5TcHHa7dgXCCrY0WfY72RzahlwA1jhZigac54Iw7ttA4ZzlLKY9DuBW34/JfpdAKIFYcqWF2psXA5GEBbVNG/4ne+/vSM12U+pag5kRD/F9uGvF+srQzeHiawwz3IJbMyO/M+rcWdy2eXulkLhLmMx30v7mPpzIq62o/ataUojurgQjvR/BrfoU8rvntJWX5R+58TLsSTCKRyU/8FIU53XWtgUzpFjenLBraKHts+hGBRX+OJoxONuEWt8E3cvws+bOLHxT9DmGJSttYGOUG9fiehKMxU6fBIfM1bbvi27EOsrCvGa8PE6PgaGRvteaEDfMtsRHPZVFNLlxyl3q6Ekwt1jNifI3tFpw++xAiMNGjzha9SEiwnPLgFIyBVv1kOjlIJqbdByqgKlF5KaUv855zw7k6P653GK1LIK2kVg937L3avAilw5ODRGhdipuzV5yBYi+rS1pR5g1EUbYy16p6xjgOUduSl13znsSDEVssexwMsR5nFonxJ3XJsWSWfrhDdnQmo58foWBvbkHk64w4NlPr8MGIeWLq7ljlBtXlz0JHsmt3nTVEsHFxTMtcefSZKAauSJAvEvm8TvsyTvKHUsT4vqrNGE0ipMeJ1qecedro755KFvWbNnPu6CbcMxHl2kJRrZ0hVFiFwYMo8jAZ2wbZDBkGqeklN7Gl1pecVtzlGNPHAVgbeCOZmFlVYslbsAPxUs4xGkiYvsN6FbdNMfbnpc/OtfJxYYO+ZlOWn2PsK25jsIwaUgua4MJ4e2Wt3kvWQyCWQtNyPLZMZHsgwtvBEZLzj3r8XJunJ6iCWiEWIphQlPkOh7prNPlqJVJIcnsSkUl5IBc6OrX9gQ7Wvdyz3pdxX7Scbjx5nnarmir5uQAL9FJ46Wj+PqNINh4QNJRI+SAXBTbLPXdF6qWki+WUhT0PPuSt2GTKVO3yUI/dGqwTppCnTqf+XhYgzJFEQwNul/Q8cFJ44vg+9oT2/G02u9kNnFcNT8hGjxGp4LaOT/onTOKGkyrHFG7BSNfJoXYd96UeM2PTl8E49eTV9LCrt1mhFNugnZPvDzgVBud5yu3dedL/iiE3TWdhp6ubWJPDnTTM50vgplhmUpswx3oMD+HkRQsr0btjUoY6UZn5iO5jXLJ583orpkITErMib1fvHwT3NEhs3hB+qRfQ8Xp2WqeFOG2dX6erX7SFpfD6ztnfxoZ6QLmNvZeBouu+yaY+evqUs+HeRazYXVdxMN7fp6tftIW4af1laNqYRpcxJqYaxkrShSIYDTTh9DsfahIl/ZXjh5Fvemkn1rpJ612IQsScrpQqFoMrG3MC3TqfgxEMJV3ZVJv4Lnwiq4hJ10CFkMV1lHkcdRpQTsqoq7BtMMfdTpAk4MYE2vHV7/HwATTUFqmHsGsLdd5ucUOLcDA/uQQL+WL9Y33XbcF7eSNsiXt2GAtXqQxdOqkHznKgRGMv3vWz7dQBN+bkcelUJv9GGTXIWrRbUE7fkTdknbscEqvHyG2xNhPnuK0odHu6kg/hxep24sVj/edt+eohvAK7QV5pgbJU2hT5/MsDMlqN7aAqY2tjmKXNKEJpu56lXwAv81DLnbsS9zZLOg7Xi/dhdeDPFOD5Cm0qfOZQ5jzGr1rMbEkpjo6vdIYIXh9h+yTlliPAmB4obTMi+H2TOtBamOQPKVLWvqKFwbEkFgS09Ja9K8YIZjmOjPpbqyAeNjNNCeoxyF+WtCOP3HUYNqaixqMiuwi6uERLF2S+LhkjGDa7GpPb0H/+PHx7HNKal3ErWfa9duCdnyNoyVNW2xNN01yrBYdgZ2NYdHpMF+NEkxH7s+kHpPS2lLsVJjprMW63L77bUE7uuJqSdPeeFgQM2Ln+GPqaJxgOtaZSWzCs+Q7r7V05hybKFSYZ2mYvH58L8aCWBEzPzp000ZCMBzOz2pMbigkuc5l1YCuszrpwjxLw+TV8c1JU4fJ844Qo1GsfL0GdPJ7HSMhmEbvWCQvd2aS9zi366hHr5yChqmFYfI69nWOzutDYkOMiJVOviBpIiOYzrAmd7Unf82GVz6S3+fYIgdpQTta+jTmcDlpwxy5dpmYEBtiFEaXV95ICXaMs/HQ3SM2fX5GRTYrk7aCtqAdP/sinN3h2Dh8XuXeO5b7TRQNKsdG4TEWgmnw5ysSf373q+HVe47nz5KIKCRoC9rxJeqW9Ccn1bm3Dg2vvnd5+lnHZtTH2AhmQdiB399jLfjvkfy7UeztZ+IZakJHMWkMFt59NPfB3iOyzeQgRrGd8b7HSjAd4BDcLa3JW947qp4YHDYbZc1EK9iEjkKgsSBd7Tqmnrq5JbnS1PBjoX6vz7ET7Di0pjXxh/2nxfKefvWlcy7s0UTt05mJqetn7wV19OAJmcHU4N/q5jGdrmwEsyA3zpZ75zRZbfvO5H+PcPiuLyp0Ch6mBe3oN1GDEfwle/CseBThnFqunyc9F2k7tqM4lpVgp0BLZyaePD4gZqE2v46GTqAWWNgWtONLyJa0QlSe/5y+KJoXzpB/cnSW81gRBBOARTNkP2rzT4aHxUq0hrWWRhYCF7YF7egK2pLGdrIHoePWKxustXhrVjERAiuGYAfgdFp2I7IN98ZdiSBlO3VrtInnr+ODri7cahSCqb1PYuvTciEGLXY6OirlWHEEO8AArA+a6qw1eHfahsh0f0FPw3U4z8Sz07HtpQtLXbLZvHgJI8pLEMZhFXx9x8lbaceKJdgBCuAdTiflg1gg0Ixzj2AHG0YaGPOc1q11jl63Y6mWNJbCMC7VZqyEnJ1OyF/AN9c4kW424rrmGScrLke87ADM80jzNP/QoGrB8S783Q6mb0NNahjKyX+gtk/HLR1b8QXdrU32Qse+wZwd/fAe6LqI72/DDuclb8fL+kM4VpV8+96qqtz+1lmQzenkN+HvY/wILvEKt3uzdwTDplGobVNAVAN3kHE2sLDD4CNSOsgbwO32gpDqDKO2blwk+5kfOvlyM4O/XdCJiJc1qSFQQ6CGQA2BGgI1BGoI1BCoIVBDoIZADYEJgsD/ARC3Lj0uddQQAAAAAElFTkSuQmCC'
        let itemHtmls = [];
        let _self = this;
        this.props.dataSource.forEach(function (item, index, arr) {
            _self.tempArr.push(item.date);
            let itemInfo = []
            _.each(item.info,msgInfo=>{
                itemInfo.push(<div>{msgInfo}</div>);                
            })
            itemHtmls.push(
                <div className="listItem clearfloat">
                    <div className="listItemContent clearfloat">
                        <div className="listItemContent-date">
                            <UIDateTimePicker value={item.date} io='out' layout="horizontal" widthAllocation="0,12"/>
                        </div>
                        <div className="listItemContent-content">
                            <div>
                                {_self.props.noImg ? 
                                        <noscript/>
                                    :
                                    <div className="imgarea">
                                        {item.msgcount ? <div className="msgnum">{item.msgcount}</div> : ''}
                                        {/*<img src={item.imgUrl != '' ? item.imgUrl : personPhoto} />*/}
                                        <span className="imProfile">
                                            <img style={{ opacity: item.online == 1 ? '1' : '0.3' }} src={item.imgUrl != '' ? item.imgUrl : personPhoto} />
                                        </span>
                                    </div>
                                }
                                <div className="peroninfoarea" onClick={_self.gotoUrl.bind(_self, item.url)}>
                                    {item.userName}
                                    <span className="graytxt">
                                        {_self.props.noLine ? '' : item.online == 1 ? ' - ' + r18n.Online : '- ' + r18n.Offline}
                                    </span>
                                    <div className="infomation">
                                        {itemInfo}
                                    </div>
                                    <div className="messageinfo">
                                        {/*{item.msg[0]}*/}
                                        {_self.getImMsgHtmlDom(item.msg[0])}
                                    </div>
                                </div>
                                <div className="time">
                                    {item.time}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>);
        });

        return itemHtmls;
    }

    gotoUrl(url) {
        if (this.props.handleEvent) {
            this.props.handleEvent(url)
        }
    }



    //截取消息文本方法
    parseImMsgContent(msgContent) {
        // msgContent = 'wwwww-<a href=http://172.25.18.37:8080/ui/application/#/applicationView?appId=223836061 target="_blank">BO183301010000044</a>/nsffsfasfwewe';
        // debugger
        let specialStr1 = '<a href=';

        if (msgContent.indexOf(specialStr1) != -1) {
            let specialStr2 = 'target="_blank">';
            let specialStr3 = '</a>';
            let specialStr4 = '\n';
            let firstStr = msgContent.substr(0, msgContent.indexOf(specialStr2) - 1);
            let secondStr = msgContent.substr(msgContent.lastIndexOf(specialStr2) + specialStr2.length);
            let firstText = firstStr.substr(0, firstStr.indexOf(specialStr1));
            let url = firstStr.substr(firstStr.lastIndexOf(specialStr1)+ specialStr1.length);
            let secondText = secondStr.substr(0, secondStr.indexOf(specialStr3));
            let thirdStr  = secondStr.substr(secondStr.lastIndexOf(specialStr3) + specialStr3.length);
            let thirdText = thirdStr.substr(thirdStr.lastIndexOf(specialStr4) + specialStr4.length);
            thirdText = this.cutOutString(thirdText, 40);
            return(<span>{firstText}<a href={url} target="_blank">{secondText}</a>{thirdText}</span>);
        }else{
            msgContent = this.cutOutString(msgContent, 60);
            return msgContent;
        }
    }


    getImMsgHtmlDom(msgContent) {
        return (<div>
            {this.parseImMsgContent(msgContent)}
        </div>);
    }

     cutOutString(messageText, charNumber) {
        if (messageText.length > charNumber) {
            return messageText.substring(0, charNumber) + "...";
        }
        return messageText;
    }
}

ImTimeLine.propTypes = $.extend({}, Component.propTypes, {
    dataSource: PropTypes.object,
    noLine: PropTypes.bool,
    noImg: PropTypes.bool,
});


ImTimeLine.defaultProps = $.extend({}, Component.defaultProps, {
    noLine: false,
    noImg: false
});
