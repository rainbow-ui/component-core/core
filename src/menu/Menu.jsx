import Component from "../basic/Component";
import { Util } from 'rainbow-desktop-tools';
import Param from "../basic/Param";
import OnClickEvent from '../event/OnClickEvent';
import PropTypes from 'prop-types';
import { LocalContext, PageContext, StoreContext } from 'rainbow-desktop-cache';

export default class Menu extends Component {
    renderComponent() {
        let className = "navbar-nav menu " + this.props.className
        return (
            <ul id={this.componentId} className={className} style={this.props.style}>
                {this.renderMenuBar(this)}
            </ul>
        );
    }

    /**@ignore
     * Render menubar
     */
    renderMenuBar(component) {
        if (component.props.children != null) {
            let _self = this;
            if (React.Children.count(component.props.children) == 1) {
                if ($.isArray(component.props.children) && component.props.children.length == 1) {
                    return this.renderMenuBarChildren(component.props.children[0]);
                } else {
                    return this.renderMenuBarChildren(component.props.children);
                }
            } else {
                return component.props.children.map(function (child, index) {
                    return _self.renderMenuBarChildren(child, index);
                });
            }
        }
    }

    /**@ignore
     * Render menubar children
     */
    renderMenuBarChildren(component, index) {
        let _self = this;

        if (component.props.visibled != undefined && !Util.parseBool(this.getProperty("visibled", component))) {
            return <noscript />;
        } else if (component.props.componentType == "MenuItem") {
            return component;
        } else if (component.props.componentType == "Separator") {
            return component;
        } else if (component.props.componentType == "SubMenu") {
            if (!Util.parseBool(component.props.visibled)) {
                return <noscript />;
            }
            return (
                <li className={"nav-item " + component.props.active + (component.props.fakeMenuItem == 'true'?' fakeMenuItem':'')} onClick={this.onClickItem.bind(component)}>
                    <a className="nav-link dropdown-toggle" href="javascript: void(0);"
                        aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" >
                        {Util.parseBool(component.props.icon) ?  _self.getIcon(component) : null}
                        {Util.parseBool(component.props.badges) ? _self.getbadges(component) : null}
                        {Util.parseBool(component.props.badges) ? null : !Util.parseBool(component.props.noI18n) ? <span onClick={this.onSubClickItem.bind(component)}> {this.getI18n(component.props.value)} </span> : <span onClick={this.onSubClickItem.bind(component)}> {component.props.value} </span>}
                    </a>
                    <ul id="menuItemListBox" className={"dropdown-menu dropdown-menu-right menu-item-list " + component.props.className} style={{ display: 'none' }}>
                        {this.renderSubMenuBar(component)}
                    </ul>
                </li>
            )
        } else {
            return (
                <li>
                    {component}
                </li>
            )
        }
    }


    bindEvent() {
        const components = $(".navbar").find(".menu-item-list");
        $.each(components, function (c) {
            const component = $(this);
            component.unbind("mouseover");
            component.unbind("mouseout");
            let boxa = component.prev();
            boxa.unbind("click");
            boxa.bind("click", function(e){
                if(component.css('display') == 'none'){
                    component.show();
                }else{
                    component.hide();
                }
                $(document).one("click", function(){
                    component.hide();
                });
                component.bind("mouseover", function () {
                    component.show();
                })
                component.bind("mouseout", function () {
                    component.hide();
                })
                e.stopPropagation();
            });
        })
        // const components = $(".navbar").find(".menu-item-list");
        // $.each(components, function (c) {
        //     const component = $(this);
        //     component.prev().unbind("mouseover");
        //     component.unbind("mouseover");
        //     component.unbind("mouseout");
        //     component.parent().bind("mouseout");
        //     component.prev().bind("mouseover", function () {
        //         component.show();
        //         // let left = event && event.target ? event.target.offsetLeft - 120 : 0; // event && event.clientX ? event.clientX / 2 - 100 : 0;
        //         // if (left != 0) {
        //             $(component).css({ right: '0px', width: 'min-content', top: '50px' })
        //         // }
        //     });
        //     component.bind("mouseover", function () {
        //         component.show();
        //     })
        //     component.bind("mouseout", function () {
        //         component.hide();
        //     })
        //     component.parent().bind("mouseout", function () {
        //         component.hide();
        //     })
        // })
    }
    bindHover(){
        const subMenuComponents = $(".nav-item").find(".dropdown-menu > .dropdown-submenu");
        let _self = this
        $.each(subMenuComponents, function (c) {
            const subMenuComponent = $(this);
            subMenuComponent.bind("mouseover", function () {
                subMenuComponent.addClass("show");
                subMenuComponent.parent().css({ display: 'block' })
                subMenuComponent.find('.dropdown-menu').addClass("show");
                if (_self.subMenuPosition){
                    _self.onSubMenuClick(_self.subMenuPosition)
                }
            })
            subMenuComponent.bind("mouseout", function () {
                $(".nav-item .dropdown-menu .dropdown-submenu").removeClass("show");
                $(".nav-item .dropdown-menu .dropdown-submenu .dropdown-menu").removeClass("show");
            })
        })
    }
    onSubMenuClick(subMenuPosition) {
        const subMenu = $(".dropdown-menu").find(".dropdown-menu.show");
        let _self = this
        $.each(subMenu, function (c) {
                const subMenuItem = $(this);
                let top = subMenuItem.parent().position().top;
                let totalWidth = subMenu.innerWidth() + 2;
                if (subMenuPosition && subMenuPosition == 'right'){ 
                    $(subMenu).css({ top: top, right: -totalWidth, width: 'min-content'})
                } else if (subMenuPosition && subMenuPosition == 'left') {// 默认left
                    $(subMenu).css({ top: top, left: -totalWidth, width: 'min-content'})
                }
        })
    }
    componentDidUpdate() {
        // this.bindEvent();
    }

    componentDidMount() {
        this.bindEvent();
        if (this.hoverShow){
            this.bindHover();
        }
    }

    renderSubMenuBar(component) {
        let _self = this;
        if (React.Children.count(component.props.children) == 1) {
            if ($.isArray(component.props.children) && component.props.children.length == 1) {
                return this.renderSubMenuBarChildren(component.props.children[0]);
            } else {
                return this.renderSubMenuBarChildren(component.props.children);
            }
        } else {
            return component.props.children.map(function (child, index) {
                return _self.renderSubMenuBarChildren(child);
            });
        }
    }

    onClickItem(event) {
        $(".navbar-nav.menu li a").removeClass("highlight");
        $(event.target).addClass("highlight");
        $(event.target).parent().parent().children("a:eq(0)").addClass("highlight");
        event.preventDefault();
        if (Util.parseBool(this.props["enabled"])) {
            return
        }
        if (this.props.onClick != undefined) {
            this.props.onClick(new OnClickEvent(this, event, Param.getParameter(this)));
        }
    }
    onSubClickItem(event) {
        if (this.props.onClick != undefined) {
            this.props.onClick(new OnClickEvent(this, event, Param.getParameter(this)));
        }
    }

    renderSubMenuBarChildren(components) {
        if ($.isArray(components)) {
            const returnMenu = [];
            components.forEach((component) => {
                returnMenu.push(this.buildComponent(component));
            });
            return returnMenu;
        } else {
            return this.buildComponent(components);
        }
    }

    buildComponent(component) {
        let _self = this;
        if (!component) {
            return (
                <li>
                </li>
            );
        } else if (component.props.visibled != undefined && !Util.parseBool(this.getProperty("visibled", component))) {
            return <noscript />;
        } else if (component.props.componentType == "MenuItem") {
            if (component.props.category && component.props.category == 'language') {
                let langid = sessionStorage.getItem('system_i18nKey');
                // let langList = LocalContext.get('UserLanguageList');
                // let curLang = langList.find((item) => item['@pk'] == langid);
                return (
                    <li className={"dropdown-item" + (component.props.fakeMenuItem == 'true'?' fakeMenuItem':'')} {...component.props} onClick={_self.onClickItem.bind(component)} style={{ paddingLeft: langid == component.props.value ? '5px' : ''}} >
                        {
                            component.props.imageIcon ? 
                            <img src={component.props.imageIcon} style={{ width: '14px', height: '14px', marginRight: '10px' }}/>
                            :
                            <noscript/>
                        }
                        {_self.getbadges(component)}
                        <span className={langid == component.props.value ? "rainbow Check ": ''}>&nbsp;</span>
                        
                        {Util.parseBool(component.props.badges) ? null : !Util.parseBool(component.props.noI18n) ? this.getI18n(component.props.value) : component.props.value}
                    </li>
                );
            } else {
                return (
                    <li className={"dropdown-item" + (component.props.fakeMenuItem == 'true'?' fakeMenuItem':'')} {...component.props} onClick={_self.onClickItem.bind(component)}>
                        {
                            component.props.imageIcon ? 
                            <img src={component.props.imageIcon} style={{ width: '14px', height: '14px', marginRight: '10px' }}/>
                            :
                            <noscript/>
                        }
                        {_self.getbadges(component)}
                        {/* &radic; */}
                        {Util.parseBool(component.props.badges) ? null : !Util.parseBool(component.props.noI18n) ? this.getI18n(component.props.value) : component.props.value}
                    </li>
                );
            }
        } else if (component.props.componentType == "Separator") {
            return component;
        } else if (component.props.componentType == "SubMenu") {
            if (component.props.subMenuPosition){
                this.subMenuPosition = component.props.subMenuPosition
            }
            if (component.props.hoverShow != undefined && Util.parseBool(component.props.hoverShow)){
                this.hoverShow = Util.parseBool(component.props.hoverShow)
            }
            return (
                <li className="dropdown-submenu" {...component.props} onClick={_self.onClickItem.bind(component)}>
                    {
                        component.props.imageIcon ? 
                        <img src={component.props.imageIcon} style={{ width: '14px', height: '14px', marginRight: '10px' }}/>
                        :
                        <noscript/>
                    }
                   <a href="javascript: void (0);" className="dropdown-toggle" data-toggle="dropdown" onClick={this.onSubMenuClick.bind(this,component.props.subMenuPosition)}>
                    {!Util.parseBool(component.props.noI18n) ? this.getI18n(component.props.value) : component.props.value }
                    </a>
                    <ul className="dropdown-menu dropdown-menu-right">
                        {this.renderSubMenuBar(component)}
                    </ul>
                </li>
            );
        }
    }

    getIcon(component) {
        if (component.props.icon != null && component.props.icon != '' && component.props.icon != undefined) {
            return (<span style={{ width: '20px', display: 'inline-block' }}><span className={component.props.icon} style={{ paddingRight: "5px" }} /></span>);
        }
        return <noscript />;
    }
    getbadges(component) {
        if (component.props.badges != null && component.props.badges != '' && component.props.badges != undefined) {
            const iconClass = `${component.props.icon} badges`
            if (component.props.value) {
                return (<span style={{display: 'inline-block' }}><span className={iconClass} style={{ paddingRight: "5px" }} />
                {!Util.parseBool(component.props.noI18n) ? this.getI18n(component.props.value) : component.props.value }
                {this.renderBadges(component)}</span>);
            }
            return (<span style={{display: 'inline-block' }}><span className={iconClass} style={{ paddingRight: "5px" }} />
                    {!Util.parseBool(component.props.noI18n) ? this.getI18n(component.props.value) : component.props.value }
                    {this.renderBadges(component)}</span>);
        }
        return <noscript />;
    }

    renderBadges(component) {
        if (component.props.badges) {
            return (<span className={"badge badge-pill badge-" + component.props.badgeClass}>{component.props.badges}</span>)
        }
    }
}

/**@ignore
* MenuBar component prop types
*/
Menu.propTypes = $.extend({}, Component.propTypes, {
    id: PropTypes.string,
    style: PropTypes.object,
    className: PropTypes.string
});

/**@ignore
* Get MenuBar component default props
*/
Menu.defaultProps = $.extend({}, Component.defaultProps, {
    style: {},
    className: ''
});
