// import "./js/jquery.jside.menu.js";
require("./js/jquery.jside.menu")
import Component from "../../basic/Component";
import PropTypes from 'prop-types';
import { LocalContext } from 'rainbow-desktop-cache';
import Logo from '../../image/logo.png'
import LogoMin from '../../image/logo_min.png'

export default class SideMenu extends Component {
    constructor(props) {
        super(props);
        this.data = props.data
    }
    componentWillUpdate(nextProps, nextState){
        this.data =  nextProps.data
    }
    renderComponent() {
        // let data = this.props.data;
        let styleClass = 'menu-container ' + this.props.styleClass;
        let allId = this.props.id ? this.props.id : "rainbow_sideMenu"
        return (
            <div id={allId} class="rainbow_sideMenu">
                {/* <div class="menu-head">
                    <span class="layer">
                        <div class="col">
                            <div class="row for-pic">
                                <div class="profile-pic">
                                    <img src="image/asif-mughal.jpg" alt="Asif Mughal" />
                                </div>
                            </div>
                            <div class="row for-name">
                                <h3 title="User Name"> Asif Mughal </h3>
                                <span class="tagline"> Tagline text goes here</span>
                            </div>
                        </div>
                    </span>
                </div> */}
                <nav className={styleClass} style={this.props.style} >
                <img class="logo_rainbow" src={Logo} />
                <img class="logo_min_rainbow" src={LogoMin} />                
                    <span id="defaultSideMenu" className="rainbow Menu" onClick={this.showMenu.bind(this, allId)}></span>
                    <ul class="menu-items">
                        {this.renderItems(this, this.data)}
                    </ul>
                </nav>
            </div>
        );
    }

    renderItems(component, data) {
        if (data && data.length > 0) {
            let menuItems = [];
            data.forEach(item => {
                if (item.children && item.children.length > 0) {
                    menuItems.push(this.buildDropdownItems(item))
                } else {
                    menuItems.push(this.buildItems(item))
                }
            });
            return menuItems;
        }
    }

    evalClick(component, event) {
        if (component && component.onClick) {
            component.onClick(component);
        }
        LocalContext.put('sidemenu', component.deepPath);
        // window.location.href = window.location.origin + '/#/'+component.to;
    }

    buildItems(component) {
        let classNameDef = component && component.icon ? component.icon : "rainbow User";
        return (
            <li id={component.id} class="isControl"><li class="menu-item-li"><span className={classNameDef}></span> <a onClick={this.evalClick.bind(this, component)}>
                {component.label} </a></li></li>
        )
    }

    buildDropdownItems(component) {
        let classNameDef = component && component.icon ? component.icon : "rainbow User";
        return (
            <li id={component.id} class="has-sub isControl"> <li class="dropdown-label menu-item-li"><span class={classNameDef}></span>
                <span class="dropdown-heading">{component.label} </span><span class="glyphicon glyphicon-chevron-down super_dropdown"></span></li>
                <ul>
                    {this.renderDropdownItemsChildOne(component.children)}
                </ul>
            </li>
        )
    }

    renderDropdownItemsChildOne(children) {
        if (children && children.length > 0) {
            let menuItemChildren = [];
            children.forEach(itemChild => {
                if (itemChild.children && itemChild.children.length > 0) {
                    menuItemChildren.push(this.buildDropdownItemsChildOne(itemChild))
                } else {
                    menuItemChildren.push(this.buildItemsChild(itemChild))
                }
            });
            return menuItemChildren;
        }
    }

    buildItemsChild(itemChild) {
        return (
            <li id={itemChild.id} class="isCount"> <a onClick={this.evalClick.bind(this, itemChild)}> {itemChild.label} </a> </li>
        )
    }

    buildDropdownItemsChildOne(itemChildOne) {
        return (
            <li id={itemChildOne.id} class="has-sub-two isCount">
                <li class="dropdown-label menu-item-li-two">
                    <span class="dropdown-heading-two">{itemChildOne.label}</span>
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </li>
                <ul>
                    {this.renderDropdownItemsChildTwo(itemChildOne.children)}
                </ul>
            </li>
        )
    }

    renderDropdownItemsChildTwo(itemChildTwo) {
        if (itemChildTwo && itemChildTwo.length > 0) {
            let menuItemChildrenTwo = [];
            itemChildTwo.forEach(itemChild => {
                menuItemChildrenTwo.push(this.buildDropdownItemsChildTwo(itemChild))
            });
            return menuItemChildrenTwo;
        }
    }

    buildDropdownItemsChildTwo(itemChildTwo) {
        return (
            <li id={itemChildTwo.id}> <a onClick={this.evalClick.bind(this, itemChildTwo)}>{itemChildTwo.label} </a> </li>
        )
    }

    showMenu(menuId) {
        let meunElement = $('#' + menuId);
        if (meunElement.length > 0 && meunElement.hasClass('isClose')) {
            meunElement.removeClass('isClose');
            meunElement.children().addClass('sideMenuOpen');
            meunElement.children().removeClass('sideMenuClose');
            // $('#app').css({ 'marginLeft': '280px' });
            meunElement.find(".logo_min_rainbow").css({'display':'none'});
            meunElement.find(".logo_rainbow").css({'display':'inherit'});                                              
            // $('#divTop').removeClassremoveClass("divTop")
            // this.setState({menu: menu3});
        } else {
            meunElement.addClass('isClose');
            meunElement.children().addClass('sideMenuClose');
            meunElement.children().removeClass('sideMenuOpen');
            // $('#app').css({ 'marginLeft': '45px' });
            meunElement.find(".logo_rainbow").css({'display':'none'});  
            meunElement.find(".logo_min_rainbow").css({'display':'block'});                                  
            // $('#divTop').addClass("divTop")
            // this.setState({menu: menu1});
        }
    }


    componentDidUpdate() {
        $(".menu-container").jSideMenu({
            jSidePosition: "sideMenuOpen", //possible options position-left or position-right

            jSideSticky: true, // menubar will be fixed on top, false to set static

            jSideSkin: "default-skin", // to apply custom skin, just put its name in this string
        });
        // $('#app').css({ 'marginLeft': '280px' });
        let clickPath = LocalContext.get('sidemenu');
        if (clickPath) {
            let arrPath = clickPath.split('-');
            let oldClick = arrPath.slice(-1)[0];
            let subTwo = $('#' + oldClick).parents(".has-sub-two");
            let sub = $('#' + oldClick).parents(".has-sub");
            if (subTwo.length > 0 && subTwo.hasClass(".two_click")) {
                subTwo.find("ul").children().removeClass("two_click")
            } else {
                subTwo.find("ul").children().addClass("two_click")
            }
            if (subTwo.length > 0) {
                let ttn = subTwo.find("ul li").length;
                // let tth = subTwo.find("ul li").outerHeight();    
                let tdropdown = ttn * 44;
                let ttdrop = subTwo.find("ul");
                $(ttdrop).animate({ "height": tdropdown }, 100);
            }
            let cn = sub.find("ul li.two_click").length;
            let ch = sub.find("ul li.isCount").length;
            let opendropdown = (cn + ch) * 44
            let cpdrop = sub.children("ul");
            $(cpdrop).animate({ "height": opendropdown }, 100);

            if (subTwo.length > 0) {
                if (subTwo.find("glyphicon-chevron-down")) {
                    subTwo.find(".glyphicon").removeClass("glyphicon-chevron-down")
                    subTwo.find(".glyphicon").addClass("glyphicon-chevron-up")
                } else {
                    sub.find(".glyphicon").removeClass("glyphicon-chevron-up")
                    sub.find(".glyphicon").addClass("glyphicon-chevron-down")
                }
            }
            if (sub.length > 0) {
                let aa = sub.find(".super_dropdown.glyphicon-chevron-down")
                if (sub.find(".super_dropdown.glyphicon-chevron-down")) {
                    sub.find(".super_dropdown.glyphicon-chevron-down").removeClass("glyphicon-chevron-down")
                    sub.find(".super_dropdown").addClass("glyphicon-chevron-up")
                } else {
                    sub.find(".super_dropdown").removeClass("glyphicon-chevron-up")
                    sub.find(".super_dropdown").addClass("glyphicon-chevron-down")
                }
            }
        }
        let openSideMenu = this.props.openSideMenu == 'false' ? false : true;
        let allId = this.props.id ? this.props.id : "rainbow_sideMenu"  
        if(!openSideMenu){
            let meunElement = $('#' + allId);
            meunElement.removeClass('isClose');
            this.showMenu(allId);
        }
    }

    componentDidMount() {
        // $(".menu-container").jSideMenu({
        //     jSidePosition: "sideMenuOpen", //possible options position-left or position-right

        //     jSideSticky: true, // menubar will be fixed on top, false to set static

        //     jSideSkin: "default-skin", // to apply custom skin, just put its name in this string
        // });
        // $('#app').css({ 'marginLeft': '280px' });
        let clickPath = LocalContext.get('sidemenu');
        if (clickPath) {
            let arrPath = clickPath.split('-');
            let oldClick = arrPath.slice(-1)[0];
            let subTwo = $('#' + oldClick).parents(".has-sub-two");
            let sub = $('#' + oldClick).parents(".has-sub");
            if (subTwo.length > 0 && subTwo.hasClass(".two_click")) {
                subTwo.find("ul").children().removeClass("two_click")
            } else {
                subTwo.find("ul").children().addClass("two_click")
            }
            if (subTwo.length > 0) {
                let ttn = subTwo.find("ul li").length;
                // let tth = subTwo.find("ul li").outerHeight();    
                let tdropdown = ttn * 44;
                let ttdrop = subTwo.find("ul");
                $(ttdrop).animate({ "height": tdropdown }, 100);
            }
            let cn = sub.find("ul li.two_click").length;
            let ch = sub.find("ul li.isCount").length;
            let opendropdown = (cn + ch) * 44
            let cpdrop = sub.children("ul");
            $(cpdrop).animate({ "height": opendropdown }, 100);

            if (subTwo.length > 0) {
                if (subTwo.find("glyphicon-chevron-down")) {
                    subTwo.find(".glyphicon").removeClass("glyphicon-chevron-down")
                    subTwo.find(".glyphicon").addClass("glyphicon-chevron-up")
                } else {
                    sub.find(".glyphicon").removeClass("glyphicon-chevron-up")
                    sub.find(".glyphicon").addClass("glyphicon-chevron-down")
                }
            }
            if (sub.length > 0) {
                let aa = sub.find(".super_dropdown.glyphicon-chevron-down")
                if (sub.find(".super_dropdown.glyphicon-chevron-down")) {
                    sub.find(".super_dropdown.glyphicon-chevron-down").removeClass("glyphicon-chevron-down")
                    sub.find(".super_dropdown").addClass("glyphicon-chevron-up")
                } else {
                    sub.find(".super_dropdown").removeClass("glyphicon-chevron-up")
                    sub.find(".super_dropdown").addClass("glyphicon-chevron-down")
                }
            }
        }
        let openSideMenu = this.props.openSideMenu == 'false' ? false : true;
        let allId = this.props.id ? this.props.id : "rainbow_sideMenu"  
        if(!openSideMenu){
            let meunElement = $('#' + allId);
            meunElement.removeClass('isClose');
            this.showMenu(allId);
        }
    }
};