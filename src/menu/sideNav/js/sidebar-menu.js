$.sidebarMenu = function(menu) {
  let animationSpeed = 300;
  $(menu).on('click', 'li a', function(e) {
    let $this = $(this);
    let checkElement = $this.next();

    // 1. If the menu is visible
    if (checkElement.is('.treeview-menu') && checkElement.is(':visible')) {
      checkElement.slideUp(animationSpeed, function() {
        checkElement.removeClass('menu-open');
      });
      checkElement.parent("li").removeClass("active");
    }

    // 2. If the menu is not visible
    else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
      //Get the parent menu
      let parent = $this.parents('ul').first();
      //Close all open menus within the parent
      let ul = parent.find('ul:visible').slideUp(animationSpeed);
      //Remove the menu-open class from the parent
      ul.removeClass('menu-open');
      //Get the parent li
      let parent_li = $this.parent("li");
      //Open the target menu and add the menu-open class
      checkElement.slideDown(animationSpeed, function() {
        //Add the class active to the parent li
        checkElement.addClass('menu-open');
        parent.find('li.active').removeClass('active');
        parent_li.addClass('active');
      });
    }

    // // 3. 没有.treeview-menu(没有子)的单个菜单也需要active
    // else if ($this.is('.noChild')){ 
    //   var parent = $this.parents("li").parents("ul").first();
    //   var parent_li = $this.parent("li");
    //     $this.slideDown(animationSpeed, function() {
    //       //Add the class active to the parent li
    //       parent.find('li.active').removeClass('active');
    //       parent_li.addClass('active');
    //   })
    // }

    // 1.二级菜单高亮
    if ($this.parents('.lastChild') && $this.parents('.lastChild').first()){
      let parent_li = $this.parent(".lastChild");
      $this.slideDown(animationSpeed, function() {
        //Add the class active to the parent li
        // parent.find('li.active').removeClass('active');
        $this.parents('.sidebar-menu').find(".childActive").removeClass('childActive');
        parent_li.addClass('childActive');
      })
    }

    if ($this.is('.noChild')) {
      let parent = $this.parents('ul').first();
      parent.find('li.active').removeClass('active');
      let parent_li = $this.parent("li");
      parent_li.addClass('active');
    }

    //if this isn't a link, prevent the page from being redirected
    if (checkElement.is('.treeview-menu') || $this.is('.noChild')) {
      e.preventDefault();
    }
  });
}
