require("./js/sidebar-menu")
import Component from "../../basic/Component";
import PropTypes from 'prop-types';
import { PageContext, SessionContext } from 'rainbow-desktop-cache';
import { Util } from "rainbow-desktop-tools";
import Logo from '../../image/logo.png'
import LogoMin from '../../image/logo_min.png'
// import r18n from '../../i18n/reactjs-tag.i18n';
import { _ } from "core-js";


export default class SideNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allData: JSON.parse(JSON.stringify(props.data)), //全部的data
            data: [], //会变化的data
            searchValue: ''
        }
    }
    componentWillUpdate(nextProps, nextState) {
        if (!this.state.allData || this.state.allData && this.state.allData.length == 0) {
            this.state.allData = JSON.parse(JSON.stringify(nextProps.data))
            this.addI18nToData()
        }
        this.addDeepPath(this.state.allData, null);
        if (this.state.searchValue && this.state.searchValue.length > 0) {
            this.commonSearchData(this.state.searchValue, true)
        } else {
            this.state.data = this.state.allData
        }

    }
    componentWillMount() {
        this.addI18nToData()
        if (_.isEmpty(this.state.data)) {
            this.setState({ data: this.state.allData })
        }
        this.addDeepPath(this.state.allData, null);
    }
    renderComponent() {
        // let data = this.props.data;
        let styleClass = 'menu-container ' + this.props.styleClass;
        let allId = this.props.id ? this.props.id : "rainbow_sideNav"
        let LogoSrc = this.props.logo ? this.props.logo : Logo;
        let LogoMinSrc = this.props.logoMin ? this.props.logoMin : LogoMin;
        let skin = this.props.skin || 'default'; // sessionStorage.getItem('skin') || 'default';
        if (this.props.logoUrl) {
            LogoSrc = this.props.logoUrl;

        }
        if (this.props.miniLogUrl) {
            LogoMinSrc = this.props.miniLogUrl
        }
        return (
            <div id={allId} class="rainbow_sideNav sideNavMenuOpen">
                <div className={"adminlogoblock adminlogo" + skin + "_area"}>
                    <img class="logo_rainbow" src={LogoSrc} onClick={this.logoClick.bind(this)} onError={this.logoError.bind(this)}/>
                    <img class="logo_min_rainbow" onClick={this.logoClick.bind(this)} src={LogoMinSrc} onError={this.logoMiniError.bind(this)}/>
                </div>
                {
                    Util.parseBool(this.props.searchable) ?
                        <div className="menu_search menufixedSearch">
                            <span className='rainbow Search'></span>
                            <input type="text" onChange={this.onSearchData.bind(this)} id='menu_search_box' autocomplete="off" />
                        </div>
                        :
                        null
                }
                <ul class="sidebar-menu fixedSideBarmenu">
                    {this.renderItems(this.state.data)}
                </ul>

            </div>
        );
    }

    logoError(){
        if(this.props.logoError){
            this.props.logoError();
        }
    }

    logoMiniError(){
        if(this.props.logoMiniError){
            this.props.logoMiniError();
        }
    }

    logoClick() {
        if (this.props.logoClick) {
            this.props.logoClick()
        }
        if(Util.parseBool(this.props.logResetMenu)){
            this.resetMenu();
        }
    }
    onSearchData(event) {
        this.setState({ searchValue: event.target.value });
        this.commonSearchData(event.target.value, false)

    }
    addI18nToData() {
        if (!_.isEmpty(this.state.allData)) {
            this.addI18nToDataCircle(this.state.allData);
        }
    }
    addI18nToDataCircle(data) {
        for (let item of data) {
            if (item.Value) {
                item['i18nValue'] = i18n[item.Value];
            }
            if (item.Children && item.Children.length > 0) {
                this.addI18nToDataCircle(item.Children);
            }
        }
    }

    commonSearchData(value, isComponentupdate) {
        if (value.length > 0) {
            let data = JSON.parse(JSON.stringify(this.state.allData)) // menu的总数据
            let searchInfo = value.toLowerCase().replace(/\s+/g, ""); //填入的字段
            let SearchData = []
            for (let i = 0; i < data.length; i++) {
                let code = data[i].i18nValue ? data[i].i18nValue.toLowerCase().replace(/\s+/g, "") : data[i].i18nKey.toLowerCase().replace(/\s+/g, ""); // menu的第一层数据
                if (code.includes(searchInfo)) {
                    SearchData.push(data[i])
                } else {
                    if (data[i].Children) {
                        let secondLevel = 1
                        for (let x = 0; x < data[i].Children.length; x++) {
                            let name = data[i].Children[x].i18nValue ? data[i].Children[x].i18nValue.toLowerCase().replace(/\s+/g, "") : data[i].Children[x].i18nKey.toLowerCase().replace(/\s+/g, ""); // menu的第二层数据
                            if (name.includes(searchInfo)) {
                                if (secondLevel == 1) {
                                    secondLevel = 2
                                    SearchData.push(data[i])
                                }
                            } else {
                                if (data[i].Children[x].Children) {
                                    let thirdLevel = 1
                                    for (let y = 0; y < data[i].Children[x].Children.length; y++) {
                                        let name = data[i].Children[x].Children[y].i18nValue ? data[i].Children[x].Children[y].i18nValue.toLowerCase().replace(/\s+/g, "") : data[i].Children[x].Children[y].i18nKey.toLowerCase().replace(/\s+/g, ""); // menu的第三层数据
                                        if (name.includes(searchInfo)) {
                                            if (thirdLevel == 1 && secondLevel == 1) {
                                                thirdLevel = 2
                                                SearchData.push(data[i])
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (isComponentupdate) {
                this.state.data = SearchData
            } else {
                this.setState({ data: SearchData })
            }
        } else {
            if (isComponentupdate) {
                this.state.data = this.state.allData
            } else {
                this.setState({ data: this.state.allData })
            }
        }
    }
    renderItems(data) {
        if (data && data.length > 0) {
            let menuItems = [];
            data.forEach(item => {
                // const value = this.props.noI18n?item.Value:i18n[item.Value];
                if (item.Children && item.Children.length > 0) {
                    menuItems.push(this.buildDropdownItems(item))
                } else {
                    const value = this.props.noI18n ? item.Value : i18n[item.Value];
                    // menuItems.push(this.buildCommonItems(item))
                    menuItems.push(
                        <li id={item.Id} class="treeview">
                            <a onClick={this.evalClick.bind(this, item)} class="noChild">
                                <span className={item.Icon + " treeIcon"}></span> <span className="treeValue">{value}</span>
                            </a>
                        </li>
                    )
                }
            });
            return menuItems;
        }
    }

    hoverNode(id) {
        if ($('.sideNavMenuOpen') && $('.sideNavMenuOpen').length > 0) {
            return;
        }
        // let menuContainerHeight = $('#cloud_sideMenu')[0].scrollHeight;
        let lastMenuHeight = parseInt($('#float' + id).height());

        let bodyHeight = document.body.clientHeight;

        // if (lastMenuHeight + curPosition > bodyHeight ) {
        //     curPosition = curPosition - curPosition / 2;
        // }

        let Obj = document.querySelector('[id="' + id + '"]').getBoundingClientRect();
        let curPosition2 = Obj.top;
        // console.log('hoverNode', id, 'lastMenuHeight:' + lastMenuHeight, 'bodyHeight:' + bodyHeight, 'menuContainerHeight:' + menuContainerHeight, 'top: ' + top, 'scrollH: ' + scrollH, 

        if (curPosition2 + lastMenuHeight > bodyHeight) {
            curPosition2 = bodyHeight - lastMenuHeight;
        }

        $(`#float${id}`).show();
        $(`#float${id}`).css({ top: curPosition2 + 'px' })
    }

    hoverNodeFloat(id) {
        if ($('.sideNavMenuOpen') && $('.sideNavMenuOpen').length > 0) {
            return;
        }
        if ($('#first' + id)) {
            let obj = document.querySelector('#first' + id).getBoundingClientRect();
            // let top = $('#first' + id).parent('ul').parent('div').css('top');
            let left = $('#first' + id).parent('ul').parent('div').css('left').replace('px', '');
            let thridHeight = $('#float-third' + id).height();
            let bodyHeight = document.body.clientHeight;
            $(`#float-third${id}`).show();
            let top = obj.top;
            if (obj.top + thridHeight > bodyHeight) {
                top = bodyHeight - thridHeight;
            }
            $(`#float-third${id}`).css({ top: top, left: parseInt(left) + 250 + 'px' });
            // console.log('third', id, top, '  event: ' +obj.top)
        }

    }

    leavelNode(id) {
        $(`#float${id}`).hide();
    }

    leavelNodeFloat(id) {
        $(`#float-third${id}`).hide();
    }

    buildDropdownItems(component) {
        let classNameDef = component && component.Icon ? component.Icon : "";
        const value = this.props.noI18n ? component.Value : i18n[component.Value] ? i18n[component.Value] : component.Value;
        return (
            <li id={component.Id} className={component.TextIndent ? "treeview TextIndent" : "treeview"} onMouseEnter={this.hoverNode.bind(this, component.Id)} onMouseLeave={this.leavelNode.bind(this, component.Id)}>
                <a onClick={this.evalClick.bind(this, component)}>
                    <div class="treeview-group">
                        <span>
                            <span className={classNameDef + " treeIcon"}></span> <span className="treeValue">{value}</span>
                        </span>
                        <span class="rainbow SingArrowDown"></span>
                    </div>
                </a>
                <ul class="treeview-menu" parentid={component.Id}>

                    {this.renderDropdownItems(component.Children)}

                </ul>
                <div class="hoverNodeContainer" id={'float' + component.Id}>
                    <ul>
                        {this.renderDropdownItemsFloat(component.Children)}
                    </ul>
                </div>
            </li>
        )
    }

    buildDropdownItemsFloat(component) {
        let classNameDef = component && component.Icon ? component.Icon : "";
        const value = this.props.noI18n ? component.Value : i18n[component.Value] ? i18n[component.Value] : component.Value;
        return (
            <li id={'first' + component.Id} className={component.TextIndent ? "treeview TextIndent" : "treeview"} onMouseEnter={this.hoverNodeFloat.bind(this, component.Id)} onMouseLeave={this.leavelNodeFloat.bind(this, component.Id)}>
                <a onClick={this.evalClick.bind(this, component)}>
                    <div class="treeview-group">
                        <span>
                            <span className={classNameDef + " treeIcon"}></span> <span className="treeValue">{value}</span>
                        </span>
                        <span class="rainbow SingArrowDown"></span>
                    </div>
                </a>
                <ul class="treeview-menu" parentid={'first' + component.Id}>

                    {this.renderDropdownItemsFloat(component.Children)}

                </ul>
                <div class="hoverNodeContainer third" id={'float-third' + component.Id}>
                    <ul>
                        {this.renderDropdownItemsFloat(component.Children)}
                    </ul>
                </div>
            </li>
        )
    }

    buildItemsChild(component) {
        const value = this.props.noI18n ? component.Value : i18n[component.Value] ? i18n[component.Value] : component.Value;
        return (
            <li id={component.Id} class="treeview lastChild">
                <a onClick={this.evalClick.bind(this, component)}>
                    <span>{value}</span>
                </a>
            </li>
        )
    }

    buildItemsChildFloat(component) {
        const value = this.props.noI18n ? component.Value : i18n[component.Value] ? i18n[component.Value] : component.Value;
        return (
            <li id={'float' + component.Id} class="treeview lastChild">
                <a onClick={this.evalClick.bind(this, component)}>
                    <span>{value}</span>
                </a>
            </li>
        )
    }

    renderDropdownItems(Children) {
        if (Children && Children.length > 0) {
            let menuItemChildren = [];
            Children.forEach(itemChild => {
                if (itemChild.Children && itemChild.Children.length > 0) {
                    menuItemChildren.push(this.buildDropdownItems(itemChild))
                } else {
                    menuItemChildren.push(this.buildItemsChild(itemChild))
                }
            });
            return menuItemChildren;
        }
    }

    renderDropdownItemsFloat(Children) {
        if (Children && Children.length > 0) {
            let menuItemChildren = [];
            Children.forEach(itemChild => {
                if (itemChild.Children && itemChild.Children.length > 0) {
                    menuItemChildren.push(this.buildDropdownItemsFloat(itemChild))
                } else {
                    menuItemChildren.push(this.buildItemsChildFloat(itemChild))
                }
            });
            return menuItemChildren;
        }
    }

    evalClick(component, event) {
        if (component && component.onClick) {
            component.onClick(component);
        }
        let allId = this.props.id ? this.props.id : "rainbow_sideNav"
        // let id = "";
        // if (component.deepPath.slice(3,4) != '-'){
        //     id = component.deepPath.slice(0,4);
        // } else {
        //     id = component.deepPath.slice(0,3);
        // }

        // let id2 = component.deepPath.slice(0,1);
        // setTimeout(() => {
        //     if ($('#' + id).hasClass('active')){
        //         LocalContext.put('sidemenu', id);
        //     } else if ($('#' + id2).hasClass('active')){
        //         LocalContext.put('sidemenu', id2);
        //     } else {
        //         LocalContext.remove('sidemenu');
        //     }
        // }, 500);
        PageContext.put('deepPath', component.DeepPath);
        let idList = component.DeepPath.split("-");
        let resultList = [];
        // setTimeout(() => {
            idList.forEach(item => {
                if ($('#' + item).hasClass('active')) {
                    resultList.push(item);
                }
            });
            PageContext.put('idList', resultList);
        // }, 500);
        PageContext.put('height', $(document.getElementById(allId)).scrollTop());

        // if (component.EntranceUrl) {
        //     LocalContext.put('iframeSrc', component.EntranceUrl);
        // }
        if (this.props.nodeClick) {
            this.props.nodeClick(component);
        }

        if (component.Children == undefined || component.Children.length == 0) {
            if (event && event.target && $(event.target).parents('ul') && $(event.target).parents('ul').parent('li') && $(event.target).parents('ul').parent('li').length == 0) {
                let openObj = $('ul.menu-open');
                if (!openObj.hasClass('sidebar-menu')) {
                    openObj.hide();
                    openObj.removeClass('menu-open');
                }
            }
        }
    }

    // buildCommonItems(component) {
    //     return (
    //         <li id={component.Id} class="treeview" style="color: #fff">
    //             <a onClick={this.evalClick.bind(this, component)}>
    //                 <span>{component.Value}</span>
    //             </a>
    //         </li>
    //     )
    // }

    addDeepPath(data, parentDeepPath) {
        data.forEach(item => {
            let parentPath = parentDeepPath ? parentDeepPath : "";
            if (parentPath) {
                item.DeepPath = parentPath + "-" + item.Id;
            } else {
                item.DeepPath = item.Id + "";
            }

            if (item.Children) {
                this.addDeepPath(item.Children, item.DeepPath);
            }
        });
    }

    static menuSwitch(menuId) {
        let meunElement = $('#' + menuId);
        if (meunElement && meunElement.hasClass("sideNavMenuOpen")) {
            meunElement.addClass('sideNavMenuClose');
            meunElement.removeClass('sideNavMenuOpen');
            meunElement.find(".logo_rainbow").css({ 'display': 'none' });
            meunElement.find(".logo_min_rainbow").css({ 'display': 'block' });
        } else if (meunElement && meunElement.hasClass("sideNavMenuClose")) {
            meunElement.addClass('sideNavMenuOpen');
            meunElement.removeClass('sideNavMenuClose');
            meunElement.find(".logo_min_rainbow").css({ 'display': 'none' });
            meunElement.find(".logo_rainbow").css({ 'display': 'inherit' });
        }
    }

    resetMenu(){
        let sideMenu = $('.sidebar-menu');
        sideMenu.find('.active').removeClass('active');
        var ul = sideMenu.find('ul:visible').slideUp(300);
        ul.removeClass('menu-open');
        ul.find(".childActive").removeClass('childActive');
    }

    componentDidUpdate() {
        let height = PageContext.get('height');
        let allId = this.props.id ? this.props.id : "rainbow_sideNav";
        let idList = PageContext.get('idList');
        if (idList) {
            idList.forEach(item => {
                $('#' + item).addClass('active');
            });
        }
        let ele = document.getElementById(allId);
        ele.scrollTop = height;
        if (SessionContext.get('curMenuId')) {
            let menuID = SessionContext.get('curMenuId');
            let menu = $('#' + menuID);
            let ul = menu.parent('ul');
            if (ul && ul.length > 0) {
                let arrLi = $(ul[0]).children('li');
                this.removeCusClass(arrLi, 'childActive')
            }
            menu.addClass('childActive');
            if (menu.parents('.treeview-menu')&&menu.parents('.treeview-menu').length>0) {             
                menu.parents('.treeview-menu').addClass('menu-open');
                menu.parents('.treeview-menu').css({ 'display': 'block' })                
            }
        }


    }

    removeCusClass(arr, classname) {
        if (arr && arr.length > 0) {
            for (let i = 0; i < arr.length; i++) {
                let item = arr[i];
                if ($(item).hasClass(classname)) {
                    $(item).removeClass(classname);
                }
            }
        }
    }

    componentDidMount() {
        $.sidebarMenu($('.sidebar-menu'))
        let height = PageContext.get('height');
        let allId = this.props.id ? this.props.id : "rainbow_sideNav";
        let idList = PageContext.get('idList');
        if (idList) {
            idList.forEach(item => {
                $('#' + item).addClass('active');
            });
        }

        let ele = document.getElementById(allId);
        ele.scrollTop = height;
        if (SessionContext.get('curMenuId')) {
            let menuID = SessionContext.get('curMenuId');
            $('#' + menuID).addClass('childActive');
            $($('#' + menuID).parent('ul')[0]).addClass('menu-open');
            $($('#' + menuID).parent('ul')[0]).css({ 'display': 'block' })
        }
        $('.hoverNodeContainer').insertAfter($('.sidebar-menu'));
        if(!Util.parseBool(this.props.isMenuOpen)){
            let meunElement = $('#' + allId);
            if (meunElement && meunElement.hasClass("sideNavMenuOpen")) {
                meunElement.addClass('sideNavMenuClose');
                meunElement.removeClass('sideNavMenuOpen');
                meunElement.find(".logo_rainbow").css({ 'display': 'none' });
                meunElement.find(".logo_min_rainbow").css({ 'display': 'block' });
            }
        }
    }
};

/**@ignore
    * SideNav component prop types
    */
SideNav.propTypes = $.extend({}, Component.propTypes, {
    logo: PropTypes.string,
    logoMin: PropTypes.string,
    isMenuOpen: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    logResetMenu: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
});

/**@ignore
* Get SideNav component default props
*/
SideNav.defaultProps = $.extend({}, Component.defaultProps, {
    logo: "",
    logoMin: "",
    isMenuOpen: true,
    logResetMenu: true
});
