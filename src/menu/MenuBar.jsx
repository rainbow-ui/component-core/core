﻿import Component from "../basic/Component";
import Param from "../basic/Param";
import OnClickEvent from '../event/OnClickEvent';
import {ELUtil} from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';

export default class MenuBar extends Component {
    renderUser() {
        return (
            <div className="navbar-form navbar-right">
                <div className="bar_right">
                    <div className="float_left usericon">
                        <i className="fa fa-user"></i>
                    </div>
                    <div className="float_left userinfo">
                        {this.props.userName}
                    </div>
                    <div className="float_right logouticon">
                        <a href="javascript: void(0);" onClick={this.logout.bind(this)}>
                            <i className="fa fa-power-off"></i>
                        </a>
                    </div>
                </div>
            </div>
        );
    }

    renderBusinessUser() {
        return (
            <ul>
                <li className="dropdown" style={{float:'right',display:'block',margin:'16px'}}>
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                        <span className="fa fa-gear" style={{paddingRight: "5px"}}/><b className="caret"/>
                    </a>
                    {this.props.businessValue}
                </li>
            </ul>
        );
    }

    renderComponent() {
        let backgroundWeight = this.props.backgroundWeight ? 'navbar-' + this.props.backgroundWeight : null;
        let styleClass = this.props.styleClass ? 'bg-' + this.props.styleClass : null;
        let fixtop = ELUtil.parseBoolValue(this.props.fixtop) ? 'fixed-top': null;

        return (
            <nav className={"navbar navbar-toggleable-sm " + backgroundWeight + " " + styleClass + " " + fixtop}
                style={this.props.style}>
                {
                    this.props.logo ?
                        <a className="navbar-brand" href="javascript:void (0);" onClick={this.clickLogoHandler.bind(this)}>
                            {this.props.logo()}
                        </a> :
                        null
                }
                {
                    this.props.headerTitle ?
                        <a className="headerTitle-navbar-brand" href={this.props.headerTitleTarget} onClick={this.clickHeaderTitleHandler.bind(this)}>
                            {this.props.headerTitle}
                        </a> :
                        null
                } 
                 {/*{
                    this.props.subHeaderTitle ?
                        <a className="subHeaderTitle-separate">
                            |
                        </a> :
                        null
                }*/}
                 {
                    this.props.subHeaderTitle ?
                        <div href={this.props.subHeaderTitleTarget} onClick={this.clickSubHeaderTitleHandler.bind(this)}>
                            {this.props.subHeaderTitle()}
                        </div> :
                        null
                }
                <div className="navbar-collapse" id={this.componentId}>
                    {this.props.children}
                </div>
                {
                    this.props.renderRight ?
                        this.props.renderRight() :
                        null
                }
                {ELUtil.parseBoolValue(this.props.showBusinessUser) ? this.renderBusinessUser() : null}
                {ELUtil.parseBoolValue(this.props.showUser) ? this.renderUser() : null}
                {
                    this.props.toplogo ?
                    <div className="topcardrightlogo">
                        <img src={this.props.toplogo} style={{ width: '50px', height: '30px' }}></img>
                    </div>
                    : ''}
            </nav>
        );
    }

    // componentDidMount() {
    //     $('#app').css({'marginTop': '75px'});
    // }

    clickLogoHandler(event) {
        if (this.props.onLogoClick != undefined) {
            this.props.onLogoClick(new OnClickEvent(this, event, Param.getParameter(this)));
        }
    }

    clickHeaderTitleHandler(event) {
        if (this.props.onHeaderTitleClick != undefined) {
            this.props.onHeaderTitleClick(new OnClickEvent(this, event, Param.getParameter(this)));
        }
    }
    clickSubHeaderTitleHandler(event) {
        if (this.props.onSubHeaderTitleClick != undefined) {
            this.props.onSubHeaderTitleClick(new OnClickEvent(this, event, Param.getParameter(this)));
        }
    }

    logout(event) {
        if (this.props.logout != undefined) {
            this.props.logout(new OnClickEvent(this, event, Param.getParameter(this)));
        }
    }

};


/**@ignore
 * MenuBar component prop types
 */
MenuBar.propTypes = $.extend({}, Component.propTypes, {
    id: PropTypes.string,
    style: PropTypes.object,
    headerTitle: PropTypes.string,
    subHeaderTitle: PropTypes.func,
    styleClass: PropTypes.string,
    logo: PropTypes.func,
    fixtop: PropTypes.string,
    businessValue: PropTypes.string,
    backgroundWeight: PropTypes.string,
    showUser: PropTypes.string,
    showBusinessUser: PropTypes.string,
    userName: PropTypes.string,
    userRole: PropTypes.string,
    renderRight: PropTypes.func,
    logout: PropTypes.func,
    onLogoClick: PropTypes.func,
    onHeaderTitleClick: PropTypes.func,
    onSubHeaderTitleClick: PropTypes.func,
    headerTitleTarget: PropTypes.string,
    subHeaderTitleTarget: PropTypes.string,
});

/**@ignore
 * Get MenuBar component default props
 */
MenuBar.defaultProps = $.extend({}, Component.defaultProps, {
    style: {},
    fixtop: 'true',
    showUser: "false",
    userName: "User",
    userRole: "Administrator"
});
