import Component from "../basic/Component";
import PropTypes from 'prop-types';

export default class CustomMenu extends Component {
    renderComponent() {
        return (
            <div dangerouslySetInnerHTML={{__html: this.props.htmlStr}}>
            </div>
        )
    }
};

CustomMenu.propTypes = $.extend({}, Component.propTypes, {
    htmlStr: PropTypes.string
});

CustomMenu.defaultProps = $.extend({}, Component.defaultProps, {
    htmlStr: ''
})