import Component from "../basic/Component";
import PropTypes from 'prop-types';

export default class SubMenu extends Component {
	
	renderComponent(){
		return <noscript/>;
	}
	
};


/**@ignore
 * SubMenu component prop types
 */
SubMenu.propTypes = $.extend({}, Component.propTypes, {
	id: PropTypes.string,
	value: PropTypes.string,
	icon: PropTypes.string,
	visibled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
	badges: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
	badgeClass: PropTypes.string,
	subMenuPosition: PropTypes.string,
	hoverShow: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
});

/**@ignore
 * Get subMenu component default props
 */
SubMenu.defaultProps = $.extend({}, Component.defaultProps,{
	componentType: "SubMenu",
	visibled: true,
	badgeClass:'success',
	subMenuPosition: 'left',
	hoverShow: false
});
