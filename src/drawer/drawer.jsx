import Component from '../basic/Component';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import PubSub from 'pubsub-js'
import UniqueId from '../basic/UniqueId';
// import { Util } from 'rainbow-desktop-tools';

export default class Drawer extends Component {
  constructor(props) {
    super(props);
    this.openDrawer = this.openDrawer.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
    this.onAnimationEnded = this.onAnimationEnded.bind(this);
    this.state = {
      open: this.props.open,
      hiddenOverlay: true,
      hiddenDrawer: true
    }
  }

  componentWillMount() {
    if(this.props&&this.props.id){
      PubSub.subscribe(this.props.id, (msg,data)=> {
        if(data&&data=="open"){
          this.openDrawer()
        }else if(data&&data=="close"){
          this.closeDrawer()
        }
      })
    }
  }

  /**@ignore
   * Open drawer
   */
  static open(drawerId) {
    PubSub.publish(drawerId,"open")
  }

  /**@ignore
   * Close drawer
   */
  static close(drawerId) {
    PubSub.publish(drawerId,"close")   
  }

  onAnimationEnded() {
    if (!this.state.open) {
      this.setState({
        hiddenOverlay: true,
        hiddenDrawer: true
      });
    }
  }


  closeDrawer() {
    this.setState({
      open: false
    },()=>{
      window.parent.postMessage({type:'hidemask'}, '*');
      if (this.props.onClose) {
        this.props.onClose();
      }
    });
  }
  openDrawer() {
    this.setState({
      hiddenOverlay: false,
      hiddenDrawer: false,
      open: true
    },()=>{
      window.parent.postMessage({type:'showmask'}, '*');
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.open != this.state.open) {
      nextProps.open ? this.openDrawer(): this.closeDrawer();
    }
  }

  componentDidMount () {
    this.drawer.addEventListener('webkitAnimationEnd', this.onAnimationEnded);
  }

  componentWillUnmount() {
    this.drawer.removeEventListener('webkitAnimationEnd', this.onAnimationEnded);
  }

  getOverlayClassName() {
    return classNames(
      'react-drawer-overlay',
      'overlay',
      'animated',
      {
        'fadeIn': this.state.open,
        'fadeOut': !this.state.open,
        'hidden': this.state.hiddenOverlay
      }
    );
  }

  getDrawerClassName() {
    const position = this.props.position || 'right';
    const themeAttr = `drawer-${position}`;
    let direction, start;
    if (this.state.open) {
      direction = 'In';
      switch(position) {
      case 'top':
        start = 'Down'; break;
      case 'bottom':
        start = 'Up'; break;
      case 'left':
        start = 'Left'; break;
      case 'right':
        start = 'Right'; break;
      }
    } else {
      direction = 'Out';
      switch(position) {
      case 'top':
        start = 'Up'; break;
      case 'bottom':
        start = 'Down'; break;
      case 'left':
        start = 'Left'; break;
      case 'right':
        start = 'Right'; break;
      }
    }
    const fade = 'fade' + direction + start;
    return classNames(
      'react-drawer-drawer',
      'drawer',
      'drawer-content',
      themeAttr,
      'animated',
      fade,
      {
        'hidden': this.state.hiddenDrawer
      }
    );
  }
  render() {
    let styleClass = this.props.styleClass?this.props.styleClass: "";
    const overlayClass = this.getOverlayClassName();
    const drawerClass = this.getDrawerClassName() + " " + styleClass;

    return (
      <div id={this.props&&this.props.id?this.props.id:UniqueId.generateId()}>
        {!this.props.noOverlay ? 
          <div ref={(c) => this.overlay = c} className={overlayClass} onClick={this.closeDrawer}>
          </div>: 
          null
        }
        <div className={drawerClass} ref={(c) => this.drawer = c} style={{width: this.props.width}}>
          {this.renderDrawerHeader()}
          {this.props.children}
        </div>
      </div>
    );
  }

  renderDrawerHeader() {
    return(
      <div className="drawer-header">
        {
          this.props.closeable?
          <button type="button" className="close" onClick={this.closeDrawer}>&times;</button>:
          null
        }
        <h4 className="drawer-title" style={{display: this.props.title?"block": "none"}}>{this.props.title ? this.getI18n(this.props.title) : ""}</h4>
      </div> 
    );
  }

}

Drawer.propTypes = {
  width: PropTypes.string,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  position: PropTypes.oneOf(['top', 'bottom', 'right', 'left']),
  noOverlay: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  styleClass: PropTypes.string,
  closeable: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};

Drawer.defaultProps = {
  position: "right",
  width: "auto",
  noOverlay: false,
  closeable: true
}
