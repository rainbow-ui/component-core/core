import config from "config";
import { Util } from 'rainbow-desktop-tools';

module.exports = {

	// Message Type
	SUCCESS:	"success",
	INFO: 		"info",
	WARNING: 	"warning",
	ERROR: 		"error",

	// Message Position
	POSITION_TOP_LEFT: 					"toast-top-left",
	POSITION_TOP_CENTER: 				"toast-top-center",
	POSITION_TOP_RIGHT: 				"toast-top-right",
	POSITION_TOP_FULL_WIDTH:		"toast-top-full-width",

	POSITION_BOTTOM_LEFT: 			"toash-bottom-left",
	POSITION_BOTTOM_CENTER: 		"toast-bottom-center",
	POSITION_BOTTOM_RIGHT: 			"toast-botton-right",
	POSITION_BOTTOM_FULL_WIDTH:	"toast-bottom-full-width",

	// Default message position
	//DEFAULT_POSITION: 					"toast-top-right",

	getMessageOption: function(position, timeOut, extendedTimeOut, progressBar){
		if (undefined == timeOut || null == timeOut || '' == timeOut) {
			timeOut = 5000;
		}
		if (undefined == extendedTimeOut || null == extendedTimeOut || '' == extendedTimeOut) {
			extendedTimeOut = 1000;
		}
		if (undefined == progressBar || null == progressBar) {
			progressBar = true;
		}
		return {
			closeButton: true,//设置显示"X" 关闭按钮
			debug: false,//是否为调试
			progressBar: progressBar,//设置显示timeout时间进度条
			positionClass: position,//设置toastr显示位置的class
			onclick: null,
			showDuration: "300", //显示动作（从无到有这个动作）持续的时间
			hideDuration: "1000", //隐藏动作持续的时间
			timeOut: timeOut, //设置toastr过多久关闭
			extendedTimeOut: extendedTimeOut,//设置当你鼠标滑入后的timeout，该timeout会更新关闭所需的timeout
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
			preventDuplicates:true
		};
  },

    clear: function(){
		toastr.clear();
	},

	// success -> green
	success: function(message, title, position, timeOut, extendedTimeOut, progressBar){
		//toastr.success(title, message);
		if(!timeOut){
			timeOut = config.DEFAULT_MESSAGE_POSITION&&config.DEFAULT_MESSAGE_POSITION.SUCCESS_TIME_OUT?config.DEFAULT_MESSAGE_POSITION.SUCCESS_TIME_OUT:5000;
		}
		if(!extendedTimeOut){
			extendedTimeOut = config.DEFAULT_MESSAGE_POSITION&&config.DEFAULT_MESSAGE_POSITION.SUCCESS_EXTENDED_TIME_OUT?config.DEFAULT_MESSAGE_POSITION.SUCCESS_EXTENDED_TIME_OUT:1000;
		}
		toastr.options = this.getMessageOption(this.handlerPosition(this.SUCCESS, position), timeOut, extendedTimeOut, progressBar);
		let $toast = toastr[this.SUCCESS](this.handleMessage(message), title);
	},

	// info -> blue
	info: function(message, title, position, timeOut, extendedTimeOut, progressBar){
		//toastr.info(title, message);
		if(!timeOut){
			timeOut = config.DEFAULT_MESSAGE_POSITION&&config.DEFAULT_MESSAGE_POSITION.INFO_TIME_OUT?config.DEFAULT_MESSAGE_POSITION.INFO_TIME_OUT:5000;
		}
		if(!extendedTimeOut){
			extendedTimeOut = config.DEFAULT_MESSAGE_POSITION&&config.DEFAULT_MESSAGE_POSITION.INFO_EXTENDED_TIME_OUT?config.DEFAULT_MESSAGE_POSITION.INFO_EXTENDED_TIME_OUT:1000;
		}
		toastr.options = this.getMessageOption(this.handlerPosition(this.INFO, position), timeOut, extendedTimeOut, progressBar);
		let $toast = toastr[this.INFO](this.handleMessage(message), title);
	},

	// warning -> orange
	warning: function(message, title, position, timeOut, extendedTimeOut, progressBar){
		//toastr.warning(title, message);
		if(!timeOut){
			timeOut = config.DEFAULT_MESSAGE_POSITION&&config.DEFAULT_MESSAGE_POSITION.WARNING_TIME_OUT?config.DEFAULT_MESSAGE_POSITION.WARNING_TIME_OUT:5000;
		}
		if(!extendedTimeOut){
			extendedTimeOut = config.DEFAULT_MESSAGE_POSITION&&config.DEFAULT_MESSAGE_POSITION.WARNING_EXTENDED_TIME_OUT?config.DEFAULT_MESSAGE_POSITION.WARNING_EXTENDED_TIME_OUT:1000;
		}
		toastr.options = this.getMessageOption(this.handlerPosition(this.WARNING, position), timeOut, extendedTimeOut, progressBar);
		let $toast = toastr[this.WARNING](this.handleMessage(message), title);
	},

	// error -> red
	error: function(message, title, position, timeOut, extendedTimeOut, progressBar, callback){
		//toastr.error(title, message);

		if (!timeOut) {
			timeOut = config.DEFAULT_MESSAGE_POSITION&&config.DEFAULT_MESSAGE_POSITION.ERROR_TIME_OUT?config.DEFAULT_MESSAGE_POSITION.ERROR_TIME_OUT:60 * 60 * 1000;
		}
		if (!extendedTimeOut) {
			extendedTimeOut = config.DEFAULT_MESSAGE_POSITION&&config.DEFAULT_MESSAGE_POSITION.ERROR_EXTENDED_TIME_OUT?config.DEFAULT_MESSAGE_POSITION.ERROR_EXTENDED_TIME_OUT:60 * 60 * 1000;
		}
		if(!progressBar){
			progressBar = config.DEFAULT_MESSAGE_POSITION&&config.DEFAULT_MESSAGE_POSITION.ERROR_PROGRESS_BAR&&Util.parseBool(config.DEFAULT_MESSAGE_POSITION.ERROR_PROGRESS_BAR)?true:false;
		}
		toastr.options = this.getMessageOption(this.handlerPosition(this.ERROR, position), timeOut, extendedTimeOut, progressBar);
		if (callback && typeof callback == "function") {
			toastr.options.onclick = callback;
		}
		let $toast = toastr[this.ERROR](this.handleMessage(message), title);
	},

	handlerPosition: function(messageType, position){
		switch(messageType){
			case (this.SUCCESS):
				if(position == undefined || position == null){
					return config.DEFAULT_MESSAGE_POSITION.SUCCESS_POSITION;
				}
				return position;

			case (this.INFO):
				if(position == undefined || position == null){
					return config.DEFAULT_MESSAGE_POSITION.INFO_POSITION;
				}
				return position;

			case (this.WARNING):
				if(position == undefined || position == null){
					return config.DEFAULT_MESSAGE_POSITION.WARNING_POSITION;
				}
				return position;

			case (this.ERROR):
				if(position == undefined || position == null){
					return config.DEFAULT_MESSAGE_POSITION.ERROR_POSITION;
				}
				return position;

			default:
				return config.DEFAULT_MESSAGE_POSITION.DEFAULT_POSITION;
		}
	},

	handleMessage(message){
		if(_.isArray(message)){
			for (let i = 0; i < message.length - 1; i++) {
				message[i] = message[i].concat("<br/>");
			}
			return message;
		}else{
			return message;
		}
	}

}
