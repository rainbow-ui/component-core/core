﻿import Component from "../basic/Component";
import config from "config";
import { Util } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types';

export default class Dialog extends Component {

    componentDidUpdate() {
        super.componentDidUpdate();
        if (Util.parseBool(this.props.dragable)) this.handleMouseDownEvent();
        if(Util.parseBool(this.props.noTabIndex)){
            $("#" + this.componentId).removeAttr("tabIndex");
        }
    }

    handleMouseDownEvent() {
        const dialogObj = $("#" + this.componentId);
        const selectObj = $("select");
        const spanObj = $("span");
        const inputObj = $("input");
        const tdObj = $(".tabthClass");
        const textAreaObj = $("textarea");
        const dragableObj = $("#" + this.componentId).children("div");
        let mx = 0, my = 0;      //鼠标x、y轴坐标（相对于left，top）
        let dx = 0, dy = 0;      //对话框坐标（同上）
        let isDraging = false;   //不可拖动
        let diaWidth = document.body.clientWidth;
        dragableObj.unbind("mousedown mousemove mouseup");
        //鼠标按下
        dragableObj.mousedown(function (event) {
            let e = event || window.event;// 改let或const报错
            mx = e.pageX;      //点击时鼠标X坐标
            my = e.pageY;      //点击时鼠标Y坐标
            dx = dialogObj[0].offsetLeft;
            dy = dialogObj[0].offsetTop;
            isDraging = true;      //标记对话框可拖动
        });
        spanObj.mousedown(function (e) {
            isDraging = false;
        });
        tdObj.mousedown(function (e) {
            isDraging = false;
        });
        inputObj.mousedown(function (e) {
            isDraging = false;
        });
        selectObj.mousedown(function (e) {
            isDraging = false;
        });
        textAreaObj.mousedown(function (e) {
            isDraging = false;
        });
        //鼠标移动
        dragableObj.mousemove(function (event) {
            let e = event || window.event;  // 改let或const报错
            let x = e.pageX;      //移动时鼠标X坐标
            let y = e.pageY;      //移动时鼠标Y坐标
            if (isDraging) {        //判断对话框能否拖动
                let moveX = dx + x - mx;      //移动后对话框新的left值
                let moveY = dy + y - my;      //移动后对话框新的top值
                dialogObj[0].style.left = moveX + 'px';       //重新设置对话框的left
                dialogObj[0].style.top = moveY + 'px';     //重新设置对话框的top
                $(dialogObj).css({ 'width': diaWidth + 'px' });
            }
        });
        spanObj.mousemove(function (e) {
            isDraging = false;
        });
        tdObj.mousemove(function (e) {
            isDraging = false;
        });
        inputObj.mousemove(function (e) {
            isDraging = false;
        });
        selectObj.mousemove(function (e) {
            isDraging = false;
        });
        textAreaObj.mousemove(function (e) {
            isDraging = false;
        });
        //鼠标抬起
        $("body").mouseup(function () {
            isDraging = false;
        });
    }

    /**@ignore
     * Get dialog
     */
    static getDialog(dialogId) {
        return $("#" + dialogId);
    }

    /**@ignore
     * Show dialog
     */
    static show(dialogId) {
        return new Promise((resolve) => {
            $("#" + dialogId + "_hiddenBtn").trigger("click", { status: "show" });
            this.getDialog(dialogId).modal("show");
            $(document.body).eq(0).addClass("modal-open-self").css("overflow", "hidden");
            window.parent.postMessage({type:'showmask'}, '*');
            resolve(dialogId);
        });
    }

    /**@ignore
     * Hide dialog
     */
    static hide(dialogId, autoOverflow = true) {
        return new Promise((resolve) => {
                $("#" + dialogId + "_hiddenBtn").trigger("click", { status: "hide" });
                this.getDialog(dialogId).modal("hide");
                if (autoOverflow) {
                    $(document.body).eq(0).removeClass("modal-open-self").css("overflow", "auto");
                }
                window.parent.postMessage({type:'hidemask'}, '*');
                resolve(dialogId);
            }
        );
    }

    /**@ignore
     * Toggle dialog
     */
    static toggle(dialogId) {
        $("#" + dialogId + "_hiddenBtn").trigger("click", { status: "toggle" });
        this.getDialog(dialogId).modal("toggle");
    }

    constructor(props) {
        super(props);

        this.state = {
            status: "hide"
        };
    }


    renderComponent() {
        let className = 'modal fade ' + this.props.className + (this.props.dragable ? "dragable" : "");
        return (
            <div id={this.componentId} className={className} tabIndex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard={Util.parseBool(config.DOES_ESC_CLOSE_DIALOG) ? "true" : "false"}
                 data-backdrop={this.props.backdrop} data-show="true" style={{display: `${this.state.status == 'show'?'block':'none'}`}}>
                {this.props.maskClick ? <div className="modal-dialog-background" onClick={this.onCloseDialog.bind(this)}></div>:null}
                <div className="modal-dialog" style={{ width: this.props.width, height: this.props.height }}>
                    {this.renderDialogContent()}
                </div>
                <button id={this.componentId + "_hiddenBtn"} type="button" style={{ display: "none" }} />
            </div>
        );
    }

    /**@ignore
     * Render dialog header
     */
    renderDialogHeader() {
        let className = Util.parseBool(this.props.titleBackGround) ? "modal-header modal-header-color" : "modal-header";
        return (
            <div className={className}>
                {
                    Util.parseBool(this.props.closeable) ?
                        <button type="button" className="close" onClick={this.onCloseDialog.bind(this)}>&times;</button> :
                        null
                }
                {
                    this.props.title?
                    <h4 className="modal-title" >{this.props.title ? this.getI18n(this.props.title) : ""}</h4>:
                    <div style={{height: "15px"}}></div>
                }
            </div>
        );
    }

    /**@ignore
     * Render dialog content
     */
    renderDialogContent() {
        if (this.state.status == "show") {
            let dialogArray = this.getDialogArray();

            return (
                <div className="modal-content">
                    {this.renderDialogHeader()}
                    <div className="modal-body">{dialogArray[0]}</div>
                    {dialogArray[1]}
                </div>
            );
        }
    }

    /**@ignore
     * Get dialog content and footer element
     */
    getDialogArray() {
        let dialogArray = [[], []];

        React.Children.forEach(this.props.children, function (child) {
            if (child.props.componentType == "DialogFooter") {
                dialogArray[1].push(child);
            } else {
                dialogArray[0].push(child);
            }
        });

        return dialogArray;
    }

    componentDidMount() {
        let _self = this;

        $("#" + this.componentId + "_hiddenBtn").click(function (event, data) {
            event.preventDefault();

            if (data.status == "toggle") {
                if (_self.state.status == "show") {
                    _self.setState({ status: "hide" });
                } else {
                    _self.setState({ status: "show" });
                }
            } else {
                _self.setState({ status: data.status });
            }
        });
    }

    onCloseDialog() {
        if (this.props.onClose != undefined) {
            this.props.onClose.call();
        }

        Dialog.hide(this.componentId);
    }

};


/**@ignore
 * Dialog component prop types
 */
Dialog.propTypes = $.extend({}, Component.propTypes, {
    id: PropTypes.string.isRequired,
    title: PropTypes.string,
    modal: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    backdrop: PropTypes.oneOf(["static", "true", "false"]),
    keyboard: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    closeable: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    dragable: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    onClose: PropTypes.func,
    titleBackGround: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    className: PropTypes.string,
    noTabIndex: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    maskClick: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
});

/**@ignore
 * Get dialog component default props
 */
Dialog.defaultProps = $.extend({}, Component.defaultProps, {
    modal: true,
    width: "auto !important",
    height: "auto !important",
    backdrop: "static",
    closeable: true,
    titleBackGround: false,
    className: '',
    dragable: false,
    noTabIndex: false,
    maskClick: false
});
