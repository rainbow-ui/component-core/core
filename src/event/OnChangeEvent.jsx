import OnEvent from "./OnEvent";

export default class OnChangeEvent extends OnEvent {

    constructor(component, jsEvent, paramJson, newValue, oldValue, realValue, currLevel) {
        super(component, jsEvent, paramJson);
        if(newValue&&newValue["id"]){
            this.newObject = newValue;
            this.newValue = newValue["id"];
        }else{
            this.newValue = newValue;
            this.oldValue = oldValue;
        }
        if (realValue) {
            this.realValue = realValue;
        }
        if (currLevel) {
            this.currLevel = currLevel;
        }
    }

    getNewValue() {
        return this.newValue;
    }

    getOldValue() {
        return this.oldValue;
    }

    toString() {
        return this.newValue + "/" + this.oldValue;
    }

}

