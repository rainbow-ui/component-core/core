'use strict';

module.exports = {
    //basic
    Component: require('./basic/Component'),
    Command: require('./basic/Component'),
    CodeTable: require('./basic/CodeTable'),
    CodeTableSorter: require('./basic/CodeTableSorter'),
    Param: require('./basic/Param'),
    KeyValue: require('./basic/KeyValue'),
    UIEvent: require('./basic/Event'),

    //button
    UIButton: require('./button/Button'),
    UILink: require('./button/Link'),
    UISplitButton: require('./button/SplitButton'),
    UISplitButtonItem: require('./button/ButtonItem'),
    UIButtonGroup:require('./button/buttonGroup/component/ButtonGroup'), 
    UIButtonItem:require('./button/buttonGroup/component/ButtonItem'),
    UIChoose:require('./button/choose/component/Choose'),

    //input
    UIInput: require('./basic/Input'),
    UIText: require('./input/Text'),
    UITextarea: require('./input/Textarea'),
    UIDateTimePicker: require('./input/dateTimePicker/component/DateTimePicker'),
    UIRadio: require('./input/Radio'),
    UICheckbox: require('./input/Checkbox'),
    UIPassword: require('./input/Password'),
    UINumber: require('./input/Number'),
    UICurrency: require('./input/Currency'),
    UIPercent: require('./input/Percent'),
    UIEmail: require('./input/Email'),
    UISwitch: require('./input/Switch'),
    UISelect: require('./input/search/component/Select'),
    UISearch: require('./input/search/component/Search'),
    
    UIAdvCascade:require('./input/advCascade/component/AdvCascade'),
    UIFilter:require('./input/filter/component/Filter'),
    UIRating:require('./input/rating/component/Rating'),
    UIPicklist:require('./input/picklist/component/Picklist'),
    UIInputNumber:require('./input/spinner/component/Spinner'),
    UIBlank: require('./input/Blank'),
    UIMultiselect: require('./input/Multiselect'),

    UIOutputCodetable: require('./input/OutputCodetable'),

    //container
    UIDataTable: require('./container/DataTable'),
    UIColumn: require('./container/Column'),
    UIBox: require('./container/Box'),
    UICell: require('./container/Cell'),
    UISmartPanelGrid: require('./container/SmartPanelGrid'),
    UITab: require('./container/tab/Tab'),
    UITabItem: require('./container/tab/TabItem'),
    UIWizard: require('./container/wizard/Wizard'),
    UIWizardStep: require('./container/wizard/WizardStep'),
    UICard: require('./container/card/Card'),
    UICardGroup: require('./container/card/CardGroup'),
    UIUpdatePanel: require('./container/UpdatePanel'),
    UIPopover:require('./container/popover/component/Popover'),
    UIFoldCard: require('./container/FoldCard'),
    UIPopConfirm: require('./container/PopConfirm'),

    //dialog
    UIMessageHelper: require('./dialog/MessageHelper'),
    UIDialog: require('./dialog/Dialog'),
    UIConfirmDialog: require('./dialog/ConfirmDialog'),
    UIAlerts:require('./dialog/Alerts'),

    //drawer
    UIDrawer:require('./drawer/drawer'),

    //file
    UIFileDownload: require('./file/FileDownload'),
    UIFileUpload: require('./file/FileUpload'),

    // data
    UITree:require('./data/tree/component/Tree'),
    UICode:require('./data/code/component/Code'),
    UITimeLine:require('./data/imTimeLine/component/ImTimeLine'),
    

    //menu
    UIMenuBar: require('./menu/MenuBar'),
    UIMenu: require('./menu/Menu'),
    UIMenuItem: require('./menu/MenuItem'),
    UISubMenu: require('./menu/SubMenu'),
    UICustomMenu: require('./menu/CustomMenu'),
    SideMenu: require('./menu/sideMenu/SideMenu'),
    UISideNav: require('./menu/sideNav/SideNav'),

    //page
    UIPage: require('./page/Page'),
    UIPagination: require('./page/Pagination'),
    UIPageFooter: require('./page/PageFooter'),

    //event
    OnEvent: require('./event/OnEvent'),
    OnBlurEvent: require('./event/OnBlurEvent'),
    OnChangeEvent: require('./event/OnChangeEvent'),
    OnClickEvent: require('./event/OnClickEvent'),
    OnFocusEvent: require('./event/OnBlurEvent'),
    OnRowSelectEvent: require('./event/OnRowSelectEvent'),
    OnTabChangeEvent: require('./event/OnTabChangeEvent'),
    OnToggleEvent: require('./event/OnToggleEvent'),

    //i18n
    I18nUtil: require('./i18n/I18NUtil'),

    //convertor
    Convertor: require('./convertor/Convertor'),
    ConvertorConstant: require('./convertor/ConvertorConstant'),
    DateTimePickerConvertor: require('./convertor/DateTimePickerConvertor'),
    
    //display
    UILabel: require('./other/Label'),
    If: require('./other/If'),
    UIIcon: require('./other/Icon'),
    UISpinner: require('./other/Spinner'),
    UIBreadcrumb: require('./other/Breadcrumb'),
    UIBreadcrumbItem: require('./other/BreadcrumbItem'),
    UIBadges:require('./other/badges/component/Badges'),
    UICarousel:require('./other/carousel/component/Carousel'),
    UICarouselItem:require('./other/carousel/component/CarouselItem'),
    UISeparator: require('./other/Separator'),
    UIProgress:require('./other/progress/component/Progress'),

    // list
    UIChooseList: require('./list/ChooseList')
};