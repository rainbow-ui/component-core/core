import PropTypes from 'prop-types';
import Command from "../basic/Command";
export default class SplitButton extends Command {
    renderComponent() {
         const {styleClass, children} = this.props;
        if (styleClass) {
            return (
                <div className="btn-group">
                    {
                        React.Children.map(children, (item) => {
                            if (item.props.styleClass) {
                                return item;
                            } 

                            return React.cloneElement(item, {
                                styleClass: styleClass
                            })
                        })
                    }
                </div>
            );
        }
    }
}

/**@ignore
 * Button component prop types
 */
SplitButton.propTypes = $.extend({}, Command.propTypes, {
    styleClass: PropTypes.oneOf(["default", "primary", "success", "warning", "danger", "info"])
});

/**@ignore
 * Get Button component default props
 */
SplitButton.defaultProps = $.extend({}, Command.defaultProps, {
    styleClass: 'primary'
});