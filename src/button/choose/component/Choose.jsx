import "../js/ui-choose.js";
// import { UIInput, OnClickEvent, Param, UIIcon, KeyValue } from "rainbowui-core";
import UIInput from '../../../basic/Input';
import OnClickEvent from '../../../event/OnClickEvent';
import Param from '../../../basic/Param';
import UIIcon from '../../../other/Icon';
import KeyValue from '../../../basic/KeyValue';
import PropTypes from 'prop-types';
import { Util } from "rainbow-desktop-tools";
import { CodeTableService } from "rainbow-desktop-codetable";
import {SessionContext} from 'rainbow-desktop-cache';
import config from "config";

export default class Choose extends UIInput {

    constructor(props) {
        super(props);
        this.state = {
            chooseList: null,
            data: null
        }
        this.codetable_key = config["DEFAULT_CODETABLE_KEYVALUE"]["KEY"];
        this.codetable_value = config["DEFAULT_CODETABLE_KEYVALUE"]["VALUE"];
        this.codetable_api_key = config["DEFAULT_API_CODETABLE_KEYVALUE"]["KEY"];
        this.codetable_api_value = config["DEFAULT_API_CODETABLE_KEYVALUE"]["VALUE"];
    }

    componentWillMount() {
        super._componentWillMount();
        let _self = this;
        this.onEvent = { newValue: this.getComponentValue(), oldValue: null };
        let { conditionMap, codeTableName } = this.props;
        let defaultClass = "default";
        let selectedClass = "selected";
        let disabledClass = this.getDisabled() ? "disabled" : "";
        if (codeTableName) {
            let getCode = { "CodeTableName": codeTableName, "ConditionMap": conditionMap };
            CodeTableService.getCodeTable(getCode).then((data) => {
                let list = [];
                _self.codeTable = _self.buildCodeTable(data);
                _.each(this.codeTable, (item, index) => {
                    this.eachList(item.id, item.text, list, index, selectedClass, disabledClass, defaultClass, this);
                });
                this.setState({ chooseList: list, data: data });
            });
        }
    }

    componentDidMount() {
        super.showLabelTooltip();
        const _self = this;
        const multiSelectFlag = Util.parseBool(_self.props.multiSelect);
        const chooseObject = $("#for-choose-" + _self.componentId);
        if (multiSelectFlag) {
            chooseObject.attr("multiple", "multiple");
            // chooseObject.addClass("choose-type-right");
        } else {
            _self.props.isFillStyle = 'true';
        }
        let { conditionMap, codeTableName } = _self.props;
        if (!codeTableName) {
            if ((_self.props.value && _self.props.value.length > 0) || _self.codeTable) {
                chooseObject.ui_choose();
            }
            this.handleChooseClick(chooseObject);
        }
        if (!Util.parseBool(_self.props.isFillStyle)) {
            chooseObject.addClass("choose-type-right");
        } else {
            chooseObject.removeClass("choose-type-right");
        }
        _self.modifyGap();
        _self.onChangeColorByEndorsement();
        if (this.props.io != "out") {
            this.initValidator();
        }
        if (this.props.model) {
            this.handleComponentValue(this.props.model[this.props.property]);
        }
    }

    componentWillReceiveProps(nextProps) {
        this.props = nextProps;
        let { conditionMap, codeTableName } = this.props;
        let defaultClass = "default";
        let selectedClass = "selected";
        let disabledClass = this.getDisabled() ? "disabled" : "";
        if (codeTableName) {
            let list = [];
            if (this.state.data) {
                this.codeTable = this.buildCodeTable(this.state.data);
                _.each(this.codeTable, (item, index) => {
                    this.eachList(item.id, item.text, list, index, selectedClass, disabledClass, defaultClass, this);
                });
                this.setState({
                    chooseList: list
                });
            }
        }
    }

    componentDidUpdate() {
        const _self = this;
        let { conditionMap, codeTableName, value } = _self.props;
        const chooseObject = $("#for-choose-" + _self.componentId);
        const multiSelectFlag = Util.parseBool(_self.props.multiSelect);
        if(!chooseObject.prop('ui-choose')){
            chooseObject.ui_choose();
        }
        this.handleChooseClick(chooseObject);
        _self.modifyGap();
        _self.onChangeColorByEndorsement();
        if (this.props.model) {
            this.handleComponentValue(this.props.model[this.props.property]);
        }
    }

    handleChooseClick(chooseObject) {
        const _self = this;
        const multiSelectFlag = Util.parseBool(_self.props.multiSelect);
        const unChooseAble = _self.props.unChooseAble;
        /**
         * onBeforeChange增加校验
         */
        let previousChoose = null;
        for(let childNode of chooseObject[0].childNodes) {
            let previousChooseId = childNode.id;
            let previousChooseObj = $("#" + previousChooseId)
            if(previousChooseObj.hasClass("selected")) {
                previousChoose = previousChooseObj
            }
        }
        if (chooseObject.prop('ui-choose')) {
            chooseObject.prop('ui-choose').click = (currentIndex, currentChoose) => {
                const currentChooseId = currentChoose[0].id;
                const currentChooseObj = $("#" + currentChooseId);
                if (_self.props.onBeforeChange) {
                    if (!_self.props.onBeforeChange(new OnClickEvent(_self, event, Param.getParameter(_self)))) {
                        if (currentChooseObj.hasClass("selected")) {
                            currentChooseObj.removeClass("selected");
                            previousChoose.addClass("selected")
                            // _self.setValue(null, null);
                            // _self.handleComponentValue(null);
                        }
                        return;
                    }
                }
                if (_self.props.value) {
                    if (multiSelectFlag) {
                        const valuelist = [];
                        _.each(currentIndex, (item) => {
                            valuelist.push(_self.props.value[item]);
                        });
                        _self.setValue(null, valuelist);
                        _self.handleComponentValue(valuelist);
                    } else {
                        let oldValue = _self.getValue();
                        if (oldValue == _self.props.value[currentIndex] && Util.parseBool(unChooseAble)) {
                            if (currentChooseObj.hasClass("selected")) {
                                currentChooseObj.removeClass("selected");
                                _self.setValue(null, null);
                                _self.handleComponentValue(null);
                            }
                        } else {
                            _self.setValue(null, currentChoose.text());
                            _self.handleComponentValue(currentChoose.text());
                        }
                    }
                } else if (_self.codeTable) {
                    if (multiSelectFlag) {
                        const valuelist = [];
                        _.each(currentIndex, (item) => {
                            valuelist.push(this.codeTable[item][_self.codetable_key]);
                        });
                        _self.setValue(null, valuelist);
                        _self.handleComponentValue(valuelist);
                    } else {
                        let oldValue = _self.getValue();
                        let codeTable = _self.codeTable;
                        if (oldValue && oldValue == codeTable[currentIndex][_self.codetable_key] && Util.parseBool(unChooseAble)) {
                            if (currentChooseObj.hasClass("selected")) {
                                currentChooseObj.removeClass("selected");
                                _self.setValue(null, null);
                                _self.handleComponentValue(null);
                            }
                        } else {
                            _self.setValue(null, codeTable[currentIndex][_self.codetable_key]);
                            _self.handleComponentValue(codeTable[currentIndex][_self.codetable_key]);
                        }
                    }
                }
                if (_self.props.onClick) _self.props.onClick(new OnClickEvent(_self, event, Param.getParameter(_self)));
                _self.onChangeColorByEndorsement();
            }
        }
    }

    handleComponentValue(value) {
        let chooseComponent = $("#for-choose-" + this.componentId);
        chooseComponent.val(value);
        this.clearValidationInfo();
    }


    renderInput() {
        let codeTableName = this.props.codeTableName;
        let styleChoose = "ui-choose" + " " + this.props.className;
        if (codeTableName) {
            return (
                <div>
                    <ul className={styleChoose} id={'for-choose-' + this.componentId} name={this.getName()}>
                        {this.state.chooseList}
                    </ul>
                </div>
            );
        } else {
            return (
                <div>
                    <ul className={styleChoose} id={'for-choose-' + this.componentId} name={this.getName()}>
                        {this.initComponent()}
                    </ul>
                </div>
            );
        }
    }

    initComponent() {
        const _self = this;
        const returnList = [];
        let defaultClass = "default";
        let selectedClass = "selected";
        let disabledClass = this.getDisabled() ? "disabled" : "";
        if (this.props.value) {
            _.each(this.props.value, (item, index) => {
                this.eachList(item, item, returnList, index, selectedClass, disabledClass, defaultClass, _self);
            });
        } else {
            const codeTable = this.props.codeTable;
            if (codeTable) {
                if (Util.isString(codeTable)) {
                    this.codeTable = eval(codeTable);
                } else if (Util.isFunction(codeTable)) {
                    this.codeTable = codeTable();
                } else {
                    this.codeTable = codeTable;
                }
                this.codeTable = this.buildCodeTable(this.codeTable);
                _.each(this.codeTable, (item, index) => {
                    this.eachList(item.id, item.text, returnList, index, selectedClass, disabledClass, defaultClass, _self);
                });
            }
        }
        return returnList;
    }

    eachList(id, text, returnList, index, selectedClass, disabledClass, defaultClass, _self) {
        let value = this.getValue(null);
        if (Util.parseBool(_self.props.valueTostring) && !_.isEmpty(value) && typeof(value) == 'string') {
            value = JSON.parse(value);
        }
        if (_.isArray(value)) {
            const result = _.find(value, (_item) => {
                return _item == id;
            });
            if (result) {
                returnList.push(
                    <li id={'for-choose-'+ this.componentId + "-li" + index} className={selectedClass + " " + disabledClass} 
                        style={{marginBottom: this.props.className?this.props.gap:''}}>
                        {_self.renderIcon(index)}
                        <span style={{ display: 'inblock', marginLeft: '5px' }}>{text}</span>
                    </li>
                );
            } else {
                returnList.push(
                    <li id={'for-choose-' + this.componentId + "-li" + index} className={defaultClass + " " + disabledClass}
                        style={{marginBottom: this.props.className?this.props.gap:''}}>
                        {_self.renderIcon(index)}
                        <span style={{ display: 'inblock', marginLeft: '5px' }}>{text}</span>
                    </li>);
            }
        } else {
            if (id == value) {
                returnList.push(
                    <li id={'for-choose-' + this.componentId + "-li" + index} className={selectedClass + " " + disabledClass}
                        style={{marginBottom: this.props.className?this.props.gap:''}}>
                        {_self.renderIcon(index)}
                        <span style={{ display: 'inblock', marginLeft: '5px' }}>{text}</span>
                    </li>);
            } else {
                returnList.push(
                    <li id={'for-choose-' + this.componentId + "-li" + index} className={defaultClass + " " + disabledClass}
                        style={{marginBottom: this.props.className?this.props.gap:''}}>
                        {_self.renderIcon(index)}
                        <span style={{ display: 'inblock', marginLeft: '5px' }}>{text}</span>
                    </li>);
            }
        }
    }

    renderIcon(i) {
        let _self = this;
        let icon = _self.props.icon;
        if (icon) {
            if (_.isArray(icon)) {
                return < UIIcon icon={icon[i]} />;
            } else {
                return <UIIcon icon={icon} />;
            }
        }
        return <noscript />;
    }

    buildCodeTable(data) {
        let _self = this;
        let dataArray = [];
        const statusCodeList = SessionContext.get('__CODETABLE_STATUS');
        if (data && data.codes && data.codes.length > 0) {
            data.codes.forEach(function (codeItem) {
                const code = {};
                code[_self.codetable_key] = codeItem[_self.codetable_key];
                code[_self.codetable_value] = codeItem[_self.codetable_value];
                if(statusCodeList&&codeItem.Status){
                    const status = _.find(statusCodeList,(statusCode)=>{
                        return statusCode==codeItem.Status;
                    });
                    if(status){
                        dataArray.push(code);
                    }
                }else{
                    dataArray.push(code);
                }
            });
        }
        if (data && data.BusinessCodeTableValueList && data.BusinessCodeTableValueList.length > 0) {
            data.BusinessCodeTableValueList.forEach(function (codeItem) {
                const code = {};
                code[_self.codetable_key] = codeItem[_self.codetable_api_key];
                code[_self.codetable_value] = codeItem[_self.codetable_api_value];
                if(statusCodeList&&codeItem.Status){
                    const status = _.find(statusCodeList,(statusCode)=>{
                        return statusCode==codeItem.Status;
                    });
                    if(status){
                        dataArray.push(code);
                    }
                }else{
                    dataArray.push(code);
                }
            });
        } else if (Util.isArray(data)) {
            data.forEach(function (codeItem) {
                const code = {};
                code[_self.codetable_key] = codeItem[_self.codetable_api_key];
                code[_self.codetable_value] = codeItem[_self.codetable_api_value];
                if(statusCodeList&&codeItem.Status){
                    const status = _.find(statusCodeList,(statusCode)=>{
                        return statusCode==codeItem.Status;
                    });
                    if(status){
                        dataArray.push(code);
                    }
                }else{
                    dataArray.push(code);
                }
            });
        }
        return dataArray;
    }

    getDisplayValue(datas) {
        if (this.props.model) {
            const data = _.find(datas, (data) => {
                const id = this.props.model[this.props.property];
                if (data.id == id) {
                    return data;
                }
            });
            if (data) {
                return Util.parseBool(this.props.showCode) ? data.display : data.text;
            } else {
                return ""
            }
        }
    }

    modifyGap() {
        const chooseObject = $("ul#for-choose-" + this.componentId + " li");
        const chooseObject_first = $("ul#for-choose-" + this.componentId + " li:first-child");
        if (this.props.gap) {
            let gap = this.props.gap;
            chooseObject.css("margin-right", gap);
        }
        if (this.props.bottomGap) {
            let bottomGap = this.props.bottomGap;
            chooseObject.css("margin-bottom", bottomGap);
        }
    }

    clearValidationInfo() {
        const chooseObject = $("#for-choose-" + this.componentId);
        const chooseObjectValue = chooseObject.val();
        const formGroupObj = chooseObject.closest(".form-group");
        const feedBackObj = chooseObject.next(".form-control-feedback");
        const errorMessageObj = chooseObject.parent("div").siblings(".help-block");
        if (formGroupObj.hasClass("has-feedback") && formGroupObj.hasClass("has-error") && chooseObjectValue) {
            formGroupObj.removeClass("has-feedback");
            formGroupObj.removeClass("has-error");
            //隐藏错误提示信息
            errorMessageObj.attr("style", "display:none");
            feedBackObj.attr("style", "display:none");
        }

    }

}


Choose.propTypes = $.extend({}, UIInput.propTypes, {
    value: PropTypes.object,
    model: PropTypes.object,
    property: PropTypes.string,
    multiSelect: PropTypes.boolean,
    gap: PropTypes.string,
    bottomGap: PropTypes.string,
    enabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    onClick: PropTypes.func,
    icon: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
    unChooseAble: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
});


Choose.defaultProps = $.extend({}, UIInput.defaultProps, {
    multiSelect: true,
    enabled: true,
    unChooseAble: false,
    componentType: "choose",
    isFillStyle: false
});
