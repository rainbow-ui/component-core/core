import Command from "../basic/Command";
import PropTypes from 'prop-types';
import { Util } from 'rainbow-desktop-tools';

export default class Link extends Command {

    renderComponent() {
        const aProps = Object.assign({}, this.props);
        delete aProps.enabled;
        delete aProps.visibled;
        // delete aProps.styleClass;
        delete aProps.causeValidation;
        const value = this.getProperty("value");
        const outputValue = value ? this.getI18n(value) : "";
        const title = this.getProperty("title");
        const outputTitle = title ? this.getI18n(title) : "";
        let buttonClassName = this.getStyleClass() + " " + this.getSize() + " " + (this.getDisabled() ? this.getDisabled() + ' ' : ' ') + (this.props.className ? this.props.className : '')

        return (
            <div class='rainbow_link' onMouseOver={this.onMouseOver.bind(this)} onMouseOut={this.onMouseOut.bind(this)}>
                <div id={'link_' + this.componentId} class='rainbow_link_tip'>
                    <div class='rainbow GroupPolicyOriginal link_icon'></div>
                </div>
                <a {...aProps} title={this.props.title ? outputTitle : ''} class={buttonClassName} href="javascript: void(0);" data-auto-test={this.getNameForTest()}
                    onClick={this.onClick.bind(this)} disabled={this.props.disabled}
                    style={this.props.style != null && this.props.style != undefined ? this.props.style : null}
                    className={this.getDisabled()} tabindex={this.props.tabindex}>
                    <div className={this.props.iconClass} style={{ display: "inline" }}>
                        {
                            this.props.icon ?
                                <span className={this.props.icon} style={{ paddingRight: "5px" }} /> :
                                null
                        }
                    </div>
                    <span className="icon_end" nativetype='link'
                        style={{ padding: "0px", marginRight: "0", textDecoration: "underline" }}>{outputValue}</span>
                    {this.props.children}
                </a>
            </div>
        )
    }

    componentDidMount() {
        $('a').click(function () {
            $(this).addClass("link_active");
        })
    }

    onMouseOver(){
        if(Util.parseBool(this.props.showIcon)){
            const linkComponent = $("#" + this.componentId);
            linkComponent.css("display","block")
            clearTimeout(this.linktime)   
        }         
    }

    onMouseOut(){
        if(Util.parseBool(this.props.showIcon)){
            const linkComponent = $("#" + this.componentId);
            this.linktime = setTimeout(function(){linkComponent.css("display","none")},1000)
        }
    }

    getStyleClass() {
        switch (this.props.styleClass) {
            case (/*this.DEFAULT*/"default"):
                return "link-default";
            case (/*this.PRIMARY*/"primary"):
                return "link-primary";
            case (/*this.SUCCESS*/"success"):
                return "link-success";
            case (/*this.INFO*/"info"):
                return "link-info";
            case (/*this.WARNING*/"warning"):
                return "link-warning";
            case (/*this.DANGER*/"danger"):
                return "link-danger";
            case (/*this.LINK*/"link"):
                return "link-link";
            case (undefined):
                return "link-primary";
            default:
                return this.props.styleClass;
        }
    }

};

/**@ignore
 * Link component prop types
 */
Link.propTypes = $.extend({}, Command.propTypes, {
    update: PropTypes.string,
    causeValidation: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    validationGroup: PropTypes.string,
    exceptValidationGroup: PropTypes.string,
    title: PropTypes.string,
    tabindex: PropTypes.string,
    showIcon: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
});

/**@ignore
 * Get Link component default props
 */
Link.defaultProps = $.extend({}, Command.defaultProps, {
    causeValidation: false,
    showIcon:false
});
