import React, { Component } from "react"
import config from "config"

export default class index extends Component {

    state = {
        activeItem: ''
    }

    render() {
        return (
            <div class="ChooseListContent">
            { this.props.title ? this.getContentTitle() : null }
            { this.props.contentList ? this.getContentList() : null }
        </div>
        )
    }

    // 获取盒子的title
    getContentTitle() {
        return (
            <span class="title">{this.props.title}</span>
        )
    }

    // 获取 button 列表
    getContentList() {
        let { contentList,activeItem } = this.props;
        return (
            <ul class="ContentListBox">
                {
                    contentList.map( item => {
                        let flag = item.itemText == activeItem ? activeItem : this.state.activeItem ? true : false;
                        return (
                            <li class={`contentListItem ${flag ? 'contentListItemSelected' : ''}`} onClick={this.func.bind(this, item.obj)}>
                                <span>{ this.props.noi18n ? item.itemText : this.getI18n(item.itemText) }</span>
                            </li>
                        )
                    })
                }
            </ul>
        )
        
    }
    func(item) {
        this.setState({activeItem: item.itemText});
        if (this.props.onClickFunc) {
            this.props.onClickFunc(item);
        }
    }

    getI18n(code) {
        let lowI18n = {};
        for(let key in i18n){
            let lowKey = key.toLowerCase();
            lowI18n[lowKey] = i18n[key]
        }
        let lowCode = code?code.toLowerCase():undefined;
        const value = lowI18n[lowCode];
        if (!_.isEmpty(lowCode)) {
            if (_.isEmpty(value)) {
                return config.DEFAULT_I18N_CONFIGURATION_GROUP + "." + lowCode;
            } else {
                return value;
            }
        }
        return value;
    }
}

